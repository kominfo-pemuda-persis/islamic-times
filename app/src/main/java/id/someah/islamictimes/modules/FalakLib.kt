        package id.someah.islamictimes.modules
import android.util.Log
import kotlin.collections.ArrayList
import kotlin.math.log10

class FalakLib {

    // Math Function
    fun Deg(x: Double) = Math.toDegrees(x)
    fun Rad(x: Double) = Math.toRadians(x)
    fun Mod(x: Double, y: Double) = x - y * Math.floor(x / y)
    fun Sign(x: Double) = if (x >0) {1} else if (x <0) {-1} else {0}

    // fungsi ini hasilnya sesuai dengan umum dipakai
    fun Double.round(decimals: Int = 2): String {
        return "%.${decimals}f".format(this)
    }

    // fungsi ini hasilnya sesuai dengan umum dipakai
    fun RoundTo(xDec: Double, Place: Int = 2): String {
        val A: Double = Math.pow(10.0,Place.toDouble())
        val H: Double = if (xDec >= 0 ) {
            Math.floor(xDec * A + 0.5) / A
        } else {
            -Math.floor(Math.abs(xDec) * A + 0.5) / A
        }
        return H.toString()
    }

    fun DHHMS(
        DHrs: Double,
        OptResult: String   = "HH:MM:SS",
        SecDecPlaces: Byte  = 2,
        PosNegSign: String  = ""
    ) : String {

        var uDHrs   : Double
        var uHrs    : Double
        var uDMin   : Double
        var uMin    : Double
        var uDSec   : Double
        var uSec    : String
        var sHrs    : String
        var sMin    : String
        var sSec    : String
        var PNS     : String

        uDHrs   = Math.abs(DHrs)
        uHrs    = Math.floor(uDHrs)
        uDMin   = (uDHrs - uHrs) * 60.0
        uMin    = Math.floor(uDMin)
        uDSec   = (uDMin - uMin) * 60.0
        uSec    = "%.${SecDecPlaces}f".format(uDSec)

        if (uSec.replace(",",".").toDouble() == 60.0) {
            uSec  = "%.${SecDecPlaces}f".format(0.0)
            uMin  = uMin + 1.0
        } else {
            uSec  = uSec
            uMin  = uMin
        }

        if (uMin.toDouble() == 60.0) {
            uMin  = 0.0
            uHrs = uHrs + 1.0
        } else {
            uMin  = uMin
            uHrs = uHrs
        }

        sHrs = when {
            uHrs.toInt() < 10       ->  "0${uHrs.toInt().toString()}"
            else                    ->  "${uHrs.toInt().toString()}"
        }

        sMin  = when {
            uMin.toInt() < 10       ->  "0${uMin.toInt().toString()}"
            else                    ->  "${uMin.toInt().toString()}"
        }

        sSec  = when {
            uSec.replace(",",".").toDouble() < 10.0  ->  "0${uSec}"
            else                    ->  "${uSec}"
        }

        PNS = when (PosNegSign) {
            "+-"    -> {
                when {
                    DHrs > 0.0  -> "+"
                    DHrs < 0.0  -> "-"
                    else        -> ""
                }
            }
            else    -> {
                when {
                    DHrs > 0.0  -> ""
                    DHrs < 0.0  -> "-"
                    else        -> ""
                }
            }
        }

        return when (OptResult) {
            "HH:MM:SS"  -> "$PNS${sHrs}:${sMin}:${sSec}"
            "HHMMSS"    -> "$PNS${sHrs}h ${sMin}m ${sSec}s"
            "MMSS"      -> "$PNS${sMin}m ${sSec}s"
            "HH:MM"     -> "$PNS${sHrs}:${sMin}"
            else        -> "$PNS${sHrs}:${sMin}:${sSec}"
        }
    }
    fun DHHM(
        DHrs: Double,
        OptResult: String  = "HH:MM",
        MinDecPlaces: Byte  = 2,
        PosNegSign: String = ""
    ) : String {

        var uDHrs   : Double
        var uHrs    : Double
        var uDMin   : Double
        var uMin    : String
        var sHrs    : String
        var sMin    : String
        var PNS     : String

        uDHrs   = Math.abs(DHrs)
        uHrs    = Math.floor(uDHrs)
        uDMin   = (uDHrs - uHrs) * 60.0
        uMin    = "%.${MinDecPlaces}f".format(uDMin)

        if (uMin.toDouble() == 60.0) {
            uMin  = "%.${MinDecPlaces}f".format(0.0)
            uHrs = uHrs + 1.0
        } else {
            uMin  = uMin
            uMin  = uMin
        }

        sHrs = when {
            uHrs.toInt() < 10       ->  "0${uHrs.toInt().toString()}"
            else                    ->  "${uHrs.toInt().toString()}"
        }

        sMin  = when {
            uMin.toDouble() < 10.0  ->  "0${uMin}"
            else                    ->  "${uMin}"
        }

        PNS = when (PosNegSign) {
            "+-"    -> {
                when {
                    DHrs > 0.0  -> "+"
                    DHrs < 0.0  -> "-"
                    else        -> ""
                }
            }
            else    -> {
                when {
                    DHrs > 0.0  -> ""
                    DHrs < 0.0  -> "-"
                    else        -> ""
                }
            }
        }

        return when (OptResult) {
            "HH:MM"  	-> "$PNS${sHrs}:${sMin}"
            "HHMM"    	-> "$PNS${sHrs}h ${sMin}m"
            else        -> "$PNS${sHrs}:${sMin}"
        }
    }

    fun DDDMS(
        DDeg: Double,
        OptResult:String = "DDMMSS",
        SDP: Byte = 2
    ): String {

        var uDDeg : Double
        var uDeg  : Double
        var uDMin : Double
        var uMin  : Double
        var uDSec : Double
        var uSec  : String
        var sDeg  : String
        var sMin  : String
        var sSec  : String
        var PNS   : String

        uDDeg = Math.abs(DDeg)
        uDeg  = Math.floor(uDDeg)
        uDMin = (uDDeg - uDeg) * 60.0
        uMin  = Math.floor(uDMin)
        uDSec = (uDMin - uMin) * 60.0
        uSec  = "%.${SDP}f".format(uDSec)

        if (uSec.replace(",",".").toDouble() == 60.0) {
            uSec  = "%.${SDP}f".format(0.0)
            uMin  = uMin + 1.0
        } else {
            uSec  = uSec
            uMin  = uMin
        }

        if (uMin.toDouble() == 60.0) {
            uMin  = 0.0
            uDeg  = uDeg + 1.0
        } else {
            uMin  = uMin
            uDeg  = uDeg
        }

        sDeg  = when {
            uDeg.toInt() < 10       ->  "00${uDeg.toInt().toString()}"
            uDeg.toInt() < 100      ->  "0${uDeg.toInt().toString()}"
            else                    ->  "${uDeg.toInt().toString()}"
        }

        sMin  = when {
            uMin.toInt() < 10       ->  "0${uMin.toInt().toString()}"
            else                    ->  "${uMin.toInt().toString()}"
        }

        sSec  = when {
            uSec.replace(",",".").toDouble() < 10.0  ->  "0${uSec}"
            else                    ->  "${uSec}"
        }

        PNS = when  {
            DDeg > 0.0  -> "+"
            DDeg < 0.0  -> "-"
            else        -> ""
        }
        return when (OptResult) {
            "DDMMSS"  	-> "$PNS${sDeg}° ${sMin}’ ${sSec}”"
            "MMSS"    	-> "$PNS${sMin}’ ${sSec}”"
            "SS"    	-> "$PNS${sSec}”"
            else        -> "$PNS${sDeg}° ${sMin}’ ${sSec}”"
        }
    }
    fun IhtiyathShalat (JamDesWs: Double, Ihtiyath: Int): Double {

        var uDHrs : Double
        var uHrs : Double
        var uDMin  : Double
        var uMin : Double
        var iht  : Double

        uDHrs   = Math.abs(JamDesWs)
        uHrs    = Math.floor(JamDesWs)
        uDMin   = (uDHrs - uHrs) * 60.0
        uMin    = Math.floor(uDMin) + Ihtiyath

        if (uMin == 60.0) {
            uMin  = 0.0
            uHrs = uHrs + 1.0
        } else {
            uMin  = uMin
            uHrs = uHrs
        }
        iht     = uHrs+uMin/60
        return iht

    }
    // KMJD
    fun KMJD(
        TglM: Byte, BlnM: Byte, ThnM: Long,
        JamDes: Double = 0.0, TimeZone: Double = 0.0
    ): Double {
        var DDUT: Double // Decimal Day at UT
        var MM  : Double
        var YM  : Double
        var GC  : Double // Gregorian Correction
        var JD  : Double
        DDUT = TglM + ((JamDes - TimeZone) / 24.0)
        if (BlnM > 2) {
            MM = BlnM.toDouble()
            YM = ThnM.toDouble()
        } else {
            MM = BlnM.toDouble() + 12
            YM = ThnM.toDouble() -  1
        }
        if ((ThnM + BlnM / 100.0 + TglM / 10000) >= 1582.1015) {
            GC = 2 - Math.floor(YM / 100) + Math.floor(YM / 400)
        } else {
            GC = 0.0
        }
        JD = Math.floor(365.25 * (YM + 4716)) +
                Math.floor(30.6001 * (MM + 1)) + DDUT + GC - 1524.5
        return JD
    }

    // JDKM
    fun JDKM(
        JD: Double, TmZn: Double = 0.0,
        OptResult: String = " ",
        fullFormat : Boolean = false,
    ) : String {
        var Alpha      : Double
        var A         : Double
        var CJD       : Double
        var Z         : Double
        var F         : Double
        var JamDes    : Double
        var B         : Double
        var C         : Double
        var D         : Double
        var E         : Double
        var TglM      : Double
        var BlnM      : Double
        var ThnM      : Double
        var ThnMHYNS  : String
        var ThnMAYNS  : String
        var NmBlnMDt  : Any
        var NmBlnM    : String
        var NoHrM     : Double
        var NmHrMDt   : Any
        var NmHrM     : String
        var NoPsM     : Double
        var NmPsMDt   : Any
        var NmPsM     : String
        var Result    : Any
        CJD      = JD + 0.5 + TmZn / 24.0
        Z       = Math.floor(CJD)
        F       = CJD - Z
        JamDes  = F * 24.0
        if (Z >= 2299161.0) {
            Alpha = Math.floor((Z - 1867216.25) / 36524.25)
            A     = Z + 1 + Alpha - Math.floor(Alpha / 4.0)
        } else {
            A     = Z
        }
        B       = A + 1524.0
        C       = Math.floor((B - 122.1) / 365.25)
        D       = Math.floor(365.25 * C)
        E       = Math.floor ((B - D) / 30.6001)
        TglM    = Math.floor(B - D - Math.floor(30.6001 * E) + F)
        if(E < 14) {
            BlnM  = E -  1.0
        } else {
            BlnM  = E - 13.0
        }
        if(BlnM > 2) {
            ThnM  = C - 4716.0
        } else {
            ThnM  = C - 4715.0
        }
        if (ThnM > 0) {
            ThnMHYNS  = (ThnM.toLong()).toString() + " M"
            ThnMAYNS  = "+" + (ThnM.toLong()).toString()
        } else {
            ThnMHYNS  = (Math.abs(ThnM).toLong() + 1).toString() + " SM"
            ThnMAYNS  = (ThnM.toLong()).toString()
        }
        NmBlnMDt    = listOf("Januari", "Februari", "Maret",
            "April", "Mei", "Juni",
            "Juli","Agustus", "September",
            "Oktober", "November", "Desember")
        NmBlnM      = NmBlnMDt.get(BlnM.toInt() - 1)
        NoHrM       = Z - 7 * Math.floor(Z / 7)
        NoPsM       = Z - 5 * Math.floor(Z / 5)
        NmHrMDt     = listOf("Senin", "Selasa", "Rabu", "Kamis",
            "Jum'at", "Sabtu", "Ahad")
        NmPsMDt     = listOf("Legi", "Pahing", "Pon", "Wage", "Kliwon")
        NmHrM = NmHrMDt.get(NoHrM.toInt())
        NmPsM = NmPsMDt.get(NoPsM.toInt())
        Result      = when ((OptResult.replace(" ","")).toUpperCase()) {
            "TGLM"    ,"TGL"                  -> (TglM).toInt()
            "BLNM"    ,"BLN"                  -> (BlnM).toInt()
            "THNM"    ,"THN"                  -> (ThnM).toLong()
            "THNMHYNS"                        -> ThnMHYNS
            "THNMAYNS"                         -> ThnMAYNS
            "NMBLNM"  ,"BULAN"                -> NmBlnM
            "NMHRM"   ,"HARI"         ,"HR"    -> NmHrM
            "NMPSM"   ,"PASARAN"    ,"PS"      -> NmPsM
            "JAMDES"  ,"JAMDESIMAL" ,"JAMD"    -> (JamDes).toDouble()
            "FRACDAY" ,"PECAHANHARI"           -> F
            else                              ->
                    (TglM.toInt()).toString() + "-" +
                    (BlnM.toInt()).toString() + "-" + ThnMHYNS
        }

        if(fullFormat) {
            return NmHrM +", " +
                    (TglM.toInt()).toString() + " " +
                    NmBlnM + " " + ThnMHYNS
        }

        return Result.toString()
    }
    // KHCJDN : Kalender Hijri ke Chronological Julian Day Number
    fun KHCJDN(
        HCalD: Int, HCalM: Int, HCalY: Long,
        HCalE: Int = 2, HCalL: Int = 2
    ): Long {
        var dltD    : Double
        var LYCor   : Double
        var HCalDN  : Double
        var CJDN    : Double
        dltD = when(HCalE) {
            1    -> 1948439.0 // 1 Al-Muḥarram 1 H = Kamis Kliwon, 15 Juli 622 M
            2    -> 1948440.0 // 1 Al-Muḥarram 1 H = Jum‘at Legi, 16 Juli 622 M
            else -> 1948440.0
        }
        LYCor = when(HCalL) {
            1    -> 15.0 // 2,5,7,10,13,15,18,21,24,26,29
            2    -> 14.0 // 2,5,7,10,13,16,18,21,24,26,29
            3    -> 11.0 // 2,5,8,10,13,16,19,21,24,27,29
            4    ->  9.0 // 2,5,8,11,13,16,19,21,24,27,30
            else -> 14.0
        }
        HCalDN = Math.floor((10631 * HCalY.toDouble() - 10631 + LYCor) / 30) +
                Math.floor((325 * HCalM.toDouble() - 320) / 11) +
                (HCalD - 1)
        CJDN   = HCalDN + dltD
        return CJDN.toLong()
    }

    // CJDNKH : Chronological Julian Day Number ke Kalender Hijri
    fun CJDNKH(
        CJDN: Long, HCalE: Int = 2, HCalL: Int = 2,
        OptResult: String = " "
    ): Any {
        var dltD      : Double
        var LYCor     : Double
        var HCalDN    : Double
        var x2        : Double
        var r2        : Double
        var x1        : Double
        var r1        : Double
        var HCalD     : Double
        var HCalM     : Double
        var HCalY     : Double
        var NmBlnHDt  : Any
        var NmBlnH    : String
        var NoHrH     : Double
        var NmHrHDt   : Any
        var NmHrH     : String
        var NoPsH     : Double
        var NmPsHDt   : Any
        var NmPsH     : String
        var Result    : Any
        dltD = when(HCalE) {
            1    -> 1948439.0 // 1 Al-Muḥarram 1 H = Kamis Kliwon, 15 Juli 622 M
            2    -> 1948440.0 // 1 Al-Muḥarram 1 H = Jum‘at Legi, 16 Juli 622 M
            else -> 1948440.0
        }
        LYCor = when(HCalL) {
            1    -> 15.0 // 2,5,7,10,13,15,18,21,24,26,29
            2    -> 14.0 // 2,5,7,10,13,16,18,21,24,26,29
            3    -> 11.0 // 2,5,8,10,13,16,19,21,24,27,29
            4    ->  9.0 // 2,5,8,11,13,16,19,21,24,27,30
            else -> 14.0
        }
        HCalDN = CJDN.toDouble() -  dltD
        x2 = Math.floor((30.0 * HCalDN + 29.0 - LYCor) / 10631)
        r2 = Mod((30 * HCalDN + 29 - LYCor),10631.0)
        x1 = Math.floor((11 * Math.floor(r2 / 30) + 5) / 325)
        r1 = Mod((11 * Math.floor(r2 / 30) + 5),325.0)
        HCalD = Math.floor(r1 / 11) + 1
        HCalM = x1 + 1
        HCalY = x2 + 1
        NmBlnHDt    = listOf("Al-Muharram", "Shafar",
            "Rabi‘ul Awwal","Rabi‘ul Akhir",
            "Jumadal Ula", "Jumadal Akhirah",
            "Rajab","Sya‘ban",
            "Ramadlan", "Syawwal",
            "Dzul Qa‘dah","Dzul Hijjah")
        NmBlnH      = NmBlnHDt.get(HCalM.toInt() - 1)
        NoHrH       = CJDN.toDouble() - 7 * Math.floor(CJDN.toDouble() / 7)
        NoPsH       = CJDN.toDouble() - 5 * Math.floor(CJDN.toDouble() / 5)
        NmHrHDt     = listOf("Senin", "Selasa", "Rabu", "Kamis",
            "Jum'at", "Sabtu", "Ahad")
        NmPsHDt     = listOf("Legi", "Pahing", "Pon", "Wage", "Kliwon")
        NmHrH = NmHrHDt.get(NoHrH.toInt())
        NmPsH = NmPsHDt.get(NoPsH.toInt())
//        Result      = when ((OptResult.replace(" ","")).toUpperCase()) {
//            "TGLH"    ,"TGL"                -> (HCalD).toInt()
//            "BLNH"    ,"BLN"                -> (HCalM).toInt()
//            "THNH"    ,"THN"                -> (HCalY).toLong()
//            "NMBLNM"  ,"BULAN"              -> NmBlnH
//            "NMHRM"   ,"HARI"       ,"HR"   -> NmHrH
//            "NMPSM"   ,"PASARAN"    ,"PS"   -> NmPsH
//            else                            -> NmHrH + " " + NmPsH + ", " +
//                    (HCalD.toInt()).toString() +
//                    " " + NmBlnH + " " +
//                    HCalY.toLong() + " H"
//        }
        return Triple<Int,Int,Long>((HCalD).toInt(),(HCalM).toInt(),(HCalY).toLong())
    }


    // Delta T
    fun DeltaT(JD: Double): Double {
        var TlM     : Long
        var JDTlMAw : Double
        var JDTlMAk : Double
        var JHrlTlM : Double
        var JHrTlM  : Double
        var dY      : Double
        var kU      : Double
        var kT      : Double
        var dltT    : Double
        var sCorr   : Double

        TlM = ((JDKM(JD, 0.0, "ThnM"))).toLong()
        JDTlMAw = KMJD(1, 1, TlM, 0.0, 0.0)
        JDTlMAk = KMJD(31, 12, TlM, 24.0, 0.0)
        JHrlTlM = JD - JDTlMAw
        JHrTlM = JDTlMAk - JDTlMAw
        dY = TlM + JHrlTlM / JHrTlM

        if (dY <= -500) {
            kU = (dY - 1820) / 100
            dltT = -20 + 32 * (kU *  kU)
        } else if((dY > -500) && (dY <= 500)) {
            kU = dY / 100
            dltT = 10583.6 - 1014.41 * kU + 33.78311 * (kU *  kU) - 5.952053 * (kU *  kU * kU) - 0.1798452 * (kU *  kU * kU * kU) + 0.022174192 * (kU *  kU * kU * kU * kU) + 0.0090316521 * (kU *  kU * kU * kU * kU * kU)
        } else if ((dY > 500) && (dY <= 1600)) {
            kU = (dY - 1000) / 100
            dltT = 1574.2 - 556.01 * kU + 71.23472 * (kU *  kU) + 0.319781 * (kU *  kU * kU) - 0.8503463 * (kU *  kU * kU * kU) - 0.005050998 * (kU *  kU * kU * kU * kU) + 0.0083572073 * (kU *  kU * kU * kU * kU * kU)
        } else if ((dY > 1600) && (dY <= 1700)) {
            kT = dY - 1600
            dltT = 120 - 0.9808 * kT - 0.01532 * (kT * kT) + (kT * kT * kT) / 7129
        } else if ((dY > 1700) && (dY <= 1800)) {
            kT = dY - 1700
            dltT = 8.83 + 0.1603 * kT - 0.0059285 * (kT * kT) + 0.00013336 * (kT * kT * kT) - (kT * kT * kT * kT) / 1174000
        } else if ((dY > 1800) && (dY <= 1860)) {
            kT = dY - 1800
            dltT = 13.72 - 0.332447 * kT + 0.0068612 * (kT * kT) + 0.0041116 * (kT * kT * kT) - 0.00037436 * (kT * kT * kT * kT) + 0.0000121272 * (kT * kT * kT * kT * kT) - 0.0000001699 * (kT * kT * kT * kT * kT * kT) + 0.000000000875 * (kT * kT * kT * kT * kT * kT * kT)
        } else if ((dY > 1860) && (dY <= 1900)) {
            kT = dY - 1860
            dltT = 7.62 + 0.5737 * kT - 0.251754 * (kT * kT) + 0.01680668 * (kT * kT * kT) - 0.0004473624 * (kT * kT * kT * kT) + (kT * kT * kT * kT * kT) / 233174
        } else if ((dY > 1900) && (dY <= 1920)) {
            kT = dY - 1900
            dltT = -2.79 + 1.494119 * kT - 0.0598939 * (kT * kT) + 0.0061966 * (kT * kT * kT) - 0.000197 * (kT * kT * kT * kT)
        } else if ((dY > 1920) && (dY <= 1941)) {
            kT = dY - 1920
            dltT = 21.2 + 0.84493 * kT - 0.0761 * (kT * kT) + 0.0020936 * (kT * kT * kT)
        } else if ((dY > 1941) && (dY <= 1961)) {
            kT = dY - 1950
            dltT = 29.07 + 0.407 * kT - (kT * kT) / 233 + (kT * kT * kT) / 2547
        } else if ((dY > 1961) && (dY <= 1986)) {
            kT = dY - 1975
            dltT = 45.45 + 1.067 * kT - (kT * kT) / 260 - (kT * kT * kT) / 718
        } else if ((dY > 1986) && (dY <= 2005)) {
            kT = dY - 2000
            dltT = 63.86 + 0.3345 * kT - 0.060374 * (kT * kT) + 0.0017275 * (kT * kT * kT) + 0.000651814 * (kT * kT * kT * kT) + 0.00002373599 * (kT * kT * kT * kT * kT)
        } else if ((dY > 2005) && (dY <= 2050)) {
            kT = dY - 2000
            dltT = 62.92 + 0.32217 * kT + 0.005589 * (kT * kT)
        } else if ((dY > 2050) && (dY <= 2150)) {
            dltT = -20 + 32 * (((dY - 1820) / 100) * ((dY - 1820) / 100)) - 0.5628 * (2150 - dY)
        } else if (dY > 2150) {
            kU = (dY - 1820) / 100
            dltT = -20 + 32 * (kU *  kU)
        } else {
            dltT = 0.0 // biar tidak error harus ada else tanpa if
        }
        if (dY < 1955 || dY > 2005) {
            sCorr   = -0.000012932 * Math.pow((dY - 1955),2.0)
            dltT    = dltT + sCorr
        } else {
            sCorr = 0.0
            dltT  = dltT
        }
        return dltT
    }

    fun NutationInLongitude (JD: Double): Double {
        val T       : Double = (JD - 2451545) / 36525
        val D       : Double = 297.85036 + 445267.11148*T - 0.0019142 * T*T + T*T*T / 189474
        val m       : Double = 357.52772 + 35999.05034 * T - 0.0001603 * T*T - T*T*T / 300000
        val M1      : Double = 134.96298 + 477198.867398 * T + 0.0086972 * T*T + T*T*T / 56250
        val F       : Double = 93.27191 + 483202.017538 * T - 0.0036825 * T*T+ T*T*T / 327270
        val Omg     : Double = 125.04452 - 1934.136261 * T + 0.0020708 * T*T + T*T*T / 450000

        val Ds       = Rad(Mod(D, 360.0))
        val ms       = Rad(Mod(m, 360.0))
        val M1s      = Rad(Mod(M1, 360.0))
        val Fs       = Rad(Mod(F, 360.0))
        val Omgs     = Rad(Mod(Omg, 360.0))

        val DeltaPsi: Double = (
                (-171996 + -174.2 * T) * Math.sin(0 * Ds + 0 * ms + 0 * M1s + 0 * Fs + 1 * Omgs)+
                        (-13187 + -1.6 * T) * Math.sin(-2 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (-2274 + -0.2 * T) * Math.sin(0 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (2062 + 0.2 * T) * Math.sin(0 * Ds + 0 * ms + 0 * M1s + 0 * Fs + 2 * Omgs)+
                        (1426 + -3.4 * T) * Math.sin(0 * Ds + 1 * ms + 0 * M1s + 0 * Fs + 0 * Omgs)+
                        (712 + 0.1 * T) * Math.sin(0 * Ds + 0 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (-517 + 1.2 * T) * Math.sin(-2 * Ds + 1 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (-386 + -0.4 * T) * Math.sin(0 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 1 * Omgs)+
                        (-301 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 1 * M1s + 2 * Fs + 2 * Omgs)+
                        (217 + -0.5 * T) * Math.sin(-2 * Ds + -1 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (-158 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (129 + 0.1 * T) * Math.sin(-2 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 1 * Omgs)+
                        (123 + 0 * T) * Math.sin(0 * Ds + 0 * ms + -1 * M1s + 2 * Fs + 2 * Omgs)+
                        (63 + 0 * T) * Math.sin(2 * Ds + 0 * ms + 0 * M1s + 0 * Fs + 0 * Omgs)+
                        (63 + 0.1 * T) * Math.sin(0 * Ds + 0 * ms + 1 * M1s + 0 * Fs + 1 * Omgs)+
                        (-59 + 0 * T) * Math.sin(2 * Ds + 0 * ms + -1 * M1s + 2 * Fs + 2 * Omgs)+
                        (-58 + -0.1 * T) * Math.sin(0 * Ds + 0 * ms + -1 * M1s + 0 * Fs + 1 * Omgs)+
                        (-51 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 1 * M1s + 2 * Fs + 1 * Omgs)+
                        (48 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 2 * M1s + 0 * Fs + 0 * Omgs)+
                        (46 + 0 * T) * Math.sin(0 * Ds + 0 * ms + -2 * M1s + 2 * Fs + 1 * Omgs)+
                        (-38 + 0 * T) * Math.sin(2 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (-31 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 2 * M1s + 2 * Fs + 2 * Omgs)+
                        (29 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 2 * M1s + 0 * Fs + 0 * Omgs)+
                        (29 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 1 * M1s + 2 * Fs + 2 * Omgs)+
                        (26 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 0 * Omgs)+
                        (-22 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 0 * Omgs)+
                        (21 + 0 * T) * Math.sin(0 * Ds + 0 * ms + -1 * M1s + 2 * Fs + 1 * Omgs)+
                        (17 + -0.1 * T) * Math.sin(0 * Ds + 2 * ms + 0 * M1s + 0 * Fs + 0 * Omgs)+
                        (16 + 0 * T) * Math.sin(2 * Ds + 0 * ms + -1 * M1s + 0 * Fs + 1 * Omgs)+
                        (-16 + 0.1 * T) * Math.sin(-2 * Ds + 2 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (-15 + 0 * T) * Math.sin(0 * Ds + 1 * ms + 0 * M1s + 0 * Fs + 1 * Omgs)+
                        (-13 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 1 * M1s + 0 * Fs + 1 * Omgs)+
                        (-12 + 0 * T) * Math.sin(0 * Ds + -1 * ms + 0 * M1s + 0 * Fs + 1 * Omgs)+
                        (11 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 2 * M1s + -2 * Fs + 0 * Omgs)+
                        (-10 + 0 * T) * Math.sin(2 * Ds + 0 * ms + -1 * M1s + 2 * Fs + 1 * Omgs)+
                        (-8 + 0 * T) * Math.sin(2 * Ds + 0 * ms + 1 * M1s + 2 * Fs + 2 * Omgs)+
                        (7 + 0 * T) * Math.sin(0 * Ds + 1 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (-7 + 0 * T) * Math.sin(-2 * Ds + 1 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (-7 + 0 * T) * Math.sin(0 * Ds + -1 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)+
                        (-7 + 0 * T) * Math.sin(2 * Ds + 0 * ms + 0 * M1s + 2 * Fs + 1 * Omgs)+
                        (6 + 0 * T) * Math.sin(2 * Ds + 0 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (6 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 2 * M1s + 2 * Fs + 2 * Omgs)+
                        (6 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 1 * M1s + 2 * Fs + 1 * Omgs)+
                        (-6 + 0 * T) * Math.sin(2 * Ds + 0 * ms + -2 * M1s + 0 * Fs + 1 * Omgs)+
                        (-6 + 0 * T) * Math.sin(2 * Ds + 0 * ms + 0 * M1s + 0 * Fs + 1 * Omgs)+
                        (5 + 0 * T) * Math.sin(0 * Ds + -1 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (-5 + 0 * T) * Math.sin(-2 * Ds + -1 * ms + 0 * M1s + 2 * Fs + 1 * Omgs)+
                        (-5 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 0 * M1s + 0 * Fs + 1 * Omgs)+
                        (-5 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 2 * M1s + 2 * Fs + 1 * Omgs)+
                        (4 + 0 * T) * Math.sin(-2 * Ds + 0 * ms + 2 * M1s + 0 * Fs + 1 * Omgs)+
                        (4 + 0 * T) * Math.sin(-2 * Ds + 1 * ms + 0 * M1s + 2 * Fs + 1 * Omgs)+
                        (4 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 1 * M1s + -2 * Fs + 0 * Omgs)+
                        (-4 + 0 * T) * Math.sin(-1 * Ds + 0 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (-4 + 0 * T) * Math.sin(-2 * Ds + 1 * ms + 0 * M1s + 0 * Fs + 0 * Omgs)+
                        (-4 + 0 * T) * Math.sin(1 * Ds + 0 * ms + 0 * M1s + 0 * Fs + 0 * Omgs)+
                        (3 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 1 * M1s + 2 * Fs + 0 * Omgs)+
                        (-3 + 0 * T) * Math.sin(0 * Ds + 0 * ms + -2 * M1s + 2 * Fs + 2 * Omgs)+
                        (-3 + 0 * T) * Math.sin(-1 * Ds + -1 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (-3 + 0 * T) * Math.sin(0 * Ds + 1 * ms + 1 * M1s + 0 * Fs + 0 * Omgs)+
                        (-3 + 0 * T) * Math.sin(0 * Ds + -1 * ms + 1 * M1s + 2 * Fs + 2 * Omgs)+
                        (-3 + 0 * T) * Math.sin(2 * Ds + -1 * ms + -1 * M1s + 2 * Fs + 2 * Omgs)+
                        (-3 + 0 * T) * Math.sin(0 * Ds + 0 * ms + 3 * M1s + 2 * Fs + 2 * Omgs)+
                        (-3 + 0 * T) * Math.sin(2 * Ds + -1 * ms + 0 * M1s + 2 * Fs + 2 * Omgs)
                )/36000000
        return DeltaPsi
    }
    fun NutationInObliquity (JD: Double): Double {
        val T       : Double
        var D       : Double
        var M       : Double
        var M1      : Double
        var F       : Double
        var Omg     : Double

        T = (JD - 2451545) / 36525
        D = 297.85036 + 445267.11148 * T - 0.0019142 * T *T + T*T*T / 189474
        M = 357.52772 + 35999.05034 * T - 0.0001603 * T*T - T*T*T / 300000
        M1 = 134.96298 + 477198.867398 * T + 0.0086972 * T*T+ T*T*T / 56250
        F = 93.27191 + 483202.017538 * T - 0.0036825 * T*T + T*T*T / 327270
        Omg = 125.04452 - 1934.136261 * T + 0.0020708 * T*T + T*T*T / 450000

        D       = Rad(Mod(D, 360.0))
        M       = Rad(Mod(M, 360.0))
        M1      = Rad(Mod(M1, 360.0))
        F       = Rad(Mod(F, 360.0))
        Omg     = Rad(Mod(Omg, 360.0))

        val DeltaEps : Double = (
                (92025 + 8.9 * T) * Math.cos(0 * D + 0 * M + 0 * M1 + 0 * F + 1 * Omg) +
                        (5736 + -3.1 * T) * Math.cos(-2 * D + 0 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (977 + -0.5 * T) * Math.cos(0 * D + 0 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (-895 + 0.5 * T) * Math.cos(0 * D + 0 * M + 0 * M1 + 0 * F + 2 * Omg) +
                        (54 + -0.1 * T) * Math.cos(0 * D + 1 * M + 0 * M1 + 0 * F + 0 * Omg) +
                        (-7 + 0 * T) * Math.cos(0 * D + 0 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (224 + -0.6 * T) * Math.cos(-2 * D + 1 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (200 + 0 * T) * Math.cos(0 * D + 0 * M + 0 * M1 + 2 * F + 1 * Omg) +
                        (129 + -0.1 * T) * Math.cos(0 * D + 0 * M + 1 * M1 + 2 * F + 2 * Omg) +
                        (-95 + 0.3 * T) * Math.cos(-2 * D + -1 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(-2 * D + 0 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (-70 + 0 * T) * Math.cos(-2 * D + 0 * M + 0 * M1 + 2 * F + 1 * Omg) +
                        (-53 + 0 * T) * Math.cos(0 * D + 0 * M + -1 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(2 * D + 0 * M + 0 * M1 + 0 * F + 0 * Omg) +
                        (-33 + 0 * T) * Math.cos(0 * D + 0 * M + 1 * M1 + 0 * F + 1 * Omg) +
                        (26 + 0 * T) * Math.cos(2 * D + 0 * M + -1 * M1 + 2 * F + 2 * Omg) +
                        (32 + 0 * T) * Math.cos(0 * D + 0 * M + -1 * M1 + 0 * F + 1 * Omg) +
                        (27 + 0 * T) * Math.cos(0 * D + 0 * M + 1 * M1 + 2 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(-2 * D + 0 * M + 2 * M1 + 0 * F + 0 * Omg) +
                        (-24 + 0 * T) * Math.cos(0 * D + 0 * M + -2 * M1 + 2 * F + 1 * Omg) +
                        (16 + 0 * T) * Math.cos(2 * D + 0 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (13 + 0 * T) * Math.cos(0 * D + 0 * M + 2 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 0 * M + 2 * M1 + 0 * F + 0 * Omg) +
                        (-12 + 0 * T) * Math.cos(-2 * D + 0 * M + 1 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 0 * M + 0 * M1 + 2 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(-2 * D + 0 * M + 0 * M1 + 2 * F + 0 * Omg) +
                        (-10 + 0 * T) * Math.cos(0 * D + 0 * M + -1 * M1 + 2 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 2 * M + 0 * M1 + 0 * F + 0 * Omg) +
                        (-8 + 0 * T) * Math.cos(2 * D + 0 * M + -1 * M1 + 0 * F + 1 * Omg) +
                        (7 + 0 * T) * Math.cos(-2 * D + 2 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (9 + 0 * T) * Math.cos(0 * D + 1 * M + 0 * M1 + 0 * F + 1 * Omg) +
                        (7 + 0 * T) * Math.cos(-2 * D + 0 * M + 1 * M1 + 0 * F + 1 * Omg) +
                        (6 + 0 * T) * Math.cos(0 * D + -1 * M + 0 * M1 + 0 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 0 * M + 2 * M1 + -2 * F + 0 * Omg) +
                        (5 + 0 * T) * Math.cos(2 * D + 0 * M + -1 * M1 + 2 * F + 1 * Omg) +
                        (3 + 0 * T) * Math.cos(2 * D + 0 * M + 1 * M1 + 2 * F + 2 * Omg) +
                        (-3 + 0 * T) * Math.cos(0 * D + 1 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(-2 * D + 1 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (3 + 0 * T) * Math.cos(0 * D + -1 * M + 0 * M1 + 2 * F + 2 * Omg) +
                        (3 + 0 * T) * Math.cos(2 * D + 0 * M + 0 * M1 + 2 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(2 * D + 0 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (-3 + 0 * T) * Math.cos(-2 * D + 0 * M + 2 * M1 + 2 * F + 2 * Omg) +
                        (-3 + 0 * T) * Math.cos(-2 * D + 0 * M + 1 * M1 + 2 * F + 1 * Omg) +
                        (3 + 0 * T) * Math.cos(2 * D + 0 * M + -2 * M1 + 0 * F + 1 * Omg) +
                        (3 + 0 * T) * Math.cos(2 * D + 0 * M + 0 * M1 + 0 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + -1 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (3 + 0 * T) * Math.cos(-2 * D + -1 * M + 0 * M1 + 2 * F + 1 * Omg) +
                        (3 + 0 * T) * Math.cos(-2 * D + 0 * M + 0 * M1 + 0 * F + 1 * Omg) +
                        (3 + 0 * T) * Math.cos(0 * D + 0 * M + 2 * M1 + 2 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(-2 * D + 0 * M + 2 * M1 + 0 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(-2 * D + 1 * M + 0 * M1 + 2 * F + 1 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 0 * M + 1 * M1 + -2 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(-1 * D + 0 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(-2 * D + 1 * M + 0 * M1 + 0 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(1 * D + 0 * M + 0 * M1 + 0 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 0 * M + 1 * M1 + 2 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 0 * M + -2 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(-1 * D + -1 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 1 * M + 1 * M1 + 0 * F + 0 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + -1 * M + 1 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(2 * D + -1 * M + -1 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(0 * D + 0 * M + 3 * M1 + 2 * F + 2 * Omg) +
                        (0 + 0 * T) * Math.cos(2 * D + -1 * M + 0 * M1 + 2 * F + 2 * Omg)
                )/ 36000000
        return DeltaEps

    }
    fun MeanObliquityOfEcliptic (JD: Double): Double {
        val T       : Double
        val U       : Double
        val Eps0    : Double

        T = (JD - 2451545) / 36525
        U = T / 100
        Eps0 = 23.0+26.0/60+21.448/3600 +(-4680.93 * U
                - 1.55 * Math.pow(U,2.0)
                + 1999.25 * Math.pow(U,3.0)
                - 51.38 * Math.pow(U,4.0)
                - 249.67 * Math.pow(U,5.0)
                - 39.05 * Math.pow(U,6.0)
                + 7.12 * Math.pow(U,7.0)
                + 27.87 * Math.pow(U,8.0)
                + 5.79 * Math.pow(U,9.0)
                + 2.45 * Math.pow(U,10.0))/3600
        return Eps0
    }
    fun ObliquityOfEcliptic(JD: Double): Double{
        val Eps0    : Double
        val DltEps  : Double
        val Eps     : Double

        Eps0 = MeanObliquityOfEcliptic(JD)
        DltEps = NutationInObliquity(JD)
        Eps = Eps0 + DltEps

        return Eps
    }

    fun EarthHeliocentricLongitude (JD: Double): Double{

        val Tau : Double
        val L0  : Double
        val L1  : Double
        val L2  : Double
        val L3  : Double
        val L4  : Double
        val L5  : Double
        var L   : Double

        Tau = (JD - 2451545) / 365250
        L0 =     175347046 * Math.cos(0 + 0 * Tau) +
                3341656 * Math.cos(4.6692568 + 6283.07585 * Tau) +
                34894 * Math.cos(4.6261 + 12566.1517 * Tau) +
                3497 * Math.cos(2.7441 + 5753.3849 * Tau) +
                3418 * Math.cos(2.8289 + 3.5231 * Tau) +
                3136 * Math.cos(3.6277 + 77713.7715 * Tau) +
                2676 * Math.cos(4.4181 + 7860.4194 * Tau) +
                2343 * Math.cos(6.1352 + 3930.2097 * Tau) +
                1324 * Math.cos(0.7425 + 11506.7698 * Tau) +
                1273 * Math.cos(2.0371 + 529.691 * Tau) +
                1199 * Math.cos(1.1096 + 1577.3435 * Tau) +
                990 * Math.cos(5.233 + 5884.927 * Tau) +
                902 * Math.cos(2.045 + 26.298 * Tau) +
                857 * Math.cos(3.508 + 398.149 * Tau) +
                780 * Math.cos(1.179 + 5223.694 * Tau) +
                753 * Math.cos(2.533 + 5507.553 * Tau) +
                505 * Math.cos(4.583 + 18849.228 * Tau) +
                492 * Math.cos(4.205 + 775.523 * Tau) +
                357 * Math.cos(2.92 + 0.067 * Tau) +
                317 * Math.cos(5.849 + 11790.629 * Tau) +
                284 * Math.cos(1.899 + 796.298 * Tau) +
                271 * Math.cos(0.315 + 10977.079 * Tau) +
                243 * Math.cos(0.345 + 5486.778 * Tau) +
                206 * Math.cos(4.806 + 2544.314 * Tau) +
                205 * Math.cos(1.869 + 5573.143 * Tau) +
                202 * Math.cos(2.458 + 6069.777 * Tau) +
                156 * Math.cos(0.833 + 213.299 * Tau) +
                132 * Math.cos(3.411 + 2942.463 * Tau) +
                126 * Math.cos(1.083 + 20.775 * Tau) +
                115 * Math.cos(0.645 + 0.98 * Tau) +
                103 * Math.cos(0.636 + 4694.003 * Tau) +
                102 * Math.cos(0.976 + 15720.839 * Tau) +
                102 * Math.cos(4.267 + 7.114 * Tau) +
                99 * Math.cos(6.21 + 2146.17 * Tau) +
                98 * Math.cos(0.68 + 155.42 * Tau) +
                86 * Math.cos(5.98 + 161000.69 * Tau) +
                85 * Math.cos(1.3 + 6275.96 * Tau) +
                85 * Math.cos(3.67 + 71430.7 * Tau) +
                80 * Math.cos(1.81 + 17260.15 * Tau) +
                79 * Math.cos(3.04 + 12036.46 * Tau) +
                75 * Math.cos(1.76 + 5088.63 * Tau) +
                74 * Math.cos(3.5 + 3154.69 * Tau) +
                74 * Math.cos(4.68 + 801.82 * Tau) +
                70 * Math.cos(0.83 + 9437.76 * Tau) +
                62 * Math.cos(3.98 + 8827.39 * Tau) +
                61 * Math.cos(1.82 + 7084.9 * Tau) +
                57 * Math.cos(2.78 + 6286.6 * Tau) +
                56 * Math.cos(4.39 + 14143.5 * Tau) +
                56 * Math.cos(3.47 + 6279.55 * Tau) +
                52 * Math.cos(0.19 + 12139.55 * Tau) +
                52 * Math.cos(1.33 + 1748.02 * Tau) +
                51 * Math.cos(0.28 + 5856.48 * Tau) +
                49 * Math.cos(0.49 + 1194.45 * Tau) +
                41 * Math.cos(5.37 + 8429.24 * Tau) +
                41 * Math.cos(2.4 + 19651.05 * Tau) +
                39 * Math.cos(6.17 + 10447.39 * Tau) +
                37 * Math.cos(6.04 + 10213.29 * Tau) +
                37 * Math.cos(2.57 + 1059.38 * Tau) +
                36 * Math.cos(1.71 + 2352.87 * Tau) +
                36 * Math.cos(1.78 + 6812.77 * Tau) +
                33 * Math.cos(0.59 + 17789.85 * Tau) +
                30 * Math.cos(0.44 + 83996.85 * Tau) +
                30 * Math.cos(2.74 + 1349.87 * Tau) +
                25 * Math.cos(3.16 + 4690.48 * Tau)

        L1 =    628331966747 * Math.cos(0 + 0 * Tau) +
                206059 * Math.cos(2.678235 + 6283.07585 * Tau) +
                4303 * Math.cos(2.6351 + 12566.1517 * Tau) +
                425 * Math.cos(1.59 + 3.523 * Tau) +
                119 * Math.cos(5.796 + 26.298 * Tau) +
                109 * Math.cos(2.966 + 1577.344 * Tau) +
                93 * Math.cos(2.59 + 18849.23 * Tau) +
                72 * Math.cos(1.14 + 529.69 * Tau) +
                68 * Math.cos(1.87 + 398.15 * Tau) +
                67 * Math.cos(4.41 + 5507.55 * Tau) +
                59 * Math.cos(2.89 + 5223.69 * Tau) +
                56 * Math.cos(2.17 + 155.42 * Tau) +
                45 * Math.cos(0.4 + 796.3 * Tau) +
                36 * Math.cos(0.47 + 775.52 * Tau) +
                29 * Math.cos(2.65 + 7.11 * Tau) +
                21 * Math.cos(5.34 + 0.98 * Tau) +
                19 * Math.cos(1.85 + 5486.78 * Tau) +
                19 * Math.cos(4.97 + 213.3 * Tau) +
                17 * Math.cos(2.99 + 6275.96 * Tau) +
                16 * Math.cos(0.03 + 2544.31 * Tau) +
                16 * Math.cos(1.43 + 2146.17 * Tau) +
                15 * Math.cos(1.21 + 10977.08 * Tau) +
                12 * Math.cos(2.83 + 1748.02 * Tau) +
                12 * Math.cos(3.26 + 5088.63 * Tau) +
                12 * Math.cos(5.27 + 1194.45 * Tau) +
                12 * Math.cos(2.08 + 4694 * Tau) +
                11 * Math.cos(0.77 + 553.57 * Tau) +
                10 * Math.cos(1.3 + 6286.6 * Tau) +
                10 * Math.cos(4.24 + 1349.87 * Tau) +
                9 * Math.cos(2.7 + 242.73 * Tau) +
                9 * Math.cos(5.64 + 951.72 * Tau) +
                8 * Math.cos(5.3 + 2352.87 * Tau) +
                6 * Math.cos(2.65 + 9437.76 * Tau) +
                6 * Math.cos(4.67 + 4690.48 * Tau)

        L2 =    52919 * Math.cos(0 + 0 * Tau) +
                8720 * Math.cos(1.0721 + 6283.0758 * Tau) +
                309 * Math.cos(0.867 + 12566.152 * Tau) +
                27 * Math.cos(0.05 + 3.52 * Tau) +
                16 * Math.cos(5.19 + 26.3 * Tau) +
                16 * Math.cos(3.68 + 155.42 * Tau) +
                10 * Math.cos(0.76 + 18849.23 * Tau) +
                9 * Math.cos(2.06 + 77713.77 * Tau) +
                7 * Math.cos(0.83 + 775.52 * Tau) +
                5 * Math.cos(4.66 + 1577.34 * Tau) +
                4 * Math.cos(1.03 + 7.11 * Tau) +
                4 * Math.cos(3.44 + 5573.14 * Tau) +
                3 * Math.cos(5.14 + 796.3 * Tau) +
                3 * Math.cos(6.05 + 5507.55 * Tau) +
                3 * Math.cos(1.19 + 242.73 * Tau) +
                3 * Math.cos(6.12 + 529.69 * Tau) +
                3 * Math.cos(0.31 + 398.15 * Tau) +
                3 * Math.cos(2.28 + 553.57 * Tau) +
                2 * Math.cos(4.38 + 5223.69 * Tau) +
                2 * Math.cos(3.75 + 0.98 * Tau)

        L3 =    289 * Math.cos(5.844 + 6283.076 * Tau) +
                35 * Math.cos(0 + 0 * Tau) +
                17 * Math.cos(5.49 + 12566.15 * Tau) +
                3 * Math.cos(5.2 + 155.42 * Tau) +
                1 * Math.cos(4.72 + 3.52 * Tau) +
                1 * Math.cos(5.3 + 18849.23 * Tau) +
                1 * Math.cos(5.97 + 242.73 * Tau)

        L4 =    114 * Math.cos(3.142 + 0 * Tau)+
                8 * Math.cos(4.13 + 6283.08 * Tau)+
                1 * Math.cos(3.84 + 12566.15 * Tau)

        L5 =    1 * Math.cos(3.14 + 0 * Tau)

        L = (L0 + L1 * Tau + L2 * Math.pow(Tau,2.0) + L3 * Math.pow(Tau,3.0) + L4 * Math.pow(Tau,4.0) + L5 * Math.pow(Tau,5.0)) / 100000000
        L = Deg(L)
        L = Mod(L, 360.0)
        return L
    }
    fun EarthHeliocentricLatitude (JD: Double): Double {
        val Tau : Double
        val B0  : Double
        val B1  : Double
        var B   : Double

        Tau = (JD - 2451545) / 365250
        B0  =   280 * Math.cos(3.199 + 84334.662 * Tau) +
                102 * Math.cos(5.422 + 5507.553 * Tau) +
                80 * Math.cos(3.88 + 5223.69 * Tau) +
                44 * Math.cos(3.7 + 2352.87 * Tau) +
                32 * Math.cos(4 + 1577.34 * Tau)
        B1  =   9 * Math.cos(3.9 + 5507.55 * Tau) +
                6 * Math.cos(1.73 + 5223.69 * Tau)
        B   =   (B0 + B1 * Tau) / 100000000
        B   = Deg(B)
        return B
    }
    fun EarthRadiusVector (JD: Double): Double {
        val Tau : Double
        val R0  : Double
        val R1  : Double
        val R2  : Double
        val R3  : Double
        val R4  : Double
        var R   : Double

        Tau = (JD - 2451545) / 365250

        R0  =   100013989 * Math.cos(0 + 0 * Tau) +
                1670700 * Math.cos(3.0984635 + 6283.07585 * Tau) +
                13956 * Math.cos(3.05525 + 12566.1517 * Tau) +
                3084 * Math.cos(5.1985 + 77713.7715 * Tau) +
                1628 * Math.cos(1.1739 + 5753.3849 * Tau) +
                1576 * Math.cos(2.8469 + 7860.4194 * Tau) +
                925 * Math.cos(5.453 + 11506.77 * Tau) +
                542 * Math.cos(4.564 + 3930.21 * Tau) +
                472 * Math.cos(3.661 + 5884.927 * Tau) +
                346 * Math.cos(0.964 + 5507.553 * Tau) +
                329 * Math.cos(5.9 + 5223.694 * Tau) +
                307 * Math.cos(0.299 + 5573.143 * Tau) +
                243 * Math.cos(4.273 + 11790.629 * Tau) +
                212 * Math.cos(5.847 + 1577.344 * Tau) +
                186 * Math.cos(5.022 + 10977.079 * Tau) +
                175 * Math.cos(3.012 + 18849.228 * Tau) +
                110 * Math.cos(5.055 + 5486.778 * Tau) +
                98 * Math.cos(0.89 + 6069.78 * Tau) +
                86 * Math.cos(5.69 + 15720.84 * Tau) +
                86 * Math.cos(1.27 + 161000.69 * Tau) +
                65 * Math.cos(0.27 + 17260.15 * Tau) +
                63 * Math.cos(0.92 + 529.69 * Tau) +
                57 * Math.cos(2.01 + 83996.85 * Tau) +
                56 * Math.cos(5.24 + 71430.7 * Tau) +
                49 * Math.cos(3.25 + 2544.31 * Tau) +
                47 * Math.cos(2.58 + 775.52 * Tau) +
                45 * Math.cos(5.54 + 9437.76 * Tau) +
                43 * Math.cos(6.01 + 6275.96 * Tau) +
                39 * Math.cos(5.36 + 4694 * Tau) +
                38 * Math.cos(2.39 + 8827.39 * Tau) +
                37 * Math.cos(0.83 + 19651.05 * Tau) +
                37 * Math.cos(4.9 + 12139.55 * Tau) +
                36 * Math.cos(1.67 + 12036.46 * Tau) +
                35 * Math.cos(1.84 + 2942.46 * Tau) +
                33 * Math.cos(0.24 + 7084.9 * Tau) +
                32 * Math.cos(0.18 + 5088.63 * Tau) +
                32 * Math.cos(1.78 + 398.15 * Tau) +
                28 * Math.cos(1.21 + 6286.6 * Tau) +
                28 * Math.cos(1.9 + 6279.55 * Tau) +
                26 * Math.cos(4.59 + 10447.39 * Tau)

        R1 =    103019 * Math.cos(1.10749 + 6283.07585 * Tau) +
                1721 * Math.cos(1.0644 + 12566.1517 * Tau) +
                702 * Math.cos(3.142 + 0 * Tau) +
                32 * Math.cos(1.02 + 18849.23 * Tau) +
                31 * Math.cos(2.84 + 5507.55 * Tau) +
                25 * Math.cos(1.32 + 5223.69 * Tau) +
                18 * Math.cos(1.42 + 1577.34 * Tau) +
                10 * Math.cos(5.91 + 10977.08 * Tau) +
                9 * Math.cos(1.42 + 6275.96 * Tau) +
                9 * Math.cos(0.27 + 5486.78 * Tau)

        R2 =    4359 * Math.cos(5.7846 + 6283.0758 * Tau) +
                124 * Math.cos(5.579 + 12566.152 * Tau) +
                12 * Math.cos(3.14 + 0 * Tau) +
                9 * Math.cos(3.63 + 77713.77 * Tau) +
                6 * Math.cos(1.87 + 5573.14 * Tau) +
                3 * Math.cos(5.47 + 18849.23 * Tau)

        R3 =    145 * Math.cos(4.273 + 6283.076 * Tau) +
                7 * Math.cos(3.92 + 12566.15 * Tau)

        R4 =    4 * Math.cos(2.56 + 6283.08 * Tau)

        R = (R0 + R1 * Tau + R2 * Math.pow(Tau,2.0) + R3 * Math.pow(Tau,3.0) + R4 * Math.pow(Tau,4.0)) / 100000000
        return R
    }
    fun SunGeocentricLongitude (JD: Double): Double{
        var L           : Double
        var B           : Double
        var Theta       : Double
        var Beta        : Double
        var T           : Double
        var Lambda1     : Double
        var DeltaTheta  : Double
        var ThetaFK5    : Double
        var DeltaPsi    : Double
        var Abberration : Double
        var Lambda      : Double

        L 			= EarthHeliocentricLongitude(JD)
        B 			= EarthHeliocentricLatitude(JD)
        Theta 		= L + 180
        Theta 		= Mod(Theta,360.0)
        Beta 		= -B

        T 			= (JD - 2451545) / 36525
        Lambda1 	= Theta - 1.397 * T - 0.00031 * T*T
        DeltaTheta 	= (-0.09033 + 0.03916 * (Math.cos(Rad(Lambda1)) + Math.sin(Rad(Lambda1))) * Math.tan(Rad(Beta))) /3600
        ThetaFK5 	= Theta + DeltaTheta

        DeltaPsi 	= NutationInLongitude(JD)
        Abberration = (-20.4898 / EarthRadiusVector(JD)) / 3600.0
        Lambda 		= ThetaFK5 + DeltaPsi + Abberration
        Lambda 		= Mod (Lambda, 360.0)

        return Lambda
    }
    fun SunGeocentricLatitude (JD: Double): Double {
        var L           : Double
        var B           : Double
        var Theta       : Double
        var Beta        : Double
        var T           : Double
        var Lambda1     : Double
        var DeltaBeta   : Double
        var BetaFK5     : Double

        L = EarthHeliocentricLongitude(JD)
        B = EarthHeliocentricLatitude(JD)*3600
        Theta = L + 180
        Theta = Mod(Theta, 360.0)
        Beta = -B

        T = (JD - 2451545) / 36525
        Lambda1 = Theta - 1.397 * T - 0.00031 * T*T
        DeltaBeta = 0.03916 * (Math.cos(Rad(Lambda1)) - Math.sin(Rad(Lambda1)))
        BetaFK5 = (Beta + DeltaBeta)
        return BetaFK5
    }
    fun SunGeocentricDistance (JD: Double): Double {
        var R: Double
        R   = EarthRadiusVector(JD)
        return R
    }
    fun SunApparentRightAscension (JD: Double): Double {
        var Lambda  : Double
        var Beta    : Double
        var Epsilon : Double
        var Alpha   : Double

        Lambda = SunGeocentricLongitude(JD)
        Beta = SunGeocentricLatitude(JD)/3600
        Epsilon = ObliquityOfEcliptic(JD)
        Alpha = Deg(Math.atan2(Math.sin(Rad(Lambda)) * Math.cos(Rad(Epsilon)) - Math.tan(Rad(Beta)) * Math.sin(Rad(Epsilon)), Math.cos(Rad(Lambda))))
        Alpha = Mod(Alpha, 360.0)
        return Alpha
    }
    fun SunApparentDeclination (JD: Double): Double {
        var Lambda  : Double
        var Beta    : Double
        var Epsilon : Double
        var Delta   : Double

        Lambda = SunGeocentricLongitude(JD)
        Beta = SunGeocentricLatitude(JD)/3600
        Epsilon = ObliquityOfEcliptic(JD)
        Delta = Deg(Math.asin(Math.sin(Rad(Beta)) * Math.cos(Rad(Epsilon)) + Math.cos(Rad(Beta)) * Math.sin(Rad(Epsilon)) * Math.sin(Rad(Lambda))))
        return Delta
    }
    fun SunEquatorialHorizontalParallax (JD: Double): Double{
        var R   : Double
        var Pi  : Double

        R = SunGeocentricDistance(JD)
        Pi = Deg(Math.asin(Math.sin(Rad(8.794 / 3600)) / R))
        return Pi
    }
    fun SunAngularSemiDiameter (JD: Double): Double{
        var R   : Double
        var s0  : Double
        var s   : Double

        R = SunGeocentricDistance(JD)
        s0 = (15 + 59.63 / 60)/60
        s = s0 / R
        return s
    }
    fun EquationOfTime (JD: Double): Double {
        var Tau: Double
        var Alpha: Double
        var DeltaPsi: Double
        var Epsilon: Double
        var L0: Double
        var E: Double

        Tau = (JD - 2451545) / 365250
        Alpha = SunApparentRightAscension(JD)
        DeltaPsi = NutationInLongitude(JD)
        Epsilon = ObliquityOfEcliptic(JD)
        L0 = 280.4664567 + 360007.6982779 * Tau + 0.03032028 * Math.pow(Tau, 2.0) + Math.pow(
            Tau,
            3.0
        ) / 49931 - Math.pow(Tau, 4.0) / 15300 - Math.pow(Tau, 5.0) / 2000000
        L0 = Mod(L0, 360.0)
        E = L0 - 0.0057183 - Alpha + DeltaPsi * Math.cos(Rad(Epsilon))
        if (Math.abs(E) * 4 < 20) {
            E = E / 15
        } else if ((Math.abs(E) * 4 >= 20) and (E > 0)) {
            E = E/15 -24
        } else if ((Math.abs(E) * 4 >= 20) and (E < 0)) {
            E = E/15+24
        } else {E = E/15}
        return E
    }

    fun MoonGeocentricLongitude (JD: Double): Double {
        var T       : Double
        var L       : Double
        var D       : Double
        var m       : Double
        var M       : Double
        var f       : Double
        var A1      : Double
        var A2      : Double
        var A3      : Double
        var E       : Double
        var lM      : Double
        var lM_True : Double
        var lM_Appa : Double

        T = (JD - 2451545) / 36525
        L = 218.3164477 + 481267.88123421 * T - 0.0015786 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 538841 - Math.pow(T, 4.0) / 65194000
        D = 297.8501921 + 445267.1114034 * T - 0.0018819 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 545868 - Math.pow(T, 4.0) / 113065000
        m = 357.5291092 + 35999.0502909 * T - 0.0001536 * Math.pow(T, 2.0) + Math.pow (T, 3.0) / 24490000
        M = 134.9633964 + 477198.8675055 * T + 0.0087414 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 69699 - Math.pow(T, 4.0) / 14712000
        f = 93.272095 + 483202.0175233 * T - 0.0036539 * Math.pow(T, 2.0) - Math.pow(T, 3.0) / 3526000 + Math.pow(T, 4.0) / 863310000
        A1 = 119.75 + 131.849 * T
        A2 = 53.09 + 479264.29 * T
        A3 = 313.45 + 481266.484 * T
        E = 1 - 0.002516 * T - 0.0000074 *T*T

        L = Rad(Mod(L, 360.0))
        D = Rad(Mod(D, 360.0))
        m = Rad(Mod(m, 360.0))
        M = Rad(Mod(M, 360.0))
        f = Rad(Mod(f, 360.0))
        A1 = Rad(Mod(A1, 360.0))
        A2 = Rad(Mod(A2, 360.0))
        A3 = Rad(Mod(A3, 360.0))

        lM      =   6288774 * Math.pow(E, (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 1 *M+ 0 * f) +
                1274027 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -1 * M + 0 * f) +
                658314 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 0 * M + 0 * f) +
                213618 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 2 * M + 0 * f) +
                -185116 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 0 * M + 0 * f) +
                -114332 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 0 * M + 2 * f) +
                58793 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -2 * M + 0 * f) +
                57066 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + -1 * M + 0 * f) +
                53322 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 1 * M + 0 * f) +
                45758 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 0 * M + 0 * f) +
                -40923 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + -1 * M + 0 * f) +
                -34720 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + 0 * M + 0 * f) +
                -30383 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 1 * M + 0 * f) +
                15327 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 0 * M + -2 * f) +
                -12528 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 1 * M + 2 * f) +
                10980 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 1 * M + -2 * f) +
                10675 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + -1 * M + 0 * f) +
                10034 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 3 * M + 0 * f) +
                8548 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + -2 * M + 0 * f) +
                -7888 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + -1 * M + 0 * f) +
                -6766 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + 0 * M + 0 * f) +
                -5163 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + -1 * M + 0 * f) +
                4987 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(1 * D + 1 * m + 0 * M + 0 * f) +
                4036 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 1 * M + 0 * f) +
                3994 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 2 * M + 0 * f) +
                3861 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + 0 * M + 0 * f) +
                3665 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -3 * M + 0 * f) +
                -2689 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + -2 * M + 0 * f) +
                -2602 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -1 * M + 2 * f) +
                2390 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + -2 * M + 0 * f) +
                -2348 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + 1 * M + 0 * f) +
                2236 * Math.pow(E,  (Math.abs(-2.0))) * Math.sin(2 * D + -2 * m + 0 * M + 0 * f) +
                -2120 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 2 * M + 0 * f) +
                -2069 * Math.pow(E,  (Math.abs(2.0))) * Math.sin(0 * D + 2 * m + 0 * M + 0 * f) +
                2048 * Math.pow(E,  (Math.abs(-2.0))) * Math.sin(2 * D + -2 * m + -1 * M + 0 * f) +
                -1773 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 1 * M + -2 * f) +
                -1595 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 0 * M + 2 * f) +
                1215 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(4 * D + -1 * m + -1 * M + 0 * f) +
                -1110 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 2 * M + 2 * f) +
                -892 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(3 * D + 0 * m + -1 * M + 0 * f) +
                -810 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + 1 * M + 0 * f) +
                759 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(4 * D + -1 * m + -2 * M + 0 * f) +
                -713 * Math.pow(E,  (Math.abs(2.0))) * Math.sin(0 * D + 2 * m + -1 * M + 0 * f) +
                -700 * Math.pow(E,  (Math.abs(2.0))) * Math.sin(2 * D + 2 * m + -1 * M + 0 * f) +
                691 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + -2 * M + 0 * f) +
                596 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 0 * M + -2 * f) +
                549 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + 1 * M + 0 * f) +
                537 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 4 * M + 0 * f) +
                520 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(4 * D + -1 * m + 0 * M + 0 * f) +
                -487 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + -2 * M + 0 * f) +
                -399 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + 0 * M + -2 * f) +
                -381 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 2 * M + -2 * f) +
                351 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(1 * D + 1 * m + 1 * M + 0 * f) +
                -340 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(3 * D + 0 * m + -2 * M + 0 * f) +
                330 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + -3 * M + 0 * f) +
                327 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 2 * M + 0 * f) +
                -323 * Math.pow(E,  (Math.abs(2.0))) * Math.sin(0 * D + 2 * m + 1 * M + 0 * f) +
                299 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(1 * D + 1 * m + -1 * M + 0 * f) +
                294 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 3 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -1 * M + -2 * f) +
                (3958 * Math.sin(A1)
                        + 1962 * Math.sin(L - f)
                        + 318 * Math.sin(A2))
        lM_True = Deg(L) + lM / 1000000
        lM_True = Mod(lM_True, 360.0)
        lM_Appa = lM_True + NutationInLongitude(JD)
        lM_Appa = Mod(lM_Appa, 360.0)
        return lM_Appa
    }
    fun MoonGeocentricLatitude (JD: Double): Double{
        var T       : Double
        var L       : Double
        var D       : Double
        var m       : Double
        var M       : Double
        var f       : Double
        var A1      : Double
        var A2      : Double
        var A3      : Double
        var E       : Double
        var bM      : Double

        T = (JD - 2451545) / 36525
        L = 218.3164477 + 481267.88123421 * T - 0.0015786 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 538841 - Math.pow(T, 4.0) / 65194000
        D = 297.8501921 + 445267.1114034 * T - 0.0018819 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 545868 - Math.pow(T, 4.0) / 113065000
        m = 357.5291092 + 35999.0502909 * T - 0.0001536 * Math.pow(T, 2.0) + Math.pow (T, 3.0) / 24490000
        M = 134.9633964 + 477198.8675055 * T + 0.0087414 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 69699 - Math.pow(T, 4.0) / 14712000
        f = 93.272095 + 483202.0175233 * T - 0.0036539 * Math.pow(T, 2.0) - Math.pow(T, 3.0) / 3526000 + Math.pow(T, 4.0) / 863310000
        A1 = 119.75 + 131.849 * T
        A2 = 53.09 + 479264.29 * T
        A3 = 313.45 + 481266.484 * T
        E = 1 - 0.002516 * T - 0.0000074 *T*T

        L = Rad(Mod(L, 360.0))
        D = Rad(Mod(D, 360.0))
        m = Rad(Mod(m, 360.0))
        M = Rad(Mod(M, 360.0))
        f = Rad(Mod(f, 360.0))
        A1 = Rad(Mod(A1, 360.0))
        A2 = Rad(Mod(A2, 360.0))
        A3 = Rad(Mod(A3, 360.0))

        bM =    (5128122 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 0 * M + 1 * f) +
                280602 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 1 * M + 1 * f) +
                277693 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 1 * M + -1 * f) +
                173237 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 0 * M + -1 * f) +
                55413 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -1 * M + 1 * f) +
                46271 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -1 * M + -1 * f) +
                32573 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 0 * M + 1 * f) +
                17198 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 2 * M + 1 * f) +
                9266 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 1 * M + -1 * f) +
                8822 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 2 * M + -1 * f) +
                8216 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 0 * M + -1 * f) +
                4324 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -2 * M + -1 * f) +
                4200 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 1 * M + 1 * f) +
                -3359 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + 0 * M + -1 * f) +
                2463 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + -1 * M + 1 * f) +
                2211 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 0 * M + 1 * f) +
                2065 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + -1 * M + -1 * f) +
                -1870 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + -1 * M + -1 * f) +
                1828 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + -1 * M + -1 * f) +
                -1794 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 0 * M + 1 * f) +
                -1749 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 0 * M + 3 * f) +
                -1565 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + -1 * M + 1 * f) +
                -1491 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + 0 * M + 1 * f) +
                -1475 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 1 * M + 1 * f) +
                -1410 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 1 * M + -1 * f) +
                -1344 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 0 * M + -1 * f) +
                -1335 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + 0 * M + -1 * f) +
                1107 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 3 * M + 1 * f) +
                1021 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + 0 * M + -1 * f) +
                833 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + -1 * M + 1 * f) +
                777 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 1 * M + -3 * f) +
                671 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + -2 * M + 1 * f) +
                607 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 0 * M + -3 * f) +
                596 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 2 * M + -1 * f) +
                491 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 1 * M + -1 * f) +
                -451 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -2 * M + 1 * f) +
                439 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 3 * M + -1 * f) +
                422 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + 2 * M + 1 * f) +
                421 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(2 * D + 0 * m + -3 * M + -1 * f) +
                -366 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + -1 * M + 1 * f) +
                -351 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + 0 * M + 1 * f) +
                331 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + 0 * M + 1 * f) +
                315 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + 1 * M + 1 * f) +
                302 * Math.pow(E,  (Math.abs(-2.0))) * Math.sin(2 * D + -2 * m + 0 * M + -1 * f) +
                -283 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(0 * D + 0 * m + 1 * M + 3 * f) +
                -229 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + 1 * M + -1 * f) +
                223 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(1 * D + 1 * m + 0 * M + -1 * f) +
                223 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(1 * D + 1 * m + 0 * M + 1 * f) +
                -220 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + -2 * M + -1 * f) +
                -220 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(2 * D + 1 * m + -1 * M + -1 * f) +
                -185 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + 1 * M + 1 * f) +
                181 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(2 * D + -1 * m + -2 * M + -1 * f) +
                -177 * Math.pow(E,  (Math.abs(1.0))) * Math.sin(0 * D + 1 * m + 2 * M + 1 * f) +
                176 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + -2 * M + -1 * f) +
                166 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(4 * D + -1 * m + -1 * M + -1 * f) +
                -164 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + 1 * M + -1 * f) +
                132 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(4 * D + 0 * m + 1 * M + -1 * f) +
                -119 * Math.pow(E,  (Math.abs(0.0))) * Math.sin(1 * D + 0 * m + -1 * M + -1 * f) +
                115 * Math.pow(E,  (Math.abs(-1.0))) * Math.sin(4 * D + -1 * m + 0 * M + -1 * f) +
                107 * Math.pow(E,  (Math.abs(-2.0))) * Math.sin(2 * D + -2 * m + 0 * M + 1 * f) +
                (-2235 * Math.sin(L)
                        + 382 * Math.sin(A3)
                        + 175 * Math.sin(A1 - f)
                        + 175 * Math.sin(A1 + f)
                        + 127 * Math.sin(L - M)
                        - 115 * Math.sin(L + M)))/1000000
        return bM
    }

    fun MoonGeocentricDistance (JD: Double): Double{
        var T       : Double
        var L       : Double
        var D       : Double
        var m       : Double
        var M       : Double
        var f       : Double
        var A1      : Double
        var A2      : Double
        var A3      : Double
        var E       : Double
        var rM      : Double
        var KM      : Double
        var AU      : Double
        var ER      : Double

        T = (JD - 2451545) / 36525
        L = 218.3164477 + 481267.88123421 * T - 0.0015786 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 538841 - Math.pow(T, 4.0) / 65194000
        D = 297.8501921 + 445267.1114034 * T - 0.0018819 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 545868 - Math.pow(T, 4.0) / 113065000
        m = 357.5291092 + 35999.0502909 * T - 0.0001536 * Math.pow(T, 2.0) + Math.pow (T, 3.0) / 24490000
        M = 134.9633964 + 477198.8675055 * T + 0.0087414 * Math.pow(T, 2.0) + Math.pow(T, 3.0) / 69699 - Math.pow(T, 4.0) / 14712000
        f = 93.272095 + 483202.0175233 * T - 0.0036539 * Math.pow(T, 2.0) - Math.pow(T, 3.0) / 3526000 + Math.pow(T, 4.0) / 863310000
        A1 = 119.75 + 131.849 * T
        A2 = 53.09 + 479264.29 * T
        A3 = 313.45 + 481266.484 * T
        E = 1 - 0.002516 * T - 0.0000074 *T*T

        L = Rad(Mod(L, 360.0))
        D = Rad(Mod(D, 360.0))
        m = Rad(Mod(m, 360.0))
        M = Rad(Mod(M, 360.0))
        f = Rad(Mod(f, 360.0))
        A1 = Rad(Mod(A1, 360.0))
        A2 = Rad(Mod(A2, 360.0))
        A3 = Rad(Mod(A3, 360.0))

        rM  =   (-20905355 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 1 * M + 0 * f) +
                -3699111 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + -1 * M + 0 * f) +
                -2955968 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + 0 * M + 0 * f) +
                -569925 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 2 * M + 0 * f) +
                48888 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(0 * D + 1 * m + 0 * M + 0 * f) +
                -3149 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 0 * M + 2 * f) +
                246158 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + -2 * M + 0 * f) +
                -152138 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(2 * D + -1 * m + -1 * M + 0 * f) +
                -170733 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + 1 * M + 0 * f) +
                -204586 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(2 * D + -1 * m + 0 * M + 0 * f) +
                -129620 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(0 * D + 1 * m + -1 * M + 0 * f) +
                108743 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(1 * D + 0 * m + 0 * M + 0 * f) +
                104755 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(0 * D + 1 * m + 1 * M + 0 * f) +
                10321 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + 0 * M + -2 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 1 * M + 2 * f) +
                79661 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 1 * M + -2 * f) +
                -34782 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(4 * D + 0 * m + -1 * M + 0 * f) +
                -23210 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 3 * M + 0 * f) +
                -21636 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(4 * D + 0 * m + -2 * M + 0 * f) +
                24208 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(2 * D + 1 * m + -1 * M + 0 * f) +
                30824 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(2 * D + 1 * m + 0 * M + 0 * f) +
                -8379 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(1 * D + 0 * m + -1 * M + 0 * f) +
                -16675 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(1 * D + 1 * m + 0 * M + 0 * f) +
                -12831 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(2 * D + -1 * m + 1 * M + 0 * f) +
                -10445 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + 2 * M + 0 * f) +
                -11650 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(4 * D + 0 * m + 0 * M + 0 * f) +
                14403 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + -3 * M + 0 * f) +
                -7003 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(0 * D + 1 * m + -2 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + -1 * M + 2 * f) +
                10056 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(2 * D + -1 * m + -2 * M + 0 * f) +
                6322 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(1 * D + 0 * m + 1 * M + 0 * f) +
                -9884 * Math.pow(E,  (Math.abs(-2.0))) * Math.cos(2 * D + -2 * m + 0 * M + 0 * f) +
                5751 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(0 * D + 1 * m + 2 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(2.0))) * Math.cos(0 * D + 2 * m + 0 * M + 0 * f) +
                -4950 * Math.pow(E,  (Math.abs(-2.0))) * Math.cos(2 * D + -2 * m + -1 * M + 0 * f) +
                4130 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + 1 * M + -2 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + 0 * M + 2 * f) +
                -3958 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(4 * D + -1 * m + -1 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 2 * M + 2 * f) +
                3258 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(3 * D + 0 * m + -1 * M + 0 * f) +
                2616 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(2 * D + 1 * m + 1 * M + 0 * f) +
                -1897 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(4 * D + -1 * m + -2 * M + 0 * f) +
                -2117 * Math.pow(E,  (Math.abs(2.0))) * Math.cos(0 * D + 2 * m + -1 * M + 0 * f) +
                2354 * Math.pow(E,  (Math.abs(2.0))) * Math.cos(2 * D + 2 * m + -1 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(2 * D + 1 * m + -2 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(2 * D + -1 * m + 0 * M + -2 * f) +
                -1423 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(4 * D + 0 * m + 1 * M + 0 * f) +
                -1117 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 4 * M + 0 * f) +
                -1571 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(4 * D + -1 * m + 0 * M + 0 * f) +
                -1739 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(1 * D + 0 * m + -2 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(2 * D + 1 * m + 0 * M + -2 * f) +
                -4421 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(0 * D + 0 * m + 2 * M + -2 * f) +
                0 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(1 * D + 1 * m + 1 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(3 * D + 0 * m + -2 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(4 * D + 0 * m + -3 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(-1.0))) * Math.cos(2 * D + -1 * m + 2 * M + 0 * f) +
                1165 * Math.pow(E,  (Math.abs(2.0))) * Math.cos(0 * D + 2 * m + 1 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(1.0))) * Math.cos(1 * D + 1 * m + -1 * M + 0 * f) +
                0 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + 3 * M + 0 * f) +
                8752 * Math.pow(E,  (Math.abs(0.0))) * Math.cos(2 * D + 0 * m + -1 * M + -2 * f))
        KM = rM/1000 + 385000.56
        AU = rM/149597870.7
        ER = rM/6371
        return KM
    }
    fun MoonApparentRightAscension (JD: Double): Double {
        var Lambda  : Double
        var Beta    : Double
        var Epsilon : Double
        var Alpha   : Double

        Lambda = MoonGeocentricLongitude(JD)
        Beta = MoonGeocentricLatitude(JD)
        Epsilon = ObliquityOfEcliptic(JD)
        Alpha = Deg(Math.atan2(Math.sin(Rad(Lambda)) * Math.cos(Rad(Epsilon)) - Math.tan(Rad(Beta)) * Math.sin(Rad(Epsilon)), Math.cos(Rad(Lambda))))
        Alpha = Mod(Alpha, 360.0)
        return Alpha
    }
    fun MoonApparentDeclination (JD: Double): Double {
        var Lambda  : Double
        var Beta    : Double
        var Epsilon : Double
        var Delta   : Double

        Lambda = MoonGeocentricLongitude(JD)
        Beta = MoonGeocentricLatitude(JD)
        Epsilon = ObliquityOfEcliptic(JD)
        Delta = Deg(Math.asin(Math.sin(Rad(Beta)) * Math.cos(Rad(Epsilon)) + Math.cos(Rad(Beta)) * Math.sin(Rad(Epsilon)) * Math.sin(Rad(Lambda))))
        return Delta
    }
    fun MoonEquatorialHorizontalParallax (JD: Double): Double {
        var R   : Double
        var Pi  : Double

        R = MoonGeocentricDistance(JD)
        Pi = Deg(Math.asin(6378.14 / R))
        return Pi
    }
    fun MoonAngularSemiDiameter (JD: Double): Double{
        var R   : Double
        var Pi  : Double
        var k   : Double
        var s   : Double

        R = MoonGeocentricDistance(JD)
        Pi = MoonEquatorialHorizontalParallax(JD)
        k = 0.271481
        s = Deg(Math.asin(k * Math.sin(Rad(Pi))))
        s = (358473400 / R) / 3600
        return s
    }
    fun MoonSunGeocentricElongation (JD: Double): Double {
        var DeltaSun    : Double
        var AlphaSun    : Double
        var DeltaMoon   : Double
        var AlphaMoon   : Double
        var Psi         : Double

        DeltaSun = SunApparentDeclination(JD)
        AlphaSun = SunApparentRightAscension(JD)
        DeltaMoon = MoonApparentDeclination(JD)
        AlphaMoon = MoonApparentRightAscension(JD)

        Psi = Deg(Math.acos(Math.sin(Rad(DeltaSun)) * Math.sin(Rad(DeltaMoon)) + Math.cos(Rad(DeltaSun)) * Math.cos(Rad(DeltaMoon)) * Math.cos(Rad(AlphaSun-AlphaMoon))))
        return Psi
    }
    fun MoonDiskIlluminatedFraction (JD: Double): Double {
        var RSun    : Double
        var RMoon   : Double
        var Psi     : Double
        var I       : Double
        var k       : Double

        RSun = SunGeocentricDistance(JD)*149598000
        RMoon = MoonGeocentricDistance(JD)
        Psi = MoonSunGeocentricElongation(JD)
        I = Deg(Math.atan2(RSun * Math.sin(Rad(Psi)), RMoon - RSun * Math.cos(Rad(Psi))))
        k = (1 + Math.cos(Rad(I))) / 2
        return k
    }

    fun MoonBrightLimbAngle (JD: Double): Double{
        var DeltaSun    : Double
        var AlphaSun    : Double
        var DeltaMoon   : Double
        var AlphaMoon   : Double
        var y_Num       : Double
        var x_Num       : Double
        var Chi         : Double

        DeltaSun = SunApparentDeclination(JD)
        AlphaSun = SunApparentRightAscension(JD)
        DeltaMoon = MoonApparentDeclination(JD)
        AlphaMoon = MoonApparentRightAscension(JD)
        y_Num = Math.cos(Rad(DeltaSun)) * Math.sin(Rad(AlphaSun - AlphaMoon))
        x_Num = Math.sin(Rad(DeltaSun)) * Math.cos(Rad(DeltaMoon)) - Math.cos(Rad(DeltaSun)) * Math.sin(Rad(DeltaMoon)) * Math.cos(Rad(AlphaSun - AlphaMoon))
        Chi = Deg(Math.atan2(y_Num, x_Num))
        Chi = Mod(Chi, 360.0)
        return Chi
    }
//fun GeograficLatitudeToGeocentricLatitude (gLat, Hight){
//    var a : Double
//    var b : Double
//    var u : Double
//    var GeocentricLatitude: Double
//
//    a = 6378.14
//    b = 6356.755
//    u = (b/a) * Math.tan(Rad(gLat))
//    GeocentricLatitude = (b/a)
//
//
//}

    fun MoonPhasesModified (HijriMonth:Byte, HijriYear: Long, MoonPhaseKind: Int): Double {
        var k: Double
        var kII: Double
        var T: Double
        var JDEMMP: Double
        var E: Double
        var M: Double
        var M1: Double
        var F: Double
        var Omg: Double
        var A1: Double
        var A2: Double
        var A3: Double
        var A4: Double
        var A5: Double
        var A6: Double
        var A7: Double
        var A8: Double
        var A9: Double
        var A10: Double
        var A11: Double
        var A12: Double
        var A13: Double
        var A14: Double
        var w: Double
        var JDECorr1: Double
        var JDECorr2: Double
        var Result: Double

        k = HijriMonth.toDouble() + 12 * HijriYear.toDouble() - 17050
        when (MoonPhaseKind) {
            1 -> kII = 0.0
            2 -> kII = 0.25
            3 -> kII = 0.5
            4 -> kII = 0.75
            else -> kII = 0.0
        }
        k = Math.floor(k) + kII

        T = k / 1236.85

        JDEMMP = 2451550.09766  + 29.530588861 * k +
                0.00015437 * Math.pow (T, 2.0) -
                0.00000015 * Math.pow (T, 3.0) +
                0.00000000073 * Math.pow (T, 4.0)

        E = 1 - 0.002516 * T - 0.0000074 * Math.pow(T, 2.0)

        M = 2.5534  + 29.1053567 * k -
                0.0000014 * Math.pow (T, 2.0)-
                0.00000011 * Math.pow (T, 3.0)
        M = Rad(Mod(M, 360.0))

        M1  = 201.5643  + 385.81693528 * k +
                0.0107582 * Math.pow (T, 2.0) +
                0.00001238 * Math.pow (T, 3.0) -
                0.000000058 * Math.pow (T, 4.0)
        M1 = Rad(Mod(M1, 360.0))

        F = 160.7108    + 390.67050284 * k -
                0.0016118 * Math.pow (T, 2.0)-
                0.00000227 * Math.pow (T, 3.0)+
                0.000000011 * Math.pow (T, 4.0)
        F = Rad(Mod(F, 360.0))

        Omg = 124.7746  - 1.56375588 * k +
                0.0020672 * Math.pow (T, 2.0)+
                0.00000215 * Math.pow (T, 3.0)
        Omg = Rad(Mod(Omg, 360.0))

        A1 = 299.77 + 0.107408 * k - 0.009173 * Math.pow (T,2.0)
        A2 = 251.88 + 0.016321 * k
        A3 = 251.83 + 26.651886 * k
        A4 = 349.42 + 36.412478 * k
        A5 = 84.66 + 18.206239 * k
        A6 = 141.74 + 53.303771 * k
        A7 = 207.14 + 2.453732 * k
        A8 = 154.84 + 7.30686 * k
        A9 = 34.52 + 27.261239 * k
        A10 = 207.19 + 0.121824 * k
        A11 = 291.34 + 1.844379 * k
        A12 = 161.72 + 24.198154 * k
        A13 = 239.56 + 25.513099 * k
        A14 = 331.55 + 3.592518 * k

        A1 = Rad(Mod(A1, 360.0))
        A2 = Rad(Mod(A2, 360.0))
        A3 = Rad(Mod(A3, 360.0))
        A4 = Rad(Mod(A4, 360.0))
        A5 = Rad(Mod(A5, 360.0))
        A6 = Rad(Mod(A6, 360.0))
        A7 = Rad(Mod(A7, 360.0))
        A8 = Rad(Mod(A8, 360.0))
        A9 = Rad(Mod(A9, 360.0))
        A10 = Rad(Mod(A10, 360.0))
        A11 = Rad(Mod(A11, 360.0))
        A12 = Rad(Mod(A12, 360.0))
        A13 = Rad(Mod(A13, 360.0))
        A14 = Rad(Mod(A14, 360.0))

        when (MoonPhaseKind) {
            1 -> JDECorr1 = -0.4072 * Math.sin(M1)+
                    0.17241 * E * Math.sin(M)+
                    0.01608 * Math.sin(2 * M1)+
                    0.01039 * Math.sin(2 * F)+
                    0.00739 * E * Math.sin(M1 - M)-
                    0.00514 * E * Math.sin(M1 + M)+
                    0.00208 * Math.pow(E,2.0) * Math.sin(2 * M)-
                    0.00111 * Math.sin(M1 - 2 * F)-
                    0.00057 * Math.sin(M1 + 2 * F)+
                    0.00056 * E * Math.sin(2 * M1 + M)-
                    0.00042 * Math.sin(3 * M1)+
                    0.00042 * E * Math.sin(M + 2 * F)+
                    0.00038 * E * Math.sin(M - 2 * F)-
                    0.00024 * E * Math.sin(2 * M1 - M)-
                    0.00017 * Math.sin(Omg)-
                    0.00007 * Math.sin(M1 + 2 * M)+
                    0.00004 * Math.sin(2 * M1 - 2 * F)+
                    0.00004 * Math.sin(3 * M)+
                    0.00003 * Math.sin(M1 + M - 2 * F)+
                    0.00003 * Math.sin(2 * M1 + 2 * F)-
                    0.00003 * Math.sin(M1 + M + 2 * F)+
                    0.00003 * Math.sin(M1 - M + 2 * F)-
                    0.00002 * Math.sin(M1 - M - 2 * F)-
                    0.00002 * Math.sin(3 * M1 + M)+
                    0.00002 * Math.sin(4 * M1)

            3 -> JDECorr1 = -0.40614 * Math.sin(M1)+
                    0.17302 * E * Math.sin(M)+
                    0.01614 * Math.sin(2 * M1)+
                    0.01043 * Math.sin(2 * F)+
                    0.00734 * E * Math.sin(M1 - M)-
                    0.00514 * E * Math.sin(M1 + M)+
                    0.00209 * Math.pow(E, 2.0) * Math.sin(2 * M)-
                    0.00111 * Math.sin(M1 - 2 * F)-
                    0.00057 * Math.sin(M1 + 2 * F)+
                    0.00056 * E * Math.sin(2 * M1 + M)-
                    0.00042 * Math.sin(3 * M1)+
                    0.00042 * E * Math.sin(M + 2 * F)+
                    0.00038 * E * Math.sin(M - 2 * F)-
                    0.00024 * E * Math.sin(2 * M1 - M)-
                    0.00017 * Math.sin(Omg)-
                    0.00007 * Math.sin(M1 + 2 * M)+
                    0.00004 * Math.sin(2 * M1 - 2 * F)+
                    0.00004 * Math.sin(3 * M)+
                    0.00003 * Math.sin(M1 + M - 2 * F)+
                    0.00003 * Math.sin(2 * M1 + 2 * F)-
                    0.00003 * Math.sin(M1 + M + 2 * F)+
                    0.00003 * Math.sin(M1 - M + 2 * F)-
                    0.00002 * Math.sin(M1 - M - 2 * F)-
                    0.00002 * Math.sin(3 * M1 + M)+
                    0.00002 * Math.sin(4 * M1)

            2,4 -> JDECorr1 = -0.62801 * Math.sin(M1)+
                    0.17172 * E * Math.sin(M)-
                    0.01183 * E * Math.sin(M1 + M)+
                    0.00862 * Math.sin(2 * M1)+
                    0.00804 * Math.sin(2 * F)+
                    0.00454 * E * Math.sin(M1 - M)+
                    0.00204 * Math.pow(E, 2.0) * Math.sin(2 * M)-
                    0.0018 * Math.sin(M1 - 2 * F)-
                    0.0007 * Math.sin(M1 + 2 * F)-
                    0.0004 * Math.sin(3 * M1)-
                    0.00034 * E * Math.sin(2 * M1 - M)+
                    0.00032 * E * Math.sin(M + 2 * F)+
                    0.00032 * E * Math.sin(M - 2 * F)-
                    0.00028 * Math.pow(E, 2.0) * Math.sin(M1 + 2 * M)+
                    0.00027 * E * Math.sin(2 * M1 + M)-
                    0.00017 * Math.sin(Omg)-
                    0.00005 * Math.sin(M1 - M - 2 * F)+
                    0.00004 * Math.sin(2.0 * M1 + 2 * F)-
                    0.00004 * Math.sin(M1 + M + 2 * F)+
                    0.00004 * Math.sin(M1 - 2 * M)+
                    0.00003 * Math.sin(M1 + M - 2 * F)+
                    0.00003 * Math.sin(3 * M)+
                    0.00002 * Math.sin(2 * M1 - 2 * F)+
                    0.00002 * Math.sin(M1 - M + 2 * F)-
                    0.00002 * Math.sin(3 * M1 + M)

            else -> JDECorr1 = 0.0
        }

        if ((MoonPhaseKind == 2) || (MoonPhaseKind == 4)) {
            w = 0.00306 - 0.00038 * E * Math.cos(M) + 0.00026 * Math.cos(M1)- 0.00002 * Math.cos(M1 - M) +
                    0.00002 * Math.cos(M1 + M) + 0.00002 * Math.cos(2 * F)
            when (MoonPhaseKind) {
                2    -> JDECorr1 = JDECorr1 + w
                4    -> JDECorr1 = JDECorr1 - w
                else -> JDECorr1 = JDECorr1
            }
        } else {
            JDECorr1 = JDECorr1
        }

        JDECorr2 =  0.000325 * Math.sin(A1) +
                0.000165 * Math.sin(A2) +
                0.000164 * Math.sin(A3) +
                0.000126 * Math.sin(A4) +
                0.00011 * Math.sin(A5) +
                0.000062 * Math.sin(A6) +
                0.00006 * Math.sin(A7) +
                0.000056 * Math.sin(A8) +
                0.000047 * Math.sin(A9) +
                0.000042 * Math.sin(A10) +
                0.00004 * Math.sin(A11) +
                0.000037 * Math.sin(A12) +
                0.000035 * Math.sin(A13) +
                0.000023 * Math.sin(A14)

        Result = JDEMMP + JDECorr1 + JDECorr2
        return Result
    }
    fun Zuhur (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Tzn: Double): Double{
        var Tgl     : Byte
        var Bln     : Byte
        var Thn     : Long
        var JD      : Double
        var JDE     : Double
        var KWD     : Double
        var Eqt     : Double
        var Zhr     : Double

        Tgl = Tanggal
        Bln = Bulan
        Thn = Tahun
        Zhr = 12.0

        for (i in 1..3) {
            JD = KMJD(Tgl, Bln, Thn, Zhr, Tzn)
            JDE = JD + (DeltaT(JD)/86400)
            Eqt = EquationOfTime(JDE)
            KWD = (Longitude-(Tzn*15))/15
            Zhr = 12 - Eqt - KWD
        }
        return Zhr
    }
    fun Ashar (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Tzn: Double): Double {
        var Tgl     : Byte
        var Bln     : Byte
        var Thn     : Long
        var JD      : Double
        var JDE     : Double
        var KWD     : Double
        var Eqt     : Double
        var Dec     : Double
        var ZM      : Double
        var hm      : Double
        var tm      : Double
        var Asr     : Double

        Tgl     = Tanggal
        Bln     = Bulan
        Thn     = Tahun
        Asr     = 15.0

        for (i in 1..3) {

            JD = KMJD(Tgl, Bln, Thn, Asr, Tzn)
            JDE = JD + (DeltaT(JD)/86400)
            Eqt = EquationOfTime(JDE)
            Dec = SunApparentDeclination(JDE)
            KWD = (Longitude-(Tzn*15))/15

            ZM = Math.abs(Latitude - Dec)
            hm = Deg(Math.atan(1/(Math.tan(Rad(ZM))+1)))
            tm = Deg(Math.acos(-Math.tan(Rad(Latitude))*Math.tan(Rad(Dec))+Math.sin(Rad(hm))/Math.cos(Rad(Latitude))/Math.cos(Rad(Dec))))
            Asr = (12 - Eqt) + tm/15 - KWD
        }
        return Asr
    }
    fun Magrib (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Elevasi: Double, Tzn: Double): Double {
        var Tgl     : Byte
        var Bln     : Byte
        var Thn     : Long
        var JD      : Double
        var JDE     : Double
        var KWD     : Double
        var Eqt     : Double
        var Dec     : Double
        var Sd      : Double
        var Dip     : Double
        var hm      : Double
        var tm      : Double
        var Mgrb    : Double

        Tgl     = Tanggal
        Bln     = Bulan
        Thn     = Tahun
        Mgrb    = 18.0

        for (i in 1..3) {
            JD  = KMJD(Tgl, Bln, Thn, Mgrb, Tzn)
            JDE = JD + (DeltaT(JD)/86400)
            Eqt = EquationOfTime(JDE)
            Dec = SunApparentDeclination(JDE)
            Sd  = SunAngularSemiDiameter(JDE)
            KWD = (Longitude-(Tzn*15))/15

            Dip = 1.76/60 *Math.sqrt(Elevasi)
            hm = -(Sd + 34.5/60 + Dip)
            tm = Deg(Math.acos(-Math.tan(Rad(Latitude))*Math.tan(Rad(Dec))+Math.sin(Rad(hm))/Math.cos(Rad(Latitude))/Math.cos(Rad(Dec))))
            Mgrb = 12 - Eqt + tm/15 - KWD
        }
        return Mgrb
    }
    fun Isya (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Tzn: Double): Double {
        var Tgl     : Byte
        var Bln     : Byte
        var Thn     : Long
        var JD      : Double
        var JDE     : Double
        var KWD     : Double
        var Eqt     : Double
        var Dec     : Double
        var hm      : Double
        var tm      : Double
        var Isy     : Double

        Tgl     = Tanggal
        Bln     = Bulan
        Thn     = Tahun
        Isy     = 19.0

        for (i in 1..3) {
            JD  = KMJD(Tgl, Bln, Thn, Isy, Tzn)
            JDE = JD + (DeltaT(JD)/86400)
            Eqt = EquationOfTime(JDE)
            Dec = SunApparentDeclination(JDE)
            KWD = (Longitude-(Tzn*15))/15

            hm = -18.0
            tm = Deg(Math.acos(-Math.tan(Rad(Latitude))*Math.tan(Rad(Dec))+Math.sin(Rad(hm))/Math.cos(Rad(Latitude))/Math.cos(Rad(Dec))))
            Isy = 12 - Eqt + tm/15 - KWD
        }
        return Isy
    }
    fun Subuh (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Tzn: Double): Double {
        var Tgl     : Byte
        var Bln     : Byte
        var Thn     : Long
        var JD      : Double
        var JDE     : Double
        var KWD     : Double
        var Eqt     : Double
        var Dec     : Double
        var hm      : Double
        var tm      : Double
        var Sbh     : Double

        Tgl     = Tanggal
        Bln     = Bulan
        Thn     = Tahun
        Sbh     = 4.0

        for (i in 1..3) {
            JD  = KMJD(Tgl, Bln, Thn, Sbh, Tzn)
            JDE = JD + (DeltaT(JD)/86400)
            Eqt = EquationOfTime(JDE)
            Dec = SunApparentDeclination(JDE)
            KWD = (Longitude-(Tzn*15))/15

            hm = -20.0
            tm = Deg(Math.acos(-Math.tan(Rad(Latitude))*Math.tan(Rad(Dec))+Math.sin(Rad(hm))/Math.cos(Rad(Latitude))/Math.cos(Rad(Dec))))
            Sbh = 12 - Eqt - tm/15 - KWD
        }
        return Sbh
    }
    fun Syuruk (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Elevasi: Double, Tzn: Double): Double {
        var Tgl     : Byte
        var Bln     : Byte
        var Thn     : Long
        var JD      : Double
        var JDE     : Double
        var KWD     : Double
        var Eqt     : Double
        var Dec     : Double
        var Sd      : Double
        var Dip     : Double
        var hm      : Double
        var tm      : Double
        var Syrk    : Double

        Tgl     = Tanggal
        Bln     = Bulan
        Thn     = Tahun
        Syrk    = 6.0

        for (i in 1..3) {
            JD  = KMJD(Tgl, Bln, Thn, Syrk, Tzn)
            JDE = JD + (DeltaT(JD)/86400)
            Eqt = EquationOfTime(JDE)
            Dec = SunApparentDeclination(JDE)
            Sd  = SunAngularSemiDiameter(JDE)
            KWD = (Longitude-(Tzn*15))/15

            Dip = 1.76/60 *Math.sqrt(Elevasi)
            hm = -(Sd + 34.5/60 + Dip)
            tm = Deg(Math.acos(-Math.tan(Rad(Latitude))*Math.tan(Rad(Dec))+Math.sin(Rad(hm))/Math.cos(Rad(Latitude))/Math.cos(Rad(Dec))))
            Syrk = 12 - Eqt - tm/15 - KWD
        }
        return Syrk
    }
    fun Duha (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Elevasi: Double, Tzn: Double): Double {
        var W_Syuruk: Double
        var Dh : Double

        W_Syuruk = Syuruk(Tanggal, Bulan, Tahun, Longitude, Latitude, Elevasi, Tzn)
        Dh       = W_Syuruk + 0.0 +15.0/60.0
        return Dh
    }
    fun NisfuLail (Tanggal: Byte, Bulan: Byte, Tahun: Long, Longitude: Double, Latitude: Double, Elevasi: Double, Tzn: Double): Double {
        var W_Magrib    : Double
        var W_Subuh     : Double
        var Interval    : Double
        var Nisfu       : Double

        W_Magrib = Magrib(Tanggal, Bulan, Tahun, Longitude, Latitude, Elevasi, Tzn)
        W_Subuh = Subuh(Tanggal, Bulan, Tahun, Longitude, Latitude, Tzn)
        Interval = (Mod(W_Subuh - W_Magrib, 24.0))/2.0
        Nisfu = W_Magrib + Interval
        return Nisfu
    }

    fun ArahQiblatSpherical (Longitude: Double, Latitude: Double): Double {
        var A               : Double
        var B               : Double
        var C               : Double
        var sB              : Double
        var cB              : Double
        var Bb              : Double
        var LatitudeKabah   : Double
        var LongitudeKabah  : Double
        var AzQ             : Double

        LatitudeKabah   = 21.0 + 25.0 / 60.0 + 21.03 / 3600.0
        LongitudeKabah  = 39.0 + 49.0 / 60.0 + 34.31 / 3600.0

        A = (90 - Latitude)
        B = (90 - LatitudeKabah)
        C = (Longitude - LongitudeKabah)

        sB = Math.sin(Rad(B)) * Math.sin(Rad(C))
        cB = Math.cos(Rad(B)) * Math.sin(Rad(A)) - Math.cos(Rad(A)) * Math.sin(Rad(B)) * Math.cos(Rad(C))
        Bb = Deg(Math.atan2(sB, cB))
        AzQ = Mod((360.0 - Bb),360.0)
        return AzQ
    }
    fun ArahQiblaWithEllipsoidCorrection (Longitude: Double, Latitude: Double): Double {
        var A : Double
        var B : Double
        var C : Double
        var e : Double
        var LatitudeTempatTerkoreksi : Double
        var LatitudeKabahTerkoreksi : Double
        var sB : Double
        var cB : Double
        var Bb : Double
        var LatitudeKabah : Double
        var LongitudeKabah : Double
        var AzQ: Double

        LatitudeKabah   = 21.0 + 25.0 / 60.0 + 21.03 / 3600.0
        LongitudeKabah  = 39.0 + 49.0 / 60.0 + 34.31 / 3600.0

        e = 0.0066943800229
        LatitudeKabahTerkoreksi = Deg(Math.atan((1 - e) * Math.tan(Rad(LatitudeKabah))))
        LatitudeTempatTerkoreksi = Deg(Math.atan((1 - e) * Math.tan(Rad(Latitude))))

        A = (90.0 - LatitudeTempatTerkoreksi)
        B = (90.0 - LatitudeKabahTerkoreksi)
        C = (Longitude - LongitudeKabah)

        sB = Math.sin(Rad(B)) * Math.sin(Rad(C))
        cB = Math.cos(Rad(B)) * Math.sin(Rad(A)) - Math.cos(Rad(A)) * Math.sin(Rad(B)) * Math.cos(Rad(C))
        Bb = Deg(Math.atan2(sB, cB))
        AzQ = Mod((360.0 - Bb),360.0)
        return AzQ
    }
    fun ArahQiblaVincenty(Longitude: Double, Latitude: Double,OptResult: String): Double {
        var S: Double
        var A: Double
        var B: Double
        var C: Double
        var e: Double
        var F: Double
        var c2SigmaM: Double
        var dSigma: Double
        var sigma: Double
        var sAlpha: Double
        var cAlpha: Double
        var c2Alpha: Double
        var c2sigma: Double
        var Bb: Double
        var ae: Double
        var be: Double
        var U1: Double
        var U2: Double
        var L0: Double
        var Lambda0: Double
        var Lambda: Double
        var sSigma: Double
        var cSigma: Double
        var iterlimit: Double
        var LatitudeKabah: Double
        var LongitudeKabah: Double
        var up2: Double
        var alpha1: Double
        var alpha2: Double


        LatitudeKabah   = 21 + 25 / 60.0 + 21.03 / 3600.0
        LongitudeKabah  = 39 + 49 / 60.0 + 34.31 / 3600.0

        F = 1.0 / 298.257223563
        ae = 6378137.000000000
        be = 6356752.314245180

        U1 = Math.atan((1 - F) * Math.tan(Rad(LatitudeKabah)))
        U2 = Math.atan((1 - F) * Math.tan(Rad(Latitude)))
        L0 = Rad(Longitude - LongitudeKabah)
        iterlimit   = 0.0
        Lambda = L0
        do {
            iterlimit++
            Lambda0 = Lambda
            cSigma = Math.sin(U1) * Math.sin(U2) + Math.cos(U1) * Math.cos(U2) * Math.cos(Lambda)
            sSigma = Math.sqrt(Math.pow((Math.cos(U2) * Math.sin(Lambda)),2.0) + Math.pow((Math.cos(U1) * Math.sin(U2) - Math.sin(U1) * Math.cos(U2) * Math.cos(Lambda)), 2.0))
            sigma = Math.atan2(sSigma, cSigma)
            sAlpha = Math.cos(U1) * Math.cos(U2) * Math.sin(Lambda) / sSigma
            c2Alpha = 1.0 - Math.pow(sAlpha, 2.0)
            c2SigmaM = cSigma - 2.0 * Math.sin(U1) * Math.sin(U2) / c2Alpha
            C = F / 16.0 * c2Alpha * (4.0 + F * (4.0 - 3.0 * c2Alpha))
            Lambda = L0 + (1.0 - C) * F * sAlpha * (sigma + C * sSigma * (c2SigmaM + C * cSigma * (-1.0 + 2.0 * Math.pow(c2SigmaM,2.0))))

        } while ( Math.abs(Lambda - Lambda0) > 0.000000000001 || iterlimit < 100.0)

        //Arah dari tempat ke Kiblat dan sebalinya

        alpha1 = Math.atan2(Math.cos(U2) * Math.sin(Lambda), Math.cos(U1) * Math.sin(U2) - Math.sin(U1) * Math.cos(U2) * Math.cos(Lambda))
        alpha2 = Math.atan2(Math.cos(U1) * Math.sin(Lambda),-Math.sin(U1) * Math.cos(U2) + Math.cos(U1) * Math.sin(U2) * Math.cos(Lambda))

        //jarak dari tempat ke Kiblat
        up2 = c2Alpha * (Math.pow(ae,2.0) - Math.pow(be,2.0)) / Math.pow (be,2.0)
        A = 1 + up2 / 16384 * (4096 + up2 * (-768 + up2 * (320 - 175 * up2)))
        B = up2 / 1024 * (256 + up2 * (-128 + up2 * (74 - 47 * up2)))

        dSigma = B * sSigma * (c2SigmaM + 0.25 * B *  (cSigma * (-1 + 2 * Math.pow(c2SigmaM,2.0)) - 1 / 6 * B * c2SigmaM *
                (-3 + 4 * Math.pow(sSigma, 2.0)) * (-3 + 4 * Math.pow(c2SigmaM, 2.0))))
        S = be * A * (sigma - dSigma)

        return when (OptResult) {
            "PtoQ"      -> Mod(180 + Deg(alpha2),360.0)
            "QtoP"      -> Mod(Deg(alpha1),360.0)
            "Dist"      -> S
            else        -> Mod(180 + Deg(alpha2),360.0)
        }
    }

    fun BayanganQiblatHarian(
        Longitude: Double, Latitude: Double, TglM: Byte, BlnM: Byte, ThnM: Long,
        TimeZone: Double, Optional: Int
    ): Double {
        var JD  : Double
        var JDE : Double
        var dm  : Double
        var e   : Double
        var AzQ : Double
        var B   : Double
        var P   : Double
        var Ca  : Double
        var BQ  : Double

        BQ = 12.0

        for ( i in 1..3) {
            JD = KMJD(TglM, BlnM, ThnM, BQ, TimeZone)
            JDE = JD + DeltaT(JD)/86400.0
            dm = SunApparentDeclination(JDE)
            e = EquationOfTime(JDE)
            AzQ = ArahQiblatSpherical(Longitude, Latitude)
            B = 90 - Latitude
            P = Deg(Math.atan(1 / (Math.cos(Rad(B)) * Math.tan(Rad(AzQ)))))
            Ca = Deg(Math.acos(Math.tan(Rad(dm)) * Math.tan(Rad(B)) * Math.cos(Rad(P))))
            BQ = when (Optional) {
                1 -> Mod(-(P - Ca) / 15 + (12 - e) + ((TimeZone * 15) - Longitude) / 15,24.0)
                2 -> Mod(-(P + Ca) / 15 + (12 - e) + ((TimeZone * 15) - Longitude) / 15,24.0)
                else -> 0.0
            }
        }

        return Mod(BQ,24.0)
    }
    fun JarakQiblatSpherical (Longitude: Double, Latitude: Double): Double {
        var gLonK : Double = (39.0 + 49.0 / 60.0 + 34.31 / 3600.0)
        var gLatK : Double = (21.0 + 25.0 / 60.0 + 21.03 / 3600.0)
        var d : Double
        var s : Double
        d = Deg(Math.acos(Math.sin(Rad(Latitude))*Math.sin(Rad(gLatK))+Math.cos(Rad(Latitude))*Math.cos(Rad(gLatK))*Math.cos(Rad(gLonK-Longitude))))
        s = 6378.137*d/57.2957795
        return s
    }
    fun JarakQiblatEllipsoid (Lt: Double, Bt: Double): Double {
        var U : Double
        var G : Double
        var J : Double
        var M : Double
        var N : Double
        var W : Double
        var p : Double
        var D : Double
        var E1 : Double
        var E2 : Double
        var f : Double
        var s : Double
        val PI : Double = 3.14159265358979

        U = ((21.0 + 25.0 / 60.0 + 21.03 / 3600.0) + Lt)/2.0
        G = ((21.0 + 25.0 / 60.0 + 21.03 / 3600.0) - Lt)/2.0
        J = ((39.0 + 49.0 / 60.0 + 34.31 / 3600.0) - Bt)/2.0
        M = Math.pow((Math.sin(Rad(G))),2.0) * Math.pow((Math.cos(Rad(J))),2.0)+ Math.pow((Math.cos(Rad(U))),2.0)*Math.pow((Math.sin(Rad(J))),2.0)
        N = Math.pow((Math.cos(Rad(G))),2.0) * Math.pow((Math.cos(Rad(J))),2.0)+ Math.pow((Math.sin(Rad(U))),2.0)*Math.pow((Math.sin(Rad(J))),2.0)
        W = Math.atan(Math.sqrt(M/N))
        p = (Math.sqrt(M*N))/W
        D = Deg(((2*W)*6378.137)/(180.0/PI))
        E1 = ((3*p) -1)/(2*N)
        E2 = ((3*p) +1)/(2*M)
        f  = 1/298.25722
        s  = D*(1+f*E1*Math.sin(Rad(U))*Math.sin(Rad(U))*Math.cos(Rad(G))*Math.cos(Rad(G)) - f*E2*Math.cos(Rad(U))*Math.cos(Rad(U))*Math.sin(Rad(G))*Math.sin(Rad(G)))
        return s
    }
    fun RashdulQiblat(ThnM: Long, TmZn: Double, Opt: Int) : String {
        val gLatK :	Double = (21.0 + 25.0 / 60.0 + 21.03 / 3600.0)
        val gLonK : Double = (39.0 + 49.0 / 60.0 + 34.31 / 3600.0)
        val TmZnK	: Double = 3.0

        var JD1		: Double
        var JD2		: Double
        var JD3		: Double
        var JDE1	: Double
        var JDE2	: Double
        var JDE3	: Double
        var EoT1	: Double
        var EoT2	: Double
        var EoT3	: Double
        var SDc1	: Double
        var SDc2	: Double
        var SDc3	: Double
        var dlt1	: Double
        var dlt2	: Double
        var dlt3	: Double
        var Trs1	: Double
        var Trs2	: Double
        var Trs3	: Double
        var JD      : Double
        var dm      : Double
        var h       : Double

        JD   = 0.0
        Trs1 = 12.00
        Trs2 = 12.00
        Trs3 = 12.00

        blnM@ for (n in 1..12) {
            tglM@ for (i in 1..31) {
                for (z in 1..3) {
                    JD1 = KMJD((i + 0).toByte(), n.toByte(), ThnM, Trs1, TmZnK)
                    JDE1 = JD1 + (DeltaT(JD1) / 86400.0)
                    EoT1 = EquationOfTime(JDE1)
                    Trs1 = 12 - EoT1 - (gLonK - (TmZnK * 15)) / 15
                    JD2 = KMJD((i + 1).toByte(), n.toByte(), ThnM, Trs2, TmZnK)
                    JDE2 = JD2 + (DeltaT(JD2) / 86400.0)
                    EoT2 = EquationOfTime(JDE2)
                    Trs2 = 12 - EoT2 - (gLonK - (TmZnK * 15)) / 15
                    JD3 = KMJD((i + 2).toByte(), n.toByte(), ThnM, Trs3, TmZnK)
                    JDE3 = JD3 + (DeltaT(JD3) / 86400.0)
                    EoT3 = EquationOfTime(JDE3)
                    Trs3 = 12 - EoT3 - (gLonK - (TmZnK * 15)) / 15

                }

                JD1 = KMJD((i + 0).toByte(), n.toByte(), ThnM, Trs1, TmZnK)
                JD2 = KMJD((i + 1).toByte(), n.toByte(), ThnM, Trs2, TmZnK)
                JD3 = KMJD((i + 2).toByte(), n.toByte(), ThnM, Trs3, TmZnK)

                SDc1 = SunApparentDeclination(JD1 + (DeltaT(JD1) / 86400.0))
                SDc2 = SunApparentDeclination(JD2 + (DeltaT(JD2) / 86400.0))
                SDc3 = SunApparentDeclination(JD3 + (DeltaT(JD3) / 86400.0))

                dlt1 = Math.abs(gLatK - SDc1)
                dlt2 = Math.abs(gLatK - SDc2)
                dlt3 = Math.abs(gLatK - SDc3)

                if ((dlt1 > dlt2) && (dlt2 < dlt3)) {
                    JD = JD2

                    when(Opt) {
                        1 -> break@blnM
                        2 -> continue@blnM
                    }
                }
            }
        }
        dm = SunApparentDeclination(JD)
        h = (90.0 - Math.abs(gLatK-dm))

        return JDKM(JD,TmZn) + " " + DHHM(JDKM(JD,TmZn,"JAM DES").toDouble(),"HH:MM",0) + ", Tinggi: " + RoundTo(h,2) +"°"
    }

    fun JDSunset(TglM: Byte, BlnM: Byte, ThnM: Long, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var JD       : Double
        var JDE      : Double
        var EoT      : Double
        var rfS      : Double
        var dltS     : Double
        var sdS      : Double
        var KWD      : Double
        var Dip      : Double
        var altS     : Double
        var coshaS   : Double
        var haS      : Double
        var JSunset  : Double

        JSunset = 17.0
        for (i in 1..3) {
            JD      = KMJD(TglM, BlnM, ThnM, JSunset, TmZn)
            JDE     = JD + DeltaT(JD) / 86400.0
            dltS    = SunApparentDeclination(JDE)
            sdS     = SunAngularSemiDiameter(JDE)
            EoT     = EquationOfTime(JDE)
            rfS         = 34.16/60.0
            Dip         = 2.1 * Math.sqrt(gAlt) / 60.0
            altS        = 0 - sdS - rfS - Dip + 0.0024
            coshaS  = (Math.sin(Rad(altS)) - Math.sin(Rad(gLat)) * Math.sin(Rad(dltS))) / (Math.cos(Rad(gLat)) * Math.cos(Rad(dltS)))
            if (Math.abs(coshaS) < 1) {
                haS     = Deg(Math.acos(coshaS))
                KWD     = gLon / 15.00 - TmZn
                JSunset = haS / 15.0 + 12.0 - EoT - KWD
            } else {
                JSunset = 0.0
            }
        }
        JD      = KMJD(TglM, BlnM, ThnM, JSunset, TmZn)
        return JD
    }

    fun JDGhurubSyams(JDNM: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
//   Deklarasi Variabel
        var CJDN    : Double
        var JDGS    : Double
        var JDEGS   : Double
        var dltS    : Double
        var sdS     : Double
        var EoT     : Double
        var rfS     : Double
        var Dip     : Double
        var altS    : Double
        var coshaS  : Double
        var haS     : Double
        var KWD     : Double
        var JSunSet : Double
//  Proses Perhitungan
        JSunSet = 17.0
        CJDN   = Math.floor(JDNM + 0.5 + (TmZn / 24.0))
        for (i in 1..3) {
            JDGS   = CJDN - 0.5 + (JSunSet - TmZn) / 24.0
            JDEGS  = JDGS + DeltaT(JDGS) / 86400.0
            dltS   = SunApparentDeclination(JDEGS)
            sdS    = SunAngularSemiDiameter(JDEGS)
            EoT    = EquationOfTime(JDEGS)
            rfS         = 34.16/60.0
            Dip         = 2.1 * Math.sqrt(gAlt) / 60.0
            altS        = 0 - sdS - rfS - Dip + 0.0024
            coshaS = (Math.sin(Rad(altS)) - Math.sin(Rad(gLat)) * Math.sin(Rad(dltS))) / (Math.cos(Rad(gLat)) * Math.cos(Rad(dltS)))
            if (Math.abs(coshaS) < 1) {
                haS     = Deg(Math.acos(coshaS))
                KWD     = gLon / 15.0 - TmZn
                JSunSet = haS / 15.0 + 12.0 - EoT - KWD
            } else {
                JSunSet = 0.0 // seharusnya menampilkan hasil error
            }
        }
        JDGS       = CJDN - 0.5 + (JSunSet - TmZn) / 24.0
//  Hasil Perhitungan
        return JDGS
    }

    fun TinggiHilal(JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double, Optional: Int): Double {
//  Deklarasi Variabel
        var JDEGhurubS  : Double
        var dltS        : Double
        var sdS         : Double
        var EoT         : Double
        var rfS         : Double
        var Dip         : Double
        var altS        : Double
        var coshaS      : Double
        var haS         : Double
        var RAm         : Double
        var RAb         : Double
        var Db          : Double
        var Sdb         : Double
        var HPb         : Double
        var tc          : Double
        var hcG         : Double //Tinggi Hilal Geocentris
        var Pc          : Double
        var hcT         : Double //Tinggi Hilal Topocentris (Center)
        var hcTA        : Double //Tinggi Hilal Topocentris Atas
        var hcTB        : Double //Tinggi Hilal Topocentris Bawah
        var RefA        : Double
        var RefT        : Double
        var RefB        : Double
        var hcMA        : Double //Tinggi Hilal Mar'i Atas
        var hcMT        : Double //Tinggi Hilal Mar'i Tengah
        var hcMB        : Double //Tinggi Hilal Mar'i Bawah
        var IH          : Double


//  Proses Perhitungan
        JDEGhurubS  = JDGhurubS + DeltaT(JDGhurubS) / 86400.00
        dltS        = SunApparentDeclination(JDGhurubS)
        sdS         = SunAngularSemiDiameter(JDGhurubS)
        rfS         = 34.16/60.0
        Dip         = 2.1 * Math.sqrt(gAlt) / 60.0
        altS        = 0 - sdS - rfS - Dip + 0.0024
        coshaS      = (Math.sin(Rad(altS)) - Math.sin(Rad(gLat)) * Math.sin(Rad(dltS))) / (Math.cos(Rad(gLat)) * Math.cos(Rad(dltS)))
        if (Math.abs(coshaS) < 1) {
            haS     = Deg(Math.acos(coshaS))
        } else {
            haS = 0.0 // seharusnya menampilkan hasil error
        }
        RAm         = SunApparentRightAscension(JDEGhurubS)
        RAb         = MoonApparentRightAscension(JDEGhurubS)
        Db          = MoonApparentDeclination(JDEGhurubS)
        Sdb         = MoonAngularSemiDiameter(JDEGhurubS)
        HPb         = MoonEquatorialHorizontalParallax(JDEGhurubS)
        tc          = RAm - RAb + haS
        hcG         = Deg(Math.asin(Math.sin(Rad(gLat)) *Math.sin(Rad(Db)) + Math.cos(Rad(gLat)) *Math.cos(Rad(Db)) * Math.cos(Rad(tc))))
        Pc          = Deg(Math.cos(Rad(hcG))*(Rad(HPb)))
        hcT         = hcG - Pc
        hcTA        = hcT + Sdb
        hcTB        = hcT - Sdb
        RefA        = 0.01695 /Math.tan (Rad(hcTA + 10.3/(hcTA + 5.1255)))
        RefT        = 0.01695 /Math.tan (Rad(hcT + 10.3/(hcT + 5.1255)))
        RefB        = 0.01695 /Math.tan (Rad(hcTB + 10.3/(hcTB + 5.1255)))
        hcMA        = hcT + RefA + Dip + Sdb
        hcMT        = hcT + RefT + Dip
        hcMB        = hcT + RefB + Dip - Sdb

        when (Optional) {
            1 -> IH = hcG
            2 -> IH = hcT
            3 -> IH = hcTA
            4 -> IH = hcTB
            5 -> IH = hcMA
            6 -> IH = hcMT
            7 -> IH = hcMB

            else -> IH = hcMT

        }

        return IH
    }
    fun Azimut(JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double, Optional: Int): Double {
//  Deklarasi Variabel
        var JDEGhurubS  : Double
        var dltS        : Double
        var sdS         : Double
        var EoT         : Double
        var rfS         : Double
        var Dip         : Double
        var altS        : Double
        var coshaS      : Double
        var haS         : Double
        var RAm         : Double
        var RAb         : Double
        var Db          : Double
        var HPb         : Double
        var Azm         : Double // Azimut Matahari
        var tc          : Double
        var Azb         : Double
        var AZ          : Double
        var PNF         : Double

//  Proses Perhitungan
        JDEGhurubS  = JDGhurubS + DeltaT(JDGhurubS) / 86400.00
        dltS        = SunApparentDeclination(JDGhurubS)
        sdS         = SunAngularSemiDiameter(JDGhurubS)
        rfS         = 34.16/60.0
        Dip         = 2.1 * Math.sqrt(gAlt) / 60.0
        altS        = 0 - sdS - rfS - Dip + 0.0024
        coshaS      = (Math.sin(Rad(altS)) - Math.sin(Rad(gLat)) * Math.sin(Rad(dltS))) / (Math.cos(Rad(gLat)) * Math.cos(Rad(dltS)))
        if (Math.abs(coshaS) < 1) {
            haS     = Deg(Math.acos(coshaS))
        } else {
            haS = 0.0 // seharusnya menampilkan hasil error
        }
        RAm         = SunApparentRightAscension(JDEGhurubS)
        RAb         = MoonApparentRightAscension(JDEGhurubS)
        Db          = MoonApparentDeclination(JDEGhurubS)
        tc          = RAm - RAb + haS
        Azm         = Deg(Math.atan( -Math.sin(Rad(gLat))/Math.tan(Rad(haS))+
                Math.cos(Rad(gLat))*Math.tan(Rad(dltS))/Math.sin(Rad(haS)))) + 270
        Azb         = Deg(Math.atan( -Math.sin(Rad(gLat))/Math.tan(Rad(tc))+
                Math.cos(Rad(gLat))*Math.tan(Rad(Db))/Math.sin(Rad(tc)))) + 270
        when (Optional) {
            1 -> AZ = Azm
            2 -> AZ = Azb
            else -> AZ = 0.0
        }

//  Hasil Perhitungan
        return AZ
    }
    fun DifferentAzimut(JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        val Daz: Double = Azimut(JDGhurubS, gLat, gLon, gAlt, TmZn,2)-
                Azimut(JDGhurubS, gLat, gLon, gAlt, TmZn,1)
        return Daz
    }
    fun LamaHilal (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var JDEGhurubS  : Double
        var dltS        : Double
        var sdS         : Double
        var Sdb         : Double
        var rfS         : Double
        var Dip         : Double
        var altS        : Double
        var coshaS      : Double
        var haS         : Double
        var RAm         : Double
        var RAb         : Double
        var tc          : Double
        var Db          : Double
        var HPb         : Double
        var NF          : Double
        var PNF         : Double
        var SBSH        : Double
        var SBS         : Double
        var Lm          : Double


        JDEGhurubS  = JDGhurubS + DeltaT(JDGhurubS) / 86400.00
        dltS        = SunApparentDeclination(JDGhurubS)
        sdS         = SunAngularSemiDiameter(JDGhurubS)
        rfS         = 34.16/60.0
        Dip         = 2.1 * Math.sqrt(gAlt) / 60.0
        altS        = 0 - sdS - rfS - Dip + 0.0024
        coshaS      = (Math.sin(Rad(altS)) - Math.sin(Rad(gLat)) * Math.sin(Rad(dltS))) / (Math.cos(Rad(gLat)) * Math.cos(Rad(dltS)))
        if (Math.abs(coshaS) < 1) {
            haS     = Deg(Math.acos(coshaS))
        } else {
            haS = 0.0 // seharusnya menampilkan hasil error
        }
        RAm         = SunApparentRightAscension(JDEGhurubS)
        RAb         = MoonApparentRightAscension(JDEGhurubS)
        Db          = MoonApparentDeclination(JDEGhurubS)
        tc          = RAm - RAb + haS
        Db          = MoonApparentDeclination(JDEGhurubS)
        Sdb         = MoonAngularSemiDiameter(JDEGhurubS)
        NF          = Deg(Math.asin((Math.sin(Rad(gLat))*Math.sin(Rad(Db)))/(Math.cos(Rad(gLat))*Math.cos(Rad(Db)))))
        HPb         = MoonEquatorialHorizontalParallax(JDEGhurubS)
        PNF         = Deg(Math.cos(Rad(NF))*(Rad(HPb)))
        SBSH        = 90.0 + NF
        SBS         = if(SBSH <90) { 90 + NF + PNF - (Sdb + 34.5/60 + Dip) } else { 90 + NF - PNF + (Sdb + 34.5/60 + Dip)}
        Lm          = (SBS - tc)/15

        return Lm
    }
    fun TerbenamHilal (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var GrbM        : Double
        var Lm          : Double
        var TH          : Double

        GrbM    = JDKM(JDGhurubS,TmZn,"Jam D").toDouble()
        Lm      = LamaHilal(JDGhurubS,gLat,gLon,gAlt,TmZn)
        TH      = GrbM + Lm
        return TH
    }
    fun ArahTerbenamHilal (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var Dip         : Double
        var JDEGhurubS  : Double
        var Db          : Double
        var Sdb         : Double
        var NF          : Double
        var HPb         : Double
        var PNF         : Double
        var SBSH        : Double
        var SBS         : Double
        var AT          : Double

        Dip         = 2.1 * Math.sqrt(gAlt) / 60.0
        JDEGhurubS  = JDGhurubS + DeltaT(JDGhurubS) / 86400.00
        Db          = MoonApparentDeclination(JDEGhurubS)
        Sdb         = MoonAngularSemiDiameter(JDEGhurubS)
        NF          = Deg(Math.asin((Math.sin(Rad(gLat))*Math.sin(Rad(Db)))/(Math.cos(Rad(gLat))*Math.cos(Rad(Db)))))
        HPb         = MoonEquatorialHorizontalParallax(JDEGhurubS)
        PNF         = Deg(Math.cos(Rad(NF))*(Rad(HPb)))
        SBSH        = 90.0 + NF
        SBS         = if(SBSH <90) { 90 + NF + PNF - (Sdb + 34.5/60 + Dip) } else { 90 + NF - PNF + (Sdb + 34.5/60 + Dip)}
        AT          = Deg(Math.atan (-Math.sin(Rad(gLat))/Math.tan(Rad(SBS))
                + Math.cos(Rad(gLat)) * Math.tan(Rad(Db))/Math.sin(Rad(SBS)))) + 270
        return AT
    }
    fun LuasCahayaHilal (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var JDEGhurubS  : Double
        var FI          : Double

        JDEGhurubS      = JDGhurubS + DeltaT(JDGhurubS) / 86400.00
        FI              = MoonDiskIlluminatedFraction(JDEGhurubS) * 100
        return FI
    }
    fun KemiringanHilal (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): String {
        var DAz : Double
        var hcMA : Double
        var MRG : Double
        var KH : String

        DAz = DifferentAzimut(JDGhurubS,gLat,gLon,gAlt,TmZn)
        hcMA = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,5)
        MRG = Deg(Math.atan(Math.abs(DAz/hcMA)))
        KH  = if (MRG < 15.0) { " Hilal Terlentang"
        } else if (MRG>15.0 && DAz > 0.0) {"Hilal Miring Utara"
        } else {"Hilal Miring Selatan"}
        return KH
    }
    fun BedaTinggiHilal (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var sdS : Double
        var rfS : Double
        var Dip : Double
        var hm : Double
        var hcT : Double
        var ARCV: Double

        sdS     = SunAngularSemiDiameter(JDGhurubS)
        rfS     = 34.16/60.0
        Dip     = 2.1 * Math.sqrt(gAlt) / 60.0
        hm      = 0 - sdS - rfS - Dip + 0.0024
        hcT     = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,2)
        ARCV    = hcT + Math.abs(hm)
        return ARCV

    }

    fun ElongasiToposentris (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var sdS : Double
        var rfS : Double
        var Dip : Double
        var hm : Double
        var hcT : Double
        var DAz : Double
        var ARCL: Double

        sdS     = SunAngularSemiDiameter(JDGhurubS)
        rfS     = 34.16/60.0
        Dip     = 2.1 * Math.sqrt(gAlt) / 60.0
        hm      = 0 - sdS - rfS - Dip + 0.0024
        DAz     = DifferentAzimut(JDGhurubS,gLat,gLon,gAlt,TmZn)
        hcT     = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,2)
        ARCL    = Deg(Math.acos(Math.sin(Rad(hcT))*Math.sin(Rad(hm))+Math.cos(Rad(hcT))*Math.cos(Rad(hm))*Math.cos(Rad(DAz))))

        return ARCL
    }
    fun LebarHilal (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var Sdb : Double
        var ARCL : Double
        var CW : Double

        Sdb  = MoonAngularSemiDiameter(JDGhurubS)
        ARCL = ElongasiToposentris(JDGhurubS,gLat,gLon,gAlt,TmZn)
        CW   = Sdb * (1 - Math.cos(Rad(ARCL)))
        return CW
    }
    fun Range_qOdeh (JDGhurubS: Double, gLat: Double, gLon: Double, gAlt: Double, TmZn: Double): Double {
        var CW : Double
        var ARCV: Double
        var q : Double

        ARCV    = BedaTinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn)
        CW      = LebarHilal(JDGhurubS,gLat,gLon,gAlt,TmZn)
        q       = ARCV-(-0.1018*Math.pow(CW*60,3.0)+0.7319*Math.pow(CW*60,2.0)-6.3226*(CW*60)+7.1651)
        return q
    }
    fun UmurHilal (JDNewMoon: Double, JDGhurubS: Double): Double {
        val UH: Double = (JDGhurubS - JDNewMoon) * 24.0
        return UH
    }

    // Fungsi GreenwichApparentSiderealTime inputnya harus JD tanpa ΔT
// Nilai ΔT bila diketahui harus dimasukkan pada parameter kedua
    fun GreenwichMeanSiderealTime(JD: Double): Double{
        var T     : Double
        var GMST  : Double
        T     = (JD - 2451545.0) / 36525 // dalam Julian Centuries
        GMST  = Mod(280.46061837    +
                360.98564736629 * (JD - 2451545.0) +
                0.000387933   * Math.pow(T,2.0)  -
                1.0 / 38710000  * Math.pow(T,3.0), 360.0) // di Kotlin: 1 / 38710000 = 0 (otomatis jadi integer semua), agar tetap jadi double salah satu dari pembilang atau penyebut atau keduanya harus diberi .0
        return GMST
    }
    // Fungsi GreenwichApparentSiderealTime inputnya harus JD tanpa ΔT
// Nilai ΔT bila diketahui harus dimasukkan pada parameter kedua
    fun GreenwichApparentSiderealTime(
        JD      : Double,      // JD tanpa ΔT
        DeltaT  : Double = 0.0 // masukkan nilai ΔT
    ): Double{
        var JDE       : Double
        var GMST      : Double
        var DeltaPsi  : Double
        var DeltaEps  : Double
        var GAST      : Double

        JDE       = JD + DeltaT / 86400
        GMST      = GreenwichMeanSiderealTime(JD)
        DeltaPsi  = NutationInLongitude(JDE)
        DeltaEps  = ObliquityOfEcliptic(JDE)
        GAST      = Mod(GMST + DeltaPsi * Math.cos(Rad(DeltaEps)), 360.0)
        return GAST
    }

    fun LocalApparentSiderealTime(JD: Double, GLon: Double, DeltaT: Double = 0.0): Double{
        var GAST      : Double
        var LAST      : Double

        GAST    = GreenwichApparentSiderealTime(JD,DeltaT)
        LAST    = Mod(GAST + GLon,360.0)
        return LAST
    }

    fun SunGreenwichHourAngle(JD: Double, DeltaT: Double = 0.0): Double{
        var JDE       : Double
        var GAST      : Double
        var AlphaS    : Double
        var GHAS      : Double

        JDE     = JD + DeltaT / 86400
        GAST    = GreenwichApparentSiderealTime(JD,DeltaT)
        AlphaS  = SunApparentRightAscension(JDE)
        GHAS    = Mod(GAST - AlphaS, 360.0)
        return  GHAS
    }

    fun MoonGreenwichHourAngle(JD: Double, DeltaT: Double = 0.0): Double{
        var JDE       : Double
        var GAST      : Double
        var AlphaM    : Double
        var GHAM      : Double

        JDE     = JD + DeltaT / 86400
        GAST    = GreenwichApparentSiderealTime(JD,DeltaT)
        AlphaM  = MoonApparentRightAscension(JDE)
        GHAM    = Mod(GAST - AlphaM, 360.0)
        return  GHAM
    }

    fun SunLocalHourAngle(JD: Double, GLon: Double, DeltaT: Double = 0.0): Double{
        var GHAS      : Double
        var LHAS      : Double

        GHAS    = SunGreenwichHourAngle(JD,DeltaT)
        LHAS    = Mod(GHAS + GLon,360.0)
        return LHAS
    }

    fun MoonLocalHourAngle(JD: Double, GLon: Double, DeltaT: Double = 0.0): Double{
        var GHAM      : Double
        var LHAM      : Double

        GHAM    = MoonGreenwichHourAngle(JD,DeltaT)
        LHAM    = Mod(GHAM + GLon,360.0)
        return LHAM
    }

    fun SunAltitude(JD: Double, GLon: Double, GLat: Double, DeltaT: Double = 0.0): Double{
        var JDE       : Double
        var dltS      : Double
        var LHAS      : Double
        var altS      : Double

        JDE     = JD + DeltaT / 86400
        dltS    = SunApparentDeclination(JDE)
        LHAS    = SunLocalHourAngle(JD,GLon,DeltaT)
        altS    = Deg(Math.asin(Math.sin(Rad(GLat))*Math.sin(Rad(dltS))+Math.cos(Rad(GLat))*Math.cos(Rad(dltS))*Math.cos(Rad(LHAS))))
        return altS
    }

    fun MoonAltitude(JD: Double, GLon: Double, GLat: Double, DeltaT: Double = 0.0): Double{
        var JDE       : Double
        var dltM      : Double
        var LHAM      : Double
        var altM      : Double

        JDE     = JD + DeltaT / 86400
        dltM    = MoonApparentDeclination(JDE)
        LHAM    = MoonLocalHourAngle(JD,GLon,DeltaT)
        altM    = Deg(Math.asin(Math.sin(Rad(GLat))*Math.sin(Rad(dltM))+Math.cos(Rad(GLat))*Math.cos(Rad(dltM))*Math.cos(Rad(LHAM))))
        return altM
    }

    fun SunAzimuth(JD: Double, GLon: Double, GLat: Double, DeltaT: Double = 0.0): Double{
        var JDE       : Double
        var dltS      : Double
        var LHAS      : Double
        var azS       : Double

        JDE     = JD + DeltaT / 86400
        dltS    = SunApparentDeclination(JDE)
        LHAS    = SunLocalHourAngle(JD,GLon,DeltaT)
        azS     = Mod(Deg(Math.atan2(Math.sin(Rad(LHAS)),Math.cos(Rad(LHAS))*Math.sin(Rad(GLat))-Math.tan(Rad(dltS))*Math.cos(Rad(GLat))))+180,360.0)
        return azS
    }


    fun MoonAzimuth(JD: Double, GLon: Double, GLat: Double, DeltaT: Double = 0.0): Double{
        var JDE       : Double
        var dltM      : Double
        var LHAM      : Double
        var azM       : Double

        JDE     = JD + DeltaT / 86400
        dltM    = MoonApparentDeclination(JDE)
        LHAM    = MoonLocalHourAngle(JD,GLon,DeltaT)
        azM     = Mod(Deg(Math.atan2(Math.sin(Rad(LHAM)),Math.cos(Rad(LHAM))*Math.sin(Rad(GLat))-Math.tan(Rad(dltM))*Math.cos(Rad(GLat))))+180,360.0)
        return azM
    }

    fun RhoSinThetaPrime(GLat: Double, GAlt: Double): Double {
        val a : Double = 6378.14
        val f : Double = 1 / 298.257
        val b : Double = a * (1 - f)
        val u : Double = Deg(Math.atan(b / a * Math.tan(Rad(GLat))))
        return b / a * Math.sin(Rad(u)) + GAlt / 6378140 * Math.sin(Rad(GLat))
    }

    fun HorizontalFromEquatorial(
        Declination           : Double,
        HourAngle             : Double,
        GeographicLatitude    : Double,
        HorizontalCoordinates : Int = 1
    ) : Double {
        val delta   : Double  = Declination
        val H       : Double  = HourAngle
        val GeogLat : Double  = GeographicLatitude
        var A       : Double
        var h       : Double
        A = Mod(Deg(Math.atan2(Math.sin(Rad(H)),Math.cos(Rad(H)) * Math.sin(Rad(GeogLat)) - Math.tan(Rad(delta)) * Math.cos(Rad(GeogLat)))) + 180.0,360.0)
        h = Deg(Math.asin(Math.sin(Rad(GeogLat)) * Math.sin(Rad(delta)) + Math.cos(Rad(GeogLat)) * Math.cos(Rad(delta)) * Math.cos(Rad(H))))
        return when (HorizontalCoordinates) {
            1     -> A
            2     -> h
            else  -> A
        }
    }

    fun RhoCosThetaPrime(GLat: Double, GAlt: Double): Double {
        val a : Double = 6378.14
        val f : Double = 1 / 298.257
        val b : Double = a * (1 - f)
        val u : Double = Deg(Math.atan(b / a * Math.tan(Rad(GLat))))
        return Math.cos(Rad(u)) + GAlt / 6378140 * Math.cos(Rad(GLat))
    }

    fun TopocentricEclipticFromGeocentricEclipticCoordinates(
        GeocentricLongitude             : Double,
        GeocentricLatitude              : Double,
        GeocentricSemiDiameter          : Double,
        ObliquityOfEcliptic             : Double,
        EquatorialHorizontalParallax    : Double,
        LocalSiderealTime               : Double,
        GeographicLatitude              : Double,
        GeographicAltitude              : Double,
        TopocentricEclipticCoordinates  : Int = 1
    ) : Double {
        val lambda  : Double = GeocentricLongitude
        val beta    : Double = GeocentricLatitude
        val s       : Double = GeocentricSemiDiameter
        val epsilon : Double = ObliquityOfEcliptic
        val pi      : Double = EquatorialHorizontalParallax
        val theta   : Double = LocalSiderealTime
        val GeogLat : Double = GeographicLatitude
        val GeogAlt : Double = GeographicAltitude
        var S       : Double // RhoSinThetaPrime
        var C       : Double // RhoCosThetaPrime
        var N       : Double
        var lambdaP : Double // TopocentricEclipticLongitude
        var betaP   : Double // TopocentricEclipticLatitude
        var sP      : Double // TopocentricEclipticSemiDiameter

        S       = RhoSinThetaPrime(GeogLat,GeogAlt)
        C       = RhoCosThetaPrime(GeogLat,GeogAlt)
        N       = Math.cos(Rad(lambda)) * Math.cos(Rad(beta)) -
                C * Math.sin(Rad(pi)) * Math.cos(Rad(theta))
        lambdaP = Mod(Deg(Math.atan2(Math.sin(Rad(lambda)) * Math.cos(Rad(beta)) - Math.sin(Rad(pi)) * (S * Math.sin(Rad(epsilon)) + C * Math.cos(Rad(epsilon)) * Math.sin(Rad(theta))), N)),360.0)
        betaP   = Deg(Math.atan((Math.cos(Rad(lambdaP)) * (Math.sin(Rad(beta)) - Math.sin(Rad(pi)) * (S * Math.cos(Rad(epsilon)) - C * Math.sin(Rad(epsilon)) * Math.sin(Rad(theta))))) / N))
        sP      = Deg(Math.asin((Math.cos(Rad(lambdaP)) * Math.cos(Rad(betaP)) * Math.sin(Rad(s))) / N))

        return when(TopocentricEclipticCoordinates) {
            1     -> lambdaP
            2     -> betaP
            3     -> sP
            else  -> lambdaP
        }
    }

    fun TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(
        GeocentricRightAscension          : Double,
        GeocentricDeclination             : Double,
        EquatorialHorizontalParallax      : Double,
        GeocentricHourAngle               : Double,
        GeographicLatitude                : Double,
        GeographicAltitude                : Double,
        TopocentricEquatorialCoordinates  : Int = 1
    ) : Double {
        val alpha     : Double = GeocentricRightAscension
        val delta     : Double = GeocentricDeclination
        val pi        : Double = EquatorialHorizontalParallax
        val H         : Double = GeocentricHourAngle
        val GeogLat   : Double = GeographicLatitude
        val GeogAlt   : Double = GeographicAltitude
        var S         : Double // RhoSinThetaPrime
        var C         : Double // RhoCosThetaPrime
        var dltAlpha  : Double
        var alphaP    : Double
        var deltaP    : Double
        var HP        : Double

        S         = RhoSinThetaPrime(GeogLat,GeogAlt)
        C         = RhoCosThetaPrime(GeogLat,GeogAlt)
        dltAlpha  = Mod(Deg(Math.atan2(-C * Math.sin(Rad(pi)) * Math.sin(Rad(H)), Math.cos(Rad(delta)) - C * Math.sin(Rad(pi)) * Math.cos(Rad(H)))),360.0)
        alphaP    = Mod(alpha + dltAlpha, 360.0)
        deltaP    = Deg(Math.atan2((Math.sin(Rad(delta)) - S * Math.sin(Rad(pi))) * Math.cos(Rad(dltAlpha)), Math.cos(Rad(delta)) - C * Math.sin(Rad(pi)) * Math.cos(Rad(H))))
        HP        = Mod(H - dltAlpha,360.0)

        return when(TopocentricEquatorialCoordinates) {
            1     -> alphaP
            2     -> deltaP
            3     -> HP
            else  -> alphaP
        }
    }

    fun SunTopocentricEclipticCoordinates(
        JulianDay                       : Double,
        GeographicLongitude             : Double,
        GeographicLatitude              : Double,
        GeographicAltitude              : Double,
        DeltaT                          : Double = 0.0,
        TopocentricEclipticCoordinates  : Int = 1
    ) : Double{
        val JD      : Double = JulianDay
        val JDE     : Double = JD + DeltaT / 86400
        val GeogLon : Double = GeographicLongitude
        val GeogLat : Double = GeographicLatitude
        val GeogAlt : Double = GeographicAltitude
        val lambdaS : Double = SunGeocentricLongitude(JDE)
        val betaS   : Double = SunGeocentricLatitude(JDE) / 3600.0
        val sS      : Double = SunAngularSemiDiameter(JDE)
        val epsS    : Double = ObliquityOfEcliptic(JDE)
        val piS     : Double = SunEquatorialHorizontalParallax(JDE)
        val thetaS  : Double = LocalApparentSiderealTime(JD,GeogLon,DeltaT)
        return when (TopocentricEclipticCoordinates) {
            1     -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaS, betaS, sS, epsS, piS, thetaS, GeogLat, GeogAlt, TopocentricEclipticCoordinates)
            2     -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaS, betaS, sS, epsS, piS, thetaS, GeogLat, GeogAlt, TopocentricEclipticCoordinates)
            3     -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaS, betaS, sS, epsS, piS, thetaS, GeogLat, GeogAlt, TopocentricEclipticCoordinates)
            else  -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaS, betaS, sS, epsS, piS, thetaS, GeogLat, GeogAlt)
        }
    }

    fun MoonTopocentricEclipticCoordinates(
        JulianDay                       : Double,
        GeographicLongitude             : Double,
        GeographicLatitude              : Double,
        GeographicAltitude              : Double,
        DeltaT                          : Double = 0.0,
        TopocentricEclipticCoordinates  : Int = 1
    ) : Double{
        val JD      : Double = JulianDay
        val JDE     : Double = JD + DeltaT / 86400
        val GeogLon : Double = GeographicLongitude
        val GeogLat : Double = GeographicLatitude
        val GeogAlt : Double = GeographicAltitude
        val lambdaM : Double = MoonGeocentricLongitude(JDE)
        val betaM   : Double = MoonGeocentricLatitude(JDE)
        val sM      : Double = MoonAngularSemiDiameter(JDE)
        val epsM    : Double = ObliquityOfEcliptic(JDE)
        val piM     : Double = MoonEquatorialHorizontalParallax(JDE)
        val thetaM  : Double = LocalApparentSiderealTime(JD,GeogLon,DeltaT)
        return when (TopocentricEclipticCoordinates) {
            1     -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaM, betaM, sM, epsM, piM, thetaM, GeogLat, GeogAlt, TopocentricEclipticCoordinates)
            2     -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaM, betaM, sM, epsM, piM, thetaM, GeogLat, GeogAlt, TopocentricEclipticCoordinates)
            3     -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaM, betaM, sM, epsM, piM, thetaM, GeogLat, GeogAlt, TopocentricEclipticCoordinates)
            else  -> TopocentricEclipticFromGeocentricEclipticCoordinates(lambdaM, betaM, sM, epsM, piM, thetaM, GeogLat, GeogAlt)
        }
    }

    fun SunTopocentricEquatorialCoordinates(
        JulianDay                         : Double,
        GeographicLongitude               : Double,
        GeographicLatitude                : Double,
        GeographicAltitude                : Double,
        DeltaT                            : Double = 0.0,
        TopocentricEquatorialCoordinates  : Int = 1
    ) : Double {
        val JD      : Double = JulianDay
        val JDE     : Double = JD + DeltaT / 86400
        val GeogLon : Double = GeographicLongitude
        val GeogLat : Double = GeographicLatitude
        val GeogAlt : Double = GeographicAltitude
        val alphaS  : Double = SunApparentRightAscension(JDE)
        val deltaS  : Double = SunApparentDeclination(JDE)
        val piS     : Double = SunEquatorialHorizontalParallax(JDE)
        val HS      : Double = SunLocalHourAngle(JD,GeogLon,DeltaT)
        return when (TopocentricEquatorialCoordinates) {
            1     -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaS, deltaS, piS, HS, GeogLat, GeogAlt, TopocentricEquatorialCoordinates)
            2     -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaS, deltaS, piS, HS, GeogLat, GeogAlt, TopocentricEquatorialCoordinates)
            3     -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaS, deltaS, piS, HS, GeogLat, GeogAlt, TopocentricEquatorialCoordinates)
            else  -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaS, deltaS, piS, HS, GeogLat, GeogAlt)
        }
    }

    fun MoonTopocentricEquatorialCoordinates(
        JulianDay                         : Double,
        GeographicLongitude               : Double,
        GeographicLatitude                : Double,
        GeographicAltitude                : Double,
        DeltaT                            : Double = 0.0,
        TopocentricEquatorialCoordinates  : Int = 1
    ) : Double {
        val JD      : Double = JulianDay
        val JDE     : Double = JD + DeltaT / 86400
        val GeogLon : Double = GeographicLongitude
        val GeogLat : Double = GeographicLatitude
        val GeogAlt : Double = GeographicAltitude
        val alphaM  : Double = MoonApparentRightAscension(JDE)
        val deltaM  : Double = MoonApparentDeclination(JDE)
        val piM     : Double = MoonEquatorialHorizontalParallax(JDE)
        val HM      : Double = MoonLocalHourAngle(JD,GeogLon,DeltaT)
        return when (TopocentricEquatorialCoordinates) {
            1     -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaM, deltaM, piM, HM, GeogLat, GeogAlt, TopocentricEquatorialCoordinates)
            2     -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaM, deltaM, piM, HM, GeogLat, GeogAlt, TopocentricEquatorialCoordinates)
            3     -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaM, deltaM, piM, HM, GeogLat, GeogAlt, TopocentricEquatorialCoordinates)
            else  -> TopocentricEquatorialFromFromGeocentricEquatorialCoordinates(alphaM, deltaM, piM, HM, GeogLat, GeogAlt)
        }
    }

    fun SunTopocentricHorizontalCoordinates (
        JulianDay                         : Double,
        GeographicLongitude               : Double,
        GeographicLatitude                : Double,
        GeographicAltitude                : Double,
        DeltaT                            : Double = 0.0,
        TopocentricHorizontalCoordinates  : Int = 1
    ): Double{
        val JD      : Double  = JulianDay
        val dltT    : Double  = DeltaT
        val GeogLon : Double  = GeographicLongitude
        val GeogLat : Double  = GeographicLatitude
        val GeogAlt : Double  = GeographicAltitude
        val deltaS  : Double  = SunTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)
        val HS      : Double  = SunTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,3)
        var AS      : Double
        var hS      : Double
        AS = HorizontalFromEquatorial(deltaS, HS, GeogLat, 1)
        hS = HorizontalFromEquatorial(deltaS, HS, GeogLat, 2)
        return when (TopocentricHorizontalCoordinates) {
            1     -> AS
            2     -> hS
            else  -> AS
        }
    }

    fun MoonTopocentricHorizontalCoordinates (
        JulianDay                         : Double,
        GeographicLongitude               : Double,
        GeographicLatitude                : Double,
        GeographicAltitude                : Double,
        DeltaT                            : Double = 0.0,
        TopocentricHorizontalCoordinates  : Int = 1
    ): Double{
        val JD      : Double  = JulianDay
        val dltT    : Double  = DeltaT
        val GeogLon : Double  = GeographicLongitude
        val GeogLat : Double  = GeographicLatitude
        val GeogAlt : Double  = GeographicAltitude
        val deltaM  : Double  = MoonTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)
        val HM      : Double  = MoonTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,3)
        var AM      : Double
        var hM      : Double
        AM = HorizontalFromEquatorial(deltaM, HM, GeogLat, 1)
        hM = HorizontalFromEquatorial(deltaM, HM, GeogLat, 2)
        return when (TopocentricHorizontalCoordinates) {
            1     -> AM
            2     -> hM
            else  -> AM
        }
    }
    fun JDEEclipseModified(
        HijriMonth: Byte, HijriYear: Long, EclipseKind: Int
    ): Double {
        var k       : Double
        var T       : Double
        var JDEMMP  : Double
        var e       : Double
        var M       : Double
        var Md      : Double
        var F       : Double
        var Omg     : Double
        var F1      : Double
        var A1      : Double
        var JDECorr : Double

        k = when(EclipseKind) {
            1     -> Math.floor(HijriMonth.toDouble() + 12 * HijriYear.toDouble() - 17048.5) + 0.0
            2     -> Math.floor(HijriMonth.toDouble() + 12 * HijriYear.toDouble() - 17049.5) + 0.5
            else  -> Math.floor(HijriMonth.toDouble() + 12 * HijriYear.toDouble() - 17048.5) + 0.0
        }
        T       = k / 1236.85
        JDEMMP  = 2451550.09766 + 29.530588861   * k +
                0.00015437    * T * T -
                0.000000150   * T * T * T +
                0.00000000073 * T * T * T * T
        e   = 1 - 0.002516 * T - 0.0000074 * T * T
        M   = 2.5534 + 29.1053567  * k -
                0.0000014  * T * T -
                0.00000011 * T * T * T
        M   = Rad(Mod(M,360.0))
        Md  = 201.5643 + 385.81693528  * k +
                0.0107582   * T * T +
                0.00001238  * T * T * T -
                0.000000058 * T * T * T * T
        Md  = Rad(Mod(Md,360.0))
        F   = 160.7108 + 390.67050284  * k -
                0.0016118   * T * T -
                0.00000227  * T * T * T +
                0.000000011 * T * T * T * T
        F   = Rad(Mod(F,360.0))
        Omg = 124.7746 - 1.56375588 * k +
                0.0020672  * T * T +
                0.00000215 * T * T * T
        Omg = Rad(Mod(Omg,360.0))
        if (Math.abs(Math.sin(F)) > 0.36) {
            return 0.0
        } else {
            F1 = Deg(F) - 0.02665 * Math.sin(Omg)
            F1 = Rad(Mod(F1, 360.0))
            A1 = 299.77 + 0.107408 * k - 0.009173 * T * T
            A1 = Rad(Mod(A1, 360.0))
            when(EclipseKind) {
                1     ->  JDECorr = -0.4075 * Math.sin(Md) +
                        0.1721 * e * Math.sin(M)
                2     ->  JDECorr = -0.4065 * Math.sin(Md) +
                        0.1727 * e * Math.sin(M)
                else  ->  JDECorr = -0.4075 * Math.sin(Md) +
                        0.1721 * e * Math.sin(M)
            }
            JDECorr = JDECorr +
                    0.0161 *     Math.sin(2 * Md) -
                    0.0097 *     Math.sin(2 * F1) +
                    0.0073 * e * Math.sin(Md - M) -
                    0.0050 * e * Math.sin(Md + M) -
                    0.0023 *     Math.sin(Md - 2 * F1) +
                    0.0021 * e * Math.sin(2 * M) +
                    0.0012 *     Math.sin(Md + 2 * F1) +
                    0.0006 * e * Math.sin(2 * Md + M) -
                    0.0004 *     Math.sin(3 * Md) -
                    0.0003 * e * Math.sin(M + 2 * F1) +
                    0.0003 *     Math.sin(A1) -
                    0.0002 * e * Math.sin(M - 2 * F1) -
                    0.0002 * e * Math.sin(2 * Md - M) -
                    0.0002 *     Math.sin(Omg)
            return JDEMMP + JDECorr
        }
    }
    fun LBesselian (HijriMonth: Byte, HijriYear: Long, OptResult: String): Double {
        var JDELunarEclipse1: Double
        var JDELunarEclipse2: Double
        var T0      : Double
        val DeltaT  : Double

        var ARmM2   : Double
        var ARmM1   : Double
        var ARm00   : Double
        var ARmP1   : Double
        var ARmP2   : Double
        var dmM2    : Double
        var dmM1    : Double
        var dm00    : Double
        var dmP1    : Double
        var dmP2    : Double
        var SdmM2   : Double
        var SdmM1   : Double
        var Sdm00   : Double
        var SdmP1   : Double
        var SdmP2   : Double
        var HPmM2   : Double
        var HPmM1   : Double
        var HPm00   : Double
        var HPmP1   : Double
        var HPmP2   : Double
        var GHAmM2  : Double
        var GHAmM1  : Double
        var GHAm00  : Double
        var GHAmP1  : Double
        var GHAmP2  : Double

        var ARbM2   : Double
        var ARbM1   : Double
        var ARb00   : Double
        var ARbP1   : Double
        var ARbP2   : Double
        var dbM2    : Double
        var dbM1    : Double
        var db00    : Double
        var dbP1    : Double
        var dbP2    : Double
        var SdbM2   : Double
        var SdbM1   : Double
        var Sdb00   : Double
        var SdbP1   : Double
        var SdbP2   : Double
        var HPbM2   : Double
        var HPbM1   : Double
        var HPb00   : Double
        var HPbP1   : Double
        var HPbP2   : Double
        var GHAbM2  : Double
        var GHAbM1  : Double
        var GHAb00  : Double
        var GHAbP1  : Double
        var GHAbP2  : Double
        var xM2     : Double
        var xM1     : Double
        var x00     : Double
        var xP1     : Double
        var xP2     : Double
        var yM2     : Double
        var yM1     : Double
        var y00     : Double
        var yP1     : Double
        var yP2     : Double
        var L12     : Double
        var L11     : Double
        var L10     : Double
        var L1P1    : Double
        var L1P2    : Double
        var L22     : Double
        var L21     : Double
        var L20     : Double
        var L2P1    : Double
        var L2P2    : Double
        var L32     : Double
        var L31     : Double
        var L30     : Double
        var L3P1    : Double
        var L3P2    : Double
        var aM2     : Double
        var aM1     : Double
        var a00     : Double
        var aP1     : Double
        var aP2     : Double
        var dM2     : Double
        var dM1     : Double
        var d00     : Double
        var dP1     : Double
        var dP2     : Double
        var eM2     : Double
        var eM1     : Double
        var e00     : Double
        var eP1     : Double
        var eP2     : Double
        var MM2     : Double
        var MM1     : Double
        var M00     : Double
        var MP1     : Double
        var MP2     : Double
        var ScM2    : Double
        var ScM1    : Double
        var Sc00    : Double
        var ScP1    : Double
        var ScP2    : Double
        var f1M2    : Double
        var f1M1    : Double
        var f100    : Double
        var f1P1    : Double
        var f1P2    : Double
        var f2M2    : Double
        var f2M1    : Double
        var f200    : Double
        var f2P1    : Double
        var f2P2    : Double

        JDELunarEclipse1 = JDEEclipseModified(HijriMonth,HijriYear,2)
        return if (JDELunarEclipse1 > 0) {
            JDELunarEclipse2 = Math.floor(JDELunarEclipse1) + (((JDELunarEclipse1 -Math.floor(JDELunarEclipse1))*24).round(0)).toDouble()/24.0
            T0               = Mod((((JDELunarEclipse2 - Math.floor(JDELunarEclipse2)) * 24).round(0).toDouble()), 24.0)
            DeltaT           = DeltaT(JDELunarEclipse2)

            ARmM2   = SunApparentRightAscension(JDELunarEclipse2 - 2/24.0)
            ARmM1   = SunApparentRightAscension(JDELunarEclipse2 - 1/24.0)
            ARm00   = SunApparentRightAscension(JDELunarEclipse2)
            ARmP1   = SunApparentRightAscension(JDELunarEclipse2 + 1/24.0)
            ARmP2   = SunApparentRightAscension(JDELunarEclipse2 + 2/24.0)

            dmM2     = SunApparentDeclination(JDELunarEclipse2 - 2/24.0)
            dmM1     = SunApparentDeclination(JDELunarEclipse2 - 1/24.0)
            dm00     = SunApparentDeclination(JDELunarEclipse2)
            dmP1     = SunApparentDeclination(JDELunarEclipse2 + 1/24.0)
            dmP2     = SunApparentDeclination(JDELunarEclipse2 + 2/24.0)

            SdmM2    = SunAngularSemiDiameter(JDELunarEclipse2 - 2/24.0)
            SdmM1    = SunAngularSemiDiameter(JDELunarEclipse2 - 1/24.0)
            Sdm00    = SunAngularSemiDiameter(JDELunarEclipse2)
            SdmP1    = SunAngularSemiDiameter(JDELunarEclipse2 + 1/24.0)
            SdmP2    = SunAngularSemiDiameter(JDELunarEclipse2 + 2/24.0)

            HPmM2    = SunEquatorialHorizontalParallax(JDELunarEclipse2 - 2/24.0)
            HPmM1    = SunEquatorialHorizontalParallax(JDELunarEclipse2 - 1/24.0)
            HPm00    = SunEquatorialHorizontalParallax(JDELunarEclipse2)
            HPmP1    = SunEquatorialHorizontalParallax(JDELunarEclipse2 + 1/24.0)
            HPmP2    = SunEquatorialHorizontalParallax(JDELunarEclipse2 + 2/24.0)

            GHAmM2   = SunGreenwichHourAngle(JDELunarEclipse2 - 2/24.0)
            GHAmM1   = SunGreenwichHourAngle(JDELunarEclipse2 - 1/24.0)
            GHAm00   = SunGreenwichHourAngle(JDELunarEclipse2)
            GHAmP1   = SunGreenwichHourAngle(JDELunarEclipse2 + 1/24.0)
            GHAmP2   = SunGreenwichHourAngle(JDELunarEclipse2 + 2/24.0)

            ARbM2    = MoonApparentRightAscension(JDELunarEclipse2 - 2/24.0)
            ARbM1    = MoonApparentRightAscension(JDELunarEclipse2 - 1/24.0)
            ARb00    = MoonApparentRightAscension(JDELunarEclipse2)
            ARbP1    = MoonApparentRightAscension(JDELunarEclipse2 + 1/24.0)
            ARbP2    = MoonApparentRightAscension(JDELunarEclipse2 + 2/24.0)

            dbM2     = MoonApparentDeclination(JDELunarEclipse2 - 2/24.0)
            dbM1     = MoonApparentDeclination(JDELunarEclipse2 - 1/24.0)
            db00     = MoonApparentDeclination(JDELunarEclipse2)
            dbP1     = MoonApparentDeclination(JDELunarEclipse2 + 1/24.0)
            dbP2     = MoonApparentDeclination(JDELunarEclipse2 + 2/24.0)

            SdbM2    = MoonAngularSemiDiameter(JDELunarEclipse2 - 2/24.0)
            SdbM1    = MoonAngularSemiDiameter(JDELunarEclipse2 - 1/24.0)
            Sdb00    = MoonAngularSemiDiameter(JDELunarEclipse2)
            SdbP1    = MoonAngularSemiDiameter(JDELunarEclipse2 + 1/24.0)
            SdbP2    = MoonAngularSemiDiameter(JDELunarEclipse2 + 2/24.0)

            HPbM2    = MoonEquatorialHorizontalParallax(JDELunarEclipse2 - 2/24.0)
            HPbM1    = MoonEquatorialHorizontalParallax(JDELunarEclipse2 - 1/24.0)
            HPb00    = MoonEquatorialHorizontalParallax(JDELunarEclipse2)
            HPbP1    = MoonEquatorialHorizontalParallax(JDELunarEclipse2 + 1/24.0)
            HPbP2    = MoonEquatorialHorizontalParallax(JDELunarEclipse2 + 2/24.0)

            GHAbM2   = MoonGreenwichHourAngle(JDELunarEclipse2 - 2/24.0)
            GHAbM1   = MoonGreenwichHourAngle(JDELunarEclipse2 - 1/24.0)
            GHAb00   = MoonGreenwichHourAngle(JDELunarEclipse2)
            GHAbP1   = MoonGreenwichHourAngle(JDELunarEclipse2 + 1/24.0)
            GHAbP2   = MoonGreenwichHourAngle(JDELunarEclipse2 + 2/24.0)

            aM2 = Mod(ARmM2+180,360.0)
            aM1 = Mod(ARmM1+180,360.0)
            a00 = Mod(ARm00+180,360.0)
            aP1 = Mod(ARmP1+180,360.0)
            aP2 = Mod(ARmP2+180,360.0)

            dM2 = -(dmM2)
            dM1 = -(dmM1)
            d00 = -(dm00)
            dP1 = -(dmP1)
            dP2 = -(dmP2)

            eM2 = 1/4.0*( ARbM2 - aM2)*Math.sin(Rad(2*dM2))*Math.sin(Rad(ARbM2 - aM2))
            eM1 = 1/4.0*( ARbM1 - aM1)*Math.sin(Rad(2*dM1))*Math.sin(Rad(ARbM1 - aM1))
            e00 = 1/4.0*( ARb00 - a00)*Math.sin(Rad(2*d00))*Math.sin(Rad(ARb00 - a00))
            eP1 = 1/4.0*( ARbP1 - aP1)*Math.sin(Rad(2*dP1))*Math.sin(Rad(ARbP1 - aP1))
            eP2 = 1/4.0*( ARbP2 - aP2)*Math.sin(Rad(2*dP2))*Math.sin(Rad(ARbP2 - aP2))

            f1M2 = 1.0086 * (0.998340 * HPbM2 + HPmM2 + SdmM2)
            f1M1 = 1.0086 * (0.998340 * HPbM1 + HPmM1 + SdmM1)
            f100 = 1.0086 * (0.998340 * HPb00 + HPm00 + Sdm00)
            f1P1 = 1.0086 * (0.998340 * HPbP1 + HPmP1 + SdmP1)
            f1P2 = 1.0086 * (0.998340 * HPbP2 + HPmP2 + SdmP2)

            f2M2 = 1.0161 * (0.998340 * HPbM2 + HPmM2 - SdmM2)
            f2M1 = 1.0161 * (0.998340 * HPbM1 + HPmM1 - SdmM1)
            f200 = 1.0161 * (0.998340 * HPb00 + HPm00 - Sdm00)
            f2P1 = 1.0161 * (0.998340 * HPbP1 + HPmP1 - SdmP1)
            f2P2 = 1.0161 * (0.998340 * HPbP2 + HPmP2 - SdmP2)

            xM2 = (ARbM2 - aM2)*Math.cos(Rad(dbM2))
            xM1 = (ARbM1 - aM1)*Math.cos(Rad(dbM1))
            x00 = (ARb00 - a00)*Math.cos(Rad(db00))
            xP1 = (ARbP1 - aP1)*Math.cos(Rad(dbP1))
            xP2 = (ARbP2 - aP2)*Math.cos(Rad(dbP2))

            yM2 = (dbM2 - dM2 + eM2)
            yM1 = (dbM1 - dM1 + eM1)
            y00 = (db00 - d00 + e00)
            yP1 = (dbP1 - dP1 + eP1)
            yP2 = (dbP2 - dP2 + eP2)

            L12  = (f1M2  + SdbM2)
            L11  = (f1M1  + SdbM1)
            L10  = (f100  + Sdb00)
            L1P1 = (f1P1  + SdbP1)
            L1P2 = (f1P2  + SdbP2)

            L22  = (f2M2  + SdbM2)
            L21  = (f2M1  + SdbM1)
            L20  = (f200  + Sdb00)
            L2P1 = (f2P1  + SdbP1)
            L2P2 = (f2P2  + SdbP2)

            L32  = (f2M2  - SdbM2)
            L31  = (f2M1  - SdbM1)
            L30  = (f200  - Sdb00)
            L3P1 = (f2P1  - SdbP1)
            L3P2 = (f2P2  - SdbP2)

            MM2 = GHAbM2
            MM1 = GHAbM1
            M00 = GHAb00
            MP1 = GHAbP1
            MP2 = GHAbP2

            ScM2 = SdbM2
            ScM1 = SdbM1
            Sc00 = Sdb00
            ScP1 = SdbP1
            ScP2 = SdbP2

            return when(OptResult) {
                "JDL" -> JDELunarEclipse2
                "DT"  -> DeltaT
                "x0" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,0)
                "x1" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,1)
                "x2" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,2)
                "x3" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,3)
                "x4" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,4)
                "y0" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,0)
                "y1" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,1)
                "y2" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,2)
                "y3" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,3)
                "y4" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,4)
                "dm0" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,0)
                "dm1" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,1)
                "dm2" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,2)
                "dm3" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,3)
                "dm4" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,4)
                "L10" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,0)
                "L11" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,1)
                "L12" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,2)
                "L13" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,3)
                "L14" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,4)
                "L20" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,0)
                "L21" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,1)
                "L22" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,2)
                "L23" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,3)
                "L24" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,4)
                "L30" -> InterpolationFromFiveTabularValues(L32,L31,L30,L3P1,L3P2,0)
                "L31" -> InterpolationFromFiveTabularValues(L32,L31,L30,L3P1,L3P2,1)
                "L32" -> InterpolationFromFiveTabularValues(L32,L31,L30,L3P1,L3P2,2)
                "L33" -> InterpolationFromFiveTabularValues(L32,L31,L30,L3P1,L3P2,3)
                "L34" -> InterpolationFromFiveTabularValues(L32,L31,L30,L3P1,L3P2,4)
                "M0" -> InterpolationFromFiveTabularValues(MM2,MM1,M00,MP1,MP2,0)
                "M1" -> InterpolationFromFiveTabularValues(MM2,MM1,M00,MP1,MP2,1)
                "M2" -> InterpolationFromFiveTabularValues(MM2,MM1,M00,MP1,MP2,2)
                "M3" -> InterpolationFromFiveTabularValues(MM2,MM1,M00,MP1,MP2,3)
                "M4" -> InterpolationFromFiveTabularValues(MM2,MM1,M00,MP1,MP2,4)
                "HP0" -> InterpolationFromFiveTabularValues(HPbM2,HPbM1,HPb00,HPbP1,HPbP2,0)
                "HP1" -> InterpolationFromFiveTabularValues(HPbM2,HPbM1,HPb00,HPbP1,HPbP2,1)
                "HP2" -> InterpolationFromFiveTabularValues(HPbM2,HPbM1,HPb00,HPbP1,HPbP2,2)
                "HP3" -> InterpolationFromFiveTabularValues(HPbM2,HPbM1,HPb00,HPbP1,HPbP2,3)
                "HP4" -> InterpolationFromFiveTabularValues(HPbM2,HPbM1,HPb00,HPbP1,HPbP2,4)
                "dc0" -> InterpolationFromFiveTabularValues(dbM2,dbM1,db00,dbP1,dbP2,0)
                "dc1" -> InterpolationFromFiveTabularValues(dbM2,dbM1,db00,dbP1,dbP2,1)
                "dc2" -> InterpolationFromFiveTabularValues(dbM2,dbM1,db00,dbP1,dbP2,2)
                "dc3" -> InterpolationFromFiveTabularValues(dbM2,dbM1,db00,dbP1,dbP2,3)
                "dc4" -> InterpolationFromFiveTabularValues(dbM2,dbM1,db00,dbP1,dbP2,4)
                "Sc0" -> InterpolationFromFiveTabularValues(ScM2,ScM1,Sc00,ScP1,ScP2,0)
                "Sc1" -> InterpolationFromFiveTabularValues(ScM2,ScM1,Sc00,ScP1,ScP2,1)
                "Sc2" -> InterpolationFromFiveTabularValues(ScM2,ScM1,Sc00,ScP1,ScP2,2)
                "Sc3" -> InterpolationFromFiveTabularValues(ScM2,ScM1,Sc00,ScP1,ScP2,3)
                "Sc4" -> InterpolationFromFiveTabularValues(ScM2,ScM1,Sc00,ScP1,ScP2,4)

                else	-> Mod(T0 +12,24.0)
            }
        } else {
            Double.NaN
        }
    }
    fun SBesselian (HijriMonth: Byte, HijriYear: Long, OptResult: String): Double {
        var JDESolarEclipse1: Double
        var JDESolarEclipse2: Double
        var T0      : Double
        val DeltaT  : Double
        var ARmM2   : Double
        var ARmM1   : Double
        var ARm00   : Double
        var ARmP1   : Double
        var ARmP2   : Double
        var dmM2    : Double
        var dmM1    : Double
        var dm00    : Double
        var dmP1    : Double
        var dmP2    : Double
        var SdmM2   : Double
        var SdmM1   : Double
        var Sdm00   : Double
        var SdmP1   : Double
        var SdmP2   : Double
        var HPmM2   : Double
        var HPmM1   : Double
        var HPm00   : Double
        var HPmP1   : Double
        var HPmP2   : Double
        var GHAmM2  : Double
        var GHAmM1  : Double
        var GHAm00  : Double
        var GHAmP1  : Double
        var GHAmP2  : Double
        var RmM2    : Double
        var RmM1    : Double
        var Rm00    : Double
        var RmP1    : Double
        var RmP2    : Double
        var ARbM2   : Double
        var ARbM1   : Double
        var ARb00   : Double
        var ARbP1   : Double
        var ARbP2   : Double
        var dbM2    : Double
        var dbM1    : Double
        var db00    : Double
        var dbP1    : Double
        var dbP2    : Double
        var SdbM2   : Double
        var SdbM1   : Double
        var Sdb00   : Double
        var SdbP1   : Double
        var SdbP2   : Double
        var HPbM2   : Double
        var HPbM1   : Double
        var HPb00   : Double
        var HPbP1   : Double
        var HPbP2   : Double
        var GHAbM2  : Double
        var GHAbM1  : Double
        var GHAb00  : Double
        var GHAbP1  : Double
        var GHAbP2  : Double
        var RbM2    : Double
        var RbM1    : Double
        var Rb00    : Double
        var RbP1    : Double
        var RbP2    : Double
        var L12     : Double
        var L11     : Double
        var L10     : Double
        var L1P1    : Double
        var L1P2    : Double
        var L22     : Double
        var L21     : Double
        var L20     : Double
        var L2P1    : Double
        var L2P2    : Double
        var bM2     : Double
        var bM1     : Double
        var b00     : Double
        var bP1     : Double
        var bP2     : Double
        var g1M2    : Double
        var g1M1    : Double
        var g100    : Double
        var g1P1    : Double
        var g1P2    : Double
        var g2M2    : Double
        var g2M1    : Double
        var g200    : Double
        var g2P1    : Double
        var g2P2    : Double
        var g3M2    : Double
        var g3M1    : Double
        var g300    : Double
        var g3P1    : Double
        var g3P2    : Double
        var gM2     : Double
        var gM1     : Double
        var g00     : Double
        var gP1     : Double
        var gP2     : Double
        var aM2     : Double
        var aM1     : Double
        var a00     : Double
        var aP1     : Double
        var aP2     : Double
        var dM2     : Double
        var dM1     : Double
        var d00     : Double
        var dP1     : Double
        var dP2     : Double
        var xM2     : Double
        var xM1     : Double
        var x00     : Double
        var xP1     : Double
        var xP2     : Double
        var yM2     : Double
        var yM1     : Double
        var y00     : Double
        var yP1     : Double
        var yP2     : Double
        var zM2     : Double
        var zM1     : Double
        var z00     : Double
        var zP1     : Double
        var zP2     : Double
        var tanf12  : Double
        var tanf11  : Double
        var tanf10  : Double
        var tanf1P1 : Double
        var tanf1P2 : Double
        var tanf22  : Double
        var tanf21  : Double
        var tanf20  : Double
        var tanf2P1 : Double
        var tanf2P2 : Double

        JDESolarEclipse1 = JDEEclipseModified(HijriMonth,HijriYear,1)
        return if (JDESolarEclipse1 > 0) {
            JDESolarEclipse2 = Math.floor(JDESolarEclipse1) + (((JDESolarEclipse1 - Math.floor(JDESolarEclipse1))*24).round(0)).toDouble()/24.0
            DeltaT  = DeltaT(JDESolarEclipse2)
            T0      = Mod((((JDESolarEclipse2 - Math.floor(JDESolarEclipse2)) * 24).round(0).toDouble()), 24.0)

            ARmM2   = SunApparentRightAscension(JDESolarEclipse2 - 2/24.0)
            ARmM1   = SunApparentRightAscension(JDESolarEclipse2 - 1/24.0)
            ARm00   = SunApparentRightAscension(JDESolarEclipse2)
            ARmP1   = SunApparentRightAscension(JDESolarEclipse2 + 1/24.0)
            ARmP2   = SunApparentRightAscension(JDESolarEclipse2 + 2/24.0)

            dmM2     = SunApparentDeclination(JDESolarEclipse2 - 2/24.0)
            dmM1     = SunApparentDeclination(JDESolarEclipse2 - 1/24.0)
            dm00     = SunApparentDeclination(JDESolarEclipse2)
            dmP1     = SunApparentDeclination(JDESolarEclipse2 + 1/24.0)
            dmP2     = SunApparentDeclination(JDESolarEclipse2 + 2/24.0)

            SdmM2    = SunAngularSemiDiameter(JDESolarEclipse2 - 2/24.0)
            SdmM1    = SunAngularSemiDiameter(JDESolarEclipse2 - 1/24.0)
            Sdm00    = SunAngularSemiDiameter(JDESolarEclipse2)
            SdmP1    = SunAngularSemiDiameter(JDESolarEclipse2 + 1/24.0)
            SdmP2    = SunAngularSemiDiameter(JDESolarEclipse2 + 2/24.0)

            HPmM2    = SunEquatorialHorizontalParallax(JDESolarEclipse2 - 2/24.0)
            HPmM1    = SunEquatorialHorizontalParallax(JDESolarEclipse2 - 1/24.0)
            HPm00    = SunEquatorialHorizontalParallax(JDESolarEclipse2)
            HPmP1    = SunEquatorialHorizontalParallax(JDESolarEclipse2 + 1/24.0)
            HPmP2    = SunEquatorialHorizontalParallax(JDESolarEclipse2 + 2/24.0)

            GHAmM2   = SunGreenwichHourAngle(JDESolarEclipse2 - 2/24.0)
            GHAmM1   = SunGreenwichHourAngle(JDESolarEclipse2 - 1/24.0)
            GHAm00   = SunGreenwichHourAngle(JDESolarEclipse2)
            GHAmP1   = SunGreenwichHourAngle(JDESolarEclipse2 + 1/24.0)
            GHAmP2   = SunGreenwichHourAngle(JDESolarEclipse2 + 2/24.0)

            RmM2   = SunGeocentricDistance(JDESolarEclipse2 - 2/24.0)
            RmM1   = SunGeocentricDistance(JDESolarEclipse2 - 1/24.0)
            Rm00   = SunGeocentricDistance(JDESolarEclipse2)
            RmP1   = SunGeocentricDistance(JDESolarEclipse2 + 1/24.0)
            RmP2   = SunGeocentricDistance(JDESolarEclipse2 + 2/24.0)

            ARbM2    = MoonApparentRightAscension(JDESolarEclipse2 - 2/24.0)
            ARbM1    = MoonApparentRightAscension(JDESolarEclipse2 - 1/24.0)
            ARb00    = MoonApparentRightAscension(JDESolarEclipse2)
            ARbP1    = MoonApparentRightAscension(JDESolarEclipse2 + 1/24.0)
            ARbP2    = MoonApparentRightAscension(JDESolarEclipse2 + 2/24.0)

            dbM2     = MoonApparentDeclination(JDESolarEclipse2 - 2/24.0)
            dbM1     = MoonApparentDeclination(JDESolarEclipse2 - 1/24.0)
            db00     = MoonApparentDeclination(JDESolarEclipse2)
            dbP1     = MoonApparentDeclination(JDESolarEclipse2 + 1/24.0)
            dbP2     = MoonApparentDeclination(JDESolarEclipse2 + 2/24.0)

            SdbM2    = MoonAngularSemiDiameter(JDESolarEclipse2 - 2/24.0)
            SdbM1    = MoonAngularSemiDiameter(JDESolarEclipse2 - 1/24.0)
            Sdb00    = MoonAngularSemiDiameter(JDESolarEclipse2)
            SdbP1    = MoonAngularSemiDiameter(JDESolarEclipse2 + 1/24.0)
            SdbP2    = MoonAngularSemiDiameter(JDESolarEclipse2 + 2/24.0)

            HPbM2    = MoonEquatorialHorizontalParallax(JDESolarEclipse2 - 2/24.0)
            HPbM1    = MoonEquatorialHorizontalParallax(JDESolarEclipse2 - 1/24.0)
            HPb00    = MoonEquatorialHorizontalParallax(JDESolarEclipse2)
            HPbP1    = MoonEquatorialHorizontalParallax(JDESolarEclipse2 + 1/24.0)
            HPbP2    = MoonEquatorialHorizontalParallax(JDESolarEclipse2 + 2/24.0)

            GHAbM2   = MoonGreenwichHourAngle(JDESolarEclipse2 - 2/24.0)
            GHAbM1   = MoonGreenwichHourAngle(JDESolarEclipse2 - 1/24.0)
            GHAb00   = MoonGreenwichHourAngle(JDESolarEclipse2)
            GHAbP1   = MoonGreenwichHourAngle(JDESolarEclipse2 + 1/24.0)
            GHAbP2   = MoonGreenwichHourAngle(JDESolarEclipse2 + 2/24.0)

            RbM2     = MoonGeocentricDistance(JDESolarEclipse2 - 2/24.0)/149597870.7
            RbM1     = MoonGeocentricDistance(JDESolarEclipse2 - 1/24.0)/149597870.7
            Rb00     = MoonGeocentricDistance(JDESolarEclipse2)/149597870.7
            RbP1     = MoonGeocentricDistance(JDESolarEclipse2 + 1/24.0)/149597870.7
            RbP2     = MoonGeocentricDistance(JDESolarEclipse2 + 2/24.0)/149597870.7
//
//    bM2 = Math.sin(Rad(HPmM2/3600.0))/RmM2/Math.sin(Rad(HPbM2))
//    bM1 = Math.sin(Rad(HPmM1/3600.0))/RmM1/Math.sin(Rad(HPbM1))
//    b00 = Math.sin(Rad(HPm00/3600.0))/Rm00/Math.sin(Rad(HPb00))
//    bP1 = Math.sin(Rad(HPmP1/3600.0))/RmP1/Math.sin(Rad(HPbP1))
//    bP2 = Math.sin(Rad(HPmP2/3600.0))/RmP2/Math.sin(Rad(HPbP2))
//
            bM2 = Math.sin(Rad(8.794/3600.0))/RmM2/Math.sin(Rad(HPbM2))
            bM1 = Math.sin(Rad(8.794/3600.0))/RmM1/Math.sin(Rad(HPbM1))
            b00 = Math.sin(Rad(8.794/3600.0))/Rm00/Math.sin(Rad(HPb00))
            bP1 = Math.sin(Rad(8.794/3600.0))/RmP1/Math.sin(Rad(HPbP1))
            bP2 = Math.sin(Rad(8.794/3600.0))/RmP2/Math.sin(Rad(HPbP2))

//    bM2 = 0.0000426636/RmM2/Math.sin(Rad(HPbM2))
//    bM1 = 0.0000426636/RmM1/Math.sin(Rad(HPbM1))
//    b00 = 0.0000426636/Rm00/Math.sin(Rad(HPb00))
//    bP1 = 0.0000426636/RmP1/Math.sin(Rad(HPbP1))
//    bP2 = 0.0000426636/RmP2/Math.sin(Rad(HPbP2))

            g1M2 = Math.cos(Rad(dmM2))*Math.cos(Rad(ARmM2))-bM2*Math.cos(Rad(dbM2))*Math.cos(Rad(ARbM2))
            g1M1 = Math.cos(Rad(dmM1))*Math.cos(Rad(ARmM1))-bM1*Math.cos(Rad(dbM1))*Math.cos(Rad(ARbM1))
            g100 = Math.cos(Rad(dm00))*Math.cos(Rad(ARm00))-b00*Math.cos(Rad(db00))*Math.cos(Rad(ARb00))
            g1P1 = Math.cos(Rad(dmP1))*Math.cos(Rad(ARmP1))-bP1*Math.cos(Rad(dbP1))*Math.cos(Rad(ARbP1))
            g1P2 = Math.cos(Rad(dmP2))*Math.cos(Rad(ARmP2))-bP2*Math.cos(Rad(dbP2))*Math.cos(Rad(ARbP2))

            g2M2 = Math.cos(Rad(dmM2))*Math.sin(Rad(ARmM2))-bM2*Math.cos(Rad(dbM2))*Math.sin(Rad(ARbM2))
            g2M1 = Math.cos(Rad(dmM1))*Math.sin(Rad(ARmM1))-bM1*Math.cos(Rad(dbM1))*Math.sin(Rad(ARbM1))
            g200 = Math.cos(Rad(dm00))*Math.sin(Rad(ARm00))-b00*Math.cos(Rad(db00))*Math.sin(Rad(ARb00))
            g2P1 = Math.cos(Rad(dmP1))*Math.sin(Rad(ARmP1))-bP1*Math.cos(Rad(dbP1))*Math.sin(Rad(ARbP1))
            g2P2 = Math.cos(Rad(dmP2))*Math.sin(Rad(ARmP2))-bP2*Math.cos(Rad(dbP2))*Math.sin(Rad(ARbP2))

            g3M2 = Math.sin(Rad(dmM2))-bM2*Math.sin(Rad(dbM2))
            g3M1 = Math.sin(Rad(dmM1))-bM1*Math.sin(Rad(dbM1))
            g300 = Math.sin(Rad(dm00))-b00*Math.sin(Rad(db00))
            g3P1 = Math.sin(Rad(dmP1))-bP1*Math.sin(Rad(dbP1))
            g3P2 = Math.sin(Rad(dmP2))-bP2*Math.sin(Rad(dbP2))

            gM2 = Math.sqrt(g1M2*g1M2+g2M2*g2M2+g3M2*g3M2)
            gM1 = Math.sqrt(g1M1*g1M1+g2M1*g2M1+g3M1*g3M1)
            g00 = Math.sqrt(g100*g100+g200*g200+g300*g300)
            gP1 = Math.sqrt(g1P1*g1P1+g2P1*g2P1+g3P1*g3P1)
            gP2 = Math.sqrt(g1P2*g1P2+g2P2*g2P2+g3P2*g3P2)

            aM2 = Mod(Deg(Math.atan2(g2M2,g1M2)),360.0)
            aM1 = Mod(Deg(Math.atan2(g2M1,g1M1)),360.0)
            a00 = Mod(Deg(Math.atan2(g200,g100)),360.0)
            aP1 = Mod(Deg(Math.atan2(g2P1,g1P1)),360.0)
            aP2 = Mod(Deg(Math.atan2(g2P2,g1P2)),360.0)

            dM2 = Deg(Math.atan(g3M2/Math.sqrt(g1M2*g1M2+g2M2*g2M2)))
            dM1 = Deg(Math.atan(g3M1/Math.sqrt(g1M1*g1M1+g2M1*g2M1)))
            d00 = Deg(Math.atan(g300/Math.sqrt(g100*g100+g200*g200)))
            dP1 = Deg(Math.atan(g3P1/Math.sqrt(g1P1*g1P1+g2P1*g2P1)))
            dP2 = Deg(Math.atan(g3P2/Math.sqrt(g1P2*g1P2+g2P2*g2P2)))

            xM2 = (Math.cos(Rad(dbM2))*Math.sin(Rad(ARbM2-aM2)))/Math.sin(Rad(HPbM2))
            xM1 = (Math.cos(Rad(dbM1))*Math.sin(Rad(ARbM1-aM1)))/Math.sin(Rad(HPbM1))
            x00 = (Math.cos(Rad(db00))*Math.sin(Rad(ARb00-a00)))/Math.sin(Rad(HPb00))
            xP1 = (Math.cos(Rad(dbP1))*Math.sin(Rad(ARbP1-aP1)))/Math.sin(Rad(HPbP1))
            xP2 = (Math.cos(Rad(dbP2))*Math.sin(Rad(ARbP2-aP2)))/Math.sin(Rad(HPbP2))

            yM2 = (Math.sin(Rad(dbM2))*Math.cos(Rad(dM2))-Math.cos(Rad(dbM2))*Math.sin(Rad(dM2))*Math.cos(Rad(ARbM2-aM2)))/Math.sin(Rad(HPbM2))
            yM1 = (Math.sin(Rad(dbM1))*Math.cos(Rad(dM1))-Math.cos(Rad(dbM1))*Math.sin(Rad(dM1))*Math.cos(Rad(ARbM1-aM1)))/Math.sin(Rad(HPbM1))
            y00 = (Math.sin(Rad(db00))*Math.cos(Rad(d00))-Math.cos(Rad(db00))*Math.sin(Rad(d00))*Math.cos(Rad(ARb00-a00)))/Math.sin(Rad(HPb00))
            yP1 = (Math.sin(Rad(dbP1))*Math.cos(Rad(dP1))-Math.cos(Rad(dbP1))*Math.sin(Rad(dP1))*Math.cos(Rad(ARbP1-aP1)))/Math.sin(Rad(HPbP1))
            yP2 = (Math.sin(Rad(dbP2))*Math.cos(Rad(dP2))-Math.cos(Rad(dbP2))*Math.sin(Rad(dP2))*Math.cos(Rad(ARbP2-aP2)))/Math.sin(Rad(HPbP2))

            zM2 = (Math.sin(Rad(dbM2))*Math.sin(Rad(dM2))+Math.cos(Rad(dbM2))*Math.cos(Rad(dM2))*Math.cos(Rad(ARbM2-aM2)))/Math.sin(Rad(HPbM2))
            zM1 = (Math.sin(Rad(dbM1))*Math.sin(Rad(dM1))+Math.cos(Rad(dbM1))*Math.cos(Rad(dM1))*Math.cos(Rad(ARbM1-aM1)))/Math.sin(Rad(HPbM1))
            z00 = (Math.sin(Rad(db00))*Math.sin(Rad(d00))+Math.cos(Rad(db00))*Math.cos(Rad(d00))*Math.cos(Rad(ARb00-a00)))/Math.sin(Rad(HPb00))
            zP1 = (Math.sin(Rad(dbP1))*Math.sin(Rad(dP1))+Math.cos(Rad(dbP1))*Math.cos(Rad(dP1))*Math.cos(Rad(ARbP1-aP1)))/Math.sin(Rad(HPbP1))
            zP2 = (Math.sin(Rad(dbP2))*Math.sin(Rad(dP2))+Math.cos(Rad(dbP2))*Math.cos(Rad(dP2))*Math.cos(Rad(ARbP2-aP2)))/Math.sin(Rad(HPbP2))

            tanf12  = Math.tan(Math.asin(0.004664019/gM2/RmM2))
            tanf11  = Math.tan(Math.asin(0.004664019/gM1/RmM1))
            tanf10  = Math.tan(Math.asin(0.004664019/g00/Rm00))
            tanf1P1 = Math.tan(Math.asin(0.004664019/gP1/RmP1))
            tanf1P2 = Math.tan(Math.asin(0.004664019/gP2/RmP2))

            tanf22  = Math.tan(Math.asin(0.004640792/gM2/RmM2))
            tanf21  = Math.tan(Math.asin(0.004640792/gM1/RmM1))
            tanf20  = Math.tan(Math.asin(0.004640792/g00/Rm00))
            tanf2P1 = Math.tan(Math.asin(0.004640792/gP1/RmP1))
            tanf2P2 = Math.tan(Math.asin(0.004640792/gP2/RmP2))

            L12     = tanf12 * (zM2+0.2725076/(0.004664019/gM2/RmM2))
            L11     = tanf11 * (zM1+0.2725076/(0.004664019/gM1/RmM1))
            L10     = tanf10 * (z00+0.2725076/(0.004664019/g00/Rm00))
            L1P1    = tanf1P1* (zP1+0.2725076/(0.004664019/gP1/RmP1))
            L1P2    = tanf1P2* (zP2+0.2725076/(0.004664019/gP2/RmP2))

            L22     = tanf22 * (zM2-0.2722810/(0.004640792/gM2/RmM2))
            L21     = tanf21 * (zM1-0.2722810/(0.004640792/gM1/RmM1))
            L20     = tanf20 * (z00-0.2722810/(0.004640792/g00/Rm00))
            L2P1    = tanf2P1* (zP1-0.2722810/(0.004640792/gP1/RmP1))
            L2P2    = tanf2P2* (zP2-0.2722810/(0.004640792/gP2/RmP2))

            return when(OptResult) {
                "JDS" -> JDESolarEclipse2
                "DT"  -> DeltaT
                "x0" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,0)
                "x1" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,1)
                "x2" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,2)
                "x3" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,3)
                "x4" -> InterpolationFromFiveTabularValues(xM2,xM1,x00,xP1,xP2,4)
                "y0" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,0)
                "y1" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,1)
                "y2" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,2)
                "y3" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,3)
                "y4" -> InterpolationFromFiveTabularValues(yM2,yM1,y00,yP1,yP2,4)
                "dm0" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,0)
                "dm1" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,1)
                "dm2" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,2)
                "dm3" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,3)
                "dm4" -> InterpolationFromFiveTabularValues(dmM2,dmM1,dm00,dmP1,dmP2,4)
                "L10" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,0)
                "L11" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,1)
                "L12" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,2)
                "L13" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,3)
                "L14" -> InterpolationFromFiveTabularValues(L12,L11,L10,L1P1,L1P2,4)
                "L20" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,0)
                "L21" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,1)
                "L22" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,2)
                "L23" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,3)
                "L24" -> InterpolationFromFiveTabularValues(L22,L21,L20,L2P1,L2P2,4)
                "M0" -> InterpolationFromFiveTabularValues(GHAmM2,GHAmM1,GHAm00,GHAmP1,GHAmP2,0)
                "M1" -> InterpolationFromFiveTabularValues(GHAmM2,GHAmM1,GHAm00,GHAmP1,GHAmP2,1)
                "M2" -> InterpolationFromFiveTabularValues(GHAmM2,GHAmM1,GHAm00,GHAmP1,GHAmP2,2)
                "M3" -> InterpolationFromFiveTabularValues(GHAmM2,GHAmM1,GHAm00,GHAmP1,GHAmP2,3)
                "M4" -> InterpolationFromFiveTabularValues(GHAmM2,GHAmM1,GHAm00,GHAmP1,GHAmP2,4)
                "f1" -> tanf10
                "f2" -> tanf20

                else	-> Mod(T0 +12,24.0)
            }
        } else {
            Double.NaN
        }
    }

    fun InterpolationFromFiveTabularValues(
        xM2: Double, xM1: Double, x00: Double, xP1: Double, xP2: Double,
        OptResult: Int
    ): Double {
        var A: Double
        var B: Double
        var C: Double
        var D: Double
        var E: Double
        var F: Double
        var G: Double
        var H: Double
        var J: Double
        var K: Double

        A = xM1 - xM2
        B = x00 - xM1
        C = xP1 - x00
        D = xP2 - xP1
        E =   B - A
        F =   C - B
        G =   D - C
        H =   F - E
        J =   G - F
        K =   J - H

        return when(OptResult) {
            0     -> x00
            1     -> ((B + C) / 2 - (H + J) / 12)
            2     -> (F / 2 - K / 24)
            3     -> ((H + J) / 12)
            4     -> (K / 24)
            else  -> x00
        }
    }
    fun JDSolarEclipseMax(HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        var JDESolarEclipse1 : Double
        var JDESolarEclipse2 : Double
        var JDSolarEclipseMax: Double
        var DeltaT: Double
        var T0  : Double
        var x0  : Double
        var x1  : Double
        var x2  : Double
        var x3  : Double
        var x4  : Double
        var y0  : Double
        var y1  : Double
        var y2  : Double
        var y3  : Double
        var y4  : Double
        var d0  : Double
        var d1  : Double
        var d2  : Double
        var d3  : Double
        var d4  : Double
        var M0  : Double
        var M1  : Double
        var M2  : Double
        var M3  : Double
        var M4  : Double
        var L10 : Double
        var L11 : Double
        var L12 : Double
        var L13 : Double
        var L14 : Double
        var L20 : Double
        var L21 : Double
        var L22 : Double
        var L23 : Double
        var L24 : Double

        var T   : Double = 0.0
        var P_  : Double = 0.0
        var S   : Double = 0.0
        var C   : Double = 0.0
        var x   : Double = 0.0
        var y   : Double = 0.0
        var d   : Double = 0.0
        var M   : Double = 0.0
        var L1  : Double = 0.0
        var L2  : Double = 0.0
        var x_  : Double = 0.0
        var y_  : Double = 0.0
        var H   : Double = 0.0
        var p   : Double = 0.0
        var q   : Double = 0.0
        var r   : Double = 0.0
        var p_  : Double = 0.0
        var q_  : Double = 0.0
        var u   : Double = 0.0
        var v   : Double = 0.0
        var a   : Double = 0.0
        var b   : Double = 0.0
        var n   : Double = 0.0
        var P   : Double = 0.0
        var Tx  : Double = 0.0

        x0 = SBesselian(HijriMonth, HijriYear, "x0")
        x1 = SBesselian(HijriMonth, HijriYear, "x1")
        x2 = SBesselian(HijriMonth, HijriYear, "x2")
        x3 = SBesselian(HijriMonth, HijriYear, "x3")
        x4 = SBesselian(HijriMonth, HijriYear, "x4")
        y0 = SBesselian(HijriMonth, HijriYear, "y0")
        y1 = SBesselian(HijriMonth, HijriYear, "y1")
        y2 = SBesselian(HijriMonth, HijriYear, "y2")
        y3 = SBesselian(HijriMonth, HijriYear, "y3")
        y4 = SBesselian(HijriMonth, HijriYear, "y4")
        d0 = SBesselian(HijriMonth, HijriYear, "dm0")
        d1 = SBesselian(HijriMonth, HijriYear, "dm1")
        d2 = SBesselian(HijriMonth, HijriYear, "dm2")
        d3 = SBesselian(HijriMonth, HijriYear, "dm3")
        d4 = SBesselian(HijriMonth, HijriYear, "dm4")
        M0 = SBesselian(HijriMonth, HijriYear, "M0")
        M1 = SBesselian(HijriMonth, HijriYear, "M1")
        M2 = SBesselian(HijriMonth, HijriYear, "M2")
        M3 = SBesselian(HijriMonth, HijriYear, "M3")
        M4 = SBesselian(HijriMonth, HijriYear, "M4")
        L10 = SBesselian(HijriMonth, HijriYear, "L10")
        L11 = SBesselian(HijriMonth, HijriYear, "L11")
        L12 = SBesselian(HijriMonth, HijriYear, "L12")
        L13 = SBesselian(HijriMonth, HijriYear, "L13")
        L14 = SBesselian(HijriMonth, HijriYear, "L14")
        L20 = SBesselian(HijriMonth, HijriYear, "L20")
        L21 = SBesselian(HijriMonth, HijriYear, "L21")
        L22 = SBesselian(HijriMonth, HijriYear, "L22")
        L23 = SBesselian(HijriMonth, HijriYear, "L23")
        L24 = SBesselian(HijriMonth, HijriYear, "L24")

        JDESolarEclipse1 = JDEEclipseModified(HijriMonth,HijriYear,1)
        JDESolarEclipse2 = Math.floor(JDESolarEclipse1) + (((JDESolarEclipse1 - Math.floor(JDESolarEclipse1)) * 24).round(0)).toDouble() / 24.0

        DeltaT = DeltaT(JDESolarEclipse2)
        T0 = ((JDESolarEclipse2 + 0.5 + (0.0/ 24.0)-Math.floor(JDESolarEclipse2 + 0.5 + (0.0 / 24.0)))*24).round(0).toDouble()

        T = T + P
        for (i in 1..5) {
            T = T
            P_ = Math.atan(0.99664719 * Math.tan(Rad(gLat)))
            S = 0.99664719 * Math.sin(P_) + (gAlt / 6378140) * Math.sin(Rad(gLat))
            C = Math.cos(P_) + (gAlt / 6378140) * Math.cos(Rad(gLat))
            x   = x0  + x1*T  + x2*T*T  + x3*T*T*T  + x4*T*T*T*T
            y   = y0  + y1*T  + y2*T*T  + y3*T*T*T  + y4*T*T*T*T
            d   = d0  + d1*T  + d2*T*T  + d3*T*T*T  + d4*T*T*T*T
            M   = M0  + M1*T  + M2*T*T  + M3*T*T*T  + M4*T*T*T*T
            L1  = L10	+ L11*T + L12*T*T + L13*T*T*T + L14*T*T*T*T
            L2  = L20	+ L21*T + L22*T*T + L23*T*T*T + L24*T*T*T*T
            x_  = x1 + 2*x2*T + 3*x3*T*T + 4*x4*T*T*T
            y_  = y1 + 2*y2*T + 3*y3*T*T + 4*y4*T*T*T
            H = Rad(M + gLon.toDouble() - 0.00417807 * DeltaT.toDouble())
            p = C * Math.sin(H)
            q = S * Math.cos(Rad(d)) - C * Math.cos(H) * Math.sin(Rad(d))
            r = S * Math.sin(Rad(d)) + C * Math.cos(H) * Math.cos(Rad(d))
            p_ = 0.01745329 * M1 * C * Math.cos(H)
            q_ = 0.01745329 * (M1 * p * Math.sin(Rad(d)) - r * d1)
            u = x - p
            v = y - q
            a = x_ - p_
            b = y_ - q_
            n = a * a + b * b
            P = -(u * a + v * b) / n
            T = T + P
        }
        Tx = T0 + T - DeltaT(JDESolarEclipse2) / 3600.0
        JDSolarEclipseMax = Math.floor(JDESolarEclipse2 + 0.5) - 0.5 + (Tx /24.0)
        return when (OptResult) {
            "JDS"   -> JDESolarEclipse2
            "T0"    -> T0
            "T"     -> T
            "Tx"    -> Tx
            "r"     -> r
            "u"     -> u
            "v"     -> v
            "a"     -> a
            "b"     -> b
            "n"     -> n
            "L1"    -> L1
            "L2"    -> L2
            "JDSMx" -> JDSolarEclipseMax
            else    -> JDSolarEclipseMax
        }
    }
    fun SolarEclipseObscuration(HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        var r   : Double
        var u   : Double
        var v   : Double
        var L1  : Double
        var L2  : Double
        var a   : Double
        var b   : Double
        var n   : Double
        var m   : Double
        var L1_ : Double
        var L2_ : Double
        var tanf1 : Double
        var tanf2 : Double
        var e   : Double
        var z   : Double
        var Tau : Double
        var Mag : Double
        var r_  : Double
        var s_  : Double
        var y   : Double
        var z_  : Double
        var B   : Double
        var C   : Double
        var Obs : Double

        r       = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "r")
        u       = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "u")
        v       = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "v")
        L1      = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "L1")
        L2      = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "L2")
        a       = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "a")
        b       = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "b")
        n       = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "n")
        tanf1   = SBesselian(HijriMonth,HijriYear,"f1")
        tanf2   = SBesselian(HijriMonth,HijriYear,"f2")

        m       = Math.sqrt(u * u + v * v)
        L1_     = L1 - r * tanf1
        L2_     = L2 - r * tanf2
        e       = Math.sqrt(n)
        z       = (a * v - u * b) / (e * L1_)
        Tau     = (L1_ / e) * Math.sqrt(1 - z * z)
        Mag     = (L1_ - m) / (L1_ + L2_)
        r_      = 2 * m / (L1_ + L2_)
        s_      = (L1_ - L2_) / (L1_ + L2_)
        y       = (s_ * s_ + r_ * r_ - 1) / (2 * r_ * s_)
        z_      = (r_ * r_ - s_ * s_ + 1) / (2 * 1 * r_)
        B       = if (Math.abs(y) > 1) {Math.acos(Sign(y).toDouble())} else Math.acos(y)
        C       = if (Math.abs(z_) > 1) {Math.acos(Sign(z_).toDouble())} else Math.acos(z_)
        Obs     = ((s_ * s_ * (B - Math.sin(2 * B) / 2) + (C - Math.sin(2 * C) / 2)) / 3.141592654) * 100

        return when (OptResult) {
            "m"   -> m
            "L2_" -> L2_
            "e"   -> e
            "Tau" -> Tau
            "Mag" -> Mag
            "Obs" -> Obs
            else  -> Obs
        }
    }
    fun JenisSolarEclipse (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double): String {
        val Mag : Double
        val m   : Double
        val L2_ : Double
        val JSE : String

        Mag = SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Mag")
        m   = SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"m")
        L2_ = SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"L2_")

        JSE = if (Mag > 0 && m > Math.abs(L2_)) {
            "GERHANA MATAHARI SEBAGIAN"
        } else if (Mag > 0 && m < Math.abs(L2_)) {
            "GERHANA MATAHARI TOTAL"
        } else if (Mag > 0 && m < Math.abs(L2_) && L2_ > 0) {
            "GERHANA MATAHARI CINCIN"
        } else {
            "TIDAK TERJADI GERHANA"
        }
        return JSE
    }
    fun JDSolarEclipseU2U3(HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        var JDESolarEclipse1 : Double
        var JDESolarEclipse2 : Double
        var JDSolarEclipseU2 : Double
        var JDSolarEclipseU3 : Double
        var a   : Double
        var b   : Double
        var v   : Double
        var u   : Double
        var e   : Double
        var L2_ : Double
        var q   : Double
        var T2  : Double
        var Tx  : Double
        var AU2 : Double
        var AU3 : Double

        JDESolarEclipse1 = JDEEclipseModified(HijriMonth,HijriYear,1)
        JDESolarEclipse2 = Math.floor(JDESolarEclipse1) + (((JDESolarEclipse1 - Math.floor(JDESolarEclipse1)) * 24).round(0)).toDouble() / 24.0
        u   = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "u")
        v   = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "v")
        a   = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "a")
        b   = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "b")
        e   = SolarEclipseObscuration(HijriMonth, HijriYear, gLat, gLon, gAlt, "e")
        L2_ = SolarEclipseObscuration(HijriMonth, HijriYear, gLat, gLon, gAlt, "L2_")
        Tx  = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "Tx")
        q   = (a * v - u * b)/(e * L2_)
        T2  = Math.abs((L2_ / e) * Math.sqrt(1-q*q))
        AU2 = Tx - T2
        AU3 = Tx + T2

        JDSolarEclipseU2 = Math.floor(JDESolarEclipse2 + 0.5) - 0.5 + (AU2 /24.0)
        JDSolarEclipseU3 = Math.floor(JDESolarEclipse2 + 0.5) - 0.5 + (AU3 /24.0)

        return when (OptResult) {
            "JDSU2" -> JDSolarEclipseU2
            "JDSU3" -> JDSolarEclipseU3
            else   -> JDSolarEclipseU2
        }
    }
    fun JDSolarEclipseU1(HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        var JDESolarEclipse1 : Double
        var JDESolarEclipse2 : Double
        var JDSolarEclipseU1: Double
        var DeltaT: Double

        var x0  : Double
        var x1  : Double
        var x2  : Double
        var x3  : Double
        var x4  : Double
        var y0  : Double
        var y1  : Double
        var y2  : Double
        var y3  : Double
        var y4  : Double
        var d0  : Double
        var d1  : Double
        var d2  : Double
        var d3  : Double
        var d4  : Double
        var M0  : Double
        var M1  : Double
        var M2  : Double
        var M3  : Double
        var M4  : Double
        var L10 : Double
        var L11 : Double
        var L12 : Double
        var L13 : Double
        var L14 : Double
        var L20 : Double
        var L21 : Double
        var L22 : Double
        var L23 : Double
        var L24 : Double
        var tanf1 : Double
        var AU1   : Double

        var T0  : Double
        var T   : Double = 0.0
        var Tau : Double = 0.0
        var P_  : Double = 0.0
        var S   : Double = 0.0
        var C   : Double = 0.0
        var x   : Double = 0.0
        var y   : Double = 0.0
        var d   : Double = 0.0
        var M   : Double = 0.0
        var L1  : Double = 0.0
        var L2  : Double = 0.0
        var x_  : Double = 0.0
        var y_  : Double = 0.0
        var H   : Double = 0.0
        var p   : Double = 0.0
        var q   : Double = 0.0
        var r   : Double = 0.0
        var p_  : Double = 0.0
        var q_  : Double = 0.0
        var u   : Double = 0.0
        var v   : Double = 0.0
        var a   : Double = 0.0
        var b   : Double = 0.0
        var n   : Double = 0.0
        var e   : Double = 0.0
        var L1_ : Double = 0.0
        var m   : Double = 0.0
        var P   : Double = 0.0

        x0 = SBesselian(HijriMonth, HijriYear, "x0")
        x1 = SBesselian(HijriMonth, HijriYear, "x1")
        x2 = SBesselian(HijriMonth, HijriYear, "x2")
        x3 = SBesselian(HijriMonth, HijriYear, "x3")
        x4 = SBesselian(HijriMonth, HijriYear, "x4")
        y0 = SBesselian(HijriMonth, HijriYear, "y0")
        y1 = SBesselian(HijriMonth, HijriYear, "y1")
        y2 = SBesselian(HijriMonth, HijriYear, "y2")
        y3 = SBesselian(HijriMonth, HijriYear, "y3")
        y4 = SBesselian(HijriMonth, HijriYear, "y4")
        d0 = SBesselian(HijriMonth, HijriYear, "dm0")
        d1 = SBesselian(HijriMonth, HijriYear, "dm1")
        d2 = SBesselian(HijriMonth, HijriYear, "dm2")
        d3 = SBesselian(HijriMonth, HijriYear, "dm3")
        d4 = SBesselian(HijriMonth, HijriYear, "dm4")
        M0 = SBesselian(HijriMonth, HijriYear, "M0")
        M1 = SBesselian(HijriMonth, HijriYear, "M1")
        M2 = SBesselian(HijriMonth, HijriYear, "M2")
        M3 = SBesselian(HijriMonth, HijriYear, "M3")
        M4 = SBesselian(HijriMonth, HijriYear, "M4")
        L10 = SBesselian(HijriMonth, HijriYear, "L10")
        L11 = SBesselian(HijriMonth, HijriYear, "L11")
        L12 = SBesselian(HijriMonth, HijriYear, "L12")
        L13 = SBesselian(HijriMonth, HijriYear, "L13")
        L14 = SBesselian(HijriMonth, HijriYear, "L14")
        L20 = SBesselian(HijriMonth, HijriYear, "L20")
        L21 = SBesselian(HijriMonth, HijriYear, "L21")
        L22 = SBesselian(HijriMonth, HijriYear, "L22")
        L23 = SBesselian(HijriMonth, HijriYear, "L23")
        L24 = SBesselian(HijriMonth, HijriYear, "L24")
        tanf1 = SBesselian(HijriMonth, HijriYear, "f1")

        JDESolarEclipse1 = JDEEclipseModified(HijriMonth,HijriYear,1)
        JDESolarEclipse2 = Math.floor(JDESolarEclipse1) + (((JDESolarEclipse1 - Math.floor(JDESolarEclipse1)) * 24).round(0)).toDouble() / 24.0
        DeltaT = DeltaT(JDESolarEclipse2)

        T0  = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "T0")
        T   = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "T")
        Tau = SolarEclipseObscuration(HijriMonth, HijriYear, gLat, gLon, gAlt, "Tau")

        T = T - Tau
        for (i in 1..5) {
            T = T
            P_ = Math.atan(0.99664719 * Math.tan(Rad(gLat)))
            S = 0.99664719 * Math.sin(P_) + (gAlt / 6378140) * Math.sin(Rad(gLat))
            C = Math.cos(P_) + (gAlt / 6378140) * Math.cos(Rad(gLat))
            x   = x0  + x1*T  + x2*T*T  + x3*T*T*T  + x4*T*T*T*T
            y   = y0  + y1*T  + y2*T*T  + y3*T*T*T  + y4*T*T*T*T
            d   = d0  + d1*T  + d2*T*T  + d3*T*T*T  + d4*T*T*T*T
            M   = M0  + M1*T  + M2*T*T  + M3*T*T*T  + M4*T*T*T*T
            L1  = L10	+ L11*T + L12*T*T + L13*T*T*T + L14*T*T*T*T
            L2  = L20	+ L21*T + L22*T*T + L23*T*T*T + L24*T*T*T*T
            x_  = x1 + 2*x2*T + 3*x3*T*T + 4*x4*T*T*T
            y_  = y1 + 2*y2*T + 3*y3*T*T + 4*y4*T*T*T
            H = Rad(M + gLon.toDouble() - 0.00417807 * DeltaT.toDouble())
            p = C * Math.sin(H)
            q = S * Math.cos(Rad(d)) - C * Math.cos(H) * Math.sin(Rad(d))
            r = S * Math.sin(Rad(d)) + C * Math.cos(H) * Math.cos(Rad(d))
            p_ = 0.01745329 * M1 * C * Math.cos(H)
            q_ = 0.01745329 * (M1 * p * Math.sin(Rad(d)) - r * d1)
            u = x - p
            v = y - q
            a = x_ - p_
            b = y_ - q_
            n = a * a + b * b
            e = Math.sqrt(n)
            L1_ = L1 - r * tanf1
            m = (a * v - u * b) / (e * L1_)
            P = -(u * a + v * b) /n - (L1_ / e ) * Math.sqrt(1-m*m)
            T = T + P
        }
        AU1 = T + P - DeltaT(JDESolarEclipse2) / 3600.0
        JDSolarEclipseU1 = Math.floor(JDESolarEclipse2 + 0.5) - 0.5 + ((T0 + AU1)/24.0)

        return when (OptResult) {
            "JDS"   -> JDESolarEclipse2
            "AU1"   -> AU1
            "JDSU1" -> JDSolarEclipseU1
            else    -> JDSolarEclipseU1
        }
    }
    fun JDSolarEclipseU4(HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        var JDESolarEclipse1 : Double
        var JDESolarEclipse2 : Double
        var DeltaT: Double

        var x0  : Double
        var x1  : Double
        var x2  : Double
        var x3  : Double
        var x4  : Double
        var y0  : Double
        var y1  : Double
        var y2  : Double
        var y3  : Double
        var y4  : Double
        var d0  : Double
        var d1  : Double
        var d2  : Double
        var d3  : Double
        var d4  : Double
        var M0  : Double
        var M1  : Double
        var M2  : Double
        var M3  : Double
        var M4  : Double
        var L10 : Double
        var L11 : Double
        var L12 : Double
        var L13 : Double
        var L14 : Double
        var L20 : Double
        var L21 : Double
        var L22 : Double
        var L23 : Double
        var L24 : Double
        var tanf1 : Double
        var AU4   : Double
        var JDSU4 : Double

        var T0  : Double
        var Tx  : Double
        var T   : Double = 0.0
        var Tau : Double = 0.0
        var P_  : Double = 0.0
        var S   : Double = 0.0
        var C   : Double = 0.0
        var x   : Double = 0.0
        var y   : Double = 0.0
        var d   : Double = 0.0
        var M   : Double = 0.0
        var L1  : Double = 0.0
        var L2  : Double = 0.0
        var x_  : Double = 0.0
        var y_  : Double = 0.0
        var H   : Double = 0.0
        var p   : Double = 0.0
        var q   : Double = 0.0
        var r   : Double = 0.0
        var p_  : Double = 0.0
        var q_  : Double = 0.0
        var u   : Double = 0.0
        var v   : Double = 0.0
        var a   : Double = 0.0
        var b   : Double = 0.0
        var n   : Double = 0.0
        var e   : Double = 0.0
        var L1_ : Double = 0.0
        var m   : Double = 0.0
        var P   : Double = 0.0

        x0 = SBesselian(HijriMonth, HijriYear, "x0")
        x1 = SBesselian(HijriMonth, HijriYear, "x1")
        x2 = SBesselian(HijriMonth, HijriYear, "x2")
        x3 = SBesselian(HijriMonth, HijriYear, "x3")
        x4 = SBesselian(HijriMonth, HijriYear, "x4")
        y0 = SBesselian(HijriMonth, HijriYear, "y0")
        y1 = SBesselian(HijriMonth, HijriYear, "y1")
        y2 = SBesselian(HijriMonth, HijriYear, "y2")
        y3 = SBesselian(HijriMonth, HijriYear, "y3")
        y4 = SBesselian(HijriMonth, HijriYear, "y4")
        d0 = SBesselian(HijriMonth, HijriYear, "dm0")
        d1 = SBesselian(HijriMonth, HijriYear, "dm1")
        d2 = SBesselian(HijriMonth, HijriYear, "dm2")
        d3 = SBesselian(HijriMonth, HijriYear, "dm3")
        d4 = SBesselian(HijriMonth, HijriYear, "dm4")
        M0 = SBesselian(HijriMonth, HijriYear, "M0")
        M1 = SBesselian(HijriMonth, HijriYear, "M1")
        M2 = SBesselian(HijriMonth, HijriYear, "M2")
        M3 = SBesselian(HijriMonth, HijriYear, "M3")
        M4 = SBesselian(HijriMonth, HijriYear, "M4")
        L10 = SBesselian(HijriMonth, HijriYear, "L10")
        L11 = SBesselian(HijriMonth, HijriYear, "L11")
        L12 = SBesselian(HijriMonth, HijriYear, "L12")
        L13 = SBesselian(HijriMonth, HijriYear, "L13")
        L14 = SBesselian(HijriMonth, HijriYear, "L14")
        L20 = SBesselian(HijriMonth, HijriYear, "L20")
        L21 = SBesselian(HijriMonth, HijriYear, "L21")
        L22 = SBesselian(HijriMonth, HijriYear, "L22")
        L23 = SBesselian(HijriMonth, HijriYear, "L23")
        L24 = SBesselian(HijriMonth, HijriYear, "L24")
        tanf1 = SBesselian(HijriMonth, HijriYear, "f1")

        JDESolarEclipse1 = JDEEclipseModified(HijriMonth,HijriYear,1)
        JDESolarEclipse2 = Math.floor(JDESolarEclipse1) + (((JDESolarEclipse1 - Math.floor(JDESolarEclipse1)) * 24).round(0)).toDouble() / 24.0
        DeltaT = DeltaT(JDESolarEclipse2)

        T0  = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "T0")
        T   = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "T")
        Tau = SolarEclipseObscuration(HijriMonth, HijriYear, gLat, gLon, gAlt, "Tau")

        T = T + Tau
        for (i in 1..5) {
            T = T
            P_ = Math.atan(0.99664719 * Math.tan(Rad(gLat)))
            S = 0.99664719 * Math.sin(P_) + (gAlt / 6378140) * Math.sin(Rad(gLat))
            C = Math.cos(P_) + (gAlt / 6378140) * Math.cos(Rad(gLat))
            x   = x0  + x1*T  + x2*T*T  + x3*T*T*T  + x4*T*T*T*T
            y   = y0  + y1*T  + y2*T*T  + y3*T*T*T  + y4*T*T*T*T
            d   = d0  + d1*T  + d2*T*T  + d3*T*T*T  + d4*T*T*T*T
            M   = M0  + M1*T  + M2*T*T  + M3*T*T*T  + M4*T*T*T*T
            L1  = L10	+ L11*T + L12*T*T + L13*T*T*T + L14*T*T*T*T
            L2  = L20	+ L21*T + L22*T*T + L23*T*T*T + L24*T*T*T*T
            x_  = x1 + 2*x2*T + 3*x3*T*T + 4*x4*T*T*T
            y_  = y1 + 2*y2*T + 3*y3*T*T + 4*y4*T*T*T
            H = Rad(M + gLon.toDouble() - 0.00417807 * DeltaT.toDouble())
            p = C * Math.sin(H)
            q = S * Math.cos(Rad(d)) - C * Math.cos(H) * Math.sin(Rad(d))
            r = S * Math.sin(Rad(d)) + C * Math.cos(H) * Math.cos(Rad(d))
            p_ = 0.01745329 * M1 * C * Math.cos(H)
            q_ = 0.01745329 * (M1 * p * Math.sin(Rad(d)) - r * d1)
            u = x - p
            v = y - q
            a = x_ - p_
            b = y_ - q_
            n = a * a + b * b
            e = Math.sqrt(n)
            L1_ = L1 - r * tanf1
            m = (a * v - u * b) / (e * L1_)
            P = -(u * a + v * b) /n + (L1_ / e ) * Math.sqrt(1-m*m)
            T = T + P
        }
        AU4 = T + P - DeltaT(JDESolarEclipse2) / 3600.0
        JDSU4 = Math.floor(JDESolarEclipse2 + 0.5) - 0.5 + ((T0 + AU4)/24.0)

        return when (OptResult) {
            "JDS"   -> JDESolarEclipse2
            "AU4"   -> AU4
            "JDSU4" -> JDSU4
            else    -> JDSU4
        }
    }

    fun SolarEclipse (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        var JDSU1 : Double
        var JDSU2 : Double
        var JDSMx : Double
        var JDSU3 : Double
        var JDSU4 : Double
        var MagU  : Double
        var Obsk  : Double
        var DurG  : Double
        var DurT  : Double
        var SEAltU1 : Double
        var SEAzmU1 : Double
        var SEAltU2 : Double
        var SEAzmU2 : Double
        var SEAltMx : Double
        var SEAzmMx : Double
        var SEAltU3 : Double
        var SEAzmU3 : Double
        var SEAltU4 : Double
        var SEAzmU4 : Double

        JDSU1 = JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")
        JDSU2 = JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU2")
        JDSMx = JDSolarEclipseMax(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSMx")
        JDSU3 = JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU3")
        JDSU4 = JDSolarEclipseU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU4")
        MagU  = SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Mag")
        Obsk  = SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Obs")
        DurG  = (JDSU4 - JDSU1) * 24
        DurT  = (JDSU3 - JDSU2) * 24
        SEAltU1 = SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU1")
        SEAzmU1 = SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU1")
        SEAltU2 = SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU2")
        SEAzmU2 = SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU2")
        SEAltMx = SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltMx")
        SEAzmMx = SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmMx")
        SEAltU3 = SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU3")
        SEAzmU3 = SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU3")
        SEAltU4 = SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU4")
        SEAzmU4 = SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU4")

        return when (OptResult) {
            "JDSU1" -> JDSU1
            "JDSU2" -> JDSU2
            "JDSMx" -> JDSMx
            "JDSU3" -> JDSU3
            "JDSU4" -> JDSU4
            "MagU"  -> MagU
            "Obsk"  -> Obsk
            "DurG"  -> DurG
            "DurT"  -> DurT
            "AltU1" -> SEAltU1
            "AzmU1" -> SEAzmU1
            "AltU2" -> SEAltU2
            "AzmU2" -> SEAzmU2
            "AltMx" -> SEAltMx
            "AzmMx" -> SEAzmMx
            "AltU3" -> SEAltU3
            "AzmU3" -> SEAzmU3
            "AltU4" -> SEAltU4
            "AzmU4" -> SEAzmU4
            else    -> JDSMx
        }
    }
    fun SEAltAzmU1 (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        val JDSU1 : Double
        val GHAU1 : Double
        val LHAU1 : Double
        val DecU1 : Double
        val AltU1 : Double
        val x     : Double
        val y     : Double
        val AzmU1  : Double

        JDSU1 = JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")
        GHAU1 = SunGreenwichHourAngle(JDSU1)
        LHAU1 = Mod(GHAU1 + gLon,360.0)
        DecU1 = SunApparentDeclination(JDSU1)
        AltU1 = Deg(Math.asin(Math.sin(Rad(gLat))*Math.sin(Rad(DecU1))+Math.cos(Rad(gLat))*Math.cos(Rad(DecU1))*Math.cos(Rad(LHAU1))))
        x     = Deg(((Math.sin(Rad(DecU1))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU1))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU1))))))
        y     = Deg(-Math.cos(Rad(DecU1))*Math.sin(Rad(LHAU1)))
        AzmU1  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult) {
            "AltU1" -> AltU1
            "AzmU1" -> AzmU1
            else    -> AltU1
        }
    }
    fun SEAltAzmU2 (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        val JDSU2 : Double
        val GHAU2 : Double
        val LHAU2 : Double
        val DecU2 : Double
        val AltU2 : Double
        val x     : Double
        val y     : Double
        val AzmU2  : Double

        JDSU2 = JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU2")
        GHAU2 = SunGreenwichHourAngle(JDSU2)
        LHAU2 = Mod(GHAU2 + gLon,360.0)
        DecU2 = SunApparentDeclination(JDSU2)
        AltU2 = Deg(Math.asin(Math.sin(Rad(gLat))*Math.sin(Rad(DecU2))+Math.cos(Rad(gLat))*Math.cos(Rad(DecU2))*Math.cos(Rad(LHAU2))))
        x     = Deg(((Math.sin(Rad(DecU2))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU2))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU2))))))
        y     = Deg(-Math.cos(Rad(DecU2))*Math.sin(Rad(LHAU2)))
        AzmU2  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult) {
            "AltU2" -> AltU2
            "AzmU2" -> AzmU2
            else    -> AltU2
        }
    }
    fun SEAltAzmMx (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        val JDSMx : Double
        val GHAMx : Double
        val LHAMx : Double
        val DecMx : Double
        val AltMx : Double
        val x     : Double
        val y     : Double
        val AzmMx  : Double

        JDSMx = JDSolarEclipseMax(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSMx")
        GHAMx = SunGreenwichHourAngle(JDSMx)
        LHAMx = Mod(GHAMx + gLon,360.0)
        DecMx = SunApparentDeclination(JDSMx)
        AltMx = Deg(Math.asin(Math.sin(Rad(gLat))*Math.sin(Rad(DecMx))+Math.cos(Rad(gLat))*Math.cos(Rad(DecMx))*Math.cos(Rad(LHAMx))))
        x     = Deg(((Math.sin(Rad(DecMx))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecMx))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAMx))))))
        y     = Deg(-Math.cos(Rad(DecMx))*Math.sin(Rad(LHAMx)))
        AzmMx  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult) {
            "AltMx" -> AltMx
            "AzmMx" -> AzmMx
            else    -> AltMx
        }
    }
    fun SEAltAzmU3 (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        val JDSU3 : Double
        val GHAU3 : Double
        val LHAU3 : Double
        val DecU3 : Double
        val AltU3 : Double
        val x     : Double
        val y     : Double
        val AzmU3  : Double

        JDSU3 = JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU3")
        GHAU3 = SunGreenwichHourAngle(JDSU3)
        LHAU3 = Mod(GHAU3 + gLon,360.0)
        DecU3 = SunApparentDeclination(JDSU3)
        AltU3 = Deg(Math.asin(Math.sin(Rad(gLat))*Math.sin(Rad(DecU3))+Math.cos(Rad(gLat))*Math.cos(Rad(DecU3))*Math.cos(Rad(LHAU3))))
        x     = Deg(((Math.sin(Rad(DecU3))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU3))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU3))))))
        y     = Deg(-Math.cos(Rad(DecU3))*Math.sin(Rad(LHAU3)))
        AzmU3  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult) {
            "AltU3" -> AltU3
            "AzmU3" -> AzmU3
            else    -> AltU3
        }
    }
    fun SEAltAzmU4 (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
        val JDSU4 : Double
        val GHAU4 : Double
        val LHAU4 : Double
        val DecU4 : Double
        val AltU4 : Double
        val x     : Double
        val y     : Double
        val AzmU4  : Double

        JDSU4 = JDSolarEclipseU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU4")
        GHAU4 = SunGreenwichHourAngle(JDSU4)
        LHAU4 = Mod(GHAU4 + gLon,360.0)
        DecU4 = SunApparentDeclination(JDSU4)
        AltU4 = Deg(Math.asin(Math.sin(Rad(gLat))*Math.sin(Rad(DecU4))+Math.cos(Rad(gLat))*Math.cos(Rad(DecU4))*Math.cos(Rad(LHAU4))))
        x     = Deg(((Math.sin(Rad(DecU4))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU4))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU4))))))
        y     = Deg(-Math.cos(Rad(DecU4))*Math.sin(Rad(LHAU4)))
        AzmU4  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult) {
            "AltU4" -> AltU4
            "AzmU4" -> AzmU4
            else    -> AltU4
        }
    }

    fun LunarEclipse(HijriMonth: Byte, HijriYear: Long, OptResult: String):Double {
        val x0  : Double
        val x1  : Double
        val x2  : Double
        val x3  : Double
        val x4  : Double
        val y0  : Double
        val y1  : Double
        val y2  : Double
        val y3  : Double
        val y4  : Double
        val L10 : Double
        val L11 : Double
        val L12 : Double
        val L13 : Double
        val L14 : Double
        val L20 : Double
        val L21 : Double
        val L22 : Double
        val L23 : Double
        val L24 : Double
        val L30 : Double
        val L31 : Double
        val L32 : Double
        val L33 : Double
        val L34 : Double
        val Sc0 : Double
        val Sc1 : Double
        val Sc2 : Double
        val Sc3 : Double
        val Sc4 : Double
        val M0  : Double
        val M1  : Double
        val M2  : Double
        val M3  : Double
        val M4  : Double
        val dc0: Double
        val dc1: Double
        val dc2: Double
        val dc3: Double
        val dc4: Double
        val HP0: Double
        val HP1: Double
        val HP2: Double
        val HP3: Double
        val HP4: Double

        val JDELunarEclipse1: Double
        val JDELunarEclipse2: Double
        val DeltaT: Double
        val T0: Double
        val T: Double
        val TG: Double
        val L1: Double
        val L2: Double
        val L3: Double
        val Sc: Double
        val z: Double
        val k: Double

        val T1: Double
        val T2: Double
        val T3: Double

        val MagP: Double
        val MagU: Double

        val DurP: Double
        val DurU: Double
        val DurT: Double
        val RadP: Double
        val RadU: Double

        val P1: Double
        val U1: Double
        val U2: Double
        val Max: Double
        val U3: Double
        val U4: Double
        val P4: Double

        x0 = LBesselian(HijriMonth, HijriYear, "x0")
        x1 = LBesselian(HijriMonth, HijriYear, "x1")
        x2 = LBesselian(HijriMonth, HijriYear, "x2")
        x3 = LBesselian(HijriMonth, HijriYear, "x3")
        x4 = LBesselian(HijriMonth, HijriYear, "x4")

        y0 = LBesselian(HijriMonth, HijriYear, "y0")
        y1 = LBesselian(HijriMonth, HijriYear, "y1")
        y2 = LBesselian(HijriMonth, HijriYear, "y2")
        y3 = LBesselian(HijriMonth, HijriYear, "y3")
        y4 = LBesselian(HijriMonth, HijriYear, "y4")

        L10 = LBesselian(HijriMonth, HijriYear, "L10")
        L11 = LBesselian(HijriMonth, HijriYear, "L11")
        L12 = LBesselian(HijriMonth, HijriYear, "L12")
        L13 = LBesselian(HijriMonth, HijriYear, "L13")
        L14 = LBesselian(HijriMonth, HijriYear, "L14")

        L20 = LBesselian(HijriMonth, HijriYear, "L20")
        L21 = LBesselian(HijriMonth, HijriYear, "L21")
        L22 = LBesselian(HijriMonth, HijriYear, "L22")
        L23 = LBesselian(HijriMonth, HijriYear, "L23")
        L24 = LBesselian(HijriMonth, HijriYear, "L24")

        L30 = LBesselian(HijriMonth, HijriYear, "L30")
        L31 = LBesselian(HijriMonth, HijriYear, "L31")
        L32 = LBesselian(HijriMonth, HijriYear, "L32")
        L33 = LBesselian(HijriMonth, HijriYear, "L33")
        L34 = LBesselian(HijriMonth, HijriYear, "L34")

        Sc0 = LBesselian(HijriMonth, HijriYear, "Sc0")
        Sc1 = LBesselian(HijriMonth, HijriYear, "Sc1")
        Sc2 = LBesselian(HijriMonth, HijriYear, "Sc2")
        Sc3 = LBesselian(HijriMonth, HijriYear, "Sc3")
        Sc4 = LBesselian(HijriMonth, HijriYear, "Sc4")

        JDELunarEclipse1 = JDEEclipseModified(HijriMonth, HijriYear, 2)
        return if (JDELunarEclipse1 > 0) {
            JDELunarEclipse2 = Math.floor(JDELunarEclipse1) + (((JDELunarEclipse1 - Math.floor(JDELunarEclipse1)) * 24).round(0)).toDouble() / 24.0

            DeltaT  = DeltaT(JDELunarEclipse2)
            T0      = Mod((((JDELunarEclipse2 - Math.floor(JDELunarEclipse2)) * 24).round(0).toDouble()), 24.0)
            T       = -(x0 * x1 + y0 * y1) / (x1 * x1 + y1 * y1)
            TG      = T0 + T - DeltaT/ 3600.0

            L1 = L10	+ L11*T + L12*T*T + L13*T*T*T + L14*T*T*T*T
            L2 = L20	+ L21*T + L22*T*T + L23*T*T*T + L24*T*T*T*T
            L3 = L30	+ L31*T + L32*T*T + L33*T*T*T + L34*T*T*T*T
            Sc = Sc0    + Sc1*T + Sc2*T*T + Sc3*T*T*T + Sc4*T*T*T*T

            z = Math.sqrt(Math.pow(x0 + x1 * T, 2.0) + Math.pow(y0 + y1 * T, 2.0))
            k = Math.sqrt(x1 * x1 + y1 * y1)

            T1 = Math.sqrt(L1 * L1 - z * z) / k
            T2 = Math.sqrt(L2 * L2 - z * z) / k
            T3 = Math.sqrt(L3 * L3 - z * z) / k

            MagP = (L1 - z) / (2 * Sc)
            MagU = (L2 - z) / (2 * Sc)

            DurP = T1*2
            DurU = T2*2
            DurT = T3*2
            RadP = L1 - Sc
            RadU = L2 - Sc

            P1  = Math.floor(JDELunarEclipse1) + (TG - T1)/24.0
            U1  = Math.floor(JDELunarEclipse1) + (TG - T2)/24.0
            U2  = Math.floor(JDELunarEclipse1) + (TG - T3)/24.0
            Max = Math.floor(JDELunarEclipse1) + TG/24.0
            U3  = Math.floor(JDELunarEclipse1) + (TG + T3)/24.0
            U4  = Math.floor(JDELunarEclipse1) + (TG + T2)/24.0
            P4  = Math.floor(JDELunarEclipse1) + (TG + T1)/24.0

            return when (OptResult) {
                "JDL" -> JDELunarEclipse2
                "DT" -> DeltaT
                "T0" -> Mod(T0+12,24.0)
                "T" -> T
                "T1" -> T1
                "T2" -> T2
                "T3" -> T3
                "P1" -> P1
                "U1" -> U1
                "U2" -> U2
                "TG" -> Max
                "U3" -> U3
                "U4" -> U4
                "P4" -> P4
                "MagP" -> MagP
                "MagU" -> MagU
                "DurP" -> DurP
                "DurU" -> DurU
                "DurT" -> DurT
                else -> TG
            }
        } else {
            Double.NaN
        }
    }
    fun JenisLunarEclipse(HijriMonth: Byte, HijriYear: Long): String {
        val MagP : Double
        val MagU : Double
        val JLE  : String

        MagP = LunarEclipse(HijriMonth,HijriYear,"MagP")
        MagU = LunarEclipse(HijriMonth,HijriYear,"MagU")

        JLE = if (MagU >= 1.0) {
            "GERHANA BULAN TOTAL"
        } else if (MagP > 0.0 && MagU < 0.0) {
            "GERHANA BULAN PENUMBRAL"
        } else if (MagU >0.0 && MagU <1.0) {
            "GERHANA BULAN SEBAGIAN"
        } else {
            "TIDAK TERJADI GERHANA"
        }

        return JLE
    }
    fun LEAltAzmMax(HijriMonth: Byte, HijriYear: Long,gLon: Double, gLat: Double,gAlt:Double, OptResult: String): Double {
        val JDLMx : Double
        val GHAMx : Double
        val LHAMx : Double
        val DecMx : Double
        val HPbMx : Double
        val hG  : Double
        val P   : Double
        val hT  : Double
        val Refr: Double
        val Dip : Double
        val hM  : Double
        val x   : Double
        val y   : Double
        val Az  : Double

        JDLMx = LunarEclipse(HijriMonth,HijriYear,"TG")
        GHAMx = MoonGreenwichHourAngle(JDLMx)
        LHAMx = Mod(GHAMx + gLon, 360.0)
        DecMx = MoonApparentDeclination(JDLMx)
        HPbMx = MoonEquatorialHorizontalParallax(JDLMx)

        hG  = Deg(Math.asin(Math.sin(Rad(gLat)) * Math.sin (Rad(DecMx)) + Math.cos(Rad(gLat))*Math.cos(Rad(DecMx)) * Math.cos(Rad(LHAMx))))
        P   = HPbMx
        hT  = hG-(P * Math.cos(Rad(hG)))
        Refr = 0.01695/Math.tan(Rad((hT+10.3/(hT+5.12555))))
        Dip  = 1.76/60*Math.sqrt(gAlt)
        hM  = hT + Refr + Dip
        x   = Deg(((Math.sin(Rad(DecMx))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecMx))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAMx))))))
        y   = Deg(-Math.cos(Rad(DecMx))*Math.sin(Rad(LHAMx)))
        Az  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult){
            "hG" -> hG
            "hT" -> hT
            "hM" -> hM
            "Az" -> Az
            else -> hM
        }
    }
    fun LEAltAzmP1(HijriMonth: Byte, HijriYear: Long,gLon: Double, gLat: Double,gAlt:Double, OptResult: String): Double {
        val JDLP1 : Double
        val GHAP1 : Double
        val LHAP1 : Double
        val DecP1 : Double
        val HPbP1 : Double
        val hG  : Double
        val P   : Double
        val hT  : Double
        val Refr: Double
        val Dip : Double
        val hM  : Double
        val x   : Double
        val y   : Double
        val Az  : Double

        JDLP1 = LunarEclipse(HijriMonth,HijriYear,"P1")
        GHAP1 = MoonGreenwichHourAngle(JDLP1)
        LHAP1 = Mod(GHAP1 + gLon, 360.0)
        DecP1 = MoonApparentDeclination(JDLP1)
        HPbP1 = MoonEquatorialHorizontalParallax(JDLP1)

        hG  = Deg(Math.asin(Math.sin(Rad(gLat)) * Math.sin (Rad(DecP1)) + Math.cos(Rad(gLat))*Math.cos(Rad(DecP1)) * Math.cos(Rad(LHAP1))))
        P   = HPbP1
        hT  = hG-(P * Math.cos(Rad(hG)))
        Refr = 0.01695/Math.tan(Rad((hT+10.3/(hT+5.12555))))
        Dip  = 1.76/60*Math.sqrt(gAlt)
        hM  = hT + Refr + Dip
        x   = Deg(((Math.sin(Rad(DecP1))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecP1))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAP1))))))
        y   = Deg(-Math.cos(Rad(DecP1))*Math.sin(Rad(LHAP1)))
        Az  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult){
            "hG" -> hG
            "hT" -> hT
            "hM" -> hM
            "Az" -> Az
            else -> hM
        }
    }
    fun LEAltAzmU1(HijriMonth: Byte, HijriYear: Long,gLon: Double, gLat: Double,gAlt:Double, OptResult: String): Double {
        val JDLU1 : Double
        val GHAU1 : Double
        val LHAU1 : Double
        val DecU1 : Double
        val HPbU1 : Double
        val hG  : Double
        val P   : Double
        val hT  : Double
        val Refr: Double
        val Dip : Double
        val hM  : Double
        val x   : Double
        val y   : Double
        val Az  : Double

        JDLU1 = LunarEclipse(HijriMonth,HijriYear,"U1")
        GHAU1 = MoonGreenwichHourAngle(JDLU1)
        LHAU1 = Mod(GHAU1 + gLon, 360.0)
        DecU1 = MoonApparentDeclination(JDLU1)
        HPbU1 = MoonEquatorialHorizontalParallax(JDLU1)

        hG  = Deg(Math.asin(Math.sin(Rad(gLat)) * Math.sin (Rad(DecU1)) + Math.cos(Rad(gLat))*Math.cos(Rad(DecU1)) * Math.cos(Rad(LHAU1))))
        P   = HPbU1
        hT  = hG-(P * Math.cos(Rad(hG)))
        Refr = 0.01695/Math.tan(Rad((hT+10.3/(hT+5.12555))))
        Dip  = 1.76/60*Math.sqrt(gAlt)
        hM  = hT + Refr + Dip
        x   = Deg(((Math.sin(Rad(DecU1))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU1))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU1))))))
        y   = Deg(-Math.cos(Rad(DecU1))*Math.sin(Rad(LHAU1)))
        Az  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult){
            "hG" -> hG
            "hT" -> hT
            "hM" -> hM
            "Az" -> Az
            else -> hM
        }
    }
    fun LEAltAzmU2(HijriMonth: Byte, HijriYear: Long,gLon: Double, gLat: Double,gAlt:Double, OptResult: String): Double {
        val JDLU2 : Double
        val GHAU2 : Double
        val LHAU2 : Double
        val DecU2 : Double
        val HPbU2 : Double
        val hG  : Double
        val P   : Double
        val hT  : Double
        val Refr: Double
        val Dip : Double
        val hM  : Double
        val x   : Double
        val y   : Double
        val Az  : Double

        JDLU2 = LunarEclipse(HijriMonth,HijriYear,"U2")
        GHAU2 = MoonGreenwichHourAngle(JDLU2)
        LHAU2 = Mod(GHAU2 + gLon, 360.0)
        DecU2 = MoonApparentDeclination(JDLU2)
        HPbU2 = MoonEquatorialHorizontalParallax(JDLU2)

        hG  = Deg(Math.asin(Math.sin(Rad(gLat)) * Math.sin (Rad(DecU2)) + Math.cos(Rad(gLat))*Math.cos(Rad(DecU2)) * Math.cos(Rad(LHAU2))))
        P   = HPbU2
        hT  = hG-(P * Math.cos(Rad(hG)))
        Refr = 0.01695/Math.tan(Rad((hT+10.3/(hT+5.12555))))
        Dip  = 1.76/60*Math.sqrt(gAlt)
        hM  = hT + Refr + Dip
        x   = Deg(((Math.sin(Rad(DecU2))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU2))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU2))))))
        y   = Deg(-Math.cos(Rad(DecU2))*Math.sin(Rad(LHAU2)))
        Az  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult){
            "hG" -> hG
            "hT" -> hT
            "hM" -> hM
            "Az" -> Az
            else -> hM
        }
    }
    fun LEAltAzmU3(HijriMonth: Byte, HijriYear: Long,gLon: Double, gLat: Double,gAlt:Double, OptResult: String): Double {
        val JDLU3 : Double
        val GHAU3 : Double
        val LHAU3 : Double
        val DecU3 : Double
        val HPbU3 : Double
        val hG  : Double
        val P   : Double
        val hT  : Double
        val Refr: Double
        val Dip : Double
        val hM  : Double
        val x   : Double
        val y   : Double
        val Az  : Double

        JDLU3 = LunarEclipse(HijriMonth,HijriYear,"U3")
        GHAU3 = MoonGreenwichHourAngle(JDLU3)
        LHAU3 = Mod(GHAU3 + gLon, 360.0)
        DecU3 = MoonApparentDeclination(JDLU3)
        HPbU3 = MoonEquatorialHorizontalParallax(JDLU3)

        hG  = Deg(Math.asin(Math.sin(Rad(gLat)) * Math.sin (Rad(DecU3)) + Math.cos(Rad(gLat))*Math.cos(Rad(DecU3)) * Math.cos(Rad(LHAU3))))
        P   = HPbU3
        hT  = hG-(P * Math.cos(Rad(hG)))
        Refr = 0.01695/Math.tan(Rad((hT+10.3/(hT+5.12555))))
        Dip  = 1.76/60*Math.sqrt(gAlt)
        hM  = hT + Refr + Dip
        x   = Deg(((Math.sin(Rad(DecU3))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU3))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU3))))))
        y   = Deg(-Math.cos(Rad(DecU3))*Math.sin(Rad(LHAU3)))
        Az  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult){
            "hG" -> hG
            "hT" -> hT
            "hM" -> hM
            "Az" -> Az
            else -> hM
        }
    }
    fun LEAltAzmU4(HijriMonth: Byte, HijriYear: Long,gLon: Double, gLat: Double,gAlt:Double, OptResult: String): Double {
        val JDLU4 : Double
        val GHAU4 : Double
        val LHAU4 : Double
        val DecU4 : Double
        val HPbU4 : Double
        val hG  : Double
        val P   : Double
        val hT  : Double
        val Refr: Double
        val Dip : Double
        val hM  : Double
        val x   : Double
        val y   : Double
        val Az  : Double

        JDLU4 = LunarEclipse(HijriMonth,HijriYear,"U4")
        GHAU4 = MoonGreenwichHourAngle(JDLU4)
        LHAU4 = Mod(GHAU4 + gLon, 360.0)
        DecU4 = MoonApparentDeclination(JDLU4)
        HPbU4 = MoonEquatorialHorizontalParallax(JDLU4)

        hG  = Deg(Math.asin(Math.sin(Rad(gLat)) * Math.sin (Rad(DecU4)) + Math.cos(Rad(gLat))*Math.cos(Rad(DecU4)) * Math.cos(Rad(LHAU4))))
        P   = HPbU4
        hT  = hG-(P * Math.cos(Rad(hG)))
        Refr = 0.01695/Math.tan(Rad((hT+10.3/(hT+5.12555))))
        Dip  = 1.76/60*Math.sqrt(gAlt)
        hM  = hT + Refr + Dip
        x   = Deg(((Math.sin(Rad(DecU4))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecU4))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAU4))))))
        y   = Deg(-Math.cos(Rad(DecU4))*Math.sin(Rad(LHAU4)))
        Az  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult){
            "hG" -> hG
            "hT" -> hT
            "hM" -> hM
            "Az" -> Az
            else -> hM
        }
    }
    fun LEAltAzmP4(HijriMonth: Byte, HijriYear: Long,gLon: Double, gLat: Double,gAlt:Double, OptResult: String): Double {
        val JDLP4 : Double
        val GHAP4 : Double
        val LHAP4 : Double
        val DecP4 : Double
        val HPbP4 : Double
        val hG  : Double
        val P   : Double
        val hT  : Double
        val Refr: Double
        val Dip : Double
        val hM  : Double
        val x   : Double
        val y   : Double
        val Az  : Double

        JDLP4 = LunarEclipse(HijriMonth,HijriYear,"P4")
        GHAP4 = MoonGreenwichHourAngle(JDLP4)
        LHAP4 = Mod(GHAP4 + gLon, 360.0)
        DecP4 = MoonApparentDeclination(JDLP4)
        HPbP4 = MoonEquatorialHorizontalParallax(JDLP4)

        hG  = Deg(Math.asin(Math.sin(Rad(gLat)) * Math.sin (Rad(DecP4)) + Math.cos(Rad(gLat))*Math.cos(Rad(DecP4)) * Math.cos(Rad(LHAP4))))
        P   = HPbP4
        hT  = hG-(P * Math.cos(Rad(hG)))
        Refr = 0.01695/Math.tan(Rad((hT+10.3/(hT+5.12555))))
        Dip  = 1.76/60*Math.sqrt(gAlt)
        hM  = hT + Refr + Dip
        x   = Deg(((Math.sin(Rad(DecP4))*Math.cos(Rad(gLat))-(Math.cos(Rad(DecP4))*Math.sin(Rad(gLat))*Math.cos(Rad(LHAP4))))))
        y   = Deg(-Math.cos(Rad(DecP4))*Math.sin(Rad(LHAP4)))
        Az  = Mod(Deg(Math.atan2(y,x)),360.0)
        return when (OptResult){
            "hG" -> hG
            "hT" -> hT
            "hM" -> hM
            "Az" -> Az
            else -> hM
        }
    }

    fun main(args:Array<String>) {

        println("KONVERSI MASEHI KE HIJRI URFI")
        var TglM  : Byte
        var BlnM  : Byte
        var ThnM  : Long
        var TGLH  : Byte
        var BLNH  : Byte
        var THNH  : Long
        var CJDN  : Long
        var JDNow : Double
        var TMZn  : Double
        // 1. Konversi Kalender Miladi ke Kalender Hijri Ishthilahi
        TglM = 3
        BlnM = 5
        ThnM = 2022
        TMZn =  7.0
        // 1.1 Konversi Kalender Miladi ke JD
        JDNow = KMJD(TglM,BlnM,ThnM,0.0,TMZn)
        // 1.2 Konversi JD ke CJDN
        CJDN = Math.floor(JDNow + 0.5 + TMZn / 24).toLong()
        // 1.3 Konversi CJDN ke Kalender Hijri
        TGLH = CJDNKH(CJDN,OptResult="TglH").toString().toByte()
        BLNH = CJDNKH(CJDN,OptResult="BlnH").toString().toByte()
        THNH = CJDNKH(CJDN,OptResult="ThnH").toString().toLong()
        println("$TGLH / $BLNH / $THNH H")

        println("JDNow : $JDNow" )


        println("==========================")

        println("PERHITUNGAN WAKTU SHALAT HARIAN & KIBLAT HARIAN DAN RASHDUL QIBLAT")
        val Tanggal: Byte       = 24
        val Bulan: Byte         = 7
        val Tahun: Long         = 2020
        val Longitude: Double   = (107 + 39/60.0 + 16.98/3600.0)
        val Latitude: Double    =-(  6 + 58/60.0 + 40.00/3600.0)
        val Elevasi: Double     = 0.0
        val Tzn: Double         = 7.0
        val Ihtiyath: Int       = 2
        var JDP : Double
        var JDQ : Double

        val Zuhur : Double        = Zuhur(Tanggal,Bulan,Tahun, Longitude,Latitude,Tzn)
        val iZuhur : Double       = IhtiyathShalat(Zuhur,Ihtiyath)
        val Ashar : Double        = Ashar(Tanggal,Bulan,Tahun, Longitude,Latitude,Tzn)
        val iAshar : Double       = IhtiyathShalat(Ashar,Ihtiyath)
        val Magrib : Double       = Magrib(Tanggal,Bulan,Tahun,Longitude,Latitude,Elevasi,Tzn)
        val iMagrib : Double      = IhtiyathShalat(Magrib,Ihtiyath)
        val Isya : Double         = Isya(Tanggal,Bulan,Tahun,Longitude,Latitude,Tzn)
        val iIsya : Double        = IhtiyathShalat(Isya,Ihtiyath)
        val Subuh : Double        = Subuh(Tanggal,Bulan,Tahun,Longitude,Latitude,Tzn)
        val iSubuh : Double       = IhtiyathShalat(Subuh,Ihtiyath)
        val NisfuLail : Double    = ((Mod(iSubuh - iMagrib, 24.0))/2.0) + iMagrib
//    val iNisfuLail : Double   = IhtiyathShalat(NisfuLail,0)
        val Syuruk : Double       = Syuruk(Tanggal,Bulan,Tahun,Longitude,Latitude,Elevasi,Tzn)
        val iSyuruk : Double      = IhtiyathShalat(Syuruk,-2)
        val Duha : Double         = Duha(Tanggal,Bulan,Tahun,Longitude,Latitude,Elevasi,Tzn)
        val iDuha : Double        = iSyuruk + 15/60.0

        val QiblatHarian1 : Double = BayanganQiblatHarian(Longitude,Latitude,Tanggal,Bulan,Tahun,Tzn,1)
        val QiblatHarian2 : Double = BayanganQiblatHarian(Longitude,Latitude,Tanggal,Bulan,Tahun,Tzn,2)

        val RQ1: Any               = RashdulQiblat(Tahun,Tzn,1)
        val RQ2: Any               = RashdulQiblat(Tahun,Tzn,2)

        println("Zuhur            : ${DHHMS(iZuhur,"HH:MM")}")
        println("Ashar            : ${DHHMS(iAshar,"HH:MM")}")
        println("Magrib           : ${DHHMS(iMagrib,"HH:MM")}")
        println("Isya             : ${DHHMS(iIsya,"HH:MM")}")
        println("Nisfu Lail       : ${DHHMS(NisfuLail,"HH:MM")}")
        println("Subuh            : ${DHHMS(iSubuh,"HH:MM")}")
        println("Syuruk           : ${DHHMS(iSyuruk,"HH:MM")}")
        println("Duha             : ${DHHMS(iDuha,"HH:MM")}")

        println("Kiblat Harian 1  : ${DHHM(QiblatHarian1,"HH:MM",0)}")
        println("Kiblat Harian 2  : ${DHHM(QiblatHarian2,"HH:MM",0)}")

        println("Rashdul Qiblat 1 : $RQ1")
        println("Rashdul Qiblat 2 : $RQ2")

        println("=====================================================")

        println("JADWAL SHALAT PER-BULAN")

        JDP 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)

        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDP,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDP,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDP,Tzn,"ThnM").toLong()

            val Zhr : Double = Zuhur(TglM,BlnM,ThnM,Longitude,Latitude,Tzn)
            val iZhr : Double = IhtiyathShalat(Zhr,2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Zuhur : ${DHHM(iZhr,"HH:MM",0)}")
                JDP = JDP + 1.0
            } else {
                break
            }
        }
        println("=====================================================")
        JDP 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)
        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDP,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDP,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDP,Tzn,"ThnM").toLong()

            val Asr : Double = Ashar(TglM,BlnM,ThnM,Longitude,Latitude,Tzn)
            val iAsr : Double = IhtiyathShalat(Asr,2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Ashar : ${DHHM(iAsr,"HH:MM",0)}")
                JDP = JDP + 1.0
            } else {
                break
            }
        }

        println("=====================================================")
        JDP 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)
        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDP,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDP,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDP,Tzn,"ThnM").toLong()

            val Mgrb : Double = Magrib(TglM,BlnM,ThnM,Longitude,Latitude,Elevasi,Tzn)
            val iMgrb : Double = IhtiyathShalat(Mgrb,2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Magrib : ${DHHM(iMgrb,"HH:MM",0)}")
                JDP = JDP + 1.0
            } else {
                break
            }
        }
        println("=====================================================")

        JDP 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)
        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDP,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDP,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDP,Tzn,"ThnM").toLong()

            val Isy : Double = Isya(TglM,BlnM,ThnM,Longitude,Latitude,Tzn)
            val iIsy : Double = IhtiyathShalat(Isy,2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Isya : ${DHHM(iIsy,"HH:MM",0)}")
                JDP = JDP + 1.0
            } else {
                break
            }
        }
        println("=====================================================")

        JDP 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)
        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDP,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDP,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDP,Tzn,"ThnM").toLong()

            val Sbh : Double = Subuh(TglM,BlnM,ThnM,Longitude,Latitude,Tzn)
            val iSbh : Double = IhtiyathShalat(Sbh,2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Subuh : ${DHHM(iSbh,"HH:MM",0)}")
                JDP = JDP + 1.0
            } else {
                break
            }
        }
        println("=====================================================")
        JDP 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)
        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDP,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDP,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDP,Tzn,"ThnM").toLong()

            val Syrk : Double = Syuruk(TglM,BlnM,ThnM,Longitude,Latitude,Elevasi,Tzn)
            val iSyrk : Double = IhtiyathShalat(Syrk,2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Syuruk : ${DHHM(iSyrk,"HH:MM",0)}")
                JDP = JDP + 1.0
            } else {
                break
            }
        }
        println("=====================================================")
        JDP 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)
        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDP,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDP,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDP,Tzn,"ThnM").toLong()

            val Dha : Double = Duha(TglM,BlnM,ThnM,Longitude,Latitude,Elevasi,Tzn)
            val iDha : Double = IhtiyathShalat(Dha,-2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Duha : ${DHHM(iDha,"HH:MM",0)}")
                JDP = JDP + 1.0
            } else {
                break
            }
        }
        println("=====================================================")
        println("JADWAL KIBLAT HARIAN PER-BULAN")

        JDQ 		= KMJD(1, Bulan, Tahun, 0.0, Tzn)

        for (i in 1..31) {
            val TglM : Byte   = JDKM(JDQ,Tzn,"TglM").toByte()
            val BlnM : Byte   = JDKM(JDQ,Tzn,"BlnM").toByte()
            val ThnM : Long   = JDKM(JDQ,Tzn,"ThnM").toLong()
            val BQ1 : Double = BayanganQiblatHarian(
                Longitude, Latitude,
                TglM, BlnM, ThnM, Tzn,1)
            val BQ2 : Double = BayanganQiblatHarian(
                Longitude, Latitude,
                TglM, BlnM, ThnM, Tzn,2)

            val sTglM: String	= if (TglM < 10) "0${TglM}" else "${TglM}"
            val sBlnM: String	= if (BlnM < 10) "0${BlnM}" else "${BlnM}"

            if (BlnM == Bulan) {
                println("${sTglM}/${sBlnM}/${ThnM} Bayang Qiblat 1 : ${DHHM(BQ1,"HH:MM",0)}   Bayang Qiblat 2 : ${DHHM(BQ2,"HH:MM",0)}")
                JDQ = JDQ + 1.0
            } else {
                break
            }
        }
        println("===============================================")

        println("PERHITUNGAN ARAH KIBLAT")

        println("Arah Qiblat Spherical     : " + DDDMS(ArahQiblatSpherical( Longitude,Latitude )))
        println("Arah Qiblat Ellipsoid     : " + DDDMS(ArahQiblaWithEllipsoidCorrection( Longitude,Latitude )))
        println("Arah Qiblat Vincenty      : " + DDDMS(ArahQiblaVincenty(Longitude,Latitude,"PtoQ")))
        println("Reverse Qiblat Vincenty   : " + DDDMS(ArahQiblaVincenty(Longitude,Latitude,"QtoP")))
        println("jarak ke Kiblat Spherical : " + JarakQiblatSpherical(Longitude,Latitude) + " KM")
        println("jarak ke Kiblat Ellipsoid : " + JarakQiblatEllipsoid(Latitude,Longitude) + " KM")
        println("jarak ke Kiblat Vincenty  : " + ArahQiblaVincenty(Longitude,Latitude,"Dist")/1000 + " KM")


        val TglMs   : Byte   =   20
        val BlnMs   : Byte   =    8
        val ThnMs   : Long   = 2020
        val TmZne   : Double =    7.0
        val DcHr    : Double =   (  11 +  0 / 60.0 +  0.00 / 3600.0)
        val GeogLon : Double = + ( 107 + 39 / 60.0 + 16.98 / 3600.0)
        val GeogLat : Double = - (   6 + 58 / 60.0 + 40.00 / 3600.0)
        val GeogAlt : Double = 692.0
        val JD      : Double = KMJD(TglMs,BlnMs,ThnMs,DcHr,TmZne)
        val dltT    : Double = DeltaT(JD)
        val JDE     : Double = JD + dltT / 86400.0


        println("=============================================================")
        println("DATA MATAHARI DAN BULAN REAL TIME")
        println("")
        println("DATA MATAHARI GEOSENTRIS")
        println("GHA            = ${DHHMS(SunGreenwichHourAngle(JD,dltT)/15.0,"HHMMSS" ).toString()}")
        println("LHA            = ${DHHMS(SunLocalHourAngle(JD,GeogLon,dltT)/15.0,"HHMMSS").toString()}")
        // Sun Geocentric Ecliptic Coordinates
        println("Bujur          = ${DDDMS(SunGeocentricLongitude(JD)).toString()}")
        println("Lintang        = ${DDDMS(SunGeocentricLatitude(JDE)/3600.0).toString()}")
        // Sun Geocentric Equatorial Coordinates
        println("RA             = ${DDDMS(SunApparentRightAscension(JD)).toString()}")
        println("Dek            = ${DDDMS(SunApparentDeclination(JDE)).toString()}")
        // Sun Geocentric Horizontal Coordinates
        println("Azimuth        = ${DDDMS(SunAzimuth(JD,GeogLon,GeogLat,dltT)).toString()}")
        println("Altitude       = ${DDDMS(SunAltitude(JD,GeogLon,GeogLat,dltT)).toString()}")

        println("")
        println("DATA MATAHARI TOPOSENTRIS")
        println("GHA            = ${DHHMS(SunTopocentricEquatorialCoordinates(JD,0.0,0.0,0.0,dltT,3)/15.0,"HHMMSS" ).toString()}")
        println("LHA            = ${DHHMS(SunTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,3)/15.0,"HHMMSS" ).toString()}")
        // Sun Topocentric Ecliptic Coordinates
        println("Bujur          = ${DDDMS(SunTopocentricEclipticCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)).toString()}")
        println("Lintang        = ${DDDMS(SunTopocentricEclipticCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)).toString()}")
        // Sun Topocentric Equatorial Coordinates
        println("RA             = ${DDDMS(SunTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)).toString()}")
        println("Dek            = ${DDDMS(SunTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)).toString()}")
        // Sun Topocentric Horizontal Coordinates
        println("Azimuth        = ${DDDMS(SunTopocentricHorizontalCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)).toString()}")
        println("Altitude       = ${DDDMS(SunTopocentricHorizontalCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)).toString()}")
        println("Back Azimuth   = ${DDDMS(Mod(SunTopocentricHorizontalCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)+180.0,360.0)).toString()}")


        println("")
        println("DATA BULAN GEOSENTRIS")
        println("GHA            = ${DHHMS(MoonGreenwichHourAngle(JD,dltT)/15.0,"HHMMSS" ).toString()}")
        println("LHA            = ${DHHMS(MoonLocalHourAngle(JD,GeogLon,dltT)/15.0,"HHMMSS").toString()}")
        // Moon Geocentric Ecliptic Coordinates
        println("Bujur          = ${DDDMS(MoonGeocentricLongitude(JDE)).toString()}")
        println("Lintang        = ${DDDMS(MoonGeocentricLatitude(JDE)).toString()}")
        // Moon Geocentric Equatorial Coordinates
        println("RA             = ${DDDMS(MoonApparentRightAscension(JDE)).toString()}")
        println("Dek            = ${DDDMS(MoonApparentDeclination(JDE)).toString()}")
        // Moon Geocentric Horizontal Coordinates
        println("Azimuth        = ${DDDMS(MoonAzimuth(JD,GeogLon,GeogLat,dltT)).toString()}")
        println("Altitude       = ${DDDMS(MoonAltitude(JD,GeogLon,GeogLat,dltT)).toString()}")

        println("")
        println("DATA BULAN TOPOSENTRIS")
        println("GHA            = ${DHHMS(MoonTopocentricEquatorialCoordinates(JD,0.0,0.0,0.0,dltT,3)/15.0,"HHMMSS" ).toString()}")
        println("LHA            = ${DHHMS(MoonTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,3)/15.0,"HHMMSS" ).toString()}")
        // Moon Topocentric Ecliptic Coordinates
        println("Bujur          = ${DDDMS(MoonTopocentricEclipticCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)).toString()}")
        println("Lintang        = ${DDDMS(MoonTopocentricEclipticCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)).toString()}")
        // Moon Topocentric Equatorial Coordinates
        println("RA             = ${DDDMS(MoonTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)).toString()}")
        println("Dek            = ${DDDMS(MoonTopocentricEquatorialCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)).toString()}")
        // Moon Topocentric Horizontal Coordinates
        println("Azimuth        = ${DDDMS(MoonTopocentricHorizontalCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)).toString()}")
        println("Altitude       = ${DDDMS(MoonTopocentricHorizontalCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,2)).toString()}")
        println("Back Azimuth   = ${DDDMS(Mod(MoonTopocentricHorizontalCoordinates(JD,GeogLon,GeogLat,GeogAlt,dltT,1)+180.0,360.0)).toString()}")


        println("")
        println("EPHEMERIS MATAHARI DAN BULAN")
        for (Jmds in 0..24 step 1) {
            println("DATA MATAHARI JAM  $Jmds TD")
            val jd      : Double    = KMJD(TglMs,BlnMs,ThnMs,Jmds.toDouble(),0.0)
            val dltT    : Double    = dltT
            val JDE     : Double    = jd + dltT / 86400.0
            val DT      : String    = RoundTo(dltT,2)
            val ALm     : String    = DDDMS(SunGeocentricLongitude(JDE),"DDMMSS",0)
            val ELm     : String    = RoundTo(SunGeocentricLatitude(JDE),2)
            val ARm     : String    = DDDMS(SunApparentRightAscension(JDE),"DDMMSS",0)
            val dm      : String    = DDDMS(SunApparentDeclination(JDE),"DDMMSS",0)
            val HPm     : String    = DDDMS(SunEquatorialHorizontalParallax(JDE),"SS",8)
            val SDm     : String    = DDDMS(SunAngularSemiDiameter(JDE),"MMSS",2)
            val Oblq    : String    = DDDMS(ObliquityOfEcliptic(JDE),"DDMMSS",0)
            val Dist    : String    = RoundTo(SunGeocentricDistance(JDE),7)
            val EoT     : String    = DHHMS(EquationOfTime(JDE),"MMSS",0)
            val GHAm    : String    = DDDMS(SunGreenwichHourAngle(JDE),"DDMMSS",0)

            println("Delta T                    : ${DT}s")
            println("Apparent Longitude         : $ALm")
            println("Ecliptic Latitude          : $ELm”")
            println("Apparent Right Ascencion   : $ARm")
            println("Apparent Declination       : $dm")
            println("Horisontal Parallaks       : $HPm")
            println("Semidiameter               : $SDm")
            println("True Obliquity             : $Oblq")
            println("True Geocentric Distance   : $Dist")
            println("Equation of Time           : $EoT")
            println("Greenwich Hour Angle       : $GHAm")
        }

        println("")
        println("==============================================")
        for (Jmds in 0..24 step 1) {
            println("DATA BULAN JAM  $Jmds TD")
            val JD      : Double    = KMJD(TglMs,BlnMs,ThnMs,Jmds.toDouble(),0.0)
            val dltT    : Double    = dltT
            val JDE     : Double    = JD + dltT / 86400.0
            val DT      : String    = RoundTo(dltT,2)
            val ALb     : String    = DDDMS(MoonGeocentricLongitude(JDE),"DDMMSS",0)
            val ELb     : String    = DDDMS(MoonGeocentricLatitude(JDE),"DDMMSS",0)
            val ARb     : String    = DDDMS(MoonApparentRightAscension(JDE),"DDMMSS",0)
            val db      : String    = DDDMS(MoonApparentDeclination(JDE),"DDMMSS",0)
            val HPb     : String    = DDDMS(MoonEquatorialHorizontalParallax(JDE),"DDMMSS",0)
            val SDb     : String    = DDDMS(MoonAngularSemiDiameter(JDE),"MMSS",2)
            val ABL     : String    = DDDMS(MoonBrightLimbAngle(JDE),"DDMMSS",0)
            val FIb     : String    = RoundTo(MoonDiskIlluminatedFraction(JDE),8)
            val GHAb    : String    = DDDMS(MoonGreenwichHourAngle(JDE),"DDMMSS",0)

            Log.d("TES",FIb)

            println("Delta T                    : ${DT}s")
            println("Apparent Longitude         : $ALb")
            println("Ecliptic Latitude          : $ELb")
            println("Apparent Right Ascencion   : $ARb")
            println("Apparent Declination       : $db")
            println("Horisontal Parallaks       : $HPb")
            println("Semidiameter               : $SDb")
            println("Angle Bright Limb          : $ABL")
            println("Fraction Illumination      : $FIb")
            println("Greenwich Hour Angle       : $GHAb")

        }
        println("===============================================")
        println("PERHITUNGAN AWAL BULAN HIJRIYAH")
        // Data Diketahui:
        val BlnH   : Byte     = 10
        val ThnH   : Long     = 1443
        val gLat   : Double   = (  5+  45/60.0 + 0/3600.0)
        val gLon   : Double   = +(95 + 21/60.0 + 0/3600.0)
        val gAlt   : Double   = 10.0
        val TmZn   : Double   = +07.00
        val TmbhHr : Byte     = 0

        // Perhitungan Waktu Ijtima
        val JDNewMoon = MoonPhasesModified(BlnH, ThnH, 1) - DeltaT(MoonPhasesModified(BlnH, ThnH, 1))/86400.0
        val JamD   : Double   = JDKM(JDNewMoon,TmZn,"Jam D").toDouble()
        println("Ijtimak Geosenteris : " + JDKM(JDNewMoon,TmZn)+ " " + "jam: " + DHHMS(JamD) + " LT")

        val DeltaT    : Double = DeltaT(MoonPhasesModified(BlnH,ThnH,1))
        val JDGhurubS : Double = JDGhurubSyams(JDNewMoon ,gLat,gLon,gAlt,TmZn)

        // Perhitungan Tinggi Hilal
        val THilal1    : Double = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,1)
        val THilal2    : Double = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,2)
        val THilal3    : Double = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,3)
        val THilal4    : Double = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,4)
        val THilal5    : Double = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,5)
        val THilal6    : Double = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,6)
        val THilal7    : Double = TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,7)
        val Azimut1    : Double = Azimut(JDGhurubS,gLat,gLon,gAlt,TmZn,1)
        val Azimut2    : Double = Azimut(JDGhurubS,gLat,gLon,gAlt,TmZn,2)
        val DAz = DifferentAzimut(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val Lm = LamaHilal(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val TH: Double = TerbenamHilal(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val ATH: Double = ArahTerbenamHilal(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val FI: Double = LuasCahayaHilal(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val MRG: String = KemiringanHilal(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val ARCV: Double = BedaTinggiHilal(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val ARCL: Double = ElongasiToposentris(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val CW: Double = LebarHilal(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val q: Double = Range_qOdeh(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val UH: Double = UmurHilal(JDNewMoon,JDGhurubS)

        // Perhitungan Hilal Hari setelah Ijtimak
        val JDNewMoon2 = MoonPhasesModified(BlnH, ThnH, 1) - DeltaT(MoonPhasesModified(BlnH, ThnH, 1))/86400.0
        val DeltaT2    : Double = DeltaT(MoonPhasesModified(BlnH,ThnH,1))
        val JDGhurubS2 : Double = JDGhurubSyams(JDNewMoon2 + TmbhHr ,gLat,gLon,gAlt,TmZn)
        val JGhurubS2  : Double = JDKM(JDGhurubS2 ,TmZn,"Jam D").toDouble()

        println("JD Saat Perhitungan : " + JDGhurubS2.toString() + " (" + JDKM(JDGhurubS2,TmZn) + " Jam: " + DHHMS(JGhurubS2).toString() +" LT"+")" )
        println("Delta T             : ${DeltaT2.round(2)}s")
        println("")
        println("Terbenam Matahari          : " + DHHMS(JGhurubS2).toString() + " LT")

        val latMatahari2: Double = SunGeocentricLatitude(JDGhurubS2)
        println("Lintang Matahari           : ${DDDMS(latMatahari2/3600)}")
        val latBulan2: Double = MoonGeocentricLatitude(JDGhurubS2)
        println("Lintang Bulan              : ${DDDMS(latBulan2)}")
        val lonMatahari2: Double = SunGeocentricLongitude(JDGhurubS2)
        println("Bujur Matahari             : ${DDDMS(lonMatahari2)}")
        val lonBulan2: Double = MoonGeocentricLongitude(JDGhurubS2)
        println("Bujur Bulan                : ${DDDMS(lonBulan2)}")


        val ARMatahari2: Double = SunApparentRightAscension(JDGhurubS2)
        println("Asensiorekta Matahari      : ${DHHMS(ARMatahari2/15)}")
        val ARBulan2: Double = MoonApparentRightAscension(JDGhurubS2)
        println("Asensiorekta Bulan         : ${DHHMS(ARBulan2/15)}")
        val dekMatahari2: Double = SunApparentDeclination(JDGhurubS2)
        println("Deklinasi Matahari         : ${DDDMS(dekMatahari2)}")
        val dekBulan2: Double = MoonApparentDeclination(JDGhurubS2)
        println("Deklinasi Bulan            : ${DDDMS(dekBulan2)}")

        val sdBulan2: Double = MoonAngularSemiDiameter(JDGhurubS2)
        println("Semidiameter Bulan         : ${DDDMS(sdBulan2)}")
        val HPBulan2: Double = MoonEquatorialHorizontalParallax(JDGhurubS2)
        println("HP Bulan                   : ${DDDMS(HPBulan2)}")
        val DistBulan2: Double = MoonGeocentricDistance(JDGhurubS2)
        println("Jarak Bulan                : $DistBulan2")
        println("")

        // Perhitungan Tinggi Hilal
        val THilal12    : Double = TinggiHilal(JDGhurubS2,gLat,gLon,gAlt,TmZn,1)
        println("Tinggi Hilal Geo           : ${DDDMS(THilal12)}")
        val THilal22    : Double = TinggiHilal(JDGhurubS2,gLat,gLon,gAlt,TmZn,2)
        println("Tinggi Hilal Topo          : ${DDDMS(THilal22)}")
        val THilal32    : Double = TinggiHilal(JDGhurubS2,gLat,gLon,gAlt,TmZn,3)
        println("Tinggi Hilal Topo Atas     : ${DDDMS(THilal32)}")
        val THilal42    : Double = TinggiHilal(JDGhurubS2,gLat,gLon,gAlt,TmZn,4)
        println("Tinggi Hilal Topo Bawah    : ${DDDMS(THilal42)}")
        val THilal52    : Double = TinggiHilal(JDGhurubS2,gLat,gLon,gAlt,TmZn,5)
        println("Tinggi Hilal Mar'i Atas    : ${DDDMS(THilal52)}")
        val THilal62    : Double = TinggiHilal(JDGhurubS2,gLat,gLon,gAlt,TmZn,6)
        println("Tinggi Hilal Mar'i Tengah  : ${DDDMS(THilal62)}")
        val THilal72    : Double = TinggiHilal(JDGhurubS2,gLat,gLon,gAlt,TmZn,7)
        println("Tinggi Hilal Mar'i Bawah   : ${DDDMS(THilal72)}")
        val Azimut12    : Double = Azimut(JDGhurubS2,gLat,gLon,gAlt,TmZn,1)
        println("Azimut Matahari            : ${DDDMS(Azimut12)}")
        val Azimut22    : Double = Azimut(JDGhurubS2,gLat,gLon,gAlt,TmZn,2)
        println("Azimut Bulan               : ${DDDMS(Azimut22)}")
        val DAz2 = DifferentAzimut(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Beda Azimut                : ${DDDMS(DAz2)}")
        val Lm2 = LamaHilal(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Lama Hilal                 : ${DHHMS(Lm2)}")
        val TH2: Double = TerbenamHilal(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Terbenam Hilal             : ${DHHMS(TH2)}")
        val ATH2: Double = ArahTerbenamHilal(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Arah terbenam Hilal        : ${DDDMS(ATH2)}")
        val FI2: Double = LuasCahayaHilal(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Fraction Illumination      : ${FI2.round(2)} %")
        val MRG2: String = KemiringanHilal(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Keadaan Hilal              : $MRG2")
        val ARCV2: Double = BedaTinggiHilal(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("ARCV (Beda Tinggi)         : ${DDDMS(ARCV2)}")
        val ARCL2: Double = ElongasiToposentris(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("ARCL (Elongasi)            : ${DDDMS(ARCL2)}")
        val CW2: Double = LebarHilal(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Lebar Hilal                : ${DDDMS(CW2,"DDMMSS",0)}")
//    println("Lebar Hilal                : ${CW.round(9)}")
        val q2: Double = Range_qOdeh(JDGhurubS2, gLat, gLon, gAlt, TmZn)
        println("Range q Odeh               : ${q2.round(3)}")
        val UH2: Double = UmurHilal(JDNewMoon2,JDGhurubS2)
        println("Umur Hilal                 : ${DHHMS(UH2)}")
        val BTime2: Double = JGhurubS2 + (4/9)*Lm2
        println("Best Time                 : ${DHHMS(BTime2)}")

        println("===============================================")
        println("DATA BEDA TINGGI se-INDONESIA")
        var b : Integer
        var k : Integer
        var JDG : Double
        var ARCV3: Double
        var Tz : Double
        var nums: DoubleArray
        var list : ArrayList<Double> = arrayListOf<Double>()
        for (b in 6 downTo -11) {
            for (k in 95..141 step 1) {
                Tz = Math.floor(k.toDouble()/15)
                JDG = JDGhurubSyams(JDNewMoon ,b.toDouble(),k.toDouble(),0.0,Tz)
                ARCV3 = BedaTinggiHilal(JDG,b.toDouble(),k.toDouble(), 0.0, Tz)
                nums = doubleArrayOf(ARCV3)
                println("${b},${k} = ${nums.toList()}")
                list.add(ARCV3)
            }
        }
        println("ARCV MAKSIMAL : ${list.maxOrNull()}")
        println("===============================================")
        println("DATA ELONGASI se-INDONESIA")
        var c : Integer
        var l : Integer
        var JDG1 : Double
        var ARCL3 : Double
        var Tz1 : Double
        var nums1: DoubleArray
        var list1 : ArrayList<Double> = arrayListOf<Double>()
        for (c in 6 downTo -11) {
            for (l in 95..141 step 1) {
                Tz1 = Math.floor(l.toDouble()/15)
                JDG1 = JDGhurubSyams(JDNewMoon ,c.toDouble(),l.toDouble(),0.0,Tz1)
                ARCL3 = ElongasiToposentris(JDG1, c.toDouble(), l.toDouble(), 0.0, Tz1)
                nums1 = doubleArrayOf(ARCL3)
                println("${c},${l} = ${nums1.toList()}")
                list1.add(ARCL3)
            }
        }
        println("ARCL MAKSIMAL : ${list1.maxOrNull()}")

        println("===============================================")
        println("KESIMPULAN BERBAGAI KRITERIA AWAL BULAN")
        val IQg : Double = if (JDNewMoon < JDGhurubS) { 0.0} else {1.0}
        val IQG : String = if (IQg == 0.0) { JDKM(JDNewMoon +1,TmZn)} else {JDKM(JDNewMoon +2 ,TmZn)}
        println("Ijtimak Qablal Ghurub      : $IQG")
        val wh : Double = if (JDNewMoon < JDGhurubS && THilal5>0) { 0.0} else {1.0}
        val WH : String = if(wh == 0.0) {JDKM(JDNewMoon+1,TmZn)} else {JDKM(JDNewMoon+2,TmZn)}
        println("Wujudul Hial               : $WH")
        val IRMabims : Double = if((JDNewMoon < JDGhurubS) && THilal5 >=2.0 || UH >=8.0 ) {0.0} else {1.0}
        val IR_MABIMS : String = if(IRMabims == 0.0) {JDKM(JDNewMoon+1,TmZn)} else {JDKM(JDNewMoon+2,TmZn)}
        println("IR Mabims                  : $IR_MABIMS")
        val IRPersis: Double = if(ARCV >= 4.0 && ARCL >= 6.4) {0.0} else {1.0}
        val IR_Persis : String = if(IRPersis == 0.0) {JDKM(JDNewMoon+1,TmZn)} else {JDKM(JDNewMoon+2,TmZn)}
        println("IR Persis                  : $IR_Persis")
        val IRrhi : Double = if(ARCV >=0.099*Math.pow(Math.abs(DAz),2.0)-1.49*Math.abs(DAz)+10.382) {0.0} else {1.0}
        val IR_RHI : String = if(IRrhi == 0.0) {JDKM(JDNewMoon+1,TmZn)} else {JDKM(JDNewMoon+2,TmZn)}
        println("IR RHI                     : $IR_RHI")
        val IROdeh: Double = if(q >=-0.96) {0.0} else {1.0}
        val IR_Odeh: String = if(IROdeh == 0.0) {JDKM(JDNewMoon+1,TmZn)} else {JDKM(JDNewMoon+2,TmZn)}
        println("IR Odeh                    : $IR_Odeh")
        val IRPersis2: Double = if(list.maxOrNull()!!>= 4.0 && list1.maxOrNull()!!>= 6.4) {0.0} else {1.0}
        val IR_Persis2 : String = if(IRPersis2 == 0.0) {JDKM(JDNewMoon+1,TmZn)} else {JDKM(JDNewMoon+2,TmZn)}
        println("IR Persis Wilayah Hukmi    : $IR_Persis2")

        val JD_IRPersis : Double = if(IRPersis2 == 0.0) {JDNewMoon + 1} else {JDNewMoon + 2}
        val JDAwalBln : Double = Math.floor(JD_IRPersis+0.5+TMZn/24)-0.5-TMZn/24
        val TglHNow     = 1 + Math.abs(JDNow - JDAwalBln).toInt()


        println("JDNow              : $JDNow" )
        println("JDAwalBulanPersis  : $JDAwalBln")
        println("TglHNow            : $TglHNow")
        println("BlnHNow            : $BLNH")
        println("ThnHNow            : $THNH")



        println("")
        println("PERHITUNGAN GERHANA BULAN")
        println("===============================================")
        val BlnHj : Byte    = 11
        val ThnHj : Long    = 1441
        val TZn   : Double  = 0.0
        val gLongT: Double  = (107 + 36/60.0 + 0/3600.0)
        val gLatT : Double  =-(  7 + 5/60.0 + 0/3600.0)
        val gAltT : Double  = 730.0

//    println("tes: " +SolarEclipseObscuration(BlnHj,ThnHj,gLatT,gLongT,gAltT,"Mag"))
//    println("tes: " +SolarEclipseObscuration(BlnHj,ThnHj,gLatT,gLongT,gAltT,"L2_"))


        println("Besselian Elements of Lunar Eclipse")
        val LBE = arrayOf(
            "JDLE " to "JDL",
            "DT   " to "DT",
            "T0   " to "T0",
            "x0   " to "x0",
            "x1   " to "x1",
            "x2   " to "x2",
            "x3   " to "x3",
            "x4   " to "x4",
            "y0   " to "y0",
            "y1   " to "y1",
            "y2   " to "y2",
            "y3   " to "y3",
            "y4   " to "y4",
            "L10  " to "L10",
            "L11  " to "L11",
            "L12  " to "L12",
            "L13  " to "L13",
            "L14  " to "L14",
            "L20  " to "L20",
            "L21  " to "L21",
            "L22  " to "L22",
            "L23  " to "L23",
            "L24  " to "L24",
            "L30  " to "L30",
            "L31  " to "L31",
            "L32  " to "L32",
            "L33  " to "L33",
            "L34  " to "L34",
            "Sc0  " to "Sc0",
            "Sc1  " to "Sc1",
            "Sc2  " to "Sc2",
            "Sc3  " to "Sc3",
            "Sc4  " to "Sc4",
            "M0   " to "M0",
            "M1   " to "M1",
            "M2   " to "M2",
            "M3   " to "M3",
            "M4   " to "M4",
            "dc0  " to "dc0",
            "dc1  " to "dc1",
            "dc2  " to "dc2",
            "dc3  " to "dc3",
            "dc4  " to "dc4",
            "HP0  " to "HP0",
            "HP1  " to "HP1",
            "HP2  " to "HP2",
            "HP3  " to "HP3",
            "HP4  " to "HP4"
        )

        for(t in LBE){
            val tVal = LBesselian(BlnHj,ThnHj,t.second.toString())
            val tVt: String = if (tVal < 0.0) tVal.round(7) else if (t.second.toString() == "T0") " "+tVal.round(0) else if (t.second.toString() =="DT") " "+tVal.round(0) +"s" else " "+tVal.round(7)
            if(!tVal.isNaN()) {
                println(t.first.toString()+"  : "+tVt)
            }else{
                println(t.first.toString()+"  : -")
            }
        }
        println("=============================================")
        val Tx = arrayOf(
            "T0" to "T0",
            "DT" to "DT",
            "T1" to "T1",
            "T2" to "T2",
            "T3" to "T3"
        )
        println(JenisLunarEclipse(BlnHj,ThnHj))
        for(t in Tx){
            val tVal = LunarEclipse(BlnHj,ThnHj,t.second.toString())
            val tTx: String  = if (t.second.toString() == "T0") tVal.round(0) else if (t.second.toString() == "DT") tVal.round(0)+"s" else DHHMS(tVal,"HH:MM:SS",0)
            if(!tVal.isNaN()) {
                println(t.first.toString()+"  : "+tTx)
            }else{
                println(t.first.toString()+"  : -")
            }
        }
        println("   ")

        //Array of Pair first : kode untuk di print, second : kode untuk di parameter
        val tLE = arrayOf(
            "P1"  to "P1",
            "U1"  to "U1",
            "U2"  to "U2",
            "Mx"  to "TG",
            "U3"  to "U3",
            "U4"  to "U4",
            "P4"  to "P4")

        for(t in tLE){
            val tVal = LunarEclipse(BlnHj,ThnHj,t.second.toString())
            if(!tVal.isNaN()) {
                val tTgl = JDKM(+tVal,TZn)
                val tJam = if (t.second.toString() == "TG") DHHMS(JDKM(tVal,TZn,"Jam Des").toDouble(),"HH:MM:SS",1) else DHHMS(JDKM(tVal,TZn,"Jam Des").toDouble(),"HH:MM:SS",0)
                println(t.first.toString()+"  :"+tTgl+", Jam: " + tJam)
            }else{
                println(t.first.toString()+"  : -")
            }
        }

        println("   ")
        val tMLE = arrayOf(
            "Magnitude Penumbra  " to "MagP",
            "Magnitude Umbra     " to "MagU"
        )
        for(t in tMLE){
            val tVal = LunarEclipse(BlnHj,ThnHj,t.second.toString())
            val tVt: String = if (tVal < 0.0) tVal.round(4) else " "+tVal.round(4)
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+tVt)
            }else{
                println(t.first.toString()+": -")
            }
        }
        val tDLE = arrayOf(
            "Durasi Penumbra     " to "DurP",
            "Durasi Umbra        " to "DurU",
            "Durasi Total        " to "DurT"
        )
        for(t in tDLE){
            val tVal = LunarEclipse(BlnHj,ThnHj,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DHHMS(tVal,"HH:MM:SS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println ("")
        val LEAltAzmMax = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        println("Altitude dan Azimut Phase Puncak (Max)")
        for (t in LEAltAzmMax) {
            val tVal = LEAltAzmMax(BlnHj,ThnHj,gLongT,gLatT,gAltT,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DDDMS(tVal,"DDMMSS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println ("")
        val LEAltAzmP1 = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        println("Altitude dan Azimut Phase P1")
        for (t in LEAltAzmP1) {
            val tVal = LEAltAzmP1(BlnHj,ThnHj,gLongT,gLatT,gAltT,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DDDMS(tVal,"DDMMSS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println ("")
        val LEAltAzmU1 = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        println("Altitude dan Azimut Phase U1")
        for (t in LEAltAzmU1) {
            val tVal = LEAltAzmU1(BlnHj,ThnHj,gLongT,gLatT,gAltT,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DDDMS(tVal,"DDMMSS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println ("")
        val LEAltAzmU2 = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        println("Altitude dan Azimut Phase U2")
        for (t in LEAltAzmU2) {
            val tVal = LEAltAzmU2(BlnHj,ThnHj,gLongT,gLatT,gAltT,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DDDMS(tVal,"DDMMSS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println ("")
        val LEAltAzmU3 = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        println("Altitude dan Azimut Phase U3")
        for (t in LEAltAzmU3) {
            val tVal = LEAltAzmU3(BlnHj,ThnHj,gLongT,gLatT,gAltT,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DDDMS(tVal,"DDMMSS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println ("")
        val LEAltAzmU4 = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        println("Altitude dan Azimut Phase U4")
        for (t in LEAltAzmU4) {
            val tVal = LEAltAzmU4(BlnHj,ThnHj,gLongT,gLatT,gAltT,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DDDMS(tVal,"DDMMSS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println ("")
        val LEAltAzmP4 = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        println("Altitude dan Azimut Phase P4")
        for (t in LEAltAzmP4) {
            val tVal = LEAltAzmP4(BlnHj,ThnHj,gLongT,gLatT,gAltT,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+": "+DDDMS(tVal,"DDMMSS",0))
            }else{
                println(t.first.toString()+": -")
            }
        }
        println("")

        println("PERHITUNGAN GERHANA MATAHARI")
        val BlHj : Byte     = 10
        val ThHj : Long     = 1441
        val TZN   : Double  = 0.0
        val gLonP: Double   = (108 + 19/60.0 + 12/3600.0)
        val gLatP : Double  =-(  6 + 19/60.0 + 48/3600.0)
        val gAltP : Double  = 2.0

        val SBE = arrayOf(
            "JDS" to "JDS",
            "DT "  to "DT",
            "T0 "  to "T0",
            "x0 "  to "x0",
            "x1 "  to "x1",
            "x2 "  to "x2",
            "x3 "  to "x3",
            "x4 "  to "x4",
            "y0 "  to "y0",
            "y1 "  to "y1",
            "y2 "  to "y2",
            "y3 "  to "y3",
            "y4 "  to "y4",
            "dm0"  to "dm0",
            "dm1"  to "dm1",
            "dm2"  to "dm2",
            "dm3"  to "dm3",
            "dm4"  to "dm4",
            "L10"  to "L10",
            "L11"  to "L11",
            "L12"  to "L12",
            "L13"  to "L13",
            "L14"  to "L14",
            "L20"  to "L20",
            "L21"  to "L21",
            "L22"  to "L22",
            "L23"  to "L23",
            "L24"  to "L24",
            "M0 "  to "M0",
            "M1 "  to "M1",
            "M2 "  to "M2",
            "M3 "  to "M3",
            "M4 "  to "M4",
            "f1 "  to "f1",
            "f2 "  to "f2"
        )
        println("Besselian Elements of Solar Eclipse")
        for(t in SBE){
            val tVal = SBesselian(BlHj,ThHj,t.second.toString())
            val tVt: String = if (tVal < 0.0) tVal.round(7) else if (t.second.toString() == "T0") " "+tVal.round(0) else if (t.second.toString() =="DT") " "+tVal.round(0) +"s" else " "+tVal.round(7)
            if(!tVal.isNaN()) {
                println(t.first.toString()+"  : "+tVt)
            }else{
                println(t.first.toString()+"  : -")
            }
        }

        println(JenisSolarEclipse(BlHj,ThHj,gLatP,gLonP,gAltP))

        val tSE = arrayOf(
            "U1" to "JDSU1",
            "U2" to "JDSU2",
            "Mx" to "JDSMx",
            "U3" to "JDSU3",
            "U4" to "JDSU4"
        )
        for(t in tSE){
            val tVal = SolarEclipse(BlHj,ThHj,gLatP,gLonP,gAltP,t.second.toString())
            if(!tVal.isNaN()) {
                val tTgl = JDKM(+tVal,TZN)
                val tJam = DHHMS(JDKM(tVal,TZN,"Jam Des").toDouble(),"HH:MM:SS",0)
                println(t.first.toString()+"  : "+tTgl+", Jam: " + tJam)
            } else {
                println(t.first.toString()+"  : -")
            }
        }

        val dSE = arrayOf(
            "Durasi Gerhana      " to "DurG",
            "Durasi Total/Cincin " to "DurT"
        )
        for(t in dSE){
            val tVal = SolarEclipse(BlHj,ThHj,gLatP,gLonP,gAltP,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+"  : "+DHHMS(tVal,"HH:MM:SS",0))
            } else {
                println(t.first.toString()+"  : -")
            }
        }

        val oSE = arrayOf(
            "Magnitude Umbra     " to "MagU",
            "Obskurasi           " to "Obsk"
        )
        for(t in oSE){
            val tVal = SolarEclipse(BlHj,ThHj,gLatP,gLonP,gAltP,t.second.toString())
            val tTx : String = if (t.second.toString() == "Obsk") tVal.round(3) + " %"  else tVal.round(3)
            if(!tVal.isNaN()) {
                println(t.first.toString()+"  : "+tTx)
            } else {
                println(t.first.toString()+"  : -")
            }
        }

        val pSE = arrayOf(
            "Altitude U1 " to "AltU1",
            "Azimut U1   " to "AzmU1",
            "Altitude U2 " to "AltU2",
            "Azimut U2   " to "AzmU2",
            "Altitude Mx " to "AltMx",
            "Azimut Mx   " to "AzmMx",
            "Altitude U3 " to "AltU3",
            "Azimut U3   " to "AzmU3",
            "Altitude U4 " to "AltU4",
            "Azimut U4   " to "AzmU4"
        )
        for(t in pSE){
            val tVal = SolarEclipse(BlHj,ThHj,gLatP,gLonP,gAltP,t.second.toString())
            if(!tVal.isNaN()) {
                println(t.first.toString()+"  : "+tVal.round(0) +"°")
            } else {
                println(t.first.toString()+"  : -")
            }
        }

    }

    fun MoonTransitRiseSet( CCalD   : Byte,
                            CCalM   : Byte,
                            CCalY   : Long,
                            GeogLat : Double,
                            GeogLon : Double,
                            GeogAlt : Double,
                            TZone   : Double,
                            TRSType : String,
                            MaxItr  : Int) : Any {

        var JD00LT    : Double
        var JD00UT    : Double
        var JDE00UT   : Double
        var alphaMm1d : Double
        var alphaM00d : Double
        var alphaMp1d : Double
        var deltaMm1d : Double
        var deltaM00d : Double
        var deltaMp1d : Double
        var Pi        : Double
        var h0        : Double
        var cosHA0    : Double
        var HA0       : Any
        var T         : Double
        var Theta0    : Double
        var M         : Double
        var sTheta0   : Double = 0.0
        var nT        : Double
        var alphaM    : Double = 0.0
        var deltaM    : Double = 0.0
        var HA        : Double
        var h         : Double
        var dltm      : Double = 0.0
        var JDTRS     : Double
        var TTRS      : Any = 0.0


        JD00UT = KMJD(CCalD, CCalM, CCalY, TZone, TZone) + -1

        for (dItr in 1..3) {
            JDE00UT = JD00UT + DeltaT(JD00UT) / 86400.0
            alphaM00d = MoonApparentRightAscension(JDE00UT)
            alphaMm1d = MoonApparentRightAscension((JDE00UT - 1))
            alphaMp1d = MoonApparentRightAscension((JDE00UT + 1))

            if (TRSType == "TRANSIT") {
                deltaM00d = 0.0
                deltaMm1d = 0.0
                deltaMp1d = 0.0
            } else {
                deltaM00d = MoonApparentDeclination(JDE00UT)
                deltaMm1d = MoonApparentDeclination((JDE00UT - 1))
                deltaMp1d = MoonApparentDeclination((JDE00UT + 1))
            }

            Pi = MoonEquatorialHorizontalParallax(JDE00UT)

            h0 = -(34 / 60.0) + 0.7275 * Pi - 0.0353 * Math.sqrt(0.0)
            cosHA0 = (Math.sin(Rad(h0)) - Math.sin(Rad(GeogLat)) * Math.sin(Rad(deltaM00d))) /(Math.cos(Rad(GeogLat)) * Math.cos(Rad(deltaM00d)))
            if (Math.abs(cosHA0) <= 1) {
                HA0 = Deg(Math.acos(cosHA0))
            } else {
                HA0 = "!circumpolar"
            }

            T = (JDE00UT - 2451545) / 36525.0

            Theta0 = (100.46061837) + (36000.770053608 * T) + (0.000387933 * T*T) - (Math.pow (T, 3.0) / 38710000) + (NutationInLongitude(JDE00UT) * Math.cos(Rad(ObliquityOfEcliptic(JDE00UT))))

            Theta0 = Mod(Theta0, 360.0)

            M = (alphaM00d - GeogLon - Theta0) / 360.0

            when (TRSType) {
                "TRANSIT "   -> M = M
                "RISE"       -> M = M - HA0.toString().toDouble() / 360.0
                "SET"       -> M = M + HA0.toString().toDouble() / 360.0
            }

            M = Mod(M, 1.0)

            for (Itr in 1..MaxItr) {
                sTheta0 = Theta0 + 360.985647 * M
                sTheta0 = Mod(sTheta0, 360.0)
                nT = M
                alphaM = Mod(
                    alphaM00d + nT / 2.0 * (Mod((alphaM00d - alphaMm1d), 360.0) + Mod(
                        (alphaMp1d - alphaM00d),
                        360.0
                    ) + nT * (Mod((alphaMp1d - alphaM00d), 360.0) - Mod((alphaM00d - alphaMm1d), 360.0))), 360.0
                )

                if (TRSType == "TRANSIT") {
                    deltaM = 0.0
                } else {
                    deltaM =
                        deltaM00d + nT / 2.0 * ((deltaM00d - deltaMm1d) + (deltaMp1d - deltaM00d) + nT * ((deltaMp1d - deltaM00d) - (deltaM00d - deltaMm1d)))
                }

                HA = sTheta0 + GeogLon - alphaM
                if (Mod(HA, 360.0) > 180.0) {
                    HA = Mod(HA, 360.0) - 360.0
                } else {
                    HA = Mod(HA, 360.0)
                }

                h = Deg(Math.asin(Math.sin(Rad(GeogLat)) * Math.sin(Rad(deltaM)) + Math.cos(Rad(GeogLat)) * Math.cos(Rad(deltaM)) * Math.cos(Rad(HA))))

                when (TRSType) {
                    "TRANSIT"       -> dltm = -HA / 360.0
                    "RISE", "SET"   -> dltm = (h - h0) / (360.0 * Math.cos(Rad(deltaM)) * Math.cos(Rad(GeogLat)) * Math.sin(Rad(HA)))
                }

                M = Mod(M + dltm, 1.0)
            }

            JDTRS = JD00UT + M
            JD00LT = KMJD(CCalD, CCalM, CCalY, 0.0, TZone)
            TTRS = JDKM(JDTRS, TZone, "JAMDES")

            if ((JDTRS >= (JD00LT + 0)) && (JDTRS <= (JD00LT + 1))) {
                TTRS = JDKM(JDTRS, TZone, "JAMDES")
            } else {
                JD00UT = JD00UT + 1
                TTRS = "x"
            }

        }
        return TTRS
    }

    fun GeocentricConjunction(HijriMonth: Byte,
                              HijriYear: Long): Double {

        val JDNewmoon : Double
        val x1 : Double
        val x2 : Double
        val x3 : Double
        val y1 : Double
        val y2 : Double
        val y3 : Double
        val A  : Double
        val B  : Double
        val C  : Double
        var n0 : Double
        val JDNewmoonGeo : Double

        JDNewmoon = MoonPhasesModified(HijriMonth, HijriYear, 1)
        x1 = JDNewmoon - 1 / 24.0
        x2 = JDNewmoon
        x3 = JDNewmoon + 1 / 24.0

        y1 = SunGeocentricLongitude(x1) - MoonGeocentricLongitude(x1)
        y2 = SunGeocentricLongitude(x2) - MoonGeocentricLongitude(x2)
        y3 = SunGeocentricLongitude(x3) - MoonGeocentricLongitude(x3)

        A = y2 - y1
        B = y3 - y2
        C = B - A

        n0 = 0.0
        for (i in 1..10) {
            n0 = -2 * y2 / (A + B + C * n0)
        }
        JDNewmoonGeo = JDNewmoon + n0 / 24.0

        return JDNewmoonGeo
    }
}