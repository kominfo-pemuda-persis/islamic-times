package id.someah.islamictimes.model

import java.util.*

data class CalendarModel (
    val isPreview : Boolean = false,
    val isActive : Boolean = false,
    val masehiNumber : String,
    val hijriNumber : String,
    var date : Date? = null,
    var fullHijriNumber : Triple<Int,Int,Long> = Triple(0,0,0)
)

data class CalendarHijri (
    val isActive : Boolean,
    val month : String,
    val dates : List<CalendarModel>
)
