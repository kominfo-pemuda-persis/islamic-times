package id.someah.islamictimes.model

enum class HijriModel {
    AlMuharram {
        override fun getString(): String = "Al-Muharram"
        override fun getDays(): Int = 30
        override fun getMonth(): Int {
            return 1
        }

    },
    Shafar {
        override fun getString(): String = "Shafar"
        override fun getDays(): Int  = 29
        override fun getMonth(): Int {
            return 2
        }
    },
    RabialAwwal {
        override fun getString(): String = "Rabi al-Awwal"
        override fun getDays(): Int = 30
        override fun getMonth(): Int {
            return 3
        }
    },
    RabialAkhir {
        override fun getString(): String = "Rabi al-Akhir"
        override fun getDays(): Int = 29
        override fun getMonth(): Int {
            return 4
        }
    },
    JumadaalUla {
        override fun getString(): String = "Jumada al-Ula"
        override fun getDays(): Int = 29
        override fun getMonth(): Int {
            return 5
        }
    },
    JumadaalAkhirah {
        override fun getString(): String = "Jumada al-Akhirah"
        override fun getDays(): Int = 29
        override fun getMonth(): Int {
            return 6
        }
    },
    Rajab {
        override fun getString(): String = "Rajab"
        override fun getDays(): Int = 30
        override fun getMonth(): Int {
            return 7
        }
    },
    Syaban {
        override fun getString(): String = "Syaban"
        override fun getDays(): Int = 29
        override fun getMonth(): Int {
            return 8
        }
    },
    Ramadhan {
        override fun getString(): String = "Ramadhan"
        override fun getDays(): Int = 30
        override fun getMonth(): Int {
            return 9
        }
    },
    Syawal {
        override fun getString(): String = "Syawal"
        override fun getDays(): Int = 29
        override fun getMonth(): Int {
            return 10
        }
    },
    Dzulqadah {
        override fun getString(): String = "Dzulqa'dah"
        override fun getDays(): Int = 30
        override fun getMonth(): Int {
            return 11
        }
    },
    Dzulhijjah {
        override fun getString(): String = "Dzulhijjah"
        override fun getDays(): Int = 29
        override fun getMonth(): Int {
            return 12
        }
    }
    ;

    abstract fun getString() : String
    abstract fun getDays() : Int
    abstract fun getMonth() : Int

}