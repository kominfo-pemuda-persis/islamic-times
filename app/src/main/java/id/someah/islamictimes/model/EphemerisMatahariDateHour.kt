package id.someah.islamictimes.model

data class EphemerisMatahariDateHour(
    val apparentLongitude : String, val eclipticLatitude : String, val apparentRightAscension : String,
    val apparentDeclination : String, val horizontalParallax : String, val semiDiameter : String,
    val trueGeocentDist : String, val equationOfTime : String, val gha : String
)