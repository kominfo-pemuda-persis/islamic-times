package id.someah.islamictimes.model

data class NextWaktuSholat(var namaSholat : String = "", var timeMili : Long = 0, var waktuSholat : String = "")