package id.someah.islamictimes.model

import android.app.AlarmManager

enum class DurationUpdate {

    MINUTES15 {
        override fun getTime(): Long {
            return AlarmManager.INTERVAL_FIFTEEN_MINUTES
        }
    },
    MINUTES20 {
        override fun getTime(): Long {
            return 20 * 60 * 1000;
        }

    },
    MINUTES30 {
        override fun getTime(): Long {
            return AlarmManager.INTERVAL_HALF_HOUR
        }

    };

    abstract fun getTime() : Long
}