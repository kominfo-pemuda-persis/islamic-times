package id.someah.islamictimes.model

data class MatahariBulan(var gha:String, var lha:String, var ra:String,
                         var dec:String, var lintang:String, var bujur:String,
                         var alt : String, var azimut:String , var backAzimut : String? = null ,
                         var decimalAzimut : String? = null , var decimalBackAzimuth : String? = null)