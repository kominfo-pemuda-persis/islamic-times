package id.someah.islamictimes.model

data class EphemerisBulanDateHour(
    val apparentLongitude : String, val apparentLatitude : String, val apparentRightAscension : String,
    val apparentDeclination : String, val horizontalParallax : String, val semiDiameter : String,
    val angleBrightLimb : String, val fractionIllumination : String, val gha : String
)