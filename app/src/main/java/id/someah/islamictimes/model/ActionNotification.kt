package id.someah.islamictimes.model

enum class ActionNotification  {
    STOP {
        override fun text(): String = "stop"
    };

    abstract fun text() : String
}