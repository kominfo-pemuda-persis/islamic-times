package id.someah.islamictimes.model

data class EphemerisMatahariBulan(val ephemerisMatahari: EphemerisMatahari, val ephemerisBulan: EphemerisBulan)