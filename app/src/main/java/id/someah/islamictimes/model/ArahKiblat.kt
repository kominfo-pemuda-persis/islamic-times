package id.someah.islamictimes.model

data class ArahKiblat(var lokasi:String = "", var bayanganMatahari:String = "", var arahKiblat: String = "",
                      var jarakKeKiblat : Double = 0.0, var kiblatHarian : String = "", var kiblatHarian2: String = "", var rashdulKiblat1 : String = "",
                      var rashdulKiblat2 : String = "", var lattitude : Double = 0.0, var longitude : Double = 0.0 )