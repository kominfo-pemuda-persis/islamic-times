package id.someah.islamictimes.preferences

import android.content.Context
import android.content.SharedPreferences

//class untuk set dan get shared preferences
//setiap fungsi get mempunyai parameter input nama preference, default value. kecuali preference double
//save preference mempunyai parameter input nama preference, value preference.
class SharedPref(context: Context) {
    private val PREFS_NAME = "islamic_times_preferences"
    private val sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)


    fun saveStringPref(KEY_NAME: String, text: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString(KEY_NAME, text)
        editor.apply()
    }

    fun getValueString(KEY_NAME: String, defultValue : String): String? {
        return sharedPref.getString(KEY_NAME, defultValue)
    }

    fun saveIntPref(KEY_NAME: String, text: Int) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putInt(KEY_NAME, text)
        editor.apply()
    }

    fun getValueInt(KEY_NAME: String, defValue : Int): Int? {
        return sharedPref.getInt(KEY_NAME, defValue)
    }

    fun saveDoublePref(KEY_NAME: String, text: Double){
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putLong(KEY_NAME, java.lang.Double.doubleToRawLongBits(text))
        editor.apply()
    }

    fun getValueDouble(KEY_NAME: String) : Double? {
        return java.lang.Double.longBitsToDouble(sharedPref.getLong(KEY_NAME, java.lang.Double.doubleToRawLongBits(0.0)))
    }

    fun saveBoolNotifPref(KEY_NAME: String, text: Boolean) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putBoolean(KEY_NAME, text)
        editor.apply()
    }

    fun getValueNotifBool(KEY_NAME: String, defaultValue : Boolean): Boolean? {
        //default notification on
        return sharedPref.getBoolean(KEY_NAME, defaultValue)
    }

    fun clearSharedPreference() {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.clear()
        editor.apply()
    }

    fun removeValue(KEY_NAME: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.remove(KEY_NAME)
        editor.apply()
    }
}