package id.someah.islamictimes.preferences

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.preference.DialogPreference
import androidx.preference.PreferenceDialogFragmentCompat
import id.someah.islamictimes.R

//class untuk menampilkan preference dialog di pengaturan altitude matahari
class CustomDialogAltMatahariPreference(context: Context?, attrs: AttributeSet?) :
    DialogPreference(context, attrs){

    init {
        dialogLayoutResource =
            R.layout.custom_dialog_alt_matahari
        negativeButtonText = null
    }

    class DialogPreferenceAltMatahariCompat(context: Context?) : PreferenceDialogFragmentCompat(){
        private lateinit var buttonMinIsya : Button
        private lateinit var buttonMinShubuh : Button
        private lateinit var buttonPlusIsya : Button
        private lateinit var buttonPlusShubuh : Button
        private lateinit var isyaValue : TextView
        private lateinit var shubuhValue : TextView

//        private val sharedPref =
//            context?.let { SharedPref(it) } //error lint but works
        private lateinit var sharedPref : SharedPref
        var counterIsya = 0
        var counterShubuh = 0

        override fun onBindDialogView(view: View?) {
            val altIsya = sharedPref.getValueInt("alt_matahari_isya", -18)
            val altShububh = sharedPref.getValueInt("alt_matahari_shubuh", -20)
            //check value dari shared preferences ada atau ngga
            if( altIsya != null){
                isyaValue.text = altIsya.toString()
            }
            if( altShububh != null){
                shubuhValue.text = altShububh.toString()
            }

            super.onBindDialogView(view)
        }

        override fun onCreateDialogView(context: Context): View {
            val view = super.onCreateDialogView(context)
            sharedPref = context.let { SharedPref(it) }
            buttonMinIsya = view.findViewById(R.id.button_min_alt_isya)
            buttonMinShubuh = view.findViewById(R.id.button_min_alt_shubuh)
            buttonPlusIsya = view.findViewById(R.id.button_plus_alt_isya)
            buttonPlusShubuh= view.findViewById(R.id.button_plus_alt_shubuh)
            isyaValue = view.findViewById(R.id.tv_alt_isya_value)
            shubuhValue = view.findViewById(R.id.tv_alt_shubuh_value)

            //plus data altIsya lalu save ke shared preferences
            buttonPlusIsya.setOnClickListener {
                counterIsya++
                isyaValue.text = counterIsya.toString()
                sharedPref?.saveIntPref("alt_matahari_isya", counterIsya)
            }
            //min data altIsya lalu save ke shared preferences
            buttonMinIsya.setOnClickListener {
                counterIsya--
                isyaValue.text = counterIsya.toString()
                sharedPref?.saveIntPref("alt_matahari_isya", counterIsya)
            }
            //plus data altSubuh lalu save ke shared preferences
            buttonPlusShubuh.setOnClickListener {
                counterShubuh++
                shubuhValue.text = counterShubuh.toString()
                sharedPref?.saveIntPref("alt_matahari_shubuh", counterShubuh)
            }
            //min data altSubuh lalu save ke shared preferences
            buttonMinShubuh.setOnClickListener {
                counterShubuh--
                shubuhValue.text = counterShubuh.toString()
                sharedPref?.saveIntPref("alt_matahari_shubuh", counterShubuh)
            }
            return view
        }

        override fun onDialogClosed(positiveResult: Boolean) {
        }

        companion object{
            fun newInstance(key:String, context: Context?) : DialogPreferenceAltMatahariCompat {
                val f =
                    DialogPreferenceAltMatahariCompat(
                        context
                    )
                val b = Bundle(1)
                b.putString(ARG_KEY, key)
                f.arguments = b
                return f
            }
        }

    }
}