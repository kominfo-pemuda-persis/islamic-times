package id.someah.islamictimes.widget.service

import android.content.Intent
import android.widget.RemoteViewsService
import id.someah.islamictimes.widget.factory.DaftarJadwalSholatFactory

class DaftarJadwalSholatViewService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent?): RemoteViewsFactory {
        return DaftarJadwalSholatFactory(this.application,intent)
    }
}