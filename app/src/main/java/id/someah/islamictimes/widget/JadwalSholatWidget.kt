package id.someah.islamictimes.widget

import android.annotation.SuppressLint
import android.app.Activity
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import id.someah.islamictimes.R
import id.someah.islamictimes.adapters.WaktuAdzanAdapter
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.convertDateIfMoreThan24Hours
import id.someah.islamictimes.ui.activity.MainActivity
import kotlinx.android.synthetic.main.fragment_dashboard.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Implementation of App Widget functionality.
 */
class JadwalSholatWidget : AppWidgetProvider() {
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

@SuppressLint("SimpleDateFormat")
internal fun updateAppWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int
) {
    val widgetText = context.getString(R.string.appwidget_text)
    // Construct the RemoteViews object
    val views = RemoteViews(context.packageName, R.layout.jadwal_sholat_widget)
    val sharedPref = SharedPref(context)
    val data = DaoManager().waktuSholatDao(context)
    val date = Date()
    val formatter = SimpleDateFormat("dd MMMM yyyy")


    if (sharedPref.getValueString("alamat_user", "") == null || sharedPref.getValueString("alamat_user", "") == "0") {
        views.setTextViewText(R.id.textViewLocation,context.getString(R.string.unlocated))
    } else {
        views.setTextViewText(R.id.textViewLocation,sharedPref.getValueString("alamat_user", "")!!.replace("Kota", ""))
    }

    views.setTextViewText(R.id.textViewNamaSholat,data?.getNextWaktuSholat()?.namaSholat)
    views.setTextViewText(R.id.textViewJamDashboard,data?.getNextWaktuSholat()?.waktuSholat?.convertDateIfMoreThan24Hours())
    views.setTextViewText(R.id.textViewTglMasehi,formatter.format(date).toString())

    views.setOnClickPendingIntent(R.id.root, PendingIntent.getActivity(context,0,
        Intent(context, MainActivity::class.java),0))


//    val adapter = WaktuAdzanAdapter(context)

    // Instruct the widget manager to update the widget
    appWidgetManager.updateAppWidget(appWidgetId, views)
}