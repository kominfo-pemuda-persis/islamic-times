package id.someah.islamictimes.widget.factory

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Binder
import android.widget.AdapterView
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import id.someah.islamictimes.R
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.model.WaktuSholat
import id.someah.islamictimes.services.utils.convertDateIfMoreThan24Hours
import org.jetbrains.annotations.Contract

class DaftarJadwalSholatFactory constructor(val application: Context , val intent : Intent?) : RemoteViewsService.RemoteViewsFactory {
    var data : ArrayList<WaktuSholat> = ArrayList()
    override fun onCreate() {
        DaoManager().waktuSholatDao(application)?.getWaktuSholat()?.let {
            data = it
        }
    }

    override fun onDataSetChanged() {
        val identityToken = Binder.clearCallingIdentity()

        DaoManager().waktuSholatDao(application)?.getWaktuSholat()?.let {
            data = it
        }
        Binder.restoreCallingIdentity(identityToken)
    }

    override fun onDestroy() {
        TODO("Not yet implemented")
    }

    override fun getCount(): Int  = data.size

    override fun getViewAt(position: Int): RemoteViews? {
       if(position == AdapterView.INVALID_POSITION || data.size == 0) {
           return null
       }

        val rv = RemoteViews(application.packageName, R.layout.item_waktu_sholat_widget)
        rv.setTextViewText(R.id.txt_nama_sholat,data[position].namaSholat)
        rv.setTextViewText(R.id.txt_jam_sholat,data[position].jamSholat.convertDateIfMoreThan24Hours())
        return rv
    }

    override fun getLoadingView(): RemoteViews? = null

    override fun getViewTypeCount(): Int = 1

    override fun getItemId(position: Int): Long = position.toLong()

    override fun hasStableIds(): Boolean = true
}