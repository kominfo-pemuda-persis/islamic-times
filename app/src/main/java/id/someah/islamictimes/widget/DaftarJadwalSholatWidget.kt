package id.someah.islamictimes.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import android.widget.Toast
import id.someah.islamictimes.R
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.convertDateIfMoreThan24Hours
import id.someah.islamictimes.ui.activity.MainActivity
import id.someah.islamictimes.widget.service.DaftarJadwalSholatViewService

/**
 * Implementation of App Widget functionality.
 */
class DaftarJadwalSholatWidget : AppWidgetProvider() {
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidgetJadwal(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

internal fun updateAppWidgetJadwal(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int
) {
    val views = RemoteViews(context.packageName, R.layout.daftar_jadwal_sholat_widget)
    val dao = DaoManager().waktuSholatDao(context)
    val sharedPref = SharedPref(context)

    if (sharedPref.getValueString("alamat_user", "") == null || sharedPref.getValueString("alamat_user", "") == "0") {
        views.setTextViewText(R.id.textViewLocation,context.getString(R.string.unlocated))
    } else {
        views.setTextViewText(R.id.textViewLocation,sharedPref.getValueString("alamat_user", "")!!.replace("Kota", ""))
    }

    dao?.getNextWaktuSholat()?.let {
        views.setTextViewText(R.id.textViewNamaSholat,it.namaSholat)
        views.setTextViewText(R.id.textViewJamDashboard,it.waktuSholat?.convertDateIfMoreThan24Hours())
        setActiveWaktuSholat(views,it.namaSholat)
    }

    dao?.getWaktuSholat()?.let {
        views.setTextViewText(R.id.tvDzuhur, it[0].jamSholat.convertDateIfMoreThan24Hours())
        views.setTextViewText(R.id.tvAsar, it[1].jamSholat.convertDateIfMoreThan24Hours())
        views.setTextViewText(R.id.tvMagrib, it[2].jamSholat.convertDateIfMoreThan24Hours())
        views.setTextViewText(R.id.tvIsya, it[3].jamSholat.convertDateIfMoreThan24Hours())
        views.setTextViewText(R.id.tvSubuh, it[5].jamSholat.convertDateIfMoreThan24Hours())
    }

    views.setOnClickPendingIntent(R.id.root, PendingIntent.getActivity(context,0,
        Intent(context,MainActivity::class.java),0))
//    val intent = Intent(context,DaftarJadwalSholatViewService::class.java)

//    views.setRemoteAdapter(R.id.widgetListView,intent)

    appWidgetManager.updateAppWidget(appWidgetId, views)
}

fun setActiveWaktuSholat(view : RemoteViews , namaSholat : String) {

    view.setToNullLayout(R.id.layoutDhuzur)
    view.setToNullLayout(R.id.layoutAsar)
    view.setToNullLayout(R.id.layoutMagrib)
    view.setToNullLayout(R.id.layoutIsya)
    view.setToNullLayout(R.id.layoutSubuh)

    when(namaSholat.toLowerCase()) {
        "zuhur" -> {
            view.setToLayout(R.id.layoutDhuzur)
        }
        "asar" -> {
            view.setToLayout(R.id.layoutAsar)
        }
        "magrib" -> {
            view.setToLayout(R.id.layoutMagrib)
        }
        "isya" -> {
            view.setToLayout(R.id.layoutIsya)
        }
        "subuh" -> {
            view.setToLayout(R.id.layoutSubuh)
        }
    }
}

fun RemoteViews.setToLayout(layout : Int) {
    this.setInt(layout,"setBackgroundResource",R.color.white_20)
}

fun RemoteViews.setToNullLayout(layout : Int) {
    this.setInt(layout,"setBackgroundResource",0)
}

