package id.someah.islamictimes.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.R
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.model.WaktuSholat
import kotlinx.android.synthetic.main.item_ihtiyat.view.*

class IhtiyathAdapter(private val context: Context) : RecyclerView.Adapter<IhtiyathAdapter.IhtiyatViewHolder>() {

    private var listWaktuSholat : ArrayList<WaktuSholat> = arrayListOf()
    private var sharedPref : SharedPref =
        SharedPref(context)

    fun setList(listWaktuSholat : ArrayList<WaktuSholat>){
        this.listWaktuSholat = listWaktuSholat
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IhtiyatViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_ihtiyat, parent, false)
        return IhtiyatViewHolder(view)
    }

    override fun getItemCount(): Int = listWaktuSholat.size

    override fun onBindViewHolder(holder: IhtiyatViewHolder, position: Int) {
        var ihtiyath : Int

        holder.bindItem(listWaktuSholat[position])
        val namaSholat = listWaktuSholat[position].namaSholat

        if(sharedPref.getValueInt("ihtiyat_$namaSholat", 2) != null){
            ihtiyath  = sharedPref.getValueInt("ihtiyat_$namaSholat", 2)!!
            //default value untuk syuruk/akhir subuh beda
            if(namaSholat == Constant.NAMA_AKHIR_SUBUH){
                ihtiyath = sharedPref.getValueInt("ihtiyat_$namaSholat", -2)!!
                holder.itemView.tv_ihtiyat_value.text = ihtiyath.toString()
            }
            //hilangin waktu akhir isya dan dhuha di ihtiyat
            else if(namaSholat == Constant.NAMA_AKHIR_ISYA || namaSholat == Constant.NAMA_SHOLAT_DUHA){
                holder.itemView.tv_ihtiyat_value.visibility = View.GONE
                holder.itemView.tv_nama_ihtiyat.visibility = View.GONE
                holder.itemView.button_plus_ihtiyat.visibility = View.GONE
                holder.itemView.button_min_ihtiyat.visibility = View.GONE
                holder.itemView.viewLineIhtiyat.visibility = View.GONE
            }
            else {
                holder.itemView.tv_ihtiyat_value.text =
                    sharedPref.getValueInt("ihtiyat_$namaSholat", 2).toString()
            }
        }
        else{
            ihtiyath = 0
            holder.itemView.tv_ihtiyat_value.text = ihtiyath.toString()
        }
        holder.itemView.button_plus_ihtiyat.setOnClickListener {
            //plus hitungan ihtiyath lalu save ke shared preferences
            ihtiyath++
            holder.itemView.tv_ihtiyat_value.text = ihtiyath.toString()
            sharedPref.saveIntPref("ihtiyat_$namaSholat", ihtiyath)
        }
        holder.itemView.button_min_ihtiyat.setOnClickListener {
            //minus hitungan ihtiyath lalu save ke shared preferences
            ihtiyath--
            holder.itemView.tv_ihtiyat_value.text = ihtiyath.toString()
            sharedPref.saveIntPref("ihtiyat_$namaSholat", ihtiyath)
        }
    }

    class IhtiyatViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(waktuSholat: WaktuSholat){
            itemView.tv_nama_ihtiyat.text = waktuSholat.namaSholat
        }
    }

}