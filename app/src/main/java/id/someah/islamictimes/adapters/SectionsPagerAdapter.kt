package id.someah.islamictimes.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.someah.islamictimes.R
import id.someah.islamictimes.ui.fragment.datamataharibulan.DataBulanMatahariFragment
import id.someah.islamictimes.ui.fragment.datamataharibulan.EphemerisBulanMatahariFragment
/*
class ini fungsinya buat view pager tab view di menu matahari dan bulan
*/
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val tabTitles = intArrayOf(R.string.realtime, R.string.manual)
    override fun getItem(position: Int): Fragment {
        var fragment : Fragment? = null
        when(position){
            0 -> fragment =
                DataBulanMatahariFragment()
            1 -> fragment =
                EphemerisBulanMatahariFragment()
        }
        return fragment as Fragment
    }

    override fun getCount(): Int {
        return tabTitles.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(tabTitles[position])
    }
}