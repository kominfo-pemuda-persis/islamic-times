package id.someah.islamictimes.adapters

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.someah.islamictimes.R
import id.someah.islamictimes.model.WaktuSholat
import id.someah.islamictimes.services.utils.convertDateIfMoreThan24Hours
import kotlinx.android.synthetic.main.item_waktu_sholat.view.*

class WaktuSholatAdapter(private val context: Activity) : RecyclerView.Adapter<WaktuSholatAdapter.WaktuSholatViewHolder>(){

    private var listWaktuSholat : ArrayList<WaktuSholat> = arrayListOf()

    fun setList(list:ArrayList<WaktuSholat>){
        this.listWaktuSholat  = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WaktuSholatViewHolder {
        val view  = LayoutInflater.from(context).inflate(R.layout.item_waktu_sholat, parent,false)
        return WaktuSholatViewHolder(view)
    }

    override fun getItemCount(): Int = listWaktuSholat.size

    override fun onBindViewHolder(holder: WaktuSholatViewHolder, position: Int) {
        holder.bindItem(listWaktuSholat[position])
    }

    class WaktuSholatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(waktuSholat : WaktuSholat){
            itemView.txt_nama_sholat.text = waktuSholat.namaSholat
            itemView.txt_jam_sholat.text = waktuSholat.jamSholat.convertDateIfMoreThan24Hours()
            itemView.imageViewNotifikasiOn.visibility = View.GONE
            itemView.imageViewNotifikasiOff.visibility = View.GONE
        }
    }
}