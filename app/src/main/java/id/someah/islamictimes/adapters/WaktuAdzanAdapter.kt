package id.someah.islamictimes.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.R
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.model.WaktuSholat
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.log
import id.someah.islamictimes.services.utils.convertDateIfMoreThan24Hours
import kotlinx.android.synthetic.main.item_waktu_sholat.view.*
import kotlin.collections.ArrayList


class WaktuAdzanAdapter (private val context:Activity) :
    RecyclerView.Adapter<WaktuAdzanAdapter.WaktuAdzanViewHolder>(){
    private var listWaktuSholat : List<WaktuSholat> = arrayListOf()

    fun setList(list:List<WaktuSholat>){
        this.listWaktuSholat  = list

        Log.d("adapter", "setList: ${list}")
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WaktuAdzanViewHolder {
        val view  = LayoutInflater.from(context).inflate(R.layout.item_waktu_sholat, parent,false)
        return WaktuAdzanViewHolder(view)
    }

    override fun getItemCount(): Int = listWaktuSholat.size

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: WaktuAdzanViewHolder, position: Int) {
        holder.bindItem(listWaktuSholat[position])
        toggleNotification(holder, position)
        indiKatorNextWaktuSholat(holder, position)
    }

    class WaktuAdzanViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(waktuSholat: WaktuSholat){
            itemView.txt_nama_sholat.text = waktuSholat.namaSholat
            itemView.txt_jam_sholat.text = waktuSholat.jamSholat.convertDateIfMoreThan24Hours()
        }
    }

    //fungsi buat mati nyalain notifikasi, value disimpan ke shared preferences
    private fun toggleNotification(holder: WaktuAdzanViewHolder, position: Int){
        val sharedPref = SharedPref(context)
        holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
        val notifikasiDzuhur =  sharedPref.getValueNotifBool("notifikasi_dzuhur", true)!!
        val notifikasiAshar =  sharedPref.getValueNotifBool("notifikasi_ashar", true)!!
        val notifikasiMaghrib =  sharedPref.getValueNotifBool("notifikasi_maghrib", true)!!
        val notifikasiIsya =  sharedPref.getValueNotifBool("notifikasi_isya", true)!!
        val notifikasiNisfuLail =  sharedPref.getValueNotifBool("notifikasi_nisfu_lail", true)!!
        val notifikasiShubuh =  sharedPref.getValueNotifBool("notifikasi_shubuh", true)!!
        val notifikasiSyuruq =  sharedPref.getValueNotifBool("notifikasi_syuruq", true)!!
        val notifikasiDhuha =  sharedPref.getValueNotifBool("notifikasi_dhuha", true)!!
        //check notifikasi nyala / mati
        if(position == 0){
            checkNotification(notifikasiDzuhur, holder)
        }
        if(position == 1){
            checkNotification(notifikasiAshar, holder)
        }
        if(position == 2){
            checkNotification(notifikasiMaghrib, holder)
        }
        if(position == 3){
            checkNotification(notifikasiIsya, holder)
        }
        if(position == 4){
            checkNotification(notifikasiNisfuLail, holder)
        }
        if(position == 5){
            checkNotification(notifikasiShubuh, holder)
        }
        if(position == 6){
            checkNotification(notifikasiSyuruq, holder)
        }
        if(position == 7){
            checkNotification(notifikasiDhuha, holder)
        }

        //matiin notifikasi
        holder.itemView.imageViewNotifikasiOn.setOnClickListener {
            if (position == 0){
                //dzuhur
                sharedPref.saveBoolNotifPref("notifikasi_dzuhur", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
            if (position == 1){
                //ashar
                sharedPref.saveBoolNotifPref("notifikasi_ashar", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
            if (position == 2){
                //maghrib
                sharedPref.saveBoolNotifPref("notifikasi_maghrib", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
            if (position == 3){
                //isya
                sharedPref.saveBoolNotifPref("notifikasi_isya", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
            if (position == 4){
                //nisfu lail
                sharedPref.saveBoolNotifPref("notifikasi_nisfu_lail", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
            if (position == 5){
                //shubuh
                sharedPref.saveBoolNotifPref("notifikasi_shubuh", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
            if (position == 6){
                //syuruq
                sharedPref.saveBoolNotifPref("notifikasi_syuruq", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
            if (position == 7){
                //dhuha
                sharedPref.saveBoolNotifPref("notifikasi_dhuha", false)
                holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
            }
        }
        //nyalain notifikasi
        holder.itemView.imageViewNotifikasiOff.setOnClickListener {
            //sharedPref.saveBoolNotifPref("notifikasi_waktu_sholat", true)
            if (position == 0){
                sharedPref.saveBoolNotifPref("notifikasi_dzuhur", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
            if (position == 1){
                sharedPref.saveBoolNotifPref("notifikasi_ashar", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
            if (position == 2){
                sharedPref.saveBoolNotifPref("notifikasi_maghrib", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
            if (position == 3){
                sharedPref.saveBoolNotifPref("notifikasi_isya", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
            if (position == 4){
                sharedPref.saveBoolNotifPref("notifikasi_nisfu_lail", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
            if (position == 5){
                sharedPref.saveBoolNotifPref("notifikasi_shubuh", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
            if (position == 6){
                sharedPref.saveBoolNotifPref("notifikasi_syuruq", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
            if (position == 7){
                sharedPref.saveBoolNotifPref("notifikasi_dhuha", true)
                holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
                holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
            }
        }
    }

    //fungsi buat check toogle notification di home (icon volume)
    private fun checkNotification(prefValue : Boolean?, holder: WaktuAdzanViewHolder){
        if(prefValue == true){
            holder.itemView.imageViewNotifikasiOn.visibility = View.VISIBLE
            holder.itemView.imageViewNotifikasiOff.visibility = View.INVISIBLE
        }
        if(prefValue == false){
            holder.itemView.imageViewNotifikasiOff.visibility = View.VISIBLE
            holder.itemView.imageViewNotifikasiOn.visibility = View.INVISIBLE
        }
    }

    //fungsi untuk memberi indikator waktu sholat selanjutnya
    private fun indiKatorNextWaktuSholat(holder: WaktuAdzanViewHolder, position: Int){
        val dao = DaoManager()
        val namaSholat = dao.waktuSholatDao(context)!!.getNextWaktuSholat().namaSholat
        if( namaSholat== Constant.NAMA_SHOLAT_ZUHUR && position == 0){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
        if(namaSholat == Constant.NAMA_SHOLAT_ASAR && position == 1){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
        if(namaSholat == Constant.NAMA_SHOLAT_MAGRIB && position == 2){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
        if(namaSholat == Constant.NAMA_SHOLAT_ISYA && position == 3){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
        if(namaSholat == Constant.NAMA_AKHIR_ISYA && position == 4){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
        if(namaSholat == Constant.NAMA_SHOLAT_SUBUH && position == 5){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
        if(namaSholat == Constant.NAMA_AKHIR_SUBUH && position == 6){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
        if(namaSholat == Constant.NAMA_SHOLAT_DUHA && position == 7){
            holder.itemView.item_sholat.setBackgroundResource(R.drawable.indikator_waktu_sholat)
        }
    }

}