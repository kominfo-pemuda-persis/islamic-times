package id.someah.islamictimes.data.repository.library

import android.util.Log
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.constant.round
import id.someah.islamictimes.data.AltAzmMatahari
import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.convertGMTtoDouble
import id.someah.islamictimes.services.utils.getMonthTitle
import javax.inject.Inject

class LibraryRumusGerhanaMatahariImpl @Inject constructor(
    private val sharedPref: SharedPref,
    private val falakLib: FalakLib,
    private val matahariLib: MatahariLib
) : LibraryRumusGerhanaMatahari {
    override fun calculateGerhanaMatahariTahunan(BlnHj: Byte, ThnHj: Long): List<DataGerhanaMatahari> {
        val gLongT = sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        var temp = ArrayList<DataGerhanaMatahari>()

        Constant.BULAN_HIJRI.forEach {
            var result = DataGerhanaMatahari(matahariLib.JenisSolarEclipse(it.second.toByte(), ThnHj, gLatT, gLongT, gAltT))

            result.month = BlnHj
            result.year = ThnHj
            result.monthName = it.first

            if(!result.jenis.toLowerCase().contains("tidak terjadi gerhana")) {
                matahariLib.getTanggalGerhana(it.second.toByte(), ThnHj, gLatT, gLongT, gAltT, "mantap").let {
                    tVal ->
                    result.waktu = " $tVal"
                    if (!tVal.second.isNaN()) {
                        val tTgl = falakLib.JDKM(tVal.second, TZn)
//                        val tJam = falakLib.DHHMS(falakLib.JDKM(tVal.second, TZn, "Jam Des").toDouble(), "HH:MM:SS", 0)
                        result.waktu = "$tTgl"
                    }
                }


                result.year = ThnHj
                result.month = it.second.toByte()

                temp.add(result)
            }


        }
        return temp.toList()
    }

    override fun calculateDurasiGerhanaMatahari(BlnHj: Byte, ThnHj: Long): Pair<String, String> {

        val gLongT = sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        var durasiGerhana = "-"
        var durasiTotal = "-"

        matahariLib.getDurasiGerhana(BlnHj,ThnHj,gLatT,gLongT,gAltT,"none").forEach {
            tVal ->
            if(!tVal.second.isNaN()) {
                when(tVal.second.toString()) {
                    "DurG" -> {
                        durasiGerhana = tVal.first.toString()+"  : "+falakLib.DHHMS(tVal.second,"HH:MM:SS",0)
                    }
                    else -> {
                        durasiTotal = tVal.first.toString()+"  : "+falakLib.DHHMS(tVal.second,"HH:MM:SS",0)
                    }
                }
            }
        }


        return Pair(durasiGerhana,durasiTotal)
    }

    override fun calculateMagnitudeObsGerhanaMatahari(
        BlnHj: Byte,
        ThnHj: Long
    ): Pair<String, String> {

        val gLongT = sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        var magnitudeUmbra = "-"
        var obskurasi = "-"

        val oSE = arrayOf(
            "Magnitude Umbra     " to "MagU",
            "Obskurasi           " to "Obsk"
        )
        for(t in oSE){
            val tVal = falakLib.SolarEclipse(BlnHj,ThnHj,gLatT,gLongT,gAltT,t.second.toString())
            val tTx : String = if (t.second.toString() == "Obsk") tVal.round(3) + " %"  else tVal.round(3)
            if(!tVal.isNaN()) {
                when(t.second.toString()) {
                    "MagU" -> {
                        magnitudeUmbra = t.first.toString()+"  : "+tTx
                    }

                    else -> {
                        obskurasi = t.first.toString()+"  : "+tTx
                    }
                }
            }
        }

        return Pair(magnitudeUmbra,obskurasi)
    }

    override fun calculateAltAzmGerhanaMatahari(BlnHj: Byte, ThnHj: Long): List<String> {

        val gLongT = sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        var temp = ArrayList<String>()

        val tVal = matahariLib.getAzimuthMatahari(BlnHj,ThnHj,gLatT,gLongT,gAltT,"none").forEach {
            tVal->
            if(!tVal.second.isNaN()) {
                temp.add(tVal.first.toString()+"  : "+tVal.second.round(0) +"°")
            } else {
                temp.add(tVal.first.toString()+"  : -")
            }
        }


        return temp
    }

    override fun calculateGerhanaMatahari(BlnHj: Byte, ThnHj: Long): DataGerhanaMatahari {
        val gLongT = sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        var result = DataGerhanaMatahari(matahariLib.JenisSolarEclipse(BlnHj, ThnHj, gLatT, gLongT, gAltT))

        result.month = BlnHj
        result.year = ThnHj

        if(!result.jenis.toLowerCase().contains("tidak terjadi gerhana")) {

            val t: Pair<String, String> = Pair("Mx", "JDSU1")
            val tVal = matahariLib.getTanggalGerhana(BlnHj, ThnHj, gLatT, gLongT, gAltT, t.second.toString())

            if (!tVal.second.isNaN()) {
                val tTgl = falakLib.JDKM(tVal.second, TZn)
                val tJam = falakLib.DHHMS(falakLib.JDKM(tVal.second, TZn, "Jam Des").toDouble(), "HH:MM:SS", 0)
                result.waktu = " $tTgl , $tJam"
            }
        }

        return result
    }

    override fun calculateDetailGerhana(BlnHj: Byte, ThnHj: Long): DataGerhanaMatahari {
        val gLongT = sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        var result = DataGerhanaMatahari("")


        var globalCalculate = matahariLib.SolarEclipse(BlnHj,ThnHj,gLatT,gLongT,gAltT,"none")

        var tempAltAzm = ArrayList<AltAzmMatahari>()

        var durasiGerhana = "-"
        var durasiTotal = "-"

        var magnitudeUmbra = "-"
        var obskurasi = "-"

        globalCalculate.forEach {
        key ->

            if(durasi.contains(key.first)) {
                when(key.first.toString()) {
                    "DurG" -> {
                        if(!key.second.isNaN()) {
                            durasiGerhana = ""+falakLib.DHHMS(key.second,"HH:MM:SS",0)
                        }
                    }
                    "DurT" -> {
                        if(!key.second.isNaN()) {
                            durasiTotal = ""+falakLib.DHHMS(key.second,"HH:MM:SS",0)
                        }
                    }
                }
            }

            if(magobs.contains(key.first)) {
                val tTx : String = if (key.first == "Obsk") key.second.round(3) + " %"  else key.second.round(3)
                if(!key.second.isNaN()) {
                    when(key.first.toString()) {
                        "MagU" -> {
                            magnitudeUmbra = ""+tTx
                        }

                        else -> {
                            obskurasi = ""+tTx
                        }
                    }
                }
            }
        }

        var position = 0
        for (i in 1..5) {
            var item  = AltAzmMatahari(globalCalculate[position].second.getTimeSolarEclipse())

            position++
            item.alt = globalCalculate[position].second.getAltAzmDegree()

            position++
            item.azm = globalCalculate[position].second.getAltAzmDegree()

            position++
            tempAltAzm.add(item)
        }


        result.altAzm = tempAltAzm
        result.durasi = Pair(durasiGerhana, durasiTotal)
        result.magnitudeObs = Pair(magnitudeUmbra,obskurasi)


        return result
    }

    val TZN   = sharedPref.getValueDouble("timezone") ?: 7.0

    private fun Double.getTimeSolarEclipse() : String {
        return if(!this.isNaN()) {
            val tTgl = falakLib.JDKM(this,TZN)
            val tJam = falakLib.DHHMS(falakLib.JDKM(this,TZN,"Jam Des").toDouble(),"HH:MM:SS",0)
            "$tJam"
        } else {
            "-"
        }
    }

    private fun Double.getAltAzmDegree() : String {
        return if(!this.isNaN()) {
//            "${this.round(0)} +\"°\""
            "${this.round(0)}  °"
        } else {
            "-"
        }
    }

    companion object {
        val ALT_AZM = listOf(
            "AltU1",
            "AzmU1",
            "AltU2",
            "AzmU2",
            "AltMx",
            "AzmMx",
            "AltU3",
            "AzmU3",
            "AltU4",
            "AzmU4"
        )

        val durasi = listOf(
            "DurT",
            "DurG"
        )

        val magobs = listOf(
            "MagU",
            "Obsk"
        )
    }
}

interface LibraryRumusGerhanaMatahari {
    fun calculateGerhanaMatahariTahunan(BlnHj: Byte, ThnHj: Long): List<DataGerhanaMatahari>
    fun calculateDurasiGerhanaMatahari(BlnHj: Byte, ThnHj: Long) : Pair<String,String>
    fun calculateMagnitudeObsGerhanaMatahari(BlnHj: Byte, ThnHj: Long) : Pair<String,String>
    fun calculateAltAzmGerhanaMatahari(BlnHj: Byte, ThnHj: Long) : List<String>
    fun calculateGerhanaMatahari(BlnHj: Byte, ThnHj: Long): DataGerhanaMatahari

    fun calculateDetailGerhana(BlnHj: Byte, ThnHj: Long) : DataGerhanaMatahari
}