package id.someah.islamictimes.data

data class HariPentingModel (
    val title : String ,
    val listDay : List<Int> = listOf(),
    val day : Int,
    val month : Int,
    val howLong : Int = 1,
    val isRepeatEveryMonth : Boolean = false,
    val isImportant : Boolean = false,
    val isEvent : Boolean = false
)