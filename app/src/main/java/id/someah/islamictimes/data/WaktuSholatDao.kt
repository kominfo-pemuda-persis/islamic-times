package id.someah.islamictimes.data

import android.content.Context
import id.someah.islamictimes.model.NextWaktuSholat
import id.someah.islamictimes.model.WaktuSholat

interface WaktuSholatDao {
    fun getWaktuSholat() : ArrayList<WaktuSholat>
    fun getNextWaktuSholat() : NextWaktuSholat
    fun getWaktuSholatByDate(tanggal : Byte, bulan : Byte, tahun : Long, context: Context) : ArrayList<WaktuSholat>
    fun getWaktuSholatBulanan(bulan : Byte, tahun : Long, context: Context) : ArrayList<WaktuSholat> //buat generate data ke pdf
}