package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetAltAzmGerhanaMatahariUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte, Long>, List<String>>() {
    override fun invoke(parameterName: Pair<Byte, Long>): List<String> {
        return repository.getAltAzmGerhanaMatahari(parameterName.first,parameterName.second)
    }
}