package id.someah.islamictimes.data

data class OfflineLocationModel(
    val locationName : String,
    val latitude : Double,
    val longitude : Double,
    val altitude : Double
) {
    var timeZone : Double = 7.0
    var timeZoneTitle : String = ""
}