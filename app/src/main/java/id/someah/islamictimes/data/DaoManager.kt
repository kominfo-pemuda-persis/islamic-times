package id.someah.islamictimes.data

import android.content.Context
/*
class untuk me-manage Data Acess Object sesuai data yang dibutuhkan
 */
class DaoManager {
    private var waktuSholatDao: WaktuSholatDao? = null
    private var dataBulanMatahariDao: DataBulanMatahariDao? = null
    private var arahKiblatDao: ArahKiblatDao? = null
    private var ephemersisMatahariBulanDao: DataEphemerisMatahariBulanDao? = null

    fun waktuSholatDao(context: Context): WaktuSholatDao? {
        if (waktuSholatDao == null) {
            waktuSholatDao = DataWaktuSholat(context)
        }
        return waktuSholatDao
    }

    fun dataBulanMatahariDao(context: Context) : DataBulanMatahariDao?{
        if(waktuSholatDao == null){
            dataBulanMatahariDao = DataBulanMatahari(context)
        }
        return dataBulanMatahariDao
    }

    fun dataArahKiblat(context: Context) : ArahKiblatDao?{
        if(arahKiblatDao == null){
            arahKiblatDao = DataArahKiblat(context)
        }
        return arahKiblatDao
    }

    fun dataEphemersisBulanMatahari(context: Context) : DataEphemerisMatahariBulanDao? {
        if (ephemersisMatahariBulanDao == null) {
            ephemersisMatahariBulanDao = DataEphemerisMatahariBulan(context)
        }
        return ephemersisMatahariBulanDao
    }
}