package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.repository.WaktuSholatRepository
import id.someah.islamictimes.model.WaktuSholat
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetDataWaktuSholatUseCase @Inject constructor(private val repository: WaktuSholatRepository) :
    BaseUseCase<Int, List<WaktuSholat>>() {
    override fun invoke(parameterName: Int): List<WaktuSholat> {
        return repository.getWaktuSholat()
    }
}