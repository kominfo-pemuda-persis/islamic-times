package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetKontakGerhanaMahatahariTahunanUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte, Long>, List<DataGerhanaMatahari>>() {
    override fun invoke(parameterName: Pair<Byte, Long>): List<DataGerhanaMatahari> {
        return repository.getKontakGerhanaMatahariTahunan(parameterName.first,parameterName.second)
    }
}