package id.someah.islamictimes.data

import android.content.Context
import id.someah.islamictimes.model.*

interface DataEphemerisMatahariBulanDao {
    fun getEphemersisMatahariBulanByDate(tanggal : Byte, bulan : Byte, tahun : Long, deltaT: Double, context: Context) : ArrayList<EphemerisMatahariBulan>
    fun getEphemerisMatahariByDateHour(tanggal : Byte, bulan : Byte, tahun : Long,jam : Int, menit : Int, detik : Int, deltaT : Double, context: Context) : EphemerisMatahariDateHour
    fun getEphemerisBulanByDateHour(tanggal : Byte, bulan : Byte, tahun : Long, jam : Int, menit : Int, detik : Int, deltaT : Double, context: Context) :EphemerisBulanDateHour
}