package id.someah.islamictimes.data

import android.content.Context
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.model.*
import id.someah.islamictimes.modules.FalakLib
import kotlin.collections.ArrayList

/*
    class untuk mengelola data matahari dan bulan manual.
    contoh output dan penggunaan fungsi bisa di lihat di library bagian fungsi main.
 */
class DataEphemerisMatahariBulan(context: Context) : DataEphemerisMatahariBulanDao {
    private val falak = FalakLib()

    //fungsi untuk menampilkan data ke PDF
    override fun getEphemersisMatahariBulanByDate(
        tanggal: Byte,
        bulan: Byte,
        tahun: Long,
        deltaT: Double,
        context: Context
    ): ArrayList<EphemerisMatahariBulan> {

        val listEphemerisMatahariBulan: ArrayList<EphemerisMatahariBulan> = arrayListOf()
        for (i in 0..24){
            val jamDesimal      : Double    = falak.KMJD(tanggal,bulan,tahun, i.toDouble())
            val dltT    : Double    = deltaT
            val jde     : Double    = jamDesimal + dltT / 86400.0

            //val DT      : String    = falak.RoundTo(dltT,2)
            //contoh output dari fungsi bisa dilihat di library bagian fungsi main
            val alm     : String    = falak.DDDMS(falak.SunGeocentricLongitude(jde),"DDMMSS",0)
            val elm     : String    = falak.RoundTo(falak.SunGeocentricLatitude(jde),2)
            val arm     : String    = falak.DDDMS(falak.SunApparentRightAscension(jde),"DDMMSS",0)
            val dm      : String    = falak.DDDMS(falak.SunApparentDeclination(jde),"DDMMSS",0)
            val hpm     : String    = falak.DDDMS(falak.SunEquatorialHorizontalParallax(jde),"SS",8)
            val sdm     : String    = falak.DDDMS(falak.SunAngularSemiDiameter(jde),"MMSS",2)
            //val oblq    : String    = falak.DDDMS(falak.ObliquityOfEcliptic(JDx),"DDMMSS",0)
            val dist    : String    = falak.RoundTo(falak.SunGeocentricDistance(jde),7)
            val eoT     : String    = falak.DHHMS(falak.EquationOfTime(jde),"MMSS",0)
            val ghaM    : String    = falak.DDDMS(falak.SunGreenwichHourAngle(jde),"DDMMSS",0)

            val alb     : String    = falak.DDDMS(falak.MoonGeocentricLongitude(jde),"DDMMSS",0)
            val elb     : String    = falak.DDDMS(falak.MoonGeocentricLatitude(jde),"DDMMSS",0)
            val arb     : String    = falak.DDDMS(falak.MoonApparentRightAscension(jde),"DDMMSS",0)
            val db      : String    = falak.DDDMS(falak.MoonApparentDeclination(jde),"DDMMSS",0)
            val hpb     : String    = falak.DDDMS(falak.MoonEquatorialHorizontalParallax(jde),"DDMMSS",0)
            val sdb     : String    = falak.DDDMS(falak.MoonAngularSemiDiameter(jde),"MMSS",2)
            val abl     : String    = falak.DDDMS(falak.MoonBrightLimbAngle(jde),"DDMMSS",0)
            val fib     : String    = falak.RoundTo(falak.MoonDiskIlluminatedFraction(jde),8)
            val ghaB    : String    = falak.DDDMS(falak.MoonGreenwichHourAngle(jde),"DDMMSS",0)

            //inisialisasi array
            val namaDataMatahari = arrayOf(
                Constant.APPARENT_LONGITUDE, Constant.ECLIPTIC_LATITUDE, Constant.APPARENT_RIGHT_ASC,
                Constant.APPARENT_DECLINATION, Constant.HORIZONTAL_PARALLAX, Constant.SEMI_DIAMETER,
                Constant.TRUE_GEOCENT_DIST, Constant.EQUATION_OF_TIME, Constant.GHA
            )

            val namaDataBulan = arrayOf(
                Constant.APPARENT_LONGITUDE, Constant.APPARENT_LATITUDE, Constant.APPARENT_RIGHT_ASC,
                Constant.APPARENT_DECLINATION, Constant.HORIZONTAL_PARALLAX, Constant.SEMI_DIAMETER,
                Constant.ANGLE_BRIGHT_LIMB, Constant.FRACTION_ILLUMINATION, Constant.GHA
            )

            val valueDataBulan = arrayOf(
                alb, elb, arb, db, hpb, sdb, abl, fib, ghaB
            )

            val valueDataMatahari = arrayOf(
                alm, elm, arm, dm, hpm, sdm, dist, eoT, ghaM
            )

            //tambah data dari array ke arraylist
            for (j in namaDataMatahari.indices){
                val ephemersisMatahari = EphemerisMatahari(namaDataMatahari[j], valueDataMatahari[j])
                val ephemersisBulan = EphemerisBulan(namaDataBulan[j], valueDataBulan[j])
                val ephemersisMatahariBulan = EphemerisMatahariBulan(ephemersisMatahari, ephemersisBulan)
                listEphemerisMatahariBulan.add(ephemersisMatahariBulan)
            }

        }
        return listEphemerisMatahariBulan
    }

    override fun getEphemerisMatahariByDateHour(
        tanggal: Byte,
        bulan: Byte,
        tahun: Long,
        jam : Int,
        menit : Int,
        detik : Int,
        deltaT : Double,
        context: Context
    ): EphemerisMatahariDateHour {

        val jamDesimal = (jam.toDouble() + (menit.toDouble() / 60)) + (detik.toDouble() / 3600)
        val jd     : Double    = falak.KMJD(tanggal,bulan,tahun,jamDesimal,0.0)
        val jde     : Double    = jd + deltaT / 86400.0
        //val dt      : String    = falak.RoundTo(deltaT,2)

        val alm     : String    = falak.DDDMS(falak.SunGeocentricLongitude(jde),"DDMMSS",0)
        val elm     : String    = falak.RoundTo(falak.SunGeocentricLatitude(jde),2)
        val arm     : String    = falak.DDDMS(falak.SunApparentRightAscension(jde),"DDMMSS",0)
        val dm      : String    = falak.DDDMS(falak.SunApparentDeclination(jde),"DDMMSS",0)
        val hpm     : String    = falak.DDDMS(falak.SunEquatorialHorizontalParallax(jde),"SS",8)
        val sdm     : String    = falak.DDDMS(falak.SunAngularSemiDiameter(jde),"MMSS",2)
        //val oblq    : String    = falak.DDDMS(falak.ObliquityOfEcliptic(JDx),"DDMMSS",0)
        val dist    : String    = falak.RoundTo(falak.SunGeocentricDistance(jde),7)
        val eoT     : String    = falak.DHHMS(falak.EquationOfTime(jde),"MMSS",0)
        val ghaM    : String    = falak.DDDMS(falak.SunGreenwichHourAngle(jde),"DDMMSS",0)

        return EphemerisMatahariDateHour(alm, elm, arm, dm, hpm, sdm, dist, eoT, ghaM)
    }

    override fun getEphemerisBulanByDateHour(
        tanggal: Byte,
        bulan: Byte,
        tahun: Long,
        jam : Int,
        menit : Int,
        detik: Int,
        deltaT: Double,
        context: Context
    ): EphemerisBulanDateHour {
        val jamDesimal = (jam.toDouble() + (menit.toDouble() / 60)) + (detik.toDouble() / 3600)
        val jd     : Double    = falak.KMJD(tanggal,bulan,tahun,jamDesimal,0.0)
        val JDE     : Double    = jd + deltaT / 86400.0

        val alb     : String    = falak.DDDMS(falak.MoonGeocentricLongitude(JDE),"DDMMSS",0)
        val elb     : String    = falak.DDDMS(falak.MoonGeocentricLatitude(JDE),"DDMMSS",0)
        val arb     : String    = falak.DDDMS(falak.MoonApparentRightAscension(JDE),"DDMMSS",0)
        val db      : String    = falak.DDDMS(falak.MoonApparentDeclination(JDE),"DDMMSS",0)
        val hpb     : String    = falak.DDDMS(falak.MoonEquatorialHorizontalParallax(JDE),"DDMMSS",0)
        val sdb     : String    = falak.DDDMS(falak.MoonAngularSemiDiameter(JDE),"MMSS",2)
        val abl     : String    = falak.DDDMS(falak.MoonBrightLimbAngle(JDE),"DDMMSS",0)
        val fib     : String    = falak.RoundTo(falak.MoonDiskIlluminatedFraction(JDE),8)
        val ghaB    : String    = falak.DDDMS(falak.MoonGreenwichHourAngle(JDE),"DDMMSS",0)

        return EphemerisBulanDateHour(alb, elb, arb, db, hpb, sdb, abl, fib, ghaB)
    }

}