package id.someah.islamictimes.data

data class DataGerhana (
    val jenisGerhana: String,
    val hijriMonth : String,
    val date : String,
    val kontakGerhana : List<KontakGerhanaModel>,
    val dataTerkait : DataTerkait
) {
    var transitTerbenam : List<TransitTerbenamModel> = listOf()
}

data class KontakGerhanaModel(
    val time : String
) {
    var phase: String = "-"
    var alt : String = "-"
    var azm : String = "-"
    var hG : String = "-"
    var hT : String = "-"
    var hM : String = "-"
    var date : String = ""
}

data class DataTerkait(
    val magnitudePenumbra : String,
    val magnitudeUmbra : String,
    val durasiPenumbra : String,
    val durasiUmbra : String,
    val durasiTotal : String
) {
    var radiusPPenumbra : String = "-"
    var radiusUmbra : String = "-"
}

data class TransitTerbenamModel(
    val tgl : String
)  {
    var transit : String = "-"
    var terbenam : String = "-"
    var terbit  : String = "-"
}