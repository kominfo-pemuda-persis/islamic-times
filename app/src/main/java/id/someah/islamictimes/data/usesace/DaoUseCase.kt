package id.someah.islamictimes.data.usesace

import android.content.Context
import id.someah.islamictimes.data.WaktuSholatDao
import id.someah.islamictimes.model.NextWaktuSholat
import id.someah.islamictimes.model.WaktuSholat
import javax.inject.Inject

class DaoUseCase @Inject constructor(private val waktuSholatDao: WaktuSholatDao) {
    fun getWaktuSholat() : ArrayList<WaktuSholat> {
        return waktuSholatDao.getWaktuSholat()
    }
    fun getNextWaktuSholat() : NextWaktuSholat {
        return waktuSholatDao.getNextWaktuSholat()
    }
}