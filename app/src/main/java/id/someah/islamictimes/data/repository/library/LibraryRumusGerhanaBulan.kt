package id.someah.islamictimes.data.repository.library

import android.content.Context
import android.icu.util.TimeZone
import android.util.Log
import id.someah.islamictimes.constant.round
import id.someah.islamictimes.data.DataGerhana
import id.someah.islamictimes.data.DataTerkait
import id.someah.islamictimes.data.KontakGerhanaModel
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.convertGMTtoDouble
import id.someah.islamictimes.services.utils.convertToSexadesimal
import id.someah.islamictimes.services.utils.decimalToSexagesimal
import us.dustinj.timezonemap.TimeZoneMap
import javax.inject.Inject

class LibraryRumusGerhanaBulanImpl @Inject
constructor(private val sharedPref: SharedPref , private val falakLib: FalakLib) : LibraryRumusGerhanaBulan {
    override fun calculate(BlnHj: Byte, ThnHj: Long): DataGerhana {

        var kontakGerhana = ArrayList<KontakGerhanaModel>()

        return DataGerhana(
            falakLib.JenisLunarEclipse(BlnHj,ThnHj),
            "",
            "",
            kontakGerhana.toList(),
            DataTerkait("","","","","")
        )
    }

    override fun getJenisDanWaktuGerhana(BlnHj: Byte, ThnHj: Long): String {
//        val gLongT =  sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLongT=  sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        val tLE = arrayOf(
            "P1"  to "P1",
            "U1"  to "U1",
            "U2"  to "U2",
            "Mx"  to "TG",
            "U3"  to "U3",
            "U4"  to "U4",
            "P4"  to "P4")

        var waktu = "-"
        for(t in tLE){
            val tVal = falakLib.LunarEclipse(BlnHj,ThnHj,t.second.toString())
            if(!tVal.isNaN()) {
                val tTgl = falakLib.JDKM(+tVal,TZn)
                val tJam = if (t.second.toString() == "TG")falakLib.DHHMS(falakLib.JDKM(tVal,TZn,"Jam Des").toDouble(),"HH:MM:SS",1) else falakLib.DHHMS(falakLib.JDKM(tVal,TZn,"Jam Des").toDouble(),"HH:MM:SS",0)
               waktu = "$tTgl , $tJam"
            }
        }

        return "${falakLib.JenisLunarEclipse(BlnHj,ThnHj)} - $waktu"
    }

    override fun getDataTerkaitGerhana(BlnHj: Byte, ThnHj: Long): DataTerkait {
        val tMLE = arrayOf(
            "Magnitude Penumbra  " to "MagP",
            "Magnitude Umbra     " to "MagU",
            "Radius Penumbra  " to "RadP",
            "Radius Umbra     " to "RadU"
        )
        var magnitudePenumbra = "-"
        var magnitudeUmbra = "-"
        var durasiPenumbra = "-"
        var durasiUmbra = "-"
        var durasiTotal = "-"

        var radiusPenumbra = "-"
        var radiusUmbra = "-"

        for(t in tMLE){
            val tVal = falakLib.LunarEclipse(BlnHj,ThnHj,t.second.toString())
            val tVt: String = if (tVal < 0.0) tVal.round(4) else " "+tVal.round(4)
            if(!tVal.isNaN()) {

                when(t.second.toString().toLowerCase()) {
                    "magp" -> magnitudePenumbra = tVt
                    "magu" -> magnitudeUmbra = tVt
                    "radp" -> radiusPenumbra =  "$tVt °"
                    "radu" -> radiusUmbra = "$tVt °"
                }

            }
        }
        val tDLE = arrayOf(
            "Durasi Penumbra     " to "DurP",
            "Durasi Umbra        " to "DurU",
            "Durasi Total        " to "DurT",
        )
        for(t in tDLE){
            val tVal = falakLib.LunarEclipse(BlnHj,ThnHj,t.second.toString())
            if(!tVal.isNaN()) {

                when {
                    t.second.toString().toLowerCase() == "durp" -> durasiPenumbra = falakLib.DHHMS(tVal,"HH:MM:SS",0)
                    t.second.toString().toLowerCase() == "duru" -> durasiUmbra = falakLib.DHHMS(tVal,"HH:MM:SS",0)
                    else -> durasiTotal = falakLib.DHHMS(tVal,"HH:MM:SS",0)
                }

            }
        }
        var item = DataTerkait(
            magnitudePenumbra,
            magnitudeUmbra,
            durasiPenumbra,
            durasiUmbra,
            durasiTotal
        )
        item.radiusPPenumbra = radiusPenumbra
        item.radiusUmbra = radiusUmbra
        return item
    }

    override fun calculateKontakGerhana(phase: GerhanaPhase, BlnHj: Byte, ThnHj: Long): KontakGerhanaModel {
        val gLongT=  sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        var time : String = ""
        var date : String = ""

        var kontak = KontakGerhanaModel("")

        for(t in tLE){

            if(phase.getString() == t.second) {

                val tVal = falakLib.LunarEclipse(BlnHj,ThnHj,t.second.toString())
                if(!tVal.isNaN()) {
                    val tTgl = falakLib.JDKM(+tVal,TZn)
//                    val tJam = if (t.second == "TG") falakLib.DHHMS(falakLib.JDKM(tVal,TZn,"Jam Des").toDouble(),"HH:MM:SS",1) else falakLib.DHHMS(falakLib.JDKM(tVal,TZn,"Jam Des").toDouble(),"HH:MM:SS",0)
                    val tJam = falakLib.DHHMS(falakLib.JDKM(tVal,TZn,"Jam Des").toDouble(),"HH:MM:SS",0)
                    time = "$tTgl,$tJam"

                    date = tTgl.split(",").last()
                }

                if(time.isEmpty()) {
                    time = "-"
                }

                kontak = KontakGerhanaModel(time)
                kontak.date = date
                kontak.phase = phase.getTitle()

                for(m in ALT_AZM) {
                    val LEAltAzmMax = arrayOf(
                        "hG" to "hG",
                        "hT" to "hT",
                        "hM" to "hM",
                        "Az" to "Az"
                    )

                    val tVal = when(t.second) {
                        GerhanaPhase.Mx.getString() -> falakLib.LEAltAzmMax(BlnHj,ThnHj,gLongT,gLatT,gAltT,m.second.toString())
                        GerhanaPhase.P1.getString() -> falakLib.LEAltAzmP1(BlnHj,ThnHj,gLongT,gLatT,gAltT,m.second.toString())
                        GerhanaPhase.U1.getString() -> falakLib.LEAltAzmU1(BlnHj,ThnHj,gLongT,gLatT,gAltT,m.second.toString())
                        GerhanaPhase.U2.getString() -> falakLib.LEAltAzmU2(BlnHj,ThnHj,gLongT,gLatT,gAltT,m.second.toString())
                        GerhanaPhase.U3.getString() -> falakLib.LEAltAzmU3(BlnHj,ThnHj,gLongT,gLatT,gAltT,m.second.toString())
                        GerhanaPhase.U4.getString() -> falakLib.LEAltAzmU4(BlnHj,ThnHj,gLongT,gLatT,gAltT,m.second.toString())
                        else -> falakLib.LEAltAzmP4(BlnHj,ThnHj,gLongT,gLatT,gAltT,m.second.toString())
                    }
                    if(!tVal.isNaN()) {
                        if(m.second.toString().toLowerCase() == "az")
//                                kontak.azm = falakLib.DDDMS(tVal,"DDMMSS",0)
                            kontak.azm = "${tVal.round(1)} °"
                        else if(m.second.toString().toLowerCase() == "hg")
                            kontak.hG = falakLib.DDDMS(tVal,"DDMMSS",0)
                        else if(m.second.toString().toLowerCase() == "ht")
                            kontak.hT = falakLib.DDDMS(tVal,"DDMMSS",0)
                        else if(m.second.toString().toLowerCase() == "hm")
//                                kontak.hM = falakLib.DDDMS(tVal,"DDMMSS",0)
                            kontak.hM = "${tVal.round(1)} °"
                    }
                }

            }
        }

        return kontak
    }

    override fun calculateGerhanaTerbitTransitTerbenam(day : Byte,bln: Byte, thn: Long): Triple<String, String, String> {

        val gLongT =  sharedPref.getValueDouble("longitude_user") ?: 0.0
        val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
        val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0
        val TZn   = sharedPref.getValueDouble("timezone") ?: 7.0

        val MRS : Any = falakLib.MoonTransitRiseSet(day,bln,thn,gLatT,gLongT,gAltT,7.0,"RISE",5)
        val MTR : Any = falakLib.MoonTransitRiseSet(day,bln,thn,gLatT,gLongT,gAltT,7.0,"TRANSIT",5)
        val MST : Any = falakLib.MoonTransitRiseSet(day,bln,thn,gLatT,gLongT,gAltT,7.0,"SET",5)

        var mrsResult = "-"
        var mtrResult = "-"
        var mstResult = "-"

        if (MRS != "x")
            mrsResult = falakLib.DHHMS(MRS.toString().toDouble(),"HH:MM:SS",0)

        if(MTR != "x")
            mtrResult = falakLib.DHHMS(MTR.toString().toDouble(),"HH:MM:SS",0)

        if(MST != "x")
            mstResult = falakLib.DHHMS(MST.toString().toDouble(),"HH:MM:SS",0)

        return Triple(mrsResult,mtrResult,mstResult)
    }

    companion object {
        val ALT_AZM = arrayOf(
            "hG" to "hG",
            "hT" to "hT",
            "hM" to "hM",
            "Az" to "Az"
        )
        val tLE = arrayOf(
            "P1"  to "P1",
            "U1"  to "U1",
            "U2"  to "U2",
            "Mx"  to "TG",
            "U3"  to "U3",
            "U4"  to "U4",
            "P4"  to "P4")
    }

}

interface LibraryRumusGerhanaBulan {
    fun calculate(BlnHj: Byte, ThnHj: Long) : DataGerhana
    fun getJenisDanWaktuGerhana(BlnHj: Byte, ThnHj: Long) : String
    fun getDataTerkaitGerhana(BlnHj: Byte, ThnHj: Long) : DataTerkait
    fun calculateKontakGerhana(phase : GerhanaPhase, BlnHj: Byte, ThnHj: Long) : KontakGerhanaModel
    fun calculateGerhanaTerbitTransitTerbenam(day : Byte,bln: Byte, thn: Long) : Triple<String,String,String>

}