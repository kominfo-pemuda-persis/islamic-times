package id.someah.islamictimes.data.repository.library

import id.someah.islamictimes.constant.round
import id.someah.islamictimes.modules.FalakLib
import javax.inject.Inject

class MatahariLib @Inject constructor(
    private val falakLib: FalakLib
) {

    fun Sign(x: Double) = if (x >0) {1} else if (x <0) {-1} else {0}

    fun JenisSolarEclipse (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double): String {
        val Mag : Double
        val m   : Double
        val L2_ : Double
        val JSE : String

        var calculate = SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Mag")
        Mag = calculate.first
        m   = calculate.second
        L2_ = calculate.third

        JSE = if (Mag > 0 && m > Math.abs(L2_)) {
            "GERHANA MATAHARI SEBAGIAN"
        } else if (Mag > 0 && m < Math.abs(L2_) && L2_ < 0) {
            "GERHANA MATAHARI TOTAL"
        } else if (Mag > 0 && m < Math.abs(L2_) && L2_ > 0) {
            "GERHANA MATAHARI CINCIN"
        } else {
            "TIDAK TERJADI GERHANA"
        }
        return JSE
    }

    fun SolarEclipseObscuration(HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String):
            Triple<Double,Double,Double>
    {
        var r   : Double
        var u   : Double
        var v   : Double
        var L1  : Double
        var L2  : Double
        var a   : Double
        var b   : Double
        var n   : Double
        var m   : Double
        var L1_ : Double
        var L2_ : Double
        var tanf1 : Double
        var tanf2 : Double
        var e   : Double
        var z   : Double
        var Tau : Double
        var Mag : Double
        var r_  : Double
        var s_  : Double
        var y   : Double
        var z_  : Double
        var B   : Double
        var C   : Double
        var Obs : Double

        val callFuction = JDSolarEclipseMax(HijriMonth, HijriYear, gLat, gLon, gAlt, "r")

        r       = callFuction[0]
        u       = callFuction[1]
        v       = callFuction[2]
        L1      = callFuction[3]
        L2      = callFuction[4]
        tanf1   = falakLib.SBesselian(HijriMonth,HijriYear,"f1")
        tanf2   = falakLib.SBesselian(HijriMonth,HijriYear,"f2")

        m       = Math.sqrt(u * u + v * v)
        L1_     = L1 - r * tanf1
        L2_     = L2 - r * tanf2

        Mag     = (L1_ - m) / (L1_ + L2_)

        return Triple(Mag , m , L2_)
    }

    fun JDSolarEclipseMax(HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): List<Double> {
        var JDESolarEclipse1 : Double
        var JDESolarEclipse2 : Double
        var JDSolarEclipseMax: Double
        var DeltaT: Double
        var T0  : Double
        var x0  : Double
        var x1  : Double
        var x2  : Double
        var x3  : Double
        var x4  : Double
        var y0  : Double
        var y1  : Double
        var y2  : Double
        var y3  : Double
        var y4  : Double
        var d0  : Double
        var d1  : Double
        var d2  : Double
        var d3  : Double
        var d4  : Double
        var M0  : Double
        var M1  : Double
        var M2  : Double
        var M3  : Double
        var M4  : Double
        var L10 : Double
        var L11 : Double
        var L12 : Double
        var L13 : Double
        var L14 : Double
        var L20 : Double
        var L21 : Double
        var L22 : Double
        var L23 : Double
        var L24 : Double

        var T   : Double = 0.0
        var P_  : Double = 0.0
        var S   : Double = 0.0
        var C   : Double = 0.0
        var x   : Double = 0.0
        var y   : Double = 0.0
        var d   : Double = 0.0
        var M   : Double = 0.0
        var L1  : Double = 0.0
        var L2  : Double = 0.0
        var x_  : Double = 0.0
        var y_  : Double = 0.0
        var H   : Double = 0.0
        var p   : Double = 0.0
        var q   : Double = 0.0
        var r   : Double = 0.0
        var p_  : Double = 0.0
        var q_  : Double = 0.0
        var u   : Double = 0.0
        var v   : Double = 0.0
        var a   : Double = 0.0
        var b   : Double = 0.0
        var n   : Double = 0.0
        var P   : Double = 0.0
        var Tx  : Double = 0.0

        x0 = falakLib.SBesselian(HijriMonth, HijriYear, "x0")
        x1 = falakLib.SBesselian(HijriMonth, HijriYear, "x1")
        x2 = falakLib.SBesselian(HijriMonth, HijriYear, "x2")
        x3 = falakLib.SBesselian(HijriMonth, HijriYear, "x3")
        x4 = falakLib.SBesselian(HijriMonth, HijriYear, "x4")
        y0 = falakLib.SBesselian(HijriMonth, HijriYear, "y0")
        y1 = falakLib.SBesselian(HijriMonth, HijriYear, "y1")
        y2 = falakLib.SBesselian(HijriMonth, HijriYear, "y2")
        y3 = falakLib.SBesselian(HijriMonth, HijriYear, "y3")
        y4 = falakLib.SBesselian(HijriMonth, HijriYear, "y4")
        d0 = falakLib.SBesselian(HijriMonth, HijriYear, "dm0")
        d1 = falakLib.SBesselian(HijriMonth, HijriYear, "dm1")
        d2 = falakLib.SBesselian(HijriMonth, HijriYear, "dm2")
        d3 = falakLib.SBesselian(HijriMonth, HijriYear, "dm3")
        d4 = falakLib.SBesselian(HijriMonth, HijriYear, "dm4")
        M0 = falakLib.SBesselian(HijriMonth, HijriYear, "M0")
        M1 = falakLib.SBesselian(HijriMonth, HijriYear, "M1")
        M2 = falakLib.SBesselian(HijriMonth, HijriYear, "M2")
        M3 = falakLib.SBesselian(HijriMonth, HijriYear, "M3")
        M4 = falakLib.SBesselian(HijriMonth, HijriYear, "M4")
        L10 = falakLib.SBesselian(HijriMonth, HijriYear, "L10")
        L11 = falakLib.SBesselian(HijriMonth, HijriYear, "L11")
        L12 = falakLib.SBesselian(HijriMonth, HijriYear, "L12")
        L13 = falakLib.SBesselian(HijriMonth, HijriYear, "L13")
        L14 = falakLib.SBesselian(HijriMonth, HijriYear, "L14")
        L20 = falakLib.SBesselian(HijriMonth, HijriYear, "L20")
        L21 = falakLib.SBesselian(HijriMonth, HijriYear, "L21")
        L22 = falakLib.SBesselian(HijriMonth, HijriYear, "L22")
        L23 = falakLib.SBesselian(HijriMonth, HijriYear, "L23")
        L24 = falakLib.SBesselian(HijriMonth, HijriYear, "L24")

        JDESolarEclipse1 = falakLib.JDEEclipseModified(HijriMonth,HijriYear,1)
        JDESolarEclipse2 = Math.floor(JDESolarEclipse1) + (((JDESolarEclipse1 - Math.floor(JDESolarEclipse1)) * 24).round(0)).toDouble() / 24.0

        DeltaT =falakLib.DeltaT(JDESolarEclipse2)
        T0 = ((JDESolarEclipse2 + 0.5 + (0.0/ 24.0)-Math.floor(JDESolarEclipse2 + 0.5 + (0.0 / 24.0)))*24).round(0).toDouble()

        T = T + P
        for (i in 1..5) {
            T = T
            P_ = Math.atan(0.99664719 * Math.tan(falakLib.Rad(gLat)))
            S = 0.99664719 * Math.sin(P_) + (gAlt / 6378140) * Math.sin(falakLib.Rad(gLat))
            C = Math.cos(P_) + (gAlt / 6378140) * Math.cos(falakLib.Rad(gLat))
            x   = x0  + x1*T  + x2*T*T  + x3*T*T*T  + x4*T*T*T*T
            y   = y0  + y1*T  + y2*T*T  + y3*T*T*T  + y4*T*T*T*T
            d   = d0  + d1*T  + d2*T*T  + d3*T*T*T  + d4*T*T*T*T
            M   = M0  + M1*T  + M2*T*T  + M3*T*T*T  + M4*T*T*T*T
            L1  = L10	+ L11*T + L12*T*T + L13*T*T*T + L14*T*T*T*T
            L2  = L20	+ L21*T + L22*T*T + L23*T*T*T + L24*T*T*T*T
            x_  = x1 + 2*x2*T + 3*x3*T*T + 4*x4*T*T*T
            y_  = y1 + 2*y2*T + 3*y3*T*T + 4*y4*T*T*T
            H = falakLib.Rad(M + gLon.toDouble() - 0.00417807 * DeltaT.toDouble())
            p = C * Math.sin(H)
            q = S * Math.cos(falakLib.Rad(d)) - C * Math.cos(H) * Math.sin(falakLib.Rad(d))
            r = S * Math.sin(falakLib.Rad(d)) + C * Math.cos(H) * Math.cos(falakLib.Rad(d))
            p_ = 0.01745329 * M1 * C * Math.cos(H)
            q_ = 0.01745329 * (M1 * p * Math.sin(falakLib.Rad(d)) - r * d1)
            u = x - p
            v = y - q
            a = x_ - p_
            b = y_ - q_
            n = a * a + b * b
            P = -(u * a + v * b) / n
            T = T + P
        }
        Tx = T0 + T - falakLib.DeltaT(JDESolarEclipse2) / 3600.0
        JDSolarEclipseMax = Math.floor(JDESolarEclipse2 + 0.5) - 0.5 + (Tx /24.0)
//        return when (OptResult) {
//            "JDS"   -> JDESolarEclipse2
//            "T0"    -> T0
//            "T"     -> T
//            "Tx"    -> Tx
//            "r"     -> r
//            "u"     -> u
//            "v"     -> v
//            "a"     -> a
//            "b"     -> b
//            "n"     -> n
//            "L1"    -> L1
//            "L2"    -> L2
//            "JDSMx" -> JDSolarEclipseMax
//            else    -> JDSolarEclipseMax
//        }

        return listOf(r,u,v,L1,L2)
    }


//    fun SolarEclipse (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Double {
//        var JDSU1 : Double
//        var JDSU2 : Double
//        var JDSMx : Double
//        var JDSU3 : Double
//        var JDSU4 : Double
//        var MagU  : Double
//        var Obsk  : Double
//        var DurG  : Double
//        var DurT  : Double
//        var SEAltU1 : Double
//        var SEAzmU1 : Double
//        var SEAltU2 : Double
//        var SEAzmU2 : Double
//        var SEAltMx : Double
//        var SEAzmMx : Double
//        var SEAltU3 : Double
//        var SEAzmU3 : Double
//        var SEAltU4 : Double
//        var SEAzmU4 : Double
//
//        JDSMx = 0.0
//        JDSU1 = 0.0
//
//        DurG = 0.0
//        DurT = 0.0
//
//        when(OptResult) {
//            "JDSMx" -> {
//                JDSMx = falakLib.JDSolarEclipseMax(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSMx")
//            }
//            "JDSU1" -> {
//                JDSU1 = falakLib.JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")
//            }
//            "DurG" -> {
//                JDSU1 = falakLib.JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")
//                JDSU4 = falakLib.JDSolarEclipseU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU4")
//
//                DurG  = (JDSU4 - JDSU1) * 24
//            }
//            "DurT" -> {
//                JDSU2 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU2")
//                JDSU3 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU3")
//
//                DurT  = (JDSU3 - JDSU2) * 24
//            }
//        }
//
////        JDSMx = falakLib.JDSolarEclipseMax(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSMx")
//
////        MagU  = falakLib.SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Mag")
////        Obsk  = falakLib.SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Obs")
//
////        SEAltU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU1")
////        SEAzmU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU1")
////        SEAltU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU2")
////        SEAzmU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU2")
////        SEAltMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltMx")
////        SEAzmMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmMx")
////        SEAltU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU3")
////        SEAzmU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU3")
////        SEAltU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU4")
////        SEAzmU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU4")
//
//
//
//        return when (OptResult) {
//            "JDSMx" -> JDSMx
//            "JDSU1" -> JDSU1
//            "DurG" -> DurG
//            "DurT" -> DurT
//            else    -> 0.0
//        }
//    }

    fun getDurasiGerhana (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): List<Pair<String,Double>> {
        var JDSU1 : Double
        var JDSU2 : Double
        var JDSMx : Double
        var JDSU3 : Double
        var JDSU4 : Double
        var MagU  : Double
        var Obsk  : Double
        var DurG  : Double
        var DurT  : Double
        var SEAltU1 : Double
        var SEAzmU1 : Double
        var SEAltU2 : Double
        var SEAzmU2 : Double
        var SEAltMx : Double
        var SEAzmMx : Double
        var SEAltU3 : Double
        var SEAzmU3 : Double
        var SEAltU4 : Double
        var SEAzmU4 : Double

        JDSMx = 0.0
        JDSU1 = 0.0

        DurG = 0.0
        DurT = 0.0


        JDSU1 = falakLib.JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")
        JDSU4 = falakLib.JDSolarEclipseU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU4")

        JDSU2 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU2")
        JDSU3 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU3")

        DurT  = (JDSU3 - JDSU2) * 24

        DurG  = (JDSU4 - JDSU1) * 24

        return listOf(
            Pair("DurT",DurT),
            Pair("DurG",DurG)
        )
    }

    fun getAzimuthMatahari (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): List<Pair<String,Double>> {
        var JDSU1 : Double
        var JDSU2 : Double
        var JDSMx : Double
        var JDSU3 : Double
        var JDSU4 : Double
        var MagU  : Double
        var Obsk  : Double
        var DurG  : Double
        var DurT  : Double
        var SEAltU1 : Double
        var SEAzmU1 : Double
        var SEAltU2 : Double
        var SEAzmU2 : Double
        var SEAltMx : Double
        var SEAzmMx : Double
        var SEAltU3 : Double
        var SEAzmU3 : Double
        var SEAltU4 : Double
        var SEAzmU4 : Double

        SEAltU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU1")
        SEAzmU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU1")
        SEAltU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU2")
        SEAzmU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU2")
        SEAltMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltMx")
        SEAzmMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmMx")
        SEAltU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU3")
        SEAzmU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU3")
        SEAltU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU4")
        SEAzmU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU4")

        return listOf(
            Pair("AltU1",SEAltU1),
            Pair("AzmU1",SEAzmU1),
            Pair("AltU2",SEAltU2),
            Pair("AzmU2",SEAzmU2),
            Pair("AltMx",SEAltMx),
            Pair("AzmMx",SEAzmMx),
            Pair("AltU3",SEAltU3),
            Pair("AzmU3",SEAzmU3),
            Pair("AltU4",SEAltU4),
            Pair("AzmU4",SEAzmU4)

        )

//        return when (OptResult) {
//            "JDSU1" -> JDSU1
//            "JDSU2" -> JDSU2
//            "JDSMx" -> JDSMx
//            "JDSU3" -> JDSU3
//            "JDSU4" -> JDSU4
//            "MagU"  -> MagU
//            "Obsk"  -> Obsk
//            "DurG"  -> DurG
//            "DurT"  -> DurT
//            "AltU1" -> SEAltU1
//            "AzmU1" -> SEAzmU1
//            "AltU2" -> SEAltU2
//            "AzmU2" -> SEAzmU2
//            "AltMx" -> SEAltMx
//            "AzmMx" -> SEAzmMx
//            "AltU3" -> SEAltU3
//            "AzmU3" -> SEAzmU3
//            "AltU4" -> SEAltU4
//            "AzmU4" -> SEAzmU4
//            else    -> JDSMx
//        }
    }

    fun SolarEclipse (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): List<Pair<String,Double>> {
        var JDSU1 : Double
        var JDSU2 : Double
        var JDSMx : Double
        var JDSU3 : Double
        var JDSU4 : Double
        var MagU  : Double
        var Obsk  : Double
        var DurG  : Double
        var DurT  : Double
        var SEAltU1 : Double
        var SEAzmU1 : Double
        var SEAltU2 : Double
        var SEAzmU2 : Double
        var SEAltMx : Double
        var SEAzmMx : Double
        var SEAltU3 : Double
        var SEAzmU3 : Double
        var SEAltU4 : Double
        var SEAzmU4 : Double

        JDSU1 = falakLib.JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")
        JDSU2 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU2")
        JDSMx = falakLib.JDSolarEclipseMax(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSMx")
        JDSU3 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU3")
        JDSU4 = falakLib.JDSolarEclipseU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU4")
        MagU  = falakLib.SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Mag")
        Obsk  = falakLib.SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Obs")
        DurG  = (JDSU4 - JDSU1) * 24
        DurT  = (JDSU3 - JDSU2) * 24
        SEAltU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU1")
        SEAzmU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU1")
        SEAltU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU2")
        SEAzmU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU2")
        SEAltMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltMx")
        SEAzmMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmMx")
        SEAltU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU3")
        SEAzmU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU3")
        SEAltU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU4")
        SEAzmU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU4")

        return listOf(
            Pair("JDSU1",JDSU1),
            Pair("AltU1",SEAltU1),
            Pair("AzmU1",SEAzmU1),
            Pair("JDSU2",JDSU2),
            Pair("AltU2",SEAltU2),
            Pair("AzmU2",SEAzmU2),
            Pair("JDSMx",JDSMx),
            Pair("AltMx",SEAltMx),
            Pair("AzmMx",SEAzmMx),
            Pair("JDSU3",JDSU3),
            Pair("AltU3",SEAltU3),
            Pair("AzmU3",SEAzmU3),
            Pair("JDSU4",JDSU4),
            Pair("AltU4",SEAltU4),
            Pair("AzmU4",SEAzmU4),
            Pair("DurT",DurT),
            Pair("DurG",DurG),
            Pair("MagU",MagU),
            Pair("Obsk",Obsk)
        )
    }

    fun getWaktuGerhana (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): List<Pair<String,Double>> {
        var JDSU1 : Double
        var JDSU2 : Double
        var JDSMx : Double
        var JDSU3 : Double
        var JDSU4 : Double
        var MagU  : Double
        var Obsk  : Double
        var DurG  : Double
        var DurT  : Double
        var SEAltU1 : Double
        var SEAzmU1 : Double
        var SEAltU2 : Double
        var SEAzmU2 : Double
        var SEAltMx : Double
        var SEAzmMx : Double
        var SEAltU3 : Double
        var SEAzmU3 : Double
        var SEAltU4 : Double
        var SEAzmU4 : Double

        JDSU1 = falakLib.JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")

        return listOf(
            Pair("JDSU1",JDSU1),
        )
    }

    fun getTanggalGerhana (HijriMonth: Byte, HijriYear: Long, gLat: Double, gLon: Double, gAlt: Double, OptResult: String): Pair<String,Double> {
        var JDSU1 : Double
        var JDSU2 : Double
        var JDSMx : Double
        var JDSU3 : Double
        var JDSU4 : Double
        var MagU  : Double
        var Obsk  : Double
        var DurG  : Double
        var DurT  : Double
        var SEAltU1 : Double
        var SEAzmU1 : Double
        var SEAltU2 : Double
        var SEAzmU2 : Double
        var SEAltMx : Double
        var SEAzmMx : Double
        var SEAltU3 : Double
        var SEAzmU3 : Double
        var SEAltU4 : Double
        var SEAzmU4 : Double

        JDSU1 = falakLib.JDSolarEclipseU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU1")
//        JDSU2 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU2")
//        JDSMx = falakLib.JDSolarEclipseMax(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSMx")
//        JDSU3 = falakLib.JDSolarEclipseU2U3(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU3")
//        JDSU4 = falakLib.JDSolarEclipseU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"JDSU4")
//        MagU  = falakLib.SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Mag")
//        Obsk  = falakLib.SolarEclipseObscuration(HijriMonth,HijriYear,gLat,gLon,gAlt,"Obs")
//        DurG  = (JDSU4 - JDSU1) * 24
//        DurT  = (JDSU3 - JDSU2) * 24
//        SEAltU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU1")
//        SEAzmU1 = falakLib.SEAltAzmU1(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU1")
//        SEAltU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU2")
//        SEAzmU2 = falakLib.SEAltAzmU2(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU2")
//        SEAltMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltMx")
//        SEAzmMx = falakLib.SEAltAzmMx(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmMx")
//        SEAltU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU3")
//        SEAzmU3 = falakLib.SEAltAzmU3(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU3")
//        SEAltU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AltU4")
//        SEAzmU4 = falakLib.SEAltAzmU4(HijriMonth,HijriYear,gLat,gLon,gAlt,"AzmU4")

        return Pair("JDSU1",JDSU1)
    }


}