package id.someah.islamictimes.data

import android.annotation.SuppressLint
import android.content.Context
import androidx.preference.PreferenceManager
import java.text.SimpleDateFormat
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.modules.FalakLib
import java.util.*

const val IHTIYATH_DZUHUR = "ihtiyat_Zuhur"
const val IHTIYATH_ASHAR = "ihtiyat_Asar"
const val IHTIYATH_MAGRIB = "ihtiyat_Magrib"
const val IHTIYATH_ISYA = "ihtiyat_Isya"
const val IHTIYATH_NISFULAIL = "ihtiyat_Akhir Isya"
const val IHTIYATH_SHUBUH = "ihtiyat_Subuh"
const val IHTIYATH_SYURUQ = "ihtiyat_Akhir Subuh"
const val IHTIYATH_DHUHA = "ihtiyat_Duha"

@SuppressLint("SimpleDateFormat")
class LocalData(context: Context) {
    //get tangagl sekarang
    private val calendar : Calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
    private val tanggal: Byte       = calendar.get(Calendar.DAY_OF_MONTH).toByte()
    private val bulan: Byte         = (calendar.get(Calendar.MONTH) + 1).toByte()
    private val tahun: Long         = calendar.get(Calendar.YEAR).toLong()

    //get data timezone
    val timezoneOffset = SimpleDateFormat("Z", Locale.getDefault()).format(calendar.time)
    private val falak = FalakLib()
    private val sharedPref = SharedPref(context)
    //val altIsya = sharedPref.getValueInt("alt_matahari_isya", -18)
    //val altShubuh = sharedPref.getValueInt("alt_matahari_shubuh", -20)
    private val sharedPreferencesSetting = PreferenceManager.getDefaultSharedPreferences(context)

    val longitude: Double = sharedPref.getValueDouble("longitude_user")!!
    val latitude: Double = sharedPref.getValueDouble("latitude_user")!!

    //private val jamDs   : Double    = 0.0
    private val modeDariSetting = sharedPreferencesSetting.getBoolean("koreksi_tinggi", false)
    val elevasi: Double = if(modeDariSetting == true) sharedPref.getValueDouble("altitude_user")!! else 0.0 //jika koreksi tinggi tempat on ngambil data altitude
    val tzn: Double = sharedPref.getValueDouble("timezone") ?: timezoneOffset.toDouble() / 100

    //val jD : Double    = falak.KMJD(tanggal,bulan,tahun,jamDs,tzn)
    //val altM : String    = falak.DDDMS(falak.Altitude(jD,longitude,latitude,1),"DDMMSS",2)
    //val altB : String    = falak.DDDMS(falak.Altitude(jD,longitude,latitude,2),"DDMMSS",2)

    val ihtiyathValueZuhur: Int = ihtiyathPrefZuhur()
    val ihtiyathValueAshar: Int =  ihtiyathPrefAshar()
    val ihtiyathValueMaghrib: Int =  ihtiyathPrefMagrib()
    val ihtiyathValueIsya: Int =  ihtiyathPrefIsya()
    //val ihtiyathValueNisfuLail: Int =  ihtiyathPrefNisfuLail()
    val ihtiyathValueShubuh: Int =  ihtiyathPrefShubuh()
    val ihtiyathValueSyuruk: Int =  ihtiyathPrefSyuruq()
    //val ihtiyathValueDhuha: Int =  ihtiyathPrefDhuha()
    val derajatKiblatSpherical = falak.ArahQiblatSpherical(longitude, latitude)
    val derajatKiblatEllipsoid = falak.ArahQiblaWithEllipsoidCorrection(longitude, latitude)
    val derajatKiblatVincenty = falak.ArahQiblaVincenty(longitude,latitude,"")

    //get waktu sholat
    //cara pemanggilan fungsi dan hasil output bisa dilihat di library bagian main function
    private val zuhur: Double = falak.Zuhur(tanggal, bulan, tahun, longitude, latitude, tzn)
    val iZuhur: Double = falak.IhtiyathShalat(zuhur, ihtiyathValueZuhur)
    private val ashar: Double = falak.Ashar(tanggal, bulan, tahun, longitude, latitude, tzn)
    val iAshar: Double = falak.IhtiyathShalat(ashar, ihtiyathValueAshar)
    private val maghrib: Double = falak.Magrib(tanggal, bulan, tahun, longitude, latitude, elevasi, tzn)
    val iMagrib: Double = falak.IhtiyathShalat(maghrib, ihtiyathValueMaghrib)
    private val isya: Double = falak.Isya(tanggal, bulan, tahun, longitude, latitude, tzn)
    val iIsya: Double = falak.IhtiyathShalat(isya, ihtiyathValueIsya)
    //private val nisfuLail: Double = falak.NisfuLail(tanggal, bulan, tahun, longitude, latitude, elevasi, tzn, altShubuh.toString().toDouble())
    private val subuh: Double = falak.Subuh(tanggal, bulan, tahun, longitude, latitude, tzn)
    val iSubuh: Double = falak.IhtiyathShalat(subuh, ihtiyathValueShubuh)
    val nisfuLail: Double = ((falak.Mod(iSubuh - iMagrib, 24.0))/2.0) + iMagrib
    private val syuruk: Double = falak.Syuruk(tanggal, bulan, tahun, longitude, latitude, elevasi, tzn)
    val iSyuruk: Double = falak.IhtiyathShalat(syuruk, ihtiyathValueSyuruk)
    //private val duha: Double = falak.Duha(tanggal, bulan, tahun, longitude, latitude, elevasi, tzn)
    val iDuha: Double = iSyuruk + 15/60.0

    //get data ihtiyath dari shared preferences
    //ihtiyath berfungsi untuk menambah atau mengurangi waktu sholat (ihtiyath = kehati-hatian waktu sholat)
    private fun ihtiyathPrefZuhur() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_DZUHUR, 2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }

    private fun ihtiyathPrefAshar() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_ASHAR, 2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }

    private fun ihtiyathPrefMagrib() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_MAGRIB, 2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }

    private fun ihtiyathPrefIsya() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_ISYA, 2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }

    private fun ihtiyathPrefNisfuLail() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_NISFULAIL, 2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }

    private fun ihtiyathPrefShubuh() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_SHUBUH, 2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }

    private fun ihtiyathPrefSyuruq() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_SYURUQ, -2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }

    private fun ihtiyathPrefDhuha() : Int{
        val ihtiyathValue = sharedPref.getValueInt(IHTIYATH_DHUHA, 2)
        if(ihtiyathValue != null){
            return ihtiyathValue.toInt()
        }
        return 0
    }
}