package id.someah.islamictimes.data.repository

import android.util.Log
import id.someah.islamictimes.data.DataGerhana
import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.data.DataTerkait
import id.someah.islamictimes.data.KontakGerhanaModel
import id.someah.islamictimes.data.repository.library.GerhanaPhase
import id.someah.islamictimes.data.repository.library.LibraryRumusGerhanaBulan
import id.someah.islamictimes.data.repository.library.LibraryRumusGerhanaMatahari
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import javax.inject.Inject

interface GerhanaRepository {
    fun getDataGerhanaYearOnly(year : Long) : List<DataGerhana>
    fun getDataGerhana(hijri : Byte,year : Long) : DataGerhana
    fun getJenisDanWaktuGerhana(hijri : Byte,year : Long) : String
    fun getDataTerkaitGerhana(hijri : Byte,year : Long) : DataTerkait
    fun getKontakGerhanaBulan(state : GerhanaPhase, hijri : Byte,year : Long) : KontakGerhanaModel
    fun getGerhanaTerbitTransitTerbenam(day : Byte,bln: Byte, thn: Long) : Triple<String,String,String>

    fun getKontakGerhanaMatahari(hijri : Byte,year : Long) : DataGerhanaMatahari
    fun getDurasiGerhanaMatahari(hijri : Byte,year : Long) : Pair<String,String>
    fun getMagnitudeObsGerhanaMatahari(hijri : Byte,year : Long) : Pair<String,String>
    fun getAltAzmGerhanaMatahari(hijri : Byte,year : Long) : List<String>

    fun getKontakGerhanaMatahariTahunan(hijri : Byte,year : Long) : List<DataGerhanaMatahari>
    fun getDetailGerhanaMatahari(hijri : Byte,year : Long) : DataGerhanaMatahari
}

class GerhanaRepositoryImpl @Inject constructor(
    private val rumusGerhanaBulan: LibraryRumusGerhanaBulan,
    private val rumusGerhanaMatahari : LibraryRumusGerhanaMatahari
) : GerhanaRepository  {

    override fun getDataGerhanaYearOnly(year: Long): List<DataGerhana> {
        TODO("Not yet implemented")
    }

    override fun getDataGerhana(hijri: Byte, year: Long): DataGerhana {
        Log.d("rumus", "getDataGerhana: called")
        return rumusGerhanaBulan.calculate(hijri,year)
    }

    override fun getJenisDanWaktuGerhana(hijri: Byte, year: Long): String {
        return rumusGerhanaBulan.getJenisDanWaktuGerhana(hijri,year)
    }

    override fun getDataTerkaitGerhana(hijri: Byte, year: Long): DataTerkait {
        return rumusGerhanaBulan.getDataTerkaitGerhana(hijri,year)
    }

    override fun getKontakGerhanaBulan(
        state: GerhanaPhase,
        hijri: Byte,
        year: Long
    ): KontakGerhanaModel {
        return rumusGerhanaBulan.calculateKontakGerhana(state,hijri,year)
    }

    override fun getGerhanaTerbitTransitTerbenam(day : Byte,bln: Byte, thn: Long): Triple<String, String, String> {
        return rumusGerhanaBulan.calculateGerhanaTerbitTransitTerbenam(day,bln,thn)
    }

    override fun getKontakGerhanaMatahari(hijri: Byte, year: Long): DataGerhanaMatahari {
        return rumusGerhanaMatahari.calculateGerhanaMatahari(hijri,year)
    }

    override fun getDurasiGerhanaMatahari(hijri: Byte, year: Long): Pair<String, String> {
        return rumusGerhanaMatahari.calculateDurasiGerhanaMatahari(hijri,year)
    }

    override fun getMagnitudeObsGerhanaMatahari(hijri: Byte, year: Long): Pair<String, String> {
        return rumusGerhanaMatahari.calculateMagnitudeObsGerhanaMatahari(hijri,year)
    }

    override fun getAltAzmGerhanaMatahari(hijri: Byte, year: Long): List<String> {
        return rumusGerhanaMatahari.calculateAltAzmGerhanaMatahari(hijri,year)
    }

    override fun getKontakGerhanaMatahariTahunan(
        hijri: Byte,
        year: Long
    ): List<DataGerhanaMatahari> {
        return rumusGerhanaMatahari.calculateGerhanaMatahariTahunan(hijri,year)
    }

    override fun getDetailGerhanaMatahari(hijri: Byte, year: Long): DataGerhanaMatahari {
        return rumusGerhanaMatahari.calculateDetailGerhana(hijri,year)
    }

}