package id.someah.islamictimes.data

import android.content.Context
import id.someah.islamictimes.model.MatahariBulan
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.Mod
import id.someah.islamictimes.services.utils.RoundTo
import java.util.*
import kotlin.collections.ArrayList
/*
    class untuk mengelola data matahari dan bulan matahari realtime.
    contoh output dan penggunaan fungsi bisa di lihat di library bagian fungsi main.
 */
class DataBulanMatahari(context: Context) : DataBulanMatahariDao {
    //get tanggal hari ini
    private val calendar : Calendar = Calendar.getInstance()
    private val tanggal: Byte       = calendar.get(Calendar.DAY_OF_MONTH).toByte()
    private val bulan: Byte         = (calendar.get(Calendar.MONTH) + 1).toByte() //di plus 1 karena output bulan dimulai dari 0
    private val tahun: Long         = calendar.get(Calendar.YEAR).toLong()

    private val falak = FalakLib()
    private val localData = LocalData(context)
    private val sharedPref = SharedPref(context)

    //fungsi untuk menampilkan data bulan dan data matahari realtime. output bisa dilihat di menu bulan dan matahari bagian realtime
    override fun getListBulanMatahari(jamDesimal : Double, lokasiAcuan : String, deltaT : Double): ArrayList<MatahariBulan> {

        //longitude, latitude, altitude ambil dari shared preferences. di save di class Locate.kt
        val longitude: Double = sharedPref.getValueDouble("longitude_user")!!
        val latitude: Double = sharedPref.getValueDouble("latitude_user")!!
        val altitude: Double = sharedPref.getValueDouble("altitude_user")!!

        val jd: Double = falak.KMJD(tanggal, bulan, tahun, jamDesimal, localData.tzn) //jam desimal, input tgl,bln,thn, hitungan jam desimal, timezone
//        val waktus =  (  11 +  0 / 60.0 +  0.00 / 3600.0)
//        val jd: Double = falak.KMJD(20, 8, 2020, waktus , 7.0) //jam desimal, input tgl,bln,thn, hitungan jam desimal, timezone

        var ghaS : String? = null
        var lhaS : String? = null
        val JDE     : Double = jd + deltaT / 86400.0
        var apparentLongitudeM: String? = null
        var apparentLatitudeM: String? = null
        var declinationM: String? = null
        var raM: String? = null
        var altM: String? = null
        var azmM: String? = null
        var azmMD: String? = null
        var backAzmM: String? = null
        var backAzmMD : String? = null

        var ghaM: String? = null
        var lhaM: String? = null
        var apparentLongitudeB: String? = null
        var apparentLatitudeB: String? = null
        var declinationB: String? = null
        var raB: String? = null
        var altB: String? = null
        var azmB: String? = null
        var azmBD: String? = null
        var backAzmB: String? = null
        var backAzmBD : String? = null


        //tampilan / hitungan data disesuaikan dengan acuan lokasi dari spinner
        if(lokasiAcuan == "toposentris"){
            //AWAL DATA MATAHARI BULAN LOKASI ACUAN TOPOSENTRIS
            //Data matahari
            //parameter input jamdesimal, longitude, latitude, deltaT
            ghaS = falak.DHHMS(falak.SunTopocentricEquatorialCoordinates(jd,0.0,0.0,0.0,deltaT,3)/15.0,"HHMMSS" )
            lhaS = falak.DHHMS(falak.SunTopocentricEquatorialCoordinates(jd,longitude,latitude,altitude,deltaT,3)/15.0,"HHMMSS" )
            apparentLongitudeM = falak.DDDMS(falak.SunTopocentricEclipticCoordinates(jd,longitude,latitude,altitude,deltaT,1))
            apparentLatitudeM = falak.DDDMS(falak.SunTopocentricEclipticCoordinates(jd,longitude,latitude,altitude,deltaT,2))
            declinationM = falak.DDDMS(falak.SunTopocentricEquatorialCoordinates(jd,longitude,latitude,altitude,deltaT,2))
            raM = falak.DDDMS(falak.SunTopocentricEquatorialCoordinates(jd,longitude,latitude,altitude,deltaT,1))
            altM = falak.DDDMS(falak.SunTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,2))
            azmM = falak.DDDMS(falak.SunTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1))
            azmMD = RoundTo(falak.SunTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1),2)
            backAzmM = falak.DDDMS(Mod(falak.SunTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1)+180.0,360.0 )).toString()
            backAzmMD = RoundTo(Mod(falak.SunTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1)+180.0,360.0 ),2).toString()

            //Data Bulan
            //input jamdesimal, longitude, latitude, deltaT
            ghaM = falak.DHHMS(falak.MoonTopocentricEquatorialCoordinates(jd,0.0,0.0,0.0,deltaT,3)/15.0,"HHMMSS" )
            lhaM = falak.DHHMS(falak.MoonTopocentricEquatorialCoordinates(jd,longitude,latitude,altitude,deltaT,3)/15.0,"HHMMSS" )
            apparentLongitudeB = falak.DDDMS(falak.MoonTopocentricEclipticCoordinates(jd,longitude,latitude,altitude,deltaT,1))
            apparentLatitudeB = falak.DDDMS(falak.MoonTopocentricEclipticCoordinates(jd,longitude,latitude,altitude,deltaT,2))
            declinationB= falak.DDDMS(falak.MoonTopocentricEquatorialCoordinates(jd,longitude,latitude,altitude,deltaT,2))
            raB = falak.DDDMS(falak.MoonTopocentricEquatorialCoordinates(jd,longitude,latitude,altitude,deltaT,1))
            altB = falak.DDDMS(falak.MoonTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,2))
            azmB = falak.DDDMS(falak.MoonTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1))
            backAzmB = falak.DDDMS(Mod(falak.MoonTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1)+180.0,360.0 )).toString()
            azmBD = RoundTo(falak.MoonTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1),2)
            backAzmBD = RoundTo(Mod(falak.MoonTopocentricHorizontalCoordinates(jd,longitude,latitude,altitude,deltaT,1)+180.0,360.0 ),2)
            //AKHIR DATA MATAHARI BULAN LOKASI ACUAN TOPOSENTRIS
        }

        if(lokasiAcuan == "geosentris"){
            //AWAL DATA MATAHARI BULAN LOKASI ACUAN GEOSENTRIS
            //Data matahari
            //parameter input jamdesimal, longitude, latitude, deltaT
            ghaS = falak.DHHMS(falak.SunGreenwichHourAngle(jd,deltaT)/15.0,"HHMMSS" )
            lhaS = falak.DHHMS(falak.SunLocalHourAngle(jd,longitude,deltaT)/15.0,"HHMMSS")
            apparentLongitudeM = falak.DDDMS(falak.SunGeocentricLongitude(jd))
            apparentLatitudeM = falak.DDDMS(falak.SunGeocentricLatitude(JDE)/3600.0)
            declinationM = falak.DDDMS(falak.SunApparentDeclination(JDE))
            raM = falak.DDDMS(falak.SunApparentRightAscension(jd))
            altM = falak.DDDMS(falak.SunAltitude(jd,+ longitude ,latitude,deltaT))
            azmM = falak.DDDMS(falak.SunAzimuth(jd,longitude,latitude,deltaT))
            azmMD = RoundTo(falak.SunAzimuth(jd,longitude,latitude,deltaT))

            ghaM = falak.DHHMS(falak.MoonGreenwichHourAngle(jd,deltaT)/15.0,"HHMMSS" )
            lhaM = falak.DHHMS(falak.MoonLocalHourAngle(jd,longitude,deltaT)/15.0,"HHMMSS")
            apparentLongitudeB = falak.DDDMS(falak.MoonGeocentricLongitude(JDE))
            apparentLatitudeB = falak.DDDMS(falak.MoonGeocentricLatitude(JDE))
            declinationB= falak.DDDMS(falak.MoonApparentDeclination(JDE))
            raB = falak.DDDMS(falak.MoonApparentRightAscension(JDE))
            altB = falak.DDDMS(falak.MoonAltitude(jd,longitude,latitude,deltaT))
            azmB = falak.DDDMS(falak.MoonAzimuth(jd,longitude,latitude,deltaT))
            azmBD = RoundTo(falak.MoonAzimuth(jd,longitude,latitude,deltaT),2)
            //AKHIR DATA MATAHARI BULAN LOKASI ACUAN GEOSENTRIS
        }

        //index list 0 add data matahari
        val listDatabulanMatahari: java.util.ArrayList<MatahariBulan> = arrayListOf()
        val dataMatahari = MatahariBulan(
            ghaS!!,
            lhaS!!,
            raM!!,
            declinationM!!,
            apparentLatitudeM!!,
            apparentLongitudeM!!,
            altM!!,
            azmM!!,
            backAzimut = backAzmM,
            decimalAzimut = azmMD,
            decimalBackAzimuth = backAzmMD
        )
        listDatabulanMatahari.add(dataMatahari)
        //index list 1 add data bulan
        val databulan = MatahariBulan(
            ghaM!!,
            lhaM!!,
            raB!!,
            declinationB!!,
            apparentLatitudeB!!,
            apparentLongitudeB!!,
            altB!!,
            azmB!!,
            backAzimut = backAzmB,
            decimalAzimut = azmBD,
            decimalBackAzimuth = backAzmBD
        )
        listDatabulanMatahari.add(databulan)
        return listDatabulanMatahari
    }

}