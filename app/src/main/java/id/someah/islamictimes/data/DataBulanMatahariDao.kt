package id.someah.islamictimes.data

import id.someah.islamictimes.model.MatahariBulan

interface DataBulanMatahariDao {
    fun getListBulanMatahari(jamDesimal : Double, lokasiAcuan : String, deltaT : Double) : ArrayList<MatahariBulan>
}