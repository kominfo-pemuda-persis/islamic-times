package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetDetailGerhanaMatahariUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte, Long>, DataGerhanaMatahari>() {
    override fun invoke(parameterName: Pair<Byte, Long>): DataGerhanaMatahari {
        return repository.getDetailGerhanaMatahari(parameterName.first,parameterName.second)
    }
}