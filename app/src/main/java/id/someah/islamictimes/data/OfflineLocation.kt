package id.someah.islamictimes.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.services.utils.convertDDDMStoDD
import java.io.File
import java.io.FileOutputStream


class OfflineLocation(context: Context, resource: Int) {

    lateinit var db : SQLiteDatabase

    init {
        val file : File? = context.getDatabasePath(Constant.DATABASE_FILE_NAME)

        if(file != null && !file.exists()) {
            if(!file.parentFile.exists()) {
                file.parentFile?.mkdir()
            }
            val inputStream = context.resources.openRawResource(resource)
            val outputStream = FileOutputStream(file)

            inputStream.use { input ->
                file.outputStream().use {
                    input.copyTo(it)
                }
            }

//            outputStream.use { out ->
//                inputStream.copyTo(out)
//            }

//            inputStream.close()
//            outputStream.close()
        }

        if(file != null)
            db = SQLiteDatabase.openOrCreateDatabase(file,null)
    }


    fun getLocation() : List<OfflineLocationModel> {
        if(this::db.isInitialized) {
            val cursor = db.rawQuery("SELECT * FROM $TABLE_NAME",null)
            if(cursor.moveToFirst()) {
                var locationList = arrayListOf<OfflineLocationModel>()

                do {


                    val latitude = convertDDDMStoDD(cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5))
                    val longitude = convertDDDMStoDD(cursor.getString(6),cursor.getString(7),cursor.getString(8),cursor.getString(9))

                    var item = OfflineLocationModel(
                        "${cursor.getString(1)},${cursor.getString(0)}",
                        latitude,
                        longitude,
                        cursor.getString(11).toDouble()
                    )
                    item.timeZone = cursor.getString(10).toDouble()
                    item.timeZoneTitle = if(cursor.getString(10).contains("-")) cursor.getString(10) else "+${cursor.getString(10)}"
                    locationList.add(
                        item
                    )
                } while (cursor.moveToNext())

                return locationList
            }
        }
        return emptyList()
    }

    companion object {
        const val TABLE_NAME = "Location"
    }

}