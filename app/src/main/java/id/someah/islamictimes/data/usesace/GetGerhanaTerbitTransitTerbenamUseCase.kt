package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetGerhanaTerbitTransitTerbenamUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Triple<Byte, Byte, Long>, Triple<String, String,String>>() {
    override fun invoke(parameterName: Triple<Byte, Byte, Long>): Triple<String, String, String> {
        return repository.getGerhanaTerbitTransitTerbenam(parameterName.first,parameterName.second,parameterName.third)
    }
}