package id.someah.islamictimes.data

import android.content.Context
import android.util.Log
import androidx.preference.PreferenceManager
import id.someah.islamictimes.model.ArahKiblat
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.RoundTo
import id.someah.islamictimes.services.utils.deleteNewLine
import id.someah.islamictimes.services.utils.getTwoDigitBehindComma
import java.text.SimpleDateFormat
import java.util.*
/*
    class untuk mengelola data arah kiblat.
    contoh output dan penggunaan fungsi bisa di lihat di library bagian fungsi main.
 */
class DataArahKiblat(context: Context) : ArahKiblatDao{

    private val sharedPref = SharedPref(context)
    private val sharedPreferencesSetting = PreferenceManager.getDefaultSharedPreferences(context)
    private val kiblatMode = sharedPreferencesSetting.getString("kiblat", "kiblat_spherical")
    private val calendar : Calendar = Calendar.getInstance()
    private val tanggal: Byte       = calendar.get(Calendar.DAY_OF_MONTH).toByte()
    private val bulan: Byte         = (calendar.get(Calendar.MONTH) + 1).toByte()
    private val tahun: Long         = calendar.get(Calendar.YEAR).toLong()

    private val falak = FalakLib()
    private val timezoneOffset = SimpleDateFormat("Z", Locale.getDefault()).format(calendar.time)
    private val tzn: Double = timezoneOffset.toDouble()/100

    //fungsi untuk get data-data activity arah kiblat
    override fun dataArahKiblat(): ArahKiblat {
        val arahKiblat: String
        val jarakKeKiblat : Double
        val longitude: Double = sharedPref.getValueDouble("longitude_user")!!
        val latitude: Double = sharedPref.getValueDouble("latitude_user")!!
//        val latitude = -(  6 + 58/60.0 + 40.00/3600.0)
//        val longitude  =   (107 + 39/60.0 + 16.98/3600.0)
        val alamat = sharedPref.getValueString("alamat_user", "")

        val splits = alamat?.deleteNewLine()?.split(',')

        val lokasi = "${splits?.get(0)} , ${splits?.get(1)}"
        Log.d("contentValues", "dataArahKiblat: ${lokasi.trim()}")
        when {
            kiblatMode.equals("kiblat_spherical") -> {
//                arahKiblat = falak.DDDMS(falak.ArahQiblatSpherical(longitude,latitude))
                arahKiblat = falak.ArahQiblatSpherical(longitude,latitude).toString()
                jarakKeKiblat = falak.JarakQiblatSpherical(longitude,latitude)
            }
            kiblatMode.equals("kiblat_ellipsoid") -> {
//                arahKiblat = falak.DDDMS(falak.ArahQiblaWithEllipsoidCorrection(longitude,latitude))
                arahKiblat = falak.ArahQiblaWithEllipsoidCorrection(longitude,latitude).toString()
                jarakKeKiblat = falak.JarakQiblatEllipsoid(latitude,longitude)
            }
            else -> {
//                arahKiblat = falak.DDDMS(falak.ArahQiblaVincenty(longitude,latitude, "PtoQ"))
                arahKiblat = falak.ArahQiblaVincenty(longitude,latitude, "PtoQ").toString()
                jarakKeKiblat = falak.ArahQiblaVincenty(longitude,latitude,"Dist") / 1000
            }
        }
        val kiblatHarian1 = falak.BayanganQiblatHarian(longitude, latitude, tanggal, bulan, tahun, tzn, 1)
        val kiblatHarian2 = falak.BayanganQiblatHarian(longitude, latitude, tanggal, bulan, tahun, tzn, 2)
        val valueKiblatHarian1 = falak.DHHM(kiblatHarian1, "HH:MM", 0)
        val valueKiblatHarian2 = falak.DHHM(kiblatHarian2, "HH:MM", 0)
        val rashdulKiblat1 = falak.RashdulQiblat(tahun, tzn, 1)
        val rashdulKiblat2 = falak.RashdulQiblat(tahun, tzn, 2)
        return ArahKiblat(lokasi,"", arahKiblat, jarakKeKiblat,
            valueKiblatHarian1,valueKiblatHarian2, rashdulKiblat1, rashdulKiblat2, latitude, longitude)
    }

}