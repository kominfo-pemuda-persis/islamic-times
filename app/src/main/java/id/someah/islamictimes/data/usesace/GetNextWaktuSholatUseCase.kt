package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.repository.WaktuSholatRepository
import id.someah.islamictimes.model.NextWaktuSholat
import id.someah.islamictimes.model.WaktuSholat
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetNextWaktuSholatUseCase @Inject constructor(private val repository: WaktuSholatRepository) :
    BaseUseCase<Unit, NextWaktuSholat>() {
    override fun invoke(parameterName: Unit): NextWaktuSholat {
        return repository.getNextWaktuSholat()
    }
}