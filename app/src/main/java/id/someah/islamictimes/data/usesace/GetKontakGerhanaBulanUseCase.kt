package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.KontakGerhanaModel
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.data.repository.library.GerhanaPhase
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetKontakGerhanaBulanUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Triple<GerhanaPhase,Byte, Long>, KontakGerhanaModel>() {
    override fun invoke(parameterName: Triple<GerhanaPhase, Byte, Long>): KontakGerhanaModel {
        return repository.getKontakGerhanaBulan(parameterName.first,parameterName.second,parameterName.third)
    }
}