package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetDurasiGerhanaMatahariUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte, Long>, Pair<String,String>>() {
    override fun invoke(parameterName: Pair<Byte, Long>): Pair<String, String> {
        return repository.getDurasiGerhanaMatahari(parameterName.first,parameterName.second)
    }
}