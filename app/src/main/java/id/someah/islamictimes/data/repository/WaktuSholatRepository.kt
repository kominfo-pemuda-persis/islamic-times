package id.someah.islamictimes.data.repository

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.model.NextWaktuSholat
import id.someah.islamictimes.model.WaktuSholat
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.SimpleFormatter
import kotlin.collections.ArrayList

interface WaktuSholatRepository {
    fun getWaktuSholat() : ArrayList<WaktuSholat>
    fun getNextWaktuSholat() : NextWaktuSholat
    fun getWaktuSholatParamLocation(longitude : Double , latitude : Double) : ArrayList<WaktuSholat>
}

class WaktuSholatRepositoryImpl(
    private val sharedPref: SharedPref,
    private val falak : FalakLib ,
    private val context: Context)  : WaktuSholatRepository {


    @SuppressLint("SimpleDateFormat")
    override fun getWaktuSholat() : ArrayList<WaktuSholat>{
        val localData = LocalData(context)

        val listWaktuSholat: ArrayList<WaktuSholat> = arrayListOf()
        val namaSholat = arrayOf(
            Constant.NAMA_SHOLAT_ZUHUR, Constant.NAMA_SHOLAT_ASAR,
            Constant.NAMA_SHOLAT_MAGRIB, Constant.NAMA_SHOLAT_ISYA,
            Constant.NAMA_AKHIR_ISYA, Constant.NAMA_SHOLAT_SUBUH,
            Constant.NAMA_AKHIR_SUBUH, Constant.NAMA_SHOLAT_DUHA
        )

        val jamSholat = arrayOf(
            falak.DHHMS(localData.iZuhur, "HH:MM"),
            falak.DHHMS(localData.iAshar, "HH:MM"),
            falak.DHHMS(localData.iMagrib, "HH:MM"),
            falak.DHHMS(localData.iIsya, "HH:MM"),
            falak.DHHMS(localData.nisfuLail, "HH:MM"),
            falak.DHHMS(localData.iSubuh, "HH:MM"),
            falak.DHHMS(localData.iSyuruk, "HH:MM"),
            falak.DHHMS(localData.iDuha, "HH:MM")
        )

        //looping untuk menambahkan data waktu sholat ke list
        for (i in namaSholat.indices) {
            val waktuSholat = WaktuSholat(
                namaSholat[i],
                jamSholat[i]
            )
            listWaktuSholat.add(waktuSholat)
        }
        return listWaktuSholat
    }

    override fun getNextWaktuSholat(): NextWaktuSholat {
        val localData = LocalData(context)

        //get data waktu sholat dari class local data
        val dzuhurStr = falak.DHHMS(localData.iZuhur, "HH:MM")
        val ashar = falak.DHHMS(localData.iAshar, "HH:MM")
        val maghrib = falak.DHHMS(localData.iMagrib, "HH:MM")
        val isya = falak.DHHMS(localData.iIsya, "HH:MM")
        val nisfuLail = falak.DHHMS(localData.nisfuLail, "HH:MM")
        val shubuh = falak.DHHMS(localData.iSubuh, "HH:MM")
        val syuruk = falak.DHHMS(localData.iSyuruk, "HH:MM")
        val dhuha = falak.DHHMS(localData.iDuha, "HH:MM")

        val waktuDzuhur = setDate(dzuhurStr)
        val waktuAshar = setDate(ashar)
        val waktuMaghrib = setDate(maghrib)
        val waktuIsya = setDate(isya)
        val waktuNisfuLail = setDate(nisfuLail)
        val waktuShubuh = setDate(shubuh)
        val waktuSyuruq = setDate(syuruk)
        val waktuDhuha = setDate(dhuha)

        val dateNow = Calendar.getInstance()
        val dateMili = Calendar.getInstance().timeInMillis
        val nextWaktuSholat: NextWaktuSholat
        val timeMili: Long

        //pembandiangan waktu sekarang dengan waktu sholat agar mendapatkan data waktu sholat selanjutnya ketika kondisi true
        when {
            dateNow.time.before(waktuShubuh.time) -> {
                timeMili = waktuShubuh.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_SHOLAT_SUBUH, timeMili,shubuh)//masukkan data ke objek
            }
            dateNow.time.before(waktuSyuruq.time) -> {
                timeMili = waktuSyuruq.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_AKHIR_SUBUH, timeMili,syuruk)
            }
            dateNow.time.before(waktuDhuha.time) -> {
                timeMili = waktuDhuha.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_SHOLAT_DUHA, timeMili,dhuha)
            }
            dateNow.time.before(waktuDzuhur.time) -> {
                timeMili = waktuDzuhur.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_SHOLAT_ZUHUR, timeMili, dzuhurStr)
            }
            dateNow.time.before(waktuAshar.time) -> {
                timeMili = waktuAshar.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_SHOLAT_ASAR, timeMili,ashar)
            }
            dateNow.time.before(waktuMaghrib.time) -> {
                timeMili = waktuMaghrib.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_SHOLAT_MAGRIB, timeMili,maghrib)
            }
            dateNow.time.before(waktuIsya.time) -> {
                timeMili = waktuIsya.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_SHOLAT_ISYA, timeMili,isya)
            }
            dateNow.time.before(waktuNisfuLail.time) -> {
                timeMili = waktuNisfuLail.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_AKHIR_ISYA, timeMili,nisfuLail)
            }
            else -> {
                //case else disini ketika waktu sudah masuk waktu nisflu lail maka waktu selanjutnya adalah waktu shubuh
                waktuShubuh.add(Calendar.DATE, 1) //maka tanggal waktu shubuh nya ditambah 1
                timeMili = waktuShubuh.timeInMillis - dateMili
                nextWaktuSholat = NextWaktuSholat(Constant.NAMA_SHOLAT_SUBUH, timeMili,shubuh)
            }
        }
        return nextWaktuSholat
    }

    override fun getWaktuSholatParamLocation(longitude : Double , latitude : Double): ArrayList<WaktuSholat> {
        val localData = LocalData(context)

        val calendar : Calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
         val tanggal: Byte       = calendar.get(Calendar.DAY_OF_MONTH).toByte()
        val bulan: Byte         = (calendar.get(Calendar.MONTH) + 1).toByte()
        val tahun: Long         = calendar.get(Calendar.YEAR).toLong()


//        val longitude: Double = sharedPref.getValueDouble("longitude_user")!!
//        val latitude: Double = sharedPref.getValueDouble("latitude_user")!!

        val zuhur: Double = falak.Zuhur(tanggal, bulan, tahun, longitude, latitude, localData.tzn)
        val iZuhur: Double = falak.IhtiyathShalat(zuhur, localData.ihtiyathValueZuhur)
        val ashar: Double = falak.Ashar(tanggal, bulan, tahun, longitude, latitude, localData.tzn)
        val iAshar: Double = falak.IhtiyathShalat(ashar, localData.ihtiyathValueAshar)
        val maghrib: Double = falak.Magrib(tanggal, bulan, tahun, longitude, latitude, localData.elevasi, localData.tzn)
        val iMagrib: Double = falak.IhtiyathShalat(maghrib,localData.ihtiyathValueMaghrib)
        val isya: Double = falak.Isya(tanggal, bulan, tahun, longitude, latitude, localData.tzn)
        val iIsya: Double = falak.IhtiyathShalat(isya, localData.ihtiyathValueIsya)
        val subuh: Double = falak.Subuh(tanggal, bulan, tahun, longitude, latitude, localData.tzn)
        val iSubuh: Double = falak.IhtiyathShalat(subuh, localData.ihtiyathValueShubuh)
        val nisfuLail: Double = ((falak.Mod(iSubuh - iMagrib, 24.0))/2.0) + iMagrib
        val syuruk: Double = falak.Syuruk(tanggal, bulan, tahun, longitude, latitude,localData.elevasi, localData.tzn)
        val iSyuruk: Double = falak.IhtiyathShalat(syuruk, localData.ihtiyathValueSyuruk)
        //val duha: Double = falak.Duha(tanggal, bulan, tahun, longitude, latitude,localData.elevasi, localData.tzn)
        val iDuha: Double = iSyuruk + 15/60.0

        val listWaktuSholat: ArrayList<WaktuSholat> = arrayListOf()
        val namaSholat = arrayOf(
            Constant.NAMA_SHOLAT_ZUHUR, Constant.NAMA_SHOLAT_ASAR,
            Constant.NAMA_SHOLAT_MAGRIB, Constant.NAMA_SHOLAT_ISYA,
            Constant.NAMA_AKHIR_ISYA, Constant.NAMA_SHOLAT_SUBUH,
            Constant.NAMA_AKHIR_SUBUH, Constant.NAMA_SHOLAT_DUHA
        )

        val jamSholat = arrayOf(
            falak.DHHMS(iZuhur, "HH:MM"),
            falak.DHHMS(iAshar, "HH:MM"),
            falak.DHHMS(iMagrib, "HH:MM"),
            falak.DHHMS(iIsya, "HH:MM"),
            falak.DHHMS(nisfuLail, "HH:MM"),
            falak.DHHMS(iSubuh, "HH:MM"),
            falak.DHHMS(iSyuruk, "HH:MM"),
            falak.DHHMS(iDuha, "HH:MM")
        )

        for (i in namaSholat.indices) {
            val waktuSholat = WaktuSholat(
                namaSholat[i],
                jamSholat[i]
            )
            listWaktuSholat.add(waktuSholat)
        }
        return listWaktuSholat
    }

    private fun setDate(waktu : String) : Calendar{
        val dateSet = Calendar.getInstance()
        dateSet.set(Calendar.HOUR_OF_DAY, waktu.split(":")[0].toInt())
        dateSet.set(Calendar.MINUTE, waktu.split(":")[1].toInt())
        dateSet.set(Calendar.SECOND, 0)
        return dateSet
    }

}
