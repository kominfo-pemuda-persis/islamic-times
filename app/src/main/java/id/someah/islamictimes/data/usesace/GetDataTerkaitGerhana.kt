package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.DataTerkait
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetDataTerkaitGerhana @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte, Long>, DataTerkait>() {
    override fun invoke(parameterName: Pair<Byte, Long>): DataTerkait {
        return repository.getDataTerkaitGerhana(parameterName.first,parameterName.second)
    }
}