package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.DataGerhana
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetJenisDanWaktuGerhana @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte, Long>, String>() {
    override fun invoke(parameterName: Pair<Byte, Long>): String {
        return repository.getJenisDanWaktuGerhana(parameterName.first,parameterName.second)
    }
}