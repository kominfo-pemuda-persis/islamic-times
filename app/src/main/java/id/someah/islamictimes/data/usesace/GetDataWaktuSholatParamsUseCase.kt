package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.repository.WaktuSholatRepository
import id.someah.islamictimes.model.WaktuSholat
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetDataWaktuSholatParamsUseCase @Inject constructor(private val repository: WaktuSholatRepository) :
    BaseUseCase<Pair<Double,Double>, List<WaktuSholat>>() {
    override fun invoke(parameterName: Pair<Double, Double>): List<WaktuSholat> {
        return repository.getWaktuSholatParamLocation(
            parameterName.first,
            parameterName.second
        )
    }
}