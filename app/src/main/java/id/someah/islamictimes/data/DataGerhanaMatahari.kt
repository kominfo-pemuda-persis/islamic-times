package id.someah.islamictimes.data

data class DataGerhanaMatahari (var jenis : String) {
    var waktu : String = "-"
    var durasi : Pair<String,String> = Pair("-","-")
    var magnitudeObs : Pair<String,String>  = Pair("","")
    var altAzm : List<AltAzmMatahari> = listOf()
    var month : Byte = 0
    var year : Long = 1443
    var monthName : String = ""
}

data class AltAzmMatahari(
    val waktu : String
) {
    var alt : String = "-"
    var azm : String = "-"
}