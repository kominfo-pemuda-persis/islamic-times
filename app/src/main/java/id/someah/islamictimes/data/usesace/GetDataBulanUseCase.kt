package id.someah.islamictimes.data.usesace

import androidx.lifecycle.MutableLiveData
import id.someah.islamictimes.data.DataGerhana
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.util.BaseUseCase
import id.someah.islamictimes.util.Result
import javax.inject.Inject

open class GetDataBulanUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte,Long>,DataGerhana>() {
    override fun invoke(parameterName: Pair<Byte, Long>): DataGerhana {
        return repository.getDataGerhana(parameterName.first,parameterName.second)
    }
}