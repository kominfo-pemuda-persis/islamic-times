package id.someah.islamictimes.data

object Actions  {
    const val START = "start_service"
    const val END = "end_service"
}