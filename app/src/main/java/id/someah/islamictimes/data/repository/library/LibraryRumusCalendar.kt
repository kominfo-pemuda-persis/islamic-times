package id.someah.islamictimes.data.repository.library

import android.util.Log
import id.someah.islamictimes.constant.round
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.convertDateFormat
import id.someah.islamictimes.services.utils.decimalToSexagesimal
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import javax.inject.Inject


class LibraryRumusCalendarImpl @Inject constructor(
    private val sharedPRef : SharedPref, private val falakLib: FalakLib
) : LibraryRumusCalendar {
    override fun konversiMasehiKeHijri(date: Date) : Triple<Int,Int,Long> {

        val formatDate = date.convertDateFormat()

        val TZn   = sharedPRef.getValueDouble("timezone") ?: 7.0
        val jdnow = falakLib.KMJD(formatDate.first.toByte(),formatDate.second.toByte(),formatDate.third.toLong(),0.0,TZn)
        val cjdn = Math.floor(jdnow + 0.5 + TZn / 24).toLong()
        val masehi = falakLib.CJDNKH(cjdn, OptResult = "empty") as Triple<Int, Int,Long>


        return masehi
    }

    override fun getAwalBulanHijri(params: Pair<Int,Long>): Date {

        val gLat   : Double   = ( 5+  54/60.0 + 0/3600.0)
        val gLon   : Double   = (95 + 21/60.0 + 0/3600.0)
        val gAlt   : Double   = 10.0
        val TmZn   : Double   = 7.00
        val TmbhHr : Byte     = 1

        val BlnH = params.first.toByte()
        val ThnH = params.second



        val TZn   = sharedPRef.getValueDouble("timezone") ?: 7.0
        val JDNewMoon = falakLib.MoonPhasesModified(params.first.toByte(), params.second, 1) - falakLib.DeltaT(falakLib.MoonPhasesModified(params.first.toByte(), params.second, 1))/86400.0

        val JDGhurubS : Double = falakLib.JDGhurubSyams(JDNewMoon ,gLat,gLon,gAlt,TmZn)
        val THilal5    : Double = falakLib.TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,6)
        val JDNewMoon2 = falakLib.MoonPhasesModified(BlnH, ThnH, 1) - falakLib.DeltaT(falakLib.MoonPhasesModified(BlnH, ThnH, 1))/86400.0
        val JDGhurubS2 : Double = falakLib.JDGhurubSyams(JDNewMoon2 + TmbhHr ,gLat,gLon,gAlt,TmZn)
        val ARCL: Double = falakLib.ElongasiToposentris(JDGhurubS, gLat, gLon, gAlt, TmZn)
        val IRPersis: Double = if(THilal5 >= 3.0 && (ARCL).round(2).toDouble() >= 6.35) {0.0} else {1.0}
        val IR_Persis : String = if(IRPersis == 0.0) {falakLib.JDKM(JDNewMoon+1,TmZn)} else {falakLib.JDKM(JDNewMoon+2,TmZn)}

        val result = IR_Persis.replace("M","").trim()

        val formatter = DateTimeFormatter.ofPattern("d-M-yyyy", Locale.ENGLISH)
        val local = LocalDate.parse(result, formatter)
        val date = Date.from(local.atStartOfDay(ZoneId.systemDefault()).toInstant())
        return date
    }
        override fun getIjtimak(params: Pair<Int, Long>): Pair<String, String> {
            val gLat   : Double   = ( 5+  54/60.0 + 0/3600.0)
            val gLon   : Double   = (95 + 21/60.0 + 0/3600.0)
            val gAlt   : Double   = 10.0
            val TmZn   : Double   = 7.00
            val TmbhHr : Byte     = 1

            val BlnH = params.first.toByte()
            val ThnH = params.second


            val JDNewMoon =  falakLib.GeocentricConjunction(BlnH,ThnH) - falakLib.DeltaT(falakLib.MoonPhasesModified(BlnH,ThnH,1)) / 86400.0
            val JDGhurubS : Double = falakLib.JDGhurubSyams(JDNewMoon ,gLat,gLon,gAlt,TmZn)
            val THilal5    : Double = falakLib.TinggiHilal(JDGhurubS,gLat,gLon,gAlt,TmZn,5)
            val ARCL: Double = falakLib.ElongasiToposentris(JDGhurubS, gLat, gLon, gAlt, TmZn)

            val JamD   : Double   = falakLib.JDKM(JDNewMoon,TmZn,"Jam D").toDouble()
            return Pair(falakLib.JDKM(JDNewMoon,TmZn, fullFormat = true),"${falakLib.DHHMS(JamD)} Tinggi Hilal ${THilal5.round(1)}°, Elongasi ${ARCL.round(1)}°")
    }
}

interface LibraryRumusCalendar {
    fun konversiMasehiKeHijri(date : Date = Date()) : Triple<Int,Int,Long>
    fun getAwalBulanHijri(params : Pair<Int,Long>) : Date
    fun getIjtimak(params : Pair<Int,Long>) : Pair<String , String>
}