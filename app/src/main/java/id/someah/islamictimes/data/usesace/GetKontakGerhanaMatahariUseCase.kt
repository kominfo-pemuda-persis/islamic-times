package id.someah.islamictimes.data.usesace

import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.data.KontakGerhanaModel
import id.someah.islamictimes.data.repository.GerhanaRepository
import id.someah.islamictimes.data.repository.library.GerhanaPhase
import id.someah.islamictimes.util.BaseUseCase
import javax.inject.Inject

class GetKontakGerhanaMatahariUseCase @Inject constructor(private val repository: GerhanaRepository) :
    BaseUseCase<Pair<Byte, Long>, DataGerhanaMatahari>() {
    override fun invoke(parameterName: Pair<Byte, Long>): DataGerhanaMatahari {
        return repository.getKontakGerhanaMatahari(parameterName.first,parameterName.second)
    }
}