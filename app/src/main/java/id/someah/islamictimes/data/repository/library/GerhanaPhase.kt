package id.someah.islamictimes.data.repository.library


enum class GerhanaPhase {
    P1 {
        override fun getString(): String = "P1"
        override fun getTitle(): String = "Awal Penumbra"
    },
    U1 {
        override fun getString(): String = "U1"
        override fun getTitle(): String = "Awal Umbra"
    },
    U2 {
        override fun getString(): String = "U2"
        override fun getTitle(): String = "Awal Total"
    },
    Mx {
        override fun getString(): String = "TG"
        override fun getTitle(): String = "Tengah Gerhana"
    },
    U3 {
        override fun getString(): String = "U3"
        override fun getTitle(): String = "Akhir Total"
    },
    U4 {
        override fun getString(): String = "U4"
        override fun getTitle(): String = "Akhir Umbra"
    },
    P4 {
        override fun getString(): String = "P4"
        override fun getTitle(): String = "Akhir Penumbra"
    };

    abstract fun getString() : String
    abstract fun getTitle() : String
}