package id.someah.islamictimes.data

import id.someah.islamictimes.model.ArahKiblat

interface ArahKiblatDao {
    fun dataArahKiblat() : ArahKiblat
}