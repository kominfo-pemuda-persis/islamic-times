package id.someah.islamictimes.data.repository

import id.someah.islamictimes.data.repository.library.LibraryRumusCalendar
import id.someah.islamictimes.services.utils.getMonth
import id.someah.islamictimes.services.utils.getMonthHijri
import java.util.*
import javax.inject.Inject

interface CalendarRepository  {
    fun convertMasehiToHijri(date : Date = Date()) : Triple<Int,Int,Long>
    fun getAwalBulanHijri(params : Pair<Int,Long>) : Date
    fun getIjtimak(params : Pair<Int,Long>) : String
}

class CalendarRepositoryImpl @Inject constructor(
    private val libraryRumusCalendar: LibraryRumusCalendar
) : CalendarRepository {
    override fun convertMasehiToHijri(date: Date): Triple<Int, Int, Long> {
        return libraryRumusCalendar.konversiMasehiKeHijri(date)
    }

    override fun getAwalBulanHijri(params: Pair<Int, Long>): Date {
        return libraryRumusCalendar.getAwalBulanHijri(params = params)
    }

    override fun getIjtimak(params: Pair<Int, Long>): String {
        val result = libraryRumusCalendar.getIjtimak(params = params)
        return "Ijtimak menjelang awal bulan ${params.first.getMonthHijri()} ${params.second}H terjadi pada ${result.first} ${result.second}"
    }

}