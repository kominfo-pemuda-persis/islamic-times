package id.someah.islamictimes.services

import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.PowerManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getSystemService
import id.someah.islamictimes.util.NotificationReceiver

class AlarmServiceReceiver : BroadcastReceiver() {


    val notification = NotificationReceiver()
    @SuppressLint("InvalidWakeLockTag")
    override fun onReceive(context: Context?, intent: Intent?) {
        val powerManager = context?.getSystemService(Context.POWER_SERVICE) as PowerManager
        val wl : PowerManager.WakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"")

        wl.acquire(10*60*1000L /*10 minutes*/)

        notification.setNotificationWaktuSholatShubuh(context)
        notification.setNotificationWaktuSholatDhuha(context)
        notification.setNotificationWaktuSholatDzuhur(context)
        notification.setNotificationWaktuSholatAshar(context)
        notification.setNotificationWaktuSholatMaghrib(context)
        notification.setNotificationWaktuSholatIsya(context)
        notification.setNotificationWaktuSholatSyuruq(context)
        notification.setNotificationWaktuSholatNisfuLail(context)

        wl.release()
    }

    fun setAlarm(context: Context) {
        log("ALARM IS SETTED")
        val intent = Intent(context , AlarmServiceReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context,0,intent,0)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(), 1000 * 60 * 30 ,pendingIntent)
    }

}