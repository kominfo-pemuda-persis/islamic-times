package id.someah.islamictimes.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.widget.Toast

class AlarmServiceChecker : Service() {

    val alarm = AlarmServiceReceiver()
    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        alarm.setAlarm(context = applicationContext)
        return START_STICKY
    }

    override fun onStart(intent: Intent?, startId: Int) {
        alarm.setAlarm(this)
    }

    override fun onBind(intent: Intent?): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }


}