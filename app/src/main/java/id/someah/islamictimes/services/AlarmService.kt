package id.someah.islamictimes.services

import android.annotation.SuppressLint
import android.app.*
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.os.SystemClock
import id.someah.islamictimes.R
import id.someah.islamictimes.data.Actions
import id.someah.islamictimes.ui.activity.MainActivity
import id.someah.islamictimes.util.ID_NOTIFICATION_SHOLAT_DZUHUR
import id.someah.islamictimes.util.NotificationReceiver
import id.someah.islamictimes.util.TYPE_SHOLAT_DZUHUR
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Exception

class AlarmService  : Service() {

    private var wakeLock : PowerManager.WakeLock? = null
    private var isServiceStarted = false

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        log("this service has been created")
        startService()
    }

    override fun onDestroy() {
        super.onDestroy()
        log("the service has been destroyer")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if(intent != null) {
            val action = intent.action
            when (action) {
                Actions.START -> startService()
                Actions.END -> stopService()
                else -> log("This should never happen. No Action in the received intent")
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }

    @SuppressLint("WakelockTimeout")
    private fun startService() {
        if(isServiceStarted) return
        isServiceStarted = true

        setServiceState(ServicesState.STARTED)

        wakeLock = (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
            newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"EndlessService::lock").apply {
                acquire()
            }
        }

        GlobalScope.launch(Dispatchers.IO) {
            while (isServiceStarted) {
                launch(Dispatchers.IO) {
                    checkAlarm()
                }
                delay(1 * 60 * 1000)
            }
        }
    }

    private fun stopService() {
        try {
            wakeLock?.let {
                if(it.isHeld) {
                    it.release()
                }
            }
            stopSelf()
        } catch (e : Exception) {

        }

        isServiceStarted  =false
        setServiceState(ServicesState.STOPPED)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        val restartService = Intent(applicationContext,AlarmService::class.java).also {
            it.setPackage(packageName)
        }

        val restartServicePendingIntent : PendingIntent = PendingIntent.getService(this,1,restartService,PendingIntent.FLAG_ONE_SHOT)
        applicationContext.getSystemService(Context.ALARM_SERVICE)

        val alarmService : AlarmManager = applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmService.set(AlarmManager.ELAPSED_REALTIME,SystemClock.elapsedRealtime() + 1000 , restartServicePendingIntent)

    }

    private fun createNotification() : Notification {
        val notificationChannelId = "ENDLESS SERVICE CHANNEL"

        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager;
            val channel = NotificationChannel(
                notificationChannelId,
                "Endless Service notifications channel",
                NotificationManager.IMPORTANCE_LOW
            ).let {
                it.description = "Endless Service channel"
                it.enableLights(true)
                it.lightColor = Color.RED
                it.enableVibration(true)
                it.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                it
            }
            notificationManager.createNotificationChannel(channel)
        }

        val pendingIntent: PendingIntent = Intent(this, MainActivity::class.java).let { notificationIntent ->
            PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }

        val builder: Notification.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) Notification.Builder(
            this,
            notificationChannelId
        ) else Notification.Builder(this)

        return builder
            .setContentTitle("Endless Service")
            .setContentText("This is your favorite endless service working")
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setTicker("Ticker text")
            .setPriority(Notification.PRIORITY_LOW) // for under android 26 compatibility
            .build()
    }



    private fun checkAlarm() {
        val alarmUp = PendingIntent.getBroadcast(
            applicationContext,
            ID_NOTIFICATION_SHOLAT_DZUHUR, Intent(TYPE_SHOLAT_DZUHUR), PendingIntent.FLAG_NO_CREATE
        ) != null

        if(alarmUp) {
            log("Alarm Active")
        } else {
            NotificationReceiver().setNotificationWaktuSholatDzuhur(applicationContext)
            log("Alarm NonActive")
        }
    }


}