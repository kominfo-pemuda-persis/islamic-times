package id.someah.islamictimes.services

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.provider.Settings
import id.someah.islamictimes.R
import id.someah.islamictimes.util.TYPE_SHOLAT_DHUHA
import id.someah.islamictimes.util.TYPE_SHOLAT_NISFU_LAIL
import id.someah.islamictimes.util.TYPE_SHOLAT_SYURUQ
import java.lang.Exception

/*
class service buat play audio adzan di background ketika mode notifikasi nya mode suara. menggunakan built-in class MediaPlayer
*/
class BackgroundSound : Service() {
    private var mediaPlayer1: MediaPlayer? = null
    private var mediaPlayer2: MediaPlayer? = null
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
    }
//
//    override fun onCreate() {
//        return
////        mediaPlayer2 = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI)
////        mediaPlayer1!!.isLooping = false
////        mediaPlayer2!!.isLooping = false
//    }

    override fun onDestroy() {
        if(mediaPlayer1!!.isPlaying) {
            mediaPlayer1!!.stop()
            mediaPlayer1!!.release()
        }
        if(mediaPlayer2!!.isPlaying) {
            mediaPlayer2!!.stop()
            mediaPlayer2!!.stop()
        }
        log("IS DESTROYED")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        log("STARTING")

        if(mediaPlayer1 == null ) {
            mediaPlayer1 = MediaPlayer.create(this,
                R.raw.adzan_compressed_mp3
            )
            mediaPlayer1!!.isLooping = false
        }

        if(mediaPlayer2 == null ){
            mediaPlayer2 = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI)
            mediaPlayer2!!.isLooping = false

        }

        val typeSholat = intent!!.getStringExtra("type_sholat")
        //pembedaan sound ketika mode notifikasi suara dan notifikasi dialog muncul

        if(typeSholat == TYPE_SHOLAT_SYURUQ || typeSholat == TYPE_SHOLAT_DHUHA || typeSholat == TYPE_SHOLAT_NISFU_LAIL){
            mediaPlayer2!!.start()
        }
        else{

            if(mediaPlayer1 != null) {
                try {
                    mediaPlayer1!!.prepare()
                }catch (e : Exception) {
                    e.printStackTrace()
                }

                if(!mediaPlayer1!!.isPlaying) {
                    mediaPlayer1!!.start()
                }
            }

        }
        return super.onStartCommand(intent, flags, startId)
//        return super.onStartidmand(intent, flags, startId)
//        return START_STICKY
    }
}