package id.someah.islamictimes.services.work

import android.content.Context
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import id.someah.islamictimes.util.NotificationReceiver
import java.util.*
import java.util.concurrent.TimeUnit

class DailyWorker(private val context: Context, params: WorkerParameters) : Worker(context, params) {

    override fun doWork(): Result {

        //set pray time alarms
        val notification = NotificationReceiver()
        notification.setNotificationWaktuSholatShubuh(context)
        notification.setNotificationWaktuSholatDhuha(context)
        notification.setNotificationWaktuSholatDzuhur(context)
        notification.setNotificationWaktuSholatAshar(context)
        notification.setNotificationWaktuSholatMaghrib(context)
        notification.setNotificationWaktuSholatIsya(context)
        notification.setNotificationWaktuSholatSyuruq(context)
        notification.setNotificationWaktuSholatNisfuLail(context)


        return Result.success()
    }

    companion object {
        const val TAG_OUTPUT = "updateTime"
    }
}