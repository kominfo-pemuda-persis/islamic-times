package id.someah.islamictimes.services

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationManagerCompat
import id.someah.islamictimes.model.ActionNotification
import id.someah.islamictimes.util.*

class AlarmReceiver  : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val getIntent = intent?.extras

        if(getIntent?.getString("action") == ActionNotification.STOP.text()) {
            val services =  Intent(context, AdzanSoundService::class.java)
            services.putExtra("type_sholat", getIntent.getString("title"))
            context?.stopService(services)

            if (context != null) {
                NotificationManagerCompat.from(context).cancelAll()
            }
        }
    }


}