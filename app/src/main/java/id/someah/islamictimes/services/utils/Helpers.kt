package id.someah.islamictimes.services.utils

import android.app.Activity
import android.app.ActivityManager
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import id.someah.islamictimes.R
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.model.HijriModel
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.ui.dialog.AdzanDialog
import id.someah.islamictimes.widget.DaftarJadwalSholatWidget
import id.someah.islamictimes.widget.JadwalSholatWidget
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.YearMonth
import java.util.*
import kotlin.collections.ArrayList
import kotlin.reflect.KClass


fun isLetters(string: String): Boolean {
    return string.filter { it in 'A'..'Z' || it in 'a'..'z' }.length == string.length
}

fun validateDate(string: String): Boolean {
    if (string.isNotEmpty()) {
        try {
            var isFormated = false

            val date = string.toString().split('/')
            if (date.size != 3) {
            } else {

                loop@ date.forEachIndexed { index, s ->

                    if (!isLetters(s) && index <= 1) {
                        isFormated = if (index != (date.size - 1)) {
                            s.length == 2
                        } else {
                            s.length == 4
                        }
                    }

                    if (index == 2) {
                        isFormated = if (s.length <= 5) {
                            if (s.contains('-')) {
                                var split = s.split('-')
                                if (split.last().isNotEmpty()) {
                                    !isLetters(split.last()) && split.last().length == 4
                                } else {
                                    false
                                }
                            } else {
                                !isLetters(s) && s.length == 4
                            }
                        } else {
                            false
                        }

                    }

                    if (!isFormated) {
                        return@forEachIndexed
                    }

                }

                if (isFormated) {
                    val convertDateToObject = YearMonth.of(date[2].toInt(), date[1].toInt())
                    val totalsDayInMonth = convertDateToObject.lengthOfMonth()

                    if (date[0].toInt() > totalsDayInMonth) {
                        isFormated = false
                    }
                }


                return isFormated
            }
        } catch (e: Exception) {
            return false
        }
    }
    return false
}

fun desimalFormat(params: Any): String {
    val decimalFormat = DecimalFormat("#,###.##")
    if (params is Double) {
        return decimalFormat.format(params)
    }
    return "0.0"
}


fun convertMasehiToHijriUrfi(tglM: String, blnM: String, thnM: String): String {
    val calendar: Calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
    val timezoneOffset = SimpleDateFormat("Z", Locale.getDefault()).format(calendar.time)

    val tzn: Double = timezoneOffset.toDouble() / 100

    Log.d("contentValues", "convertMasehiToHijriUrfi: $tzn")

    val falakLib = FalakLib()
    var TGLH: Byte
    var BLNH: Byte
    var THNH: Long
    var CJDN: Long
    var JDNow: Double

    // 1.1 Konversi Kalender Miladi ke JD
    JDNow = falakLib.KMJD(tglM.toByte(), blnM.toByte(), thnM.toLong(), 0.0, tzn)
    // 1.2 Konversi JD ke CJDN
    CJDN = Math.floor(JDNow + 0.5 + tzn / 24).toLong()
    // 1.3 Konversi CJDN ke Kalender Hijri
    TGLH = falakLib.CJDNKH(CJDN, OptResult = "TglH").toString().toByte()
    BLNH = falakLib.CJDNKH(CJDN, OptResult = "BlnH").toString().toByte()
    THNH = falakLib.CJDNKH(CJDN, OptResult = "ThnH").toString().toLong()
    println("$TGLH / $BLNH / $THNH H")

    return "$TGLH / $BLNH / $THNH H"
}

fun Activity.hideKeyboard() {
    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = this.currentFocus

    if (view != null) {
        view = View(this)
    }

    imm.hideSoftInputFromWindow(view?.windowToken, 0)
}

fun View.hideView() {
    this.visibility = View.GONE
}

fun View.showView() {
    this.visibility = View.VISIBLE
}

fun Double.decimalToSexagesimal(): String {
    val degrees = this.toInt()
    val minutes: Double
    val seconds: Double
    if (this <= 0) {
        minutes = kotlin.math.floor(60.0 * (degrees - this))
        seconds = 3600.0 * (degrees - this) - (60.0 * minutes)
    } else {
        minutes = kotlin.math.floor(60.0 * (this - degrees))
        seconds = 3600.0 * (this - degrees) - (60.0 * minutes)
    }

    return "${degrees}° ${minutes.toInt()}′ ${seconds.toInt()}″"
}

fun String.convertToParameters(): Triple<String, String, String> {
    val splits = this.split(':')
    return Triple(
        splits.first(),
        splits[1],
        splits.last()
    )
}

fun String.convertToSexadesimal(): String {
    if (this.contains("+")) {
        this.replace("+", "")
    }

    val degree = this.substringBefore("°")
    val ida = this.substringAfter("°").substringBefore("’")

    return "${degree.replace("+", "")}.${ida.trim()}"
}

fun String.convertIntoOneNumbersBehindComma(): String {
    val degree = this.substringBefore(".")
    val decimal = this.substringAfter(".").toCharArray()

    return "$degree.${decimal.first()}${decimal[1]}°"
}

fun String.currentAzimuthConvertIntoOneNumbersBehindComma(): String {
    val degree = this.substringBefore(".")
    val comma = this.substringAfter(".").toCharArray()

    if (comma.isEmpty()) return ""

    return "${degree}.${comma.first()}°"
}

fun String.currentAzimuthConvertIntoTwoNumbersBehindComma(): String {
    val degree = this.substringBefore(".")
    val comma = this.substringAfter(".").toCharArray()

    if (comma.isEmpty()) return ""

    var decimal = if (comma.size > 1) {
        comma.get(1).toString()
    } else {
        "0"
    }

    return "${degree}.${comma.first()}${decimal}°"
}

fun String.deleteNewLine(): String {
    return this.replace("\\s".toRegex(), "")
}

fun Mod(x: Double, y: Double) = x - y * Math.floor(x / y)

fun Context.showAlert() {
    val dialog = AdzanDialog()
    dialog.showsDialog = true
}

fun Activity.showSnackbar(message: String) {
    val snackbar = Snackbar.make(
        this.window.decorView.rootView,
        message,
        Snackbar.LENGTH_INDEFINITE
    )

    snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.snackbar_color))
    snackbar.setTextColor(ContextCompat.getColor(this, R.color.snackbar_text_color))
    snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.snackbar_text_color))
    snackbar.setAction("Close") {
        snackbar.dismiss()
    }.show()
}

fun createAnimation(fromDegrees: Float, toDegress: Float): RotateAnimation {
    val an = RotateAnimation(
        fromDegrees, toDegress,
        Animation.RELATIVE_TO_SELF, 0.5F, Animation.RELATIVE_TO_SELF,
        0.5f
    )
    an.duration = 500
    an.repeatCount = 0
    an.fillAfter = true
    return an
}

fun Long.toTwoDigits(): String {
    if (this.toString().length == 1) {
        return "0$this"
    }
    return "$this"
}

fun showMaterialAlert(
    context: Context,
    showAlertDialog: MaterialAlertDialogBuilder.() -> Any
): AlertDialog? {
    val dialogBuilder = MaterialAlertDialogBuilder(context, R.style.CustomAlertDialogTheme)
    dialogBuilder.background =
        context.let { ContextCompat.getDrawable(it, R.drawable.bg_dialog_fragment) }
    dialogBuilder.showAlertDialog()
    dialogBuilder.create()
    return dialogBuilder.show()
}

fun MaterialAlertDialogBuilder.positiveButton(
    name: String = "Ok",
    clickListener: (which: Int) -> Any = {}
) {
    this.setPositiveButton(name) { _, which -> clickListener(which) }
}

fun DDDMS(
    DDeg: Double,
    OptResult: String = "DDMMSS",
    SDP: Byte = 2
): String {

    var uDDeg: Double
    var uDeg: Double
    var uDMin: Double
    var uMin: Double
    var uDSec: Double
    var uSec: String
    var sDeg: String
    var sMin: String
    var sSec: String
    var PNS: String

    uDDeg = Math.abs(DDeg)
    uDeg = Math.floor(uDDeg)
    uDMin = (uDDeg - uDeg) * 60.0
    uMin = Math.floor(uDMin)
    uDSec = (uDMin - uMin) * 60.0
    uSec = "%.${SDP}f".format(uDSec)

    if (uSec.replace(",", ".").toDouble() == 60.0) {
        uSec = "%.${SDP}f".format(0.0)
        uMin = uMin + 1.0
    } else {
        uSec = uSec
        uMin = uMin
    }

    if (uMin.toDouble() == 60.0) {
        uMin = 0.0
        uDeg = uDeg + 1.0
    } else {
        uMin = uMin
        uDeg = uDeg
    }

    sDeg = when {
        uDeg.toInt() < 10 -> "00${uDeg.toInt().toString()}"
        uDeg.toInt() < 100 -> "0${uDeg.toInt().toString()}"
        else -> "${uDeg.toInt().toString()}"
    }

    sMin = when {
        uMin.toInt() < 10 -> "0${uMin.toInt().toString()}"
        else -> "${uMin.toInt().toString()}"
    }

    sSec = when {
        uSec.replace(",", ".").toDouble() < 10.0 -> "0${uSec}"
        else -> "${uSec}"
    }

    PNS = when {
        DDeg > 0.0 -> "+"
        DDeg < 0.0 -> "-"
        else -> ""
    }
    return when (OptResult) {
        "DDMMSS" -> "$PNS${sDeg}° ${sMin}’ ${sSec}”"
        "MMSS" -> "$PNS${sMin}’ ${sSec}”"
        "SS" -> "$PNS${sSec}”"
        else -> "$PNS${sDeg}° ${sMin}’ ${sSec}”"
    }
}

fun Double.getTwoDigitBehindComma(): Double {
    Log.d("convertCUK", "getTwoDigitBehindComma: ${String.format("%.3f", this).toDouble()}")
    return 0.0
}

fun RoundTo(xDec: Double, Place: Int = 2): String {
    val A: Double = Math.pow(10.0, Place.toDouble())
    val H: Double = if (xDec >= 0) {
        Math.floor(xDec * A + 0.5) / A
    } else {
        -Math.floor(Math.abs(xDec) * A + 0.5) / A
    }
    return H.toString()
}

fun String.convertDateIfMoreThan24Hours(): String {
    val date = this.split(":").firstOrNull()
    if (date != null) {
        val dateInt = date.toInt()
        if (dateInt >= 24) {
            val converted = (dateInt - 24).toString()
            return if (converted.length > 1) {
                "$converted:${this.split(":").last()}"
            } else {
                "0${converted}:${this.split(":").last()}"
            }
        }
    }
    return this
}

fun updateWidget(context: Context, default: Int = 0) {
    val widgetClass = if (default != 0) {
        DaftarJadwalSholatWidget::class.java
    } else {
        JadwalSholatWidget::class.java
    }
    var intent = Intent(context, widgetClass)

    intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
    var ids: IntArray? =
        AppWidgetManager.getInstance(context).getAppWidgetIds(ComponentName(context, widgetClass))
    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
    context.sendBroadcast(intent)
}

fun convertDDDMStoDD(degree: String, minute: String, second: String, direction: String): Double {
    val degreeDouble = if (degree.isNotEmpty()) degree.toDouble() else 0.0
    val minuteDouble = if (minute.isNotEmpty()) minute.toDouble() else 0.0
    val secondDouble = if (second.isNotEmpty()) second.toDouble() else 0.0

    var isNegative = 0
    if (direction == "S" || direction == "W") {
        isNegative = -1
    }

    var result: Double = (degreeDouble + minuteDouble / 60) + (secondDouble / 3600)
    result = RoundTo(result, 6).toDouble()

    if (isNegative != 0) {
        result *= -1
    }

    return result
}

fun String.getMonth(): Int {
    return if (this.isNotEmpty()) {
        when (this.toLowerCase()) {
            "januari" -> 1
            "februari" -> 2
            "maret" -> 3
            "april" -> 4
            "mei" -> 5
            "juni" -> 6
            "juli" -> 7
            "agustus" -> 8
            "september" -> 9
            "oktober" -> 10
            "november" -> 11
            else -> 12
        }
    } else 0
}

fun Int.getMonthTitle() : String {
    return when(this) {
         1 -> "januari"
         2 -> "februari"
         3 -> "maret"
        4  -> "april"
        5 -> "mei"
        6 -> "juni"
        7 -> "juli"
        8 -> "agustus"
        9 -> "september"
        10 -> "oktober"
        11 -> "november"
        else -> "Desember"
    }
}

fun String.convertGMTtoDouble() : Double {
    return if(this.isNotEmpty()) {

        try {
            val number : String  = this.subSequence(4,8).toString()
            val replaceNumber = number.replace(':','.',true)
            return replaceNumber.toDouble()
        } catch (e : Exception) {
            return 7.0
        }

    } else {
        7.0
    }
}

fun String.convertFormatToDateTime() : String {
    val separator = this.split(":")
    if(separator.size == 1) {
        if(!this.contains("-")) {
            if(this.length == 1 ) {
                return "0${this}:00:00"
            }
            return "$this:00:00"
        }
        return this
    } else if(separator.size == 2) {
        if(this.length == 3 ) {
            return "0${this}:00:00"
        }
        return "$this:00"
    } else {
        return this
    }
}

fun String.timeWithoutDecimal() : String {
    val split = this.split(":")
    var result = ""

    if(split.size > 0) {
        split.forEachIndexed { index, s ->
            val findComma = s.split(".")
            if(findComma.size > 0) {
                result += "${findComma.first()}"
            } else {
                result += s
            }

            if(index != (split.size - 1)) {
                result += ":"
            }
        }

        return result
    }

    return this
}

fun Date.convertDateFormat() : Triple<String,String,String> {
    val sdf = SimpleDateFormat("dd/M/yyyy")
    val currentDate = sdf.format(this).split("/")

    val days = currentDate.first()
    val month = currentDate[1]
    val years = currentDate.last()

    return  Triple(days,month,years)
}

fun Date.getDays() : String {
    val sdf = SimpleDateFormat("EEEE")
    return sdf.format(this).toLowerCase()
}

fun Date.addDays(days : Int) : Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(Calendar.DATE,days)
    return calendar.time
}

fun Date.minusDays(days : Int) : Date {
    val calendar = Calendar.getInstance()
    calendar.time = this
    calendar.add(Calendar.DATE,days)
    return calendar.time
}

fun String.alphabetNumberToArabic():String {
    var result = ""
    var en = '۰'
    for (ch in this) {
        en = ch
        when (ch) {
            '0' -> en = '۰'
            '1' -> en = '١'
            '2' -> en = '٢'
            '3' -> en =  '۳'
            '4' -> en = '٤'
            '5' -> en =  '۵'
            '6' -> en = '٦'
            '7' -> en = '۷'
            '8' -> en = '۸'
            '9' -> en = '۹'
        }
        result += en
    }
    return result
}

fun Int.getMonth() : String {
    return Constant.BULAN.single { it.second == this }.first
}

fun Int.getMonthHijri() : String {
    return enumValues<HijriModel>().find { it.getMonth() == this }?.getString().toString()
}



