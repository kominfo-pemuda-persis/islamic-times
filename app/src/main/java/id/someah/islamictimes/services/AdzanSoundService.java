package id.someah.islamictimes.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.res.ResourcesCompat;

import java.util.Timer;
import java.util.TimerTask;

import id.someah.islamictimes.BuildConfig;
import id.someah.islamictimes.R;
import id.someah.islamictimes.services.utils.HelpersKt;
import id.someah.islamictimes.widget.JadwalSholatWidget;

public class AdzanSoundService extends Service {

    final String TYPE_SHOLAT_DZUHUR = "Sudah Masuk Waktu Sholat Zuhur";
    final String TYPE_SHOLAT_ASHAR = "Sudah Masuk Waktu Sholat Asar";
    final String TYPE_SHOLAT_MAGHRIB = "Sudah Masuk Waktu Sholat Magrib";
    final String TYPE_SHOLAT_ISYA = "Sudah Masuk Waktu Sholat Isya";
    final String TYPE_SHOLAT_NISFU_LAIL = "Sudah Masuk Waktu Sholat Nisfu Lail";
    final String TYPE_SHOLAT_SHUBUH = "Sudah Masuk Waktu Sholat Subuh";
    final String TYPE_SHOLAT_SYURUQ = "Sudah Masuk Waktu Sholat Syuruq";
    final String TYPE_SHOLAT_DHUHA = "Sudah Masuk Waktu Sholat Dhuha";

    MediaPlayer mp;
    MediaPlayer mp2;

    final Handler handler = new Handler();
    final int delay = 1000; // 1000 milliseconds == 1 second



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        startForeground();

        mp = MediaPlayer.create(this, R.raw.adzan_compressed_mp3 );
        mp.setLooping(false);

        mp2 = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
        mp2.setLooping(false);

        checkingDuration();
    }

    @Override
    public void onDestroy() {
        if(mp.isPlaying()) {
            mp.stop();
        }
        if(mp2.isPlaying()) {
            mp2.stop();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String typeSholat = intent.getStringExtra("type_sholat");
        if(typeSholat != null) {
            if(typeSholat.equals(TYPE_SHOLAT_SYURUQ) || typeSholat.equals(TYPE_SHOLAT_DHUHA) || typeSholat.equals(TYPE_SHOLAT_NISFU_LAIL)){
                mp2.start();
            } else {
                mp.start();
            }
        }


        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    private void startForeground() {
        NotificationManager service = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelID = "";
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channelID = createNotificationChannel(service);
        }

        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this , channelID);
        Notification notification = notifBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_logo_islamic_times)
                .setPriority(Notification.PRIORITY_MIN)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setContentTitle("Adzan Sedang Berkumandang")
                .setContentText("Yuk kita sholat")
                .build();

        startForeground(101, notification);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager){
        String channelId = "my_service_channelid";
        String channelName = "My Foreground Service";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }


    private void checkingDuration() {
        handler.postDelayed(new Runnable() {
            public void run() {
                if(mp.isPlaying()) {
                    if(mp.getCurrentPosition() == mp.getDuration()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            stopForeground(true);
                        } else {
                            onDestroy();
                        }
                    }
                    handler.postDelayed(this, delay);
                } else {
                    if(mp2.getCurrentPosition() == mp2.getDuration()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            stopForeground(true);
                        } else {
                            onDestroy();
                        }
                    }
                    handler.postDelayed(this, delay);
                }
            }
        }, delay);
    }
}
