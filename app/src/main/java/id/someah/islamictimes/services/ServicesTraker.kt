package id.someah.islamictimes.services

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log


enum class ServicesState {
    STARTED , STOPPED
}

private const val name = "SPYSERVICES_KEY"
private const val key = "SPYSERVICES_STATE"

@SuppressLint("CommitPrefEdits")
fun Context.setServiceState(state : ServicesState) {
    val sharedPref = this.getPreferences()
    sharedPref.edit().let {
        it.putString(key,state.name)
        it.apply()
    }
}

fun Context.getServiceState() : ServicesState {
    val sharedPref = this.getPreferences()
    val value = sharedPref.getString(key,ServicesState.STOPPED.name).toString()
    return ServicesState.valueOf(value)
}

private fun Context.getPreferences() : SharedPreferences {
    return this.getSharedPreferences(name,0)
}

fun log(msg : String) {
    Log.d("SERVICES",msg)
}