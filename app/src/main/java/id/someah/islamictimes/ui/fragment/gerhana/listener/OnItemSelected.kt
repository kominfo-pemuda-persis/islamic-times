package id.someah.islamictimes.ui.fragment.gerhana.listener

import id.someah.islamictimes.ui.fragment.gerhana.PageState

interface OnItemSelected {
    fun onItemSelected(month : Byte , year : Long , state : PageState)
}