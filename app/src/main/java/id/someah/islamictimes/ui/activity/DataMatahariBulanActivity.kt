package id.someah.islamictimes.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import id.someah.islamictimes.R
import id.someah.islamictimes.adapters.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_data_matahari_bulan.*

class DataMatahariBulanActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_matahari_bulan)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //inisialisasi tab layout dan viewpager
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        viewPager.adapter = sectionsPagerAdapter
        tabLayout.setupWithViewPager(viewPager)

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_cetak_white, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_cetak_white){
            startActivity(Intent(this, CetakDataMatahariBulanActivity::class.java))
        }
        if(item.itemId == android.R.id.home){
            finish()
        }
        return true
    }
}