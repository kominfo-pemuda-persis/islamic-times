package id.someah.islamictimes.ui.fragment.calendar

import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.databinding.FragmentCalendarBinding
import id.someah.islamictimes.model.CalendarModel
import id.someah.islamictimes.services.utils.convertDateFormat
import id.someah.islamictimes.services.utils.getMonth
import id.someah.islamictimes.services.utils.hideView
import id.someah.islamictimes.services.utils.showView
import id.someah.islamictimes.util.BaseFragment
import kotlinx.android.synthetic.main.fragment_calendar.*

@AndroidEntryPoint
class CalendarFragment : BaseFragment() {

    lateinit var binding : FragmentCalendarBinding
    lateinit var controller : CalendarHijriController
    lateinit var controllerLibur : CalendarLiburController

    private val viewModel : CalendarViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun initializedViewModel() {
        viewModel.ijtimak.observe(this , Observer {
            binding.description.text = it
        })

        viewModel.loading.observe(this , Observer {

        })

        viewModel.hijri.observe(this, Observer {
                model ->

            if(model.first.getMonth() == 12) {
                hideNext()
            } else {
                showNext()
            }

            if(model.first.getMonth() == 1) {
                hidePrev()
            } else {
                showPrev()
            }

            binding.hijriMonth.text = model.first.getString()
            controller.setData(model.second.dates)
            controllerLibur.setData(model.second.dates)
            var result = ""

            val title = model.second.dates.distinctBy { it.date?.convertDateFormat()?.second }
            title.forEachIndexed { index, date ->
                if(title.size > 2) {
                    if(index != 0) {
                        result += "${date.date?.convertDateFormat()?.second?.toInt()?.getMonth()}-n-${date.date?.convertDateFormat()?.third} "
                    }
                } else {
                    result += "${date.date?.convertDateFormat()?.second?.toInt()?.getMonth()}-n-${date.date?.convertDateFormat()?.third} "
                }
            }
            result = result.trim().replace(" "," - ")
            binding.masehi.text = result.replace("-n-"," ")
        })

        viewModel.listYearHijri.observe(this , Observer {
            val adapter = ArrayAdapter(requireContext(), R.layout.item_spinner_hijri,it)
            adapter.setDropDownViewResource(R.layout.item_spinner_hijri_dropdown)

            hijriYears.adapter = adapter
            hijriYears.setSelection(3)
        })
    }

    override fun inflateView() {
        if(this::binding.isInitialized) {
            binding.toolbar.apply {
                setNavigationOnClickListener {
                    requireActivity().finish()
                }
            }

            binding.hijriYears.apply {
                background.setColorFilter(resources.getColor(R.color.white_20),PorterDuff.Mode.SRC_ATOP)
            }

            binding.hijriYears.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {
                    viewModel.currentYear(position)
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                }
            }

            binding.rvCalendar.apply {
                controller = CalendarHijriController(viewModel)
                controller.spanCount = 7

                val customLayoutManager = GridLayoutManager(requireContext(),7)
                customLayoutManager.spanSizeLookup = controller.spanSizeLookup
                layoutManager = customLayoutManager
                adapter = controller.adapter

            }

            binding.rvLibur.apply {
                controllerLibur = CalendarLiburController()
                layoutManager = LinearLayoutManager(requireContext())
                adapter = controllerLibur.adapter
            }

            binding.btnNext.apply {
                setOnClickListener {
                    viewModel.next()
                }
            }

            binding.btnPrev.apply {
                setOnClickListener {
                    viewModel.prev()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCalendarBinding.inflate(layoutInflater,container,false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    private fun startLoading() {
        binding.loading.showView()
        binding.rvCalendar.hideView()
    }

    private  fun stopLoading() {
        binding.loading.hideView()
        binding.rvCalendar.showView()
    }

    private fun showNext() {
        binding.btnNext.showView()
    }

    private fun hideNext() {
        binding.btnNext.hideView()
    }

    private fun showPrev() {
        binding.btnPrev.showView()
    }

    private fun hidePrev() {
        binding.btnPrev.hideView()
    }

    companion object {

    }


}