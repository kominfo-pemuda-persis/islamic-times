package id.someah.islamictimes.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.work.WorkerParameters
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.AdzanSoundService
import id.someah.islamictimes.services.BackgroundSound
import id.someah.islamictimes.services.work.DailyWorker
import id.someah.islamictimes.services.work.ExecuteWorker
import id.someah.islamictimes.util.Locate
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ExecuteWorker(applicationContext).startWork()

        //setup bottom navigation
        val navController = findNavController(R.id.nav_host_fragment)
        bottomNavigation.setupWithNavController(navController)
    }

}

