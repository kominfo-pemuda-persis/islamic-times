package id.someah.islamictimes.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.someah.islamictimes.R
import id.someah.islamictimes.viewmodel.WaktuSholatViewModel
import id.someah.islamictimes.viewmodel.WaktuSholatViewModelFactory
import id.someah.islamictimes.adapters.IhtiyathAdapter
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.viewmodel.WaktuSholatViewModelDao
import kotlinx.android.synthetic.main.activity_ihtiyat.*
//class buat set data dari IhtiyathAdapter ke activity
class IhtiyatActivity : AppCompatActivity() {
    private lateinit var waktuSholatViewModel: WaktuSholatViewModelDao
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ihtiyat)

        val dao = DaoManager()
        val viewModelFactory =
            WaktuSholatViewModelFactory(
                dao.waktuSholatDao(this)!!
            )
        waktuSholatViewModel = ViewModelProvider(this, viewModelFactory).get(WaktuSholatViewModelDao::class.java)

        val adapter = IhtiyathAdapter(this)
        rv_ihtiyat.hasFixedSize()
        rv_ihtiyat.layoutManager = LinearLayoutManager(this)
        waktuSholatViewModel.dataWaktuSholat.observe(this, Observer {listIhtiyat ->
            adapter.setList(listIhtiyat)
        })
        rv_ihtiyat.adapter = adapter

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
        }
        return true
    }
}