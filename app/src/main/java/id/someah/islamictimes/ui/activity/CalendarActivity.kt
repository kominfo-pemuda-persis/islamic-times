package id.someah.islamictimes.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.databinding.ActivityCalendarBinding

@AndroidEntryPoint
class CalendarActivity : AppCompatActivity() {

    lateinit var binding : ActivityCalendarBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_calendar)
    }
}