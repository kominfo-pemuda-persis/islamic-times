package id.someah.islamictimes.ui.fragment.gerhana.page.bulan

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.databinding.FragmentGerhanaBulanBinding
import id.someah.islamictimes.ui.fragment.gerhana.DaftarGerhanaController
import id.someah.islamictimes.ui.fragment.gerhana.DaftarGerhanaFragment
import id.someah.islamictimes.ui.fragment.gerhana.PageState
import id.someah.islamictimes.ui.fragment.gerhana.listener.OnFilterSelectedListener
import id.someah.islamictimes.ui.fragment.gerhana.listener.OnItemSelected
import id.someah.islamictimes.util.BaseFragment
import id.someah.islamictimes.util.EventObserver

@AndroidEntryPoint
class GerhanaBulanFragment constructor(val parentListener: OnItemSelected) : BaseFragment() , OnFilterSelectedListener {

    private val viewModel: GerhanaBulanViewModel by viewModels()
    lateinit var binding: FragmentGerhanaBulanBinding

    lateinit var controller: DaftarGerhanaController

    lateinit var listener : OnFilterSelectedListener

    override fun initializedViewModel() {

        viewModel.getDataBulan.observe(this , Observer {
            controller.setData(it)
            viewModel.stopLoading()
        })


        viewModel.loading.observe(this , Observer {
            if(it) {
                binding.shimmerGerhanaBulan.visibility = View.VISIBLE
                binding.rvDaftarGerhanaBulan.visibility = View.INVISIBLE
            } else {
                binding.shimmerGerhanaBulan.visibility = View.GONE
                binding.rvDaftarGerhanaBulan.visibility = View.VISIBLE
            }
        })

        viewModel.navigateToDetail.observe(this , EventObserver {
            parentListener.onItemSelected(it.month,it.year , PageState.BULAN)
        })

    }

    override fun inflateView() {
        if (this::binding.isInitialized) {

            listener = this

            binding.shimmerGerhanaBulan.startShimmer()

            binding.rvDaftarGerhanaBulan.apply {

                controller = DaftarGerhanaController(viewModel)
                adapter = controller.adapter
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGerhanaBulanBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }


    override fun onResume() {
        super.onResume()
        binding.shimmerGerhanaBulan.startShimmer()
        viewModel.reinit()
    }

    override fun onPause() {
        super.onPause()
        binding.shimmerGerhanaBulan.stopShimmer()
    }

    override fun onSelected(month: Byte, year: Long) {
        viewModel.mHijriYear.value = year
        viewModel.get()
    }

}