package id.someah.islamictimes.ui.fragment.calendar

import com.airbnb.epoxy.TypedEpoxyController
import id.someah.islamictimes.ItemCalendarLiburBindingModel_
import id.someah.islamictimes.ItemEmptyCalendarBindingModel_
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.model.CalendarModel
import id.someah.islamictimes.model.HijriModel
import id.someah.islamictimes.services.utils.convertDateFormat
import id.someah.islamictimes.services.utils.getMonth
import id.someah.islamictimes.services.utils.getMonthTitle

class CalendarLiburController : TypedEpoxyController<List<CalendarModel>>() {
    override fun buildModels(data: List<CalendarModel>?) {

        val isPreview = data?.singleOrNull {
            it.isPreview
        }

        if(isPreview == null) {
            Constant.HARI_PENTING.filter {
                it.isRepeatEveryMonth
            }.forEach {
                    hariPenting ->

                val calendar = data?.singleOrNull() { it.fullHijriNumber.first == hariPenting.day }

                if(calendar != null) {

                    var masehi = calendar.date?.convertDateFormat()
                    val monthName = enumValues<HijriModel>().single { it.getMonth() == calendar.fullHijriNumber.second }
                    val description = "${masehi?.first?.toInt()} ${ if(hariPenting.howLong > 1) "- "+masehi?.first?.toInt()?.plus(hariPenting.howLong) else ""} ${masehi?.second?.toInt()?.getMonthTitle()} ${masehi?.third} / ${hariPenting.day} ${ if(hariPenting.howLong > 1) "- "+hariPenting.day.plus(hariPenting.howLong) else ""} ${monthName.getString()} ${calendar.fullHijriNumber.third} H"

                    ItemCalendarLiburBindingModel_()
                        .id(hariPenting.toString())
                        .title(hariPenting.title)
                        .description(description)
                        .addTo(this)
                }
            }

            data?.forEach {
                    calendar ->
                val hariPenting = Constant.HARI_PENTING.singleOrNull {
                    it.month == calendar.fullHijriNumber.second &&
                            it.day == calendar.fullHijriNumber.first
                }

                if(hariPenting != null) {
                    val monthName = enumValues<HijriModel>().single { it.getMonth() == calendar.fullHijriNumber.second }
                    if(!hariPenting.isRepeatEveryMonth) {
                        var masehi = calendar.date?.convertDateFormat()
                        val description = "${masehi?.first?.toInt()} ${ if(hariPenting.howLong > 1) "- "+masehi?.first?.toInt()?.plus(hariPenting.howLong) else ""} ${masehi?.second?.toInt()?.getMonthTitle()} ${masehi?.third} / ${hariPenting.day} ${ if(hariPenting.howLong > 1) "- "+hariPenting.day.plus(hariPenting.howLong) else ""} ${monthName.getString()} ${calendar.fullHijriNumber.third} H"
                        ItemCalendarLiburBindingModel_()
                            .id(calendar.toString())
                            .title(hariPenting.title)
                            .description(description)
                            .addTo(this)
                    }
                }

            }
        } else {
            val filter = Constant.HARI_PENTING.filter {
                (it.day == isPreview.fullHijriNumber.first || it.listDay.singleOrNull { days -> days == isPreview.fullHijriNumber.first } != null ) &&
                        (it.month == 0 || it.month == isPreview.fullHijriNumber.second)
            }
            var masehi = isPreview.date?.convertDateFormat()
            val monthName = enumValues<HijriModel>().single { it.getMonth() == isPreview.fullHijriNumber.second }
            if (filter.isNotEmpty()) {
                filter.forEach {
                    hariPenting ->
                    val description = "${masehi?.first?.toInt()} ${ if(hariPenting.howLong > 1) "- "+masehi?.first?.toInt()?.plus(hariPenting.howLong) else ""} ${masehi?.second?.toInt()?.getMonthTitle()} ${masehi?.third} / ${hariPenting.day} ${ if(hariPenting.howLong > 1) "- "+hariPenting.day.plus(hariPenting.howLong) else ""} ${monthName.getString()} ${isPreview.fullHijriNumber.third} H"
                    ItemCalendarLiburBindingModel_()
                        .id(hariPenting.toString())
                        .title(hariPenting.title)
                        .description(description)
                        .addTo(this)
                }
            } else {
                val description = "${masehi?.first?.toInt()} ${masehi?.second?.toInt()?.getMonthTitle()} ${masehi?.third} / ${isPreview.fullHijriNumber.first}  ${monthName.getString()} ${isPreview.fullHijriNumber.third} H"

                ItemEmptyCalendarBindingModel_()
                    .id("empty")
                    .data(description)
                    .addTo(this)
            }
        }





    }
}
