package id.someah.islamictimes.ui.fragment.gerhana.detail

import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.esri.core.geometry.Line
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.constant.round
import id.someah.islamictimes.data.TransitTerbenamModel
import id.someah.islamictimes.data.repository.library.GerhanaPhase
import id.someah.islamictimes.databinding.FragmentDaftarGerhanaDetailBinding
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.*
import id.someah.islamictimes.ui.fragment.gerhana.DaftarGerhanaViewModel
import id.someah.islamictimes.util.BaseFragment
import kotlinx.android.synthetic.main.fragment_dashboard.*
import javax.inject.Inject

@AndroidEntryPoint
class DaftarGerhanaDetailFragment : BaseFragment() {

    @Inject
    lateinit var sharedPref: SharedPref

    lateinit var binding: FragmentDaftarGerhanaDetailBinding
    private val viewModel: DaftarGerhanaDetailViewModel by viewModels()

    private val args: DaftarGerhanaDetailFragmentArgs by navArgs()

    override fun initializedViewModel() {
        viewModel.data.observe(this, Observer { data ->
            if (data != null) {
                val longText = data.jenisGerhana.split("-")

                binding.title.text = longText.first()

                val formatDate = longText.last().split(",")

                if (formatDate.size > 2) {
                    binding.tanggal.text = "${formatDate.first()} , ${formatDate[1]} "
                } else {
                    binding.tanggal.text = longText.last()
                }


                data.kontakGerhana.forEach {
                    var split = ""
                    var date = "-"
                    if (it.time.isNotEmpty()) {
                        var explode = it.time.split(",")

                        if (explode.size > 1) {
                            date = "${explode.first()},${explode.get(1)}"
                        }

                        split = it.time.split(",").last()

//                        val noMilliseconds = split.split(":")
//                        if(noMilliseconds.size > 1) {
//                            split = "${noMilliseconds.first()}:${noMilliseconds[1]}"
//                        }
                    }
                    when (it.phase) {

                        GerhanaPhase.P1.getTitle() -> {
                            binding.penumbra.text = "${it.phase} (P1)"
//                            binding.penum braDate.text = "$date"
                            binding.penumbraJam.text = "${split.convertFormatToDateTime().timeWithoutDecimal()}"
                            binding.penumbraAlt.text = "${it.hM}"
                            binding.penumbraAzm.text = "${it.azm}"
                        }

                        GerhanaPhase.U1.getTitle() -> {
                            binding.umbra.text = "${it.phase} (U1)"
//                            binding.umbraDate.text = "$date"
                            binding.umbraJam.text = "${split.convertFormatToDateTime().timeWithoutDecimal()}"
                            binding.umbraAlt.text = "${it.hM}"
                            binding.umbraAzm.text = "${it.azm}"
                        }

                        GerhanaPhase.U2.getTitle() -> {
                            binding.awalMax.text = "${it.phase} (U2)"
//                            binding.awalMaxDate.text = "$date"
                            binding.awalMaxJam.text = "${split.convertFormatToDateTime().timeWithoutDecimal()}"
                            binding.awalMaxAlt.text = "${it.hM}"
                            binding.awalMaxAzm.text = "${it.azm}"
                        }

                        GerhanaPhase.Mx.getTitle() -> {
                            binding.max.text = "${it.phase} (Max)"
//                            binding.maxDate.text = "$date"
                            binding.maxJam.text = "${split.convertFormatToDateTime().timeWithoutDecimal()}"
                            binding.maxAlt.text = "${it.hM}"
                            binding.maxAzm.text = "${it.azm}"
                        }

                        GerhanaPhase.U3.getTitle() -> {
                            binding.akhirMax.text = "${it.phase} (U3)"
//                            binding.akhirMaxDate.text = "$date"
                            binding.akhirMaxJam.text = "${split.convertFormatToDateTime().timeWithoutDecimal()}"
                            binding.akhirMaxAlt.text = "${it.hM}"
                            binding.akhirMaxAzm.text = "${it.azm}"
                        }

                        GerhanaPhase.U4.getTitle() -> {
                            binding.akhirUmbra.text = "${it.phase} (U4)"
//                            binding.akhirUmbraDate.text = "$date"
                            binding.akhirUmbraJam.text = "${split.convertFormatToDateTime().timeWithoutDecimal()}"
                            binding.akhirUmbraAlt.text = "${it.hM}"
                            binding.akhirUmbraAzm.text = "${it.azm}"
                        }

                        GerhanaPhase.P4.getTitle() -> {
                            binding.akhirPenumbra.text = "${it.phase} (P4)"
//                            binding.akhirPenumbraDate.text = "$date"
                            binding.akhirPenumbraJam.text = "${split.convertFormatToDateTime().timeWithoutDecimal()}"
                            binding.akhirPenumbraAlt.text = "${it.hM}"
                            binding.akhirPenumbraAzm.text = "${it.azm}"
                        }


                    }
                }

                val dataTerkait = data.dataTerkait
                binding.magintudePenumbra.text = "${dataTerkait.magnitudePenumbra}"
                binding.magitudeUmbra.text = "${dataTerkait.magnitudeUmbra}"
                binding.durasiPenumbra.text = "${dataTerkait.durasiPenumbra}"
                binding.durasiUmbra.text = "${dataTerkait.durasiUmbra}"
                binding.durasiTotal.text = "${dataTerkait.durasiTotal}"

                binding.radiusPenumbra.text = dataTerkait.radiusPPenumbra
                binding.radiusUmbra.text = dataTerkait.radiusUmbra


                data.transitTerbenam.sortedBy { it.tgl  }.forEach { transit ->
                    transit.addToChildView()
                }


                if(data.transitTerbenam.size > 0) {
                    val sorted = data.transitTerbenam.sortedBy { it.tgl }

                    val itemFirst = sorted.first().tgl.split("/")
                    val firstDay =  itemFirst.first().toInt()
                    val monthDay = itemFirst.get(1).toInt().getMonthTitle()

                    val itemSecond = sorted.last().tgl.split("/")
                    val secondDay = itemSecond.first().toInt()
                    val secondMonth = itemSecond.get(1).toInt().getMonthTitle()


                    binding.keterangan.text = "Gerhana Dimulai ${firstDay} ${monthDay} dan \nberakhir ${secondDay} $secondMonth"
                }
//                binding.transit.text = "${data.transitTerbenam.second}"
//                binding.terbenam.text = "${data.transitTerbenam.third}"

//                binding.magintudePenumbra.text = "${dataTerkait.magnitudePenumbra}"
//                binding.magitudeUmbra.text = "${dataTerkait.magnitudeUmbra}"
//                binding.durasiPenumbra.text = "${dataTerkait.durasiPenumbra}"
//                binding.durasiUmbra.text = "${dataTerkait.durasiUmbra}"
//                binding.durasiTotal.text = "${dataTerkait.durasiTotal}"
//
//
//                binding.terbit.text = "${data.transitTerbenam.first}"
//                binding.transit.text = "${data.transitTerbenam.second}"
//                binding.terbenam.text = "${data.transitTerbenam.third}"
            }

            viewModel.stopLoading()
        })

        viewModel.loading.observe(this, Observer {
            if (it) {
                binding.layoutProgress.visibility = View.VISIBLE
//                binding.shimmerGerhanaBulanDetail.visibility = View.VISIBLE
                binding.data.visibility = View.INVISIBLE
            } else {
//                binding.shimmerGerhanaBulanDetail.visibility = View.GONE
                binding.layoutProgress.visibility = View.GONE
                binding.data.visibility = View.VISIBLE
            }
        })

        viewModel.dataMatahari.observe(this, Observer { matahari ->

            binding.title.text = matahari.jenis
            binding.tanggal.text = matahari.waktu

            binding.penumbra.text = "Awal Umbra (U1)"

            binding.umbra.text = "Awal Total (U2)"
            binding.max.text = " Akhir Total (U3)"

            binding.tv2.text = "Durasi Total"

            if(matahari.jenis.toLowerCase().contains("cincin")) {
                binding.umbra.text = "Awal Cincin (U2)"
                binding.max.text = " Akhir Cincin (U3)"
                binding.tv2.text = "Durasi Cincin"
            }

            binding.awalMax.text = "Tengah Gerhana (Mx)"
            binding.akhirMax.text = "Akhir Umbra ( U4)"

            matahari.altAzm.forEachIndexed {

                    index, data ->
                when (index) {
                    0 -> {
                        binding.penumbraJam.text = data.waktu
                        binding.penumbraAlt.text = "${data.alt}"
                        binding.penumbraAzm.text = "${data.azm}"
                    }

                    1 -> {
                        binding.umbraJam.text = data.waktu
                        binding.umbraAlt.text = "${data.alt}"
                        binding.umbraAzm.text = "${data.azm}"
                    }

                    2 -> {
                        binding.awalMaxJam.text = data.waktu
                        binding.awalMaxAlt.text = "${data.alt}"
                        binding.awalMaxAzm.text = "${data.azm}"
                    }

                    3 -> {
                        binding.maxJam.text = data.waktu
                        binding.maxAlt.text = "${data.alt}"
                        binding.maxAzm.text = "${data.azm}"
                    }

                    4 -> {
                        binding.akhirMaxJam.text = data.waktu
                        binding.akhirMaxAlt.text = "${data.alt}"
                        binding.akhirMaxAzm.text = "${data.azm}"
                    }
                }

            }

            binding.akhirPenumbra.visibility = View.GONE
            binding.akhirPenumbraJam.visibility = View.GONE
            binding.akhirPenumbraAzm.visibility = View.GONE
            binding.akhirPenumbraAlt.visibility = View.GONE

            binding.akhirUmbra.visibility = View.GONE
            binding.akhirUmbraJam.visibility = View.GONE
            binding.akhirUmbraAzm.visibility = View.GONE
            binding.akhirUmbraAlt.visibility = View.GONE



            binding.tv1.text = "Durasi Gerhana"
            binding.tv3.text = "Magnitude Umbra"
            binding.tv4.text = "Obskurasi"

            binding.tv6.hideView()
            binding.tv7.hideView()
            binding.tv8.hideView()
            binding.transit.hideView()

            binding.durasiPenumbra.hideView()
            binding.durasiTotal.hideView()
            binding.durasiUmbra.hideView()


            binding.magintudePenumbra.text = matahari.durasi.first
            binding.magitudeUmbra.text = matahari.durasi.second
            binding.radiusPenumbra.text = matahari.magnitudeObs.first
            binding.radiusUmbra.text = matahari.magnitudeObs.second


//            val u1 = matahari.altAzm.filter {
//                it.contains("U1")
//            }
//
//            binding.penumbraAlt.text = u1.first()
//            binding.penumbraAzm.text = u1.last()
//
//            val mx = matahari.altAzm.filter {
//                it.contains("mx")
//            }
//
//            binding.max.text = "Puncak Gerhana (Mx)"
//            binding.maxAlt.text = mx.first()
//            binding.maxAzm.text = mx.last()

            viewModel.stopLoading()

        })

        viewModel.progress.observe(this, Observer {
            binding.progress.progress = it.second
            binding.percentage.text = it.first

        })

    }

    override fun inflateView() {
        viewModel.hijriMonth.value = args.hijriMonth.toByte()
        viewModel.hijriYear.value = args.hijriYear
        viewModel.state.value = args.state

        viewModel.get()

        binding.shimmerGerhanaBulanDetail.startShimmer()

        binding.toolbar.apply {
            title = "${
                Constant.BULAN_HIJRI.find {
                    it.second == args.hijriMonth
                }?.first
            } , ${args.hijriYear} H"

            setNavigationOnClickListener {
                findNavController().popBackStack()
            }
        }

        if (sharedPref.getValueString(
                "alamat_user",
                ""
            ) == null || sharedPref.getValueString("alamat_user", "") == "0"
        ) {
            binding.lokasi.text = getString(R.string.unlocated)
        } else {
            val gLongT = sharedPref.getValueDouble("longitude_user") ?: 0.0
            val gLatT = sharedPref.getValueDouble("latitude_user") ?: 0.0
            val gAltT = sharedPref.getValueDouble("altitude_user") ?: 0.0

            binding.lokasi.text =
                "${
                    sharedPref.getValueString("alamat_user", "")!!.replace("Kota", "")
                } \n ( ${gLongT.decimalToSexagesimal()} , ${gLatT.decimalToSexagesimal()}, Elev : ${
                    gAltT.round(
                        3
                    )
                } Mdpl ) "
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDaftarGerhanaDetailBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        binding.shimmerGerhanaBulanDetail.stopShimmer()
    }

    private fun TransitTerbenamModel.addToChildView() {
        val child = LinearLayout(requireContext())
        child.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        child.weightSum = 4f

        val layoutTextView = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutTextView.weight = 1f

        child.addView(addTextView(layoutTextView, this.tgl))
        child.addView(addTextView(layoutTextView, this.terbit))
        child.addView(addTextView(layoutTextView, this.transit))
        child.addView(addTextView(layoutTextView, this.terbenam))

        binding.daftarGerhana.addView(child)

    }

    private fun addTextView(params: LinearLayout.LayoutParams, value: String): View {
        val textView = TextView(requireContext())

        textView.layoutParams = params
        textView.setTextAppearance(requireContext(), R.style.CustomCaption)
        textView.text = value
        textView.setTextColor(resources.getColor(R.color.black_80))
        textView.gravity = Gravity.CENTER
        textView.background = resources.getDrawable(R.drawable.cell_shape_only)

        return textView
    }

    companion object {
        val ALT_AZM = listOf(
            "AltU1",
            "AzmU1",
            "AltU2",
            "AzmU2",
            "AltMx",
            "AzmMx",
            "AltU3",
            "AzmU3",
            "AltU4",
            "AzmU4"
        )
    }
}