package id.someah.islamictimes.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.findFragment
import id.someah.islamictimes.R
import kotlinx.android.synthetic.main.fragment_parent_pengaturan.*

class ParentPengaturanFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_parent_pengaturan, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val fragmentChild = PengaturanFragment()
        val transaction  = childFragmentManager.beginTransaction()
        transaction.replace(R.id.content_pengaturan,fragmentChild).commit()
    }

}