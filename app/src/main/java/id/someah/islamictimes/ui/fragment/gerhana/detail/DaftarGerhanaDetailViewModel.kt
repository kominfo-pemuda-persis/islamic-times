package id.someah.islamictimes.ui.fragment.gerhana.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.someah.islamictimes.data.DataGerhana
import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.data.KontakGerhanaModel
import id.someah.islamictimes.data.TransitTerbenamModel
import id.someah.islamictimes.data.repository.library.GerhanaPhase
import id.someah.islamictimes.data.usesace.*
import id.someah.islamictimes.services.log
import id.someah.islamictimes.services.utils.getMonth
import id.someah.islamictimes.ui.fragment.gerhana.PageState
import kotlinx.coroutines.*
import javax.inject.Inject


@HiltViewModel
class DaftarGerhanaDetailViewModel @Inject constructor(
    private val getDataTerkaitGerhana: GetDataTerkaitGerhana,
    private val getKontakGerhanaBulanUseCase: GetKontakGerhanaBulanUseCase,
    private val getJenisDanWaktuGerhana: GetJenisDanWaktuGerhana,
    private val getDurasiGerhanaMatahariUseCase: GetDurasiGerhanaMatahariUseCase,
    private val getMagnitudeObsGerhanaMatahariUseCase: GetMagnitudeObsGerhanaMatahariUseCase,
    private val getAltAzmGerhanaMatahariUseCase: GetAltAzmGerhanaMatahariUseCase,
    private val getKontakGerhanaMatahariUseCase: GetKontakGerhanaMatahariUseCase,
    private val getGerhanaTerbitTransitTerbenamUseCase: GetGerhanaTerbitTransitTerbenamUseCase,
    private val getDetailGerhanaMatahariUseCase: GetDetailGerhanaMatahariUseCase
) : ViewModel() {
    val hijriYear = MutableLiveData<Long>()
    val hijriMonth = MutableLiveData<Byte>()
    val state = MutableLiveData<PageState>()

    private var job = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)

    private val _data = MutableLiveData<DataGerhana>()
    val data: LiveData<DataGerhana> get() = _data

    private val _dataMatahari = MutableLiveData<DataGerhanaMatahari>()
    val dataMatahari: LiveData<DataGerhanaMatahari> get() = _dataMatahari

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> get() = _loading

    private val _progress = MutableLiveData<Pair<String, Int>>()
    val progress: LiveData<Pair<String, Int>> get() = _progress

    init {
        _progress.value = Pair("Memuat", 0)
    }

    fun get() {
        _progress.value = Pair("Memuat", 0)
        _loading.value = true

        val month = hijriMonth.value ?: 0
        val year = hijriYear.value ?: 0
        val currentState = state.value ?: PageState.BULAN

        var kontak = ArrayList<KontakGerhanaModel>()
        var transitTerbenam = ArrayList<TransitTerbenamModel>()

        if (!(month.toInt() == 0 || year.toInt() == 0)) {

            if (currentState == PageState.BULAN) {
                uiScope.launch {
                    delay(1000L)

                    setProgressBar("Menghitung Data Terkait Gerhana Bulan")
                    val dataTekait = getDataTerkait(month, year)

                    setProgressBar("Menghitung Jenis Dan Waktu Gerhana")
                    val jenisDanWaktu = getJenisDanWaktuGerhana(month, year)

                    phase.map {
                        async {
                            kontak.add(
                                getKontakGerhana(it, month, year)
                            )
                        }
                    }.awaitAll()
                    var item = DataGerhana(
                        jenisDanWaktu,
                        "",
                        "",
                        kontak,
                        dataTekait
                    )

                    if (kontak.isNotEmpty()) {
                        kontak.filter { it.date.isNotEmpty() }.distinctBy {
                            it.date
                        }.map { kontak ->
                            async {
                                var time = kontak.time.split("-")
                                if (time.isNotEmpty()) {
                                    var tgl = time.last().split(",")

                                    if (tgl.size > 1) {
                                        var date = tgl[1].split(" ")

                                        var day = date[1]?.toByte() ?: 0
                                        var bln = date[2].getMonth() ?: 0
                                        var tahun = date[3]?.toLong() ?: 0

                                        setProgressBar("Menghitung Terbit , Transit dan Terbenam Gerhana")
                                        if (day != (0).toByte() && bln != 0 && tahun != (0).toLong()) {
                                            var result = getGerhanaTerbitTransitTerbenam(
                                                day,
                                                bln.toByte(),
                                                tahun
                                            )

                                            var tempItemTransit =
                                                TransitTerbenamModel("$day/$bln/$tahun")
                                            tempItemTransit.terbit = result.first
                                            tempItemTransit.transit = result.second
                                            tempItemTransit.terbenam = result.third


                                            transitTerbenam.add(
                                                tempItemTransit
                                            )

                                        }
                                    }

                                }
                            }
                        }.awaitAll()

                        item.transitTerbenam = transitTerbenam
                    }

                    if (jenisDanWaktu.isNotEmpty()) {

                    }
                    _data.value = item
                }.start()
            } else {
                uiScope.launch {

                    setProgressBar("Menghitung Kontak Gerhana", 10)
                    var item = getKontakGerhanaMatahari(month, year)

                    setProgressBar("Menghitung Detail Informasi Gerhana Matahari", 50)
                    val detail = getDetailGerhanaMatahari(month, year)


                    setProgressBar("Menghitung Durasi Gerhana Matahari")
                    item.durasi = detail.durasi

                    setProgressBar("Menghitung Altitude Dan Azimute Gerhana Matahari")
                    item.altAzm = detail.altAzm

                    setProgressBar("Menghitung Maginute Obs Gerhana Matahari")
                    item.magnitudeObs = detail.magnitudeObs


                    _dataMatahari.value = item

                }.start()
            }

        }
    }

    private suspend fun getKontakGerhana(phase: GerhanaPhase, bulanHijri: Byte, tahunHijri: Long) =
        withContext(Dispatchers.IO) {
            getKontakGerhanaBulanUseCase(Triple(phase, bulanHijri, tahunHijri))
        }

    private suspend fun getDataTerkait(bulanHijri: Byte, tahunHijri: Long) =
        withContext(Dispatchers.IO) {
            getDataTerkaitGerhana(Pair(bulanHijri, tahunHijri))
        }

    private suspend fun getJenisDanWaktuGerhana(bulanHijri: Byte, tahunHijri: Long) =
        withContext(Dispatchers.IO) {
            getJenisDanWaktuGerhana(Pair(bulanHijri, tahunHijri))
        }

    private suspend fun getDurasiGerhanaMatahari(bulanHijri: Byte, tahunHijri: Long) =
        withContext(Dispatchers.IO) {
            getDurasiGerhanaMatahariUseCase(Pair(bulanHijri, tahunHijri))
        }

    private suspend fun getMagnitudeObsGerhanaMatahari(bulanHijri: Byte, tahunHijri: Long) =
        withContext(Dispatchers.IO) {
            getMagnitudeObsGerhanaMatahariUseCase(Pair(bulanHijri, tahunHijri))
        }

    private suspend fun getAltAzmGerhanaMatahari(bulanHijri: Byte, tahunHijri: Long) =
        withContext(Dispatchers.IO) {
            getAltAzmGerhanaMatahariUseCase(Pair(bulanHijri, tahunHijri))
        }

    private suspend fun getKontakGerhanaMatahari(bulanHijri: Byte, tahunHijri: Long) =
        withContext(Dispatchers.IO) {
            getKontakGerhanaMatahariUseCase(Pair(bulanHijri, tahunHijri))
        }

    private suspend fun getGerhanaTerbitTransitTerbenam(day: Byte, bln: Byte, thn: Long) =
        withContext(Dispatchers.IO) {
            getGerhanaTerbitTransitTerbenamUseCase(Triple(day, bln, thn))
        }

    private suspend fun getDetailGerhanaMatahari(bln: Byte, thn: Long) =
        withContext(Dispatchers.IO) {
            getDetailGerhanaMatahariUseCase(Pair(bln, thn))
        }


    fun stopLoading() {
        _loading.value = false
    }

    companion object {
        val phase = listOf(
            GerhanaPhase.P1,
            GerhanaPhase.U1,
            GerhanaPhase.U2,
            GerhanaPhase.Mx,
            GerhanaPhase.U3,
            GerhanaPhase.U4,
            GerhanaPhase.P4
        )
    }

    fun setProgressBar(text: String, moveTO: Int = 20) {
        val currentProgress = progress.value ?: Pair("", 0)
        _progress.value = Pair(text, currentProgress.second + moveTO)


    }

}