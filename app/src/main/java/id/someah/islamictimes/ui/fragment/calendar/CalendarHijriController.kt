package id.someah.islamictimes.ui.fragment.calendar

import android.util.Log
import com.airbnb.epoxy.TypedEpoxyController
import id.someah.islamictimes.ItemCalendarBindingModel_
import id.someah.islamictimes.ItemHeaderDayBindingModel_
import id.someah.islamictimes.R
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.model.CalendarModel
import id.someah.islamictimes.services.utils.convertDateFormat
import id.someah.islamictimes.services.utils.getDays
import id.someah.islamictimes.util.EventListener
import java.text.DateFormatSymbols
import java.util.*

class CalendarHijriController (private val eventListener: EventListener<CalendarModel>) : TypedEpoxyController<List<CalendarModel>>() {
    override fun buildModels(data: List<CalendarModel>?) {

        listOf("Ahd","Sen","Sel","Rab","Kam","Jum","Sab").forEachIndexed { index, s ->
            ItemHeaderDayBindingModel_()
                .id(s)
                .title(s)
                .addTo(this)
        }

        val month = data?.distinctBy { it.fullHijriNumber.second }
        var maxSize = 0;
        var numberOfMonth = 0;
        month?.forEach {
            model ->
            if(maxSize == 0) {
                maxSize = data.filter { it.fullHijriNumber.second == model.fullHijriNumber.second }.size
                numberOfMonth = model.fullHijriNumber.second
            } else {
                var currentSize = data.filter { it.fullHijriNumber.second == model.fullHijriNumber.second }.size
                if(currentSize > maxSize) {
                    maxSize = currentSize
                    numberOfMonth = model.fullHijriNumber.second
                }
            }
        }

        data?.forEachIndexed { index, calendarModel ->
            var isActive = false
            var isSelected = false
            var isDayOff = false
            var isFriday = false
            var isEvent = false
            var isImportantIvent = false

            if(calendarModel.date?.convertDateFormat() == Date().convertDateFormat()) {
                isActive = true
            }

            if(calendarModel.fullHijriNumber.second == numberOfMonth) {
                isSelected = true
            }

            if(calendarModel.date?.getDays() == "sunday") {
                isDayOff = true
            }

            if(calendarModel.date?.getDays() == "friday") {
                isFriday = true
            }

            var hariPenting = Constant.HARI_PENTING.singleOrNull {
                ( it.day == calendarModel.fullHijriNumber.first ||
                it.listDay.singleOrNull { day -> day == calendarModel.fullHijriNumber.first } != null )
                        &&
                        it.month == calendarModel.fullHijriNumber.second

            }

            if(hariPenting != null) {
                if(hariPenting.isImportant) {
                    isImportantIvent = true
                }

                if(hariPenting.isEvent) {
                    isEvent = true
                }
            }

            var isOnlyEvent = Constant.HARI_PENTING.singleOrNull { hari ->
                hari.listDay.singleOrNull { it == calendarModel.fullHijriNumber.first } != null &&
                        hari.isRepeatEveryMonth
            }

            if(isOnlyEvent != null) {
                isEvent = true
            }

            var color = 0
            if(isActive) {
                color = android.R.color.white
            } else {
                if(isDayOff) {
                    color = R.color.red_danger
                } else {
                    color = R.color.black_80
                }

                if(isFriday) {
                    color = R.color.colorPrimary
                }

                if(isImportantIvent) {
                    color = R.color.colorPrimary
                }

                if(isEvent) {
                    color = R.color.yellow
                }
            }

            ItemCalendarBindingModel_()
                .isDayOff(isDayOff)
                .eventListener(eventListener)
                .preview(calendarModel.isPreview)
                .isActive(isActive)
                .color(color)
                .isSelected(isSelected)
                .id(calendarModel.toString())
                .data(calendarModel)
                .addTo(this)
        }
    }
}