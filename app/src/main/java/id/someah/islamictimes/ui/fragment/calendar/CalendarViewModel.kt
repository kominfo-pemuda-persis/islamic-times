package id.someah.islamictimes.ui.fragment.calendar

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.someah.islamictimes.data.repository.CalendarRepository
import id.someah.islamictimes.model.CalendarHijri
import id.someah.islamictimes.model.CalendarModel
import id.someah.islamictimes.model.HijriModel
import id.someah.islamictimes.services.log
import id.someah.islamictimes.services.utils.*
import kotlinx.coroutines.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import id.someah.islamictimes.util.EventListener
import java.time.LocalDate
import java.time.temporal.ChronoUnit

@HiltViewModel
class CalendarViewModel @Inject constructor(
    private val calendarRepository: CalendarRepository
) : ViewModel() , EventListener<CalendarModel> {

    private var job = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)

    private val _loading = MutableLiveData<Boolean>()
    val loading : LiveData<Boolean> get() = _loading

    private val _data = MutableLiveData<CalendarHijri>()
    val data : LiveData<CalendarHijri> get() = _data

    val paging  = MediatorLiveData<CalendarHijri>()
    private val _pagination = MutableLiveData<Int>()
    val pagination : LiveData<Int> get() = _pagination

    private val _currentHijriModel = MutableLiveData<HijriModel>()
    val currentHijriModel : LiveData<HijriModel> get() = _currentHijriModel

    val hijri = MediatorLiveData<Pair<HijriModel,CalendarHijri>>()

    private val _listYearHijri = MutableLiveData<List<String>>()
    val listYearHijri : LiveData<List<String>> get() = _listYearHijri

    private val _selectedYearHijri = MutableLiveData<Long>()
    val selectedYearHijri : LiveData<Long> get() = _selectedYearHijri

    private val _currentCalendarModel = MutableLiveData<CalendarModel>()
    val currentCalendarModel : LiveData<CalendarModel> get() = _currentCalendarModel

    private val _ijtimak = MutableLiveData<String>()
    val ijtimak : LiveData<String> get() = _ijtimak


    init {
        _listYearHijri.value = listOf(
            "1441 H",
            "1442 H",
            "1443 H",
            "1444 H",
            "1445 H"
        )

        hijri.addSource(data , Observer { items ->
            val current = currentHijriModel.value
            var now = calendarRepository.convertMasehiToHijri().second

            if(current != null) {
                now = current.getMonth()
            }

            val month = enumValues<HijriModel>().filter {
                it.getMonth() == now
            }.first()

            val datas = items

            hijri.value = Pair(month,datas)
            _loading.value = false
        })

        getCalendar();
    }

    fun next() {
        val current = currentHijriModel.value
        var numberOfNewMonth = 0
        if(current != null) {
            numberOfNewMonth = current.getMonth() + 1
        } else {
            numberOfNewMonth = calendarRepository.convertMasehiToHijri().second
        }

        if(current?.getMonth() == 12) {
            numberOfNewMonth = 1
        }

        _currentHijriModel.value = enumValues<HijriModel>().single {
            it.getMonth() == numberOfNewMonth
        }

        getCalendar();
    }

    fun prev() {
        val current = currentHijriModel.value
        var numberOfNewMonth = 0
        if(current != null) {
            numberOfNewMonth = current.getMonth() - 1
        } else {
            numberOfNewMonth = calendarRepository.convertMasehiToHijri().second
        }

        if(current?.getMonth() == 1) {
            numberOfNewMonth = 12
        }

        _currentHijriModel.value = enumValues<HijriModel>().single {
            it.getMonth() == numberOfNewMonth
        }

        getCalendar();
    }

    fun currentYear(position : Int) {
        val currentList = listYearHijri.value ?: listOf()

        _selectedYearHijri.value = currentList[position].replace("H","").trim().toLong()
        getCalendar()
    }

    fun getCalendar() {
        _loading.value = true;
        uiScope.launch {

            val current = currentHijriModel.value
            var year = selectedYearHijri.value
            val currentCalendarModel = currentCalendarModel.value


            val now = calendarRepository.convertMasehiToHijri()
            var month = now.second

            if(current != null) {
                month = current.getMonth()
            }

            if(year == null) {
                year = now.third
            }

                async {


                    var awalBulan = calendarRepository.getAwalBulanHijri(
                        Pair(
                            month,
                            year
                        )
                    )

                    var beforeBulan = calendarRepository.getAwalBulanHijri(
                        Pair(
                            month - 1,
                            year
                        )
                    )

                    var afterBulan = calendarRepository.getAwalBulanHijri(
                        Pair(
                            month + 1,
                            year
                        )
                    )

                    //Awal Bulan
                    var awalBulanFormatted = awalBulan.convertDateFormat()
                    var awalBulanLocale = LocalDate.of(awalBulanFormatted.third.toInt(),awalBulanFormatted.second.toInt(),awalBulanFormatted.first.toInt())

                    //Before Bulan
                    var beforeBulanFormatted = beforeBulan.convertDateFormat()
                    var beforeBulanLocale = LocalDate.of(beforeBulanFormatted.third.toInt(),beforeBulanFormatted.second.toInt(),beforeBulanFormatted.first.toInt())

                    //After bulan
                    var afterBulanFormatted = afterBulan.convertDateFormat()
                    var afterBulanLocale = LocalDate.of(afterBulanFormatted.third.toInt(),afterBulanFormatted.second.toInt(),afterBulanFormatted.first.toInt())

                    _ijtimak.value = calendarRepository.getIjtimak(Pair(month,year))

                    var startOnSunday = false
                    var beforeSelected  = -1
                    while (!startOnSunday) {
                        if(awalBulan.getDays() == "sunday") {
                            startOnSunday = true
                        } else {
                            beforeSelected++
                            awalBulan = awalBulan.minusDays(-1)
                        }
                    }

                        var models = ArrayList<CalendarModel>()

                        var countDate = awalBulan
                        var selectedMonth = enumValues<HijriModel>().single { it.getMonth() == month }

                        //before month
                        if(beforeSelected != -1) {
                            var beforeMonth = 0
                            var beforeYear = year
                            if(month == 1) {
                                beforeMonth = 12
                                beforeYear = beforeYear - 1
                            } else {
                                beforeMonth = month - 1
                            }

                            var daysBeforeMonth = ChronoUnit.DAYS.between(awalBulanLocale,beforeBulanLocale)
                            print("DAYS BEFORE $beforeMonth")

                            var beforeCurrentMonth = enumValues<HijriModel>().single { it.getMonth() == beforeMonth }
                            var beforeMonthStart =  Math.abs(beforeSelected - Math.abs(daysBeforeMonth.toInt()))
                            Log.d("DAYS BEFORE","$beforeSelected - ${Math.abs(daysBeforeMonth)} = $beforeMonthStart")
                            for( i in 0..beforeSelected) {
                                var isPreview = false

                                val formatedDate = countDate.convertDateFormat()
                                if(currentCalendarModel != null) {
                                    if(countDate.convertDateFormat() == currentCalendarModel.date?.convertDateFormat()) {
                                        isPreview = true
                                    }
                                }

                                models.add(
                                    CalendarModel(
                                        isPreview = isPreview,
                                        masehiNumber = "${formatedDate.first}",
                                        hijriNumber = beforeMonthStart.toString().alphabetNumberToArabic(),
                                        date = countDate,
                                        fullHijriNumber = Triple(beforeMonthStart,beforeCurrentMonth.getMonth(),beforeYear)
                                    )
                                )

                                beforeMonthStart++
                                countDate = countDate.addDays(1)
                            }
                        }

                        //Selected month
                        var start = 1
                    var selectedDays = ChronoUnit.DAYS.between(awalBulanLocale,afterBulanLocale)
                    while (start <= selectedDays.toInt()) {
                        Log.d("LONG","${selectedDays} - ${start}")

                        var isPreview = false

                            val formatedDate = countDate.convertDateFormat()
                            if(currentCalendarModel != null) {
                                if(countDate.convertDateFormat() == currentCalendarModel.date?.convertDateFormat()) {
                                    isPreview = true
                                }
                            }

                                models.add(
                                    CalendarModel(
                                        isPreview = isPreview,
                                        masehiNumber = "${formatedDate.first}",
                                        hijriNumber = start.toString().alphabetNumberToArabic(),
                                        date = countDate,
                                        fullHijriNumber = Triple(start,selectedMonth.getMonth(),year)
                                    )
                                )
                            start++
                            countDate = countDate.addDays(1)
                        }

                        //after month
                        if(month != 12 ) {
                            if(models.size < 35) {
                                var monthAfter = month
                                var yearHijriAfter = year
                                if(month == 12) {
                                    monthAfter = 1
                                    yearHijriAfter++
                                } else {
                                    monthAfter++
                                }

                                var currentMonthAfter = enumValues<HijriModel>().single { it.getMonth() == monthAfter }
                                start = 1
                                while (models.size < 35 ) {
                                    var isPreview = false

                                    val formatedDate = countDate.convertDateFormat()
                                    if(currentCalendarModel != null) {
                                        if(countDate.convertDateFormat() == currentCalendarModel.date?.convertDateFormat()) {
                                            isPreview = true
                                        }
                                    }

                                    models.add(
                                        CalendarModel(
                                            isPreview = isPreview,
                                            masehiNumber = "${formatedDate.first}",
                                            hijriNumber = start.toString().alphabetNumberToArabic(),
                                            date = countDate,
                                            fullHijriNumber = Triple(start,currentMonthAfter.getMonth(),yearHijriAfter)
                                        )
                                    )
                                    start++
                                    countDate = countDate.addDays(1)
                                }
                            }
                        }

                        val hijriName = enumValues<HijriModel>().single { it.getMonth() == month }.getString()
                        _data.value = CalendarHijri(false,hijriName,models.toList())
                }
        }
    }

    override fun openDetail(item: CalendarModel) {
        val current = currentCalendarModel.value
        when {
            current == null -> {
                _currentCalendarModel.value = item
            }
            current.date?.convertDateFormat() != item.date?.convertDateFormat() -> {
                _currentCalendarModel.value = item
            }
            else -> {
                _currentCalendarModel.value = null
            }
        }

        Log.i("CLICK","REFRESH")

        getCalendar()
    }


}