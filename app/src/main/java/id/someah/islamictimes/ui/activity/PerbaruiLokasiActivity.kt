package id.someah.islamictimes.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import id.someah.islamictimes.R
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.model.DurationUpdate
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.UpdateLocationReceiver
import kotlinx.android.synthetic.main.activity_perbarui_lokasi.*

//class buat pengaturan prioritas update
class PerbaruiLokasiActivity : AppCompatActivity(){
    @SuppressLint("SetTextI18n")
    lateinit var sharedPref: SharedPref
    lateinit var currentLocation : Location
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perbarui_lokasi)
        sharedPref =
            SharedPref(this)
        val localData = LocalData(this)

        //check selected prioritas dari shared preferences
        val sharedPrefDurasi = sharedPref.getValueString("durasi_update", "15_menit")
        val sharedPrefPrioritas = sharedPref.getValueString("prioritas_update", "using_gps")
        if (sharedPrefDurasi != null) {
            when (sharedPrefDurasi) {
                "15_menit" -> radioGroupDurasi.check(R.id.radioButton15Menit)
                "20_menit" -> radioGroupDurasi.check(R.id.radioButton20Menit)
                "25_menit" -> radioGroupDurasi.check(R.id.radioButton25Menit)
                "30_menit" -> radioGroupDurasi.check(R.id.radioButton30Menit)
            }
        }
        if(sharedPrefPrioritas != null){
            when (sharedPrefPrioritas) {
                "using_gps" -> radioGroupPrioritas.check(R.id.radioButtonGps)
                "using_wifi" -> radioGroupPrioritas.check(R.id.radioButtonWifi)
            }
        }
        //set teks data
//        txtTimezone.text = ": ${localData.timezoneOffset}"
        txtTimezone.text = ": ${sharedPref.getValueString("timezone_title","")}"
        txtAltitude.text = ": ${sharedPref.getValueDouble("altitude_user")}"
        txtLatitude.text = ": ${sharedPref.getValueDouble("latitude_user")}"
        txtLongitude.text = ": ${sharedPref.getValueDouble("longitude_user")}"
        //save data ke shared preferences ketika ada perubahan pilihan prioritas
        radioGroupPrioritas.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radioButtonGps -> {
                    sharedPref.saveStringPref(
                        "prioritas_update",
                        "using_gps"
                    )

                }
                R.id.radioButtonWifi -> {
                    sharedPref.saveStringPref(
                        "prioritas_update",
                        "using_wifi"
                    )
                }
            }
        }
        radioGroupDurasi.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radioButton15Menit -> {
                    sharedPref.saveStringPref(
                        "durasi_update",
                        "15_menit"
                    )
                    setUpdateLocation("15_menit")
                }
                R.id.radioButton20Menit -> {
                    sharedPref.saveStringPref(
                        "durasi_update",
                        "20_menit"
                    )
                    setUpdateLocation("20_menit")
                }
                R.id.radioButton25Menit -> {
                    sharedPref.saveStringPref(
                        "durasi_update",
                        "25_menit"
                    )
                    setUpdateLocation("30_menit")
                }
                R.id.radioButton30Menit -> sharedPref.saveStringPref(
                    "durasi_update",
                    "30_menit"
                )
            }
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btnUpdateLocation.setOnClickListener {
            checkLastLocation()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
        }
        return true
    }

    private fun setUpdateLocation(type : String?) {

        var repeatingIn : Long  = when(type) {
            "15_menit" -> DurationUpdate.MINUTES15.getTime()
            "20_menit" -> DurationUpdate.MINUTES20.getTime()
            else -> DurationUpdate.MINUTES30.getTime()
        }


        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val pendingIntent = Intent(applicationContext,UpdateLocationReceiver::class.java).let {
            intent -> PendingIntent.getBroadcast(applicationContext,0,intent,0)
        }
        alarmManager.setInexactRepeating(
            AlarmManager.ELAPSED_REALTIME_WAKEUP,
            SystemClock.elapsedRealtime() + repeatingIn,
            repeatingIn,
            pendingIntent
        )
    }

    private fun checkLastLocation() {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        } else {
//            val location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000,10F,locationListener)

            if(::currentLocation.isInitialized) {
                val latitude = currentLocation.latitude
                val longitude = currentLocation.longitude
                val altitude = currentLocation.altitude

                sharedPref.saveDoublePref("latitude_user",latitude)
                sharedPref.saveDoublePref("longitude_user",longitude)
                sharedPref.saveDoublePref("altitude_user",altitude)

                Toast.makeText(applicationContext, "Latitude , Longitude and Altitude is updated", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(applicationContext, "Sedang Mendapatkan lokasi terkini...", Toast.LENGTH_LONG).show()

            }
        }

    }

    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            currentLocation = location
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

        }

        override fun onProviderEnabled(provider: String) {
        }

        override fun onProviderDisabled(provider: String) {
        }

    }
}

