package id.someah.islamictimes.ui.fragment.gerhana.page.matahari

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.data.usesace.GetKontakGerhanaMahatahariTahunanUseCase
import id.someah.islamictimes.data.usesace.GetKontakGerhanaMatahariUseCase
import id.someah.islamictimes.ui.fragment.gerhana.InformasiGerhanaBulan
import id.someah.islamictimes.util.Event
import id.someah.islamictimes.util.EventListener
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class GerhanaMatahariViewModel @Inject constructor(
    private val getKontakGerhanaMatahariUseCase: GetKontakGerhanaMatahariUseCase,
    private val getKontakGerhanaMahatahariTahunanUseCase: GetKontakGerhanaMahatahariTahunanUseCase
) : ViewModel() , EventListener<DataGerhanaMatahari>{

    private var job = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)

    private val _loading = MutableLiveData<Boolean>()
    val loading : LiveData<Boolean> get() = _loading

    val mHijriYear = MutableLiveData<Long>()

    private val _getDataMatahari = MutableLiveData<List<DataGerhanaMatahari>>()
    val getDataMatahari : LiveData<List<DataGerhanaMatahari>> get() = _getDataMatahari

    private val _navigateToDetail = MutableLiveData<Event<DataGerhanaMatahari>>()
    val navigateToDetail : LiveData<Event<DataGerhanaMatahari>> get() = _navigateToDetail

    override fun openDetail(item: DataGerhanaMatahari) {
        _navigateToDetail.value = Event(item)
    }

    init {
        mHijriYear.value = 1443
        get()
    }


    fun get() {
        _loading.value = true

        val year = mHijriYear.value ?: 1443

        uiScope.launch {
            val temp = getDataJenisDanWaktu(0,year)

//            Constant.BULAN_HIJRI.map {
//                    data ->
//                async(Dispatchers.IO) {
//                    Log.d("async", "map 1")
//                    val description = getDataJenisDanWaktu(data.second.toByte() , year.toLong() )
//
//                    if(!description.jenis.toLowerCase().contains("tidak terjadi gerhana")) {
//                        temp.add(description)
//                    }
//                }
//            }.awaitAll()

            _getDataMatahari.value = temp.toList()

        }.start()
    }

    private suspend fun getDataJenisDanWaktu(bulanHijri : Byte , tahunHijri: Long) = withContext(
        Dispatchers.IO){
        getKontakGerhanaMahatahariTahunanUseCase(Pair(bulanHijri,tahunHijri))
    }

    override fun onCleared() {
        cancelJob()
    }

    fun stopLoading() {
        _loading.value = false
    }

    fun cancelJob() {
        job.cancel()
    }

}