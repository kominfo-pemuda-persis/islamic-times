package id.someah.islamictimes.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.PeriodicWorkRequest
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.adapters.WaktuAdzanAdapter
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.constant.getLocationAutocomplete
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.data.OfflineLocation
import id.someah.islamictimes.data.OfflineLocationModel
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.convertDateIfMoreThan24Hours
import id.someah.islamictimes.services.utils.toTwoDigits
import id.someah.islamictimes.services.utils.updateWidget
import id.someah.islamictimes.ui.activity.ArahKiblatActivity
import id.someah.islamictimes.ui.activity.CalendarActivity
import id.someah.islamictimes.ui.activity.DataMatahariBulanActivity
import id.someah.islamictimes.ui.activity.GerhanaActivity
import id.someah.islamictimes.util.*
import id.someah.islamictimes.viewmodel.WaktuSholatViewModel
import kotlinx.android.synthetic.main.fragment_dashboard.imageViewArahKiblat
import kotlinx.android.synthetic.main.fragment_dashboard.imageViewDataBulanMatahari
import kotlinx.android.synthetic.main.fragment_dashboard.imageViewGerhana
import kotlinx.android.synthetic.main.fragment_dashboard.imageViewKalender
import kotlinx.android.synthetic.main.fragment_dashboard.list_waktu_sholat
import kotlinx.android.synthetic.main.fragment_dashboard.textViewJamCountDown
import kotlinx.android.synthetic.main.fragment_dashboard.textViewLocation
import kotlinx.android.synthetic.main.fragment_dashboard.textViewNamaSholat
import kotlinx.android.synthetic.main.fragment_dashboard.textViewTglMasehi
import kotlinx.android.synthetic.main.fragment_dashboard_collapse.*
import kotlinx.android.synthetic.main.view_custom_snackbar.view.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import pub.devrel.easypermissions.EasyPermissions
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


@AndroidEntryPoint
class DashboardFragment : BaseFragment(), IslamicCustomDialogBaseState,
    EasyPermissions.PermissionCallbacks {
    private val LOCATION_PERMISSION = 1
    var elapsedHours: Long = 0
    var elapsedMinutes: Long = 0
    var elapsedSeconds: Long = 0
    private val notification = NotificationReceiver()

    lateinit var sharedPref: SharedPref

    @SuppressLint("SimpleDateFormat")
    private val formatter = SimpleDateFormat("dd MMMM yyyy")
    private val date = Date()
    private val cl = Calendar.getInstance()

    //    lateinit var waktuSholatViewModel: WaktuSholatViewModel
    val waktuSholatViewModel: WaktuSholatViewModel by viewModels()
    private lateinit var offlineLocation: OfflineLocation
    private lateinit var dataLocation: List<OfflineLocationModel>

    private var handlerIsRunning = false

    @Inject
    lateinit var getMyLocation: GetMyLocation

    private lateinit var mCustomDialog: IslamicCustomDialog
    lateinit var customAdapter: WaktuAdzanAdapter

    init {
        mCustomDialog = IslamicCustomDialog(R.layout.custom_dialog_location)
        mCustomDialog.state = this
    }


    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dashboard_collapse, container, false)
        return view
    }

    override fun initializedViewModel() {

    }

    override fun inflateView() {
        getMyLocation()

        shimmerLoading.startShimmer()
        shimmerMenuLoading.startShimmer()
        shimmer_header_loading.startShimmer()

        appBarLayout.addOnOffsetChangedListener(object : AppBarStateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout?, state: AppBarState) {

                toolbar.setBackgroundColor(
                    if (state == AppBarState.EXPANDED) {
                        Color.TRANSPARENT
                    } else {
                        resources.getColor(R.color.colorPrimary)
                    }
                )

            }
        })

        // Load Data dari Sqlite
        offlineLocation = OfflineLocation(requireContext(), R.raw.location)
        dataLocation = offlineLocation.getLocation()


        //val sharedPreferencesSetting = PreferenceManager.getDefaultSharedPreferences(context) //get preference from setting
        menuClicked()
        cl.time = date
        textViewTglMasehi.text = formatter.format(date).toString()
        if (sharedPref.getValueString(
                "alamat_user",
                ""
            ) == null || sharedPref.getValueString("alamat_user", "") == "0"
        ) {
            textViewLocation.text = getString(R.string.unlocated)
        } else {
            textViewLocation.text =
                sharedPref.getValueString("alamat_user", "")!!.replace("Kota", "")
        }
        setDataAfterPermissionGranted()
        setAllNotification()

        mCustomDialog.init(requireContext())
        mCustomDialog.mDialog.run {
            val autocomplete = this.findViewById<AutoCompleteTextView>(R.id.autoCompleteLokasi)
            val materialButton = this.findViewById<MaterialButton>(R.id.btnSubmit)
            val alert = this.findViewById<RelativeLayout>(R.id.viewAlertLocation)
            val btnLocation = this.findViewById<MaterialButton>(R.id.btnGps)

            autocomplete.apply {
                val listLocation = dataLocation.getLocationAutocomplete()
                setAdapter(
                    ArrayAdapter(
                        requireContext(),
                        R.layout.support_simple_spinner_dropdown_item,
                        listLocation
                    )
                )
                addTextChangedListener {
                    materialButton.isEnabled = it?.length ?: 0 > 0

                    alert.visibility =
                        if (listLocation.contains(it.toString())) View.GONE else View.VISIBLE
                }
            }

            materialButton.setOnClickListener {
                mCustomDialog.dismissCustomDialog()
            }

            btnLocation.setOnClickListener {
                reLoad("using_gps")

                dismiss()
            }
        }

        toolbar.apply {
            setOnMenuItemClickListener {
                val id = it.itemId

                if (id == R.id.btnRefresh) {
                    doRefresh()
                }

                if (id == R.id.btnLocation) {
                    mCustomDialog.showCustomDialog()
                }

                true
            }
        }

        customAdapter = WaktuAdzanAdapter(requireActivity())

        list_waktu_sholat_dashboard.apply {
            hasFixedSize()
            layoutManager = LinearLayoutManager(requireContext())
            adapter = customAdapter
            visibility = View.VISIBLE
        }

        initViewModel()
    }

    //check permission
    private fun isPermissionGranted(): Boolean {

        return if (EasyPermissions.hasPermissions(requireContext(), *Constant.perms)) {
            true
        } else {
            EasyPermissions.requestPermissions(
                this,
                "Gunakan Gps sekarang ?",
                LOCATION_PERMISSION,
                *Constant.perms
            )
            false
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        getMyLocation.getLastKnowLocation {
            if (it is Pair<*, *>) {
                waktuSholatViewModel.refresh()
            }
        }
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        mCustomDialog.showCustomDialog()
    }

    //fungsi untuk intent ketika ada menu yang di click (menu selain di bottom navigasi)
    private fun menuClicked() {
        imageViewDataBulanMatahari.setOnClickListener {
            startActivity(Intent(context, DataMatahariBulanActivity::class.java))
        }

        imageViewArahKiblat.setOnClickListener {
            startActivity(Intent(context, ArahKiblatActivity::class.java))
        }
        imageViewKalender.setOnClickListener {
            startActivity(Intent(context, CalendarActivity::class.java))
        }
        imageViewGerhana.setOnClickListener {
            startActivity(Intent(context, GerhanaActivity::class.java))
        }
    }

    //set semua notifikasi waktu sholat
    private fun setAllNotification() {
//        setWorker()
        notification.setNotificationWaktuSholatDzuhur(requireContext())
        notification.setNotificationWaktuSholatAshar(requireContext())
        notification.setNotificationWaktuSholatMaghrib(requireContext())
        notification.setNotificationWaktuSholatIsya(requireContext())
        //notification.setNotificationWaktuSholatNisfuLail(requireContext())
        notification.setNotificationWaktuSholatShubuh(requireContext())
        //notification.setNotificationWaktuSholatSyuruq(requireContext())
        notification.setNotificationWaktuSholatDhuha(requireContext())
    }

    override fun onPause() {

        shimmerLoading.stopShimmer()
        shimmer_header_loading.stopShimmer()
        shimmerMenuLoading.stopShimmer()

//        val sharedPref = SharedPref(requireContext())
//        setDataAfterPermissionGranted()
//        //get lokasi ketika onPause, karena pas aplikasi pertama dijalanin langsung ada request permission
//        if (sharedPref.getValueString("alamat_user", "") == null || sharedPref.getValueString("alamat_user", "") == "0") {
//            textViewLocation.text = getString(R.string.unlocated)
//        } else {
//            textViewLocation.text = sharedPref.getValueString("alamat_user", "")!!.replace("Kota", "")
//        }
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        getMyLocation()
    }

    @SuppressLint("SetTextI18n")
    //fungsi untuk get data dari viewmodel
    private fun initViewModel() {

        waktuSholatViewModel.countDown.observe(viewLifecycleOwner, Observer {
            var diff = it
            val secondsInMilli: Long = 1000
            val minutesInMilli = secondsInMilli * 60
            val hoursInMilli = minutesInMilli * 60

            elapsedHours = diff / hoursInMilli
            diff %= hoursInMilli
            elapsedMinutes = diff / minutesInMilli
            diff %= minutesInMilli
            elapsedSeconds = diff / secondsInMilli

            textViewJamCountDown.text =
                "-${elapsedHours.toTwoDigits()}:${elapsedMinutes.toTwoDigits()}:${elapsedSeconds.toTwoDigits()}"
        })

        waktuSholatViewModel.waktuSholat.observe(viewLifecycleOwner, Observer {
            textViewJamDashboard.text = it.convertDateIfMoreThan24Hours()
        })

        waktuSholatViewModel.dataWaktuSholat.observe(viewLifecycleOwner, Observer { list ->


            updateWidget(requireContext(), 0)
            updateWidget(requireContext(), 1)

            customAdapter.setList(list)

            setPageVisible(true)
        })

        waktuSholatViewModel.namaSholat.observe(viewLifecycleOwner, Observer {
            textViewNamaSholat.text = it
        })

    }

    //set data dari viewmodel ketika permission sudah diizinkan
    private fun setDataAfterPermissionGranted() {
//        val sharedPref = SharedPref(requireContext())
//        val sharedPrefPrioritas = sharedPref.getValueString("prioritas_update", "using_gps")
//        val locate = Locate(requireContext())
//        if (isPermissionGranted()) {
//
//            if(sharedPrefPrioritas == Constant.MANUAL_LOCATION) {
//                getDataFromViewModel()
//            } else {
//                if (locate.canGetLocation()) {
//                    locate.getAddress()
//                    getDataFromViewModel()
//                } else {
//                    if(sharedPrefPrioritas == "using_gps"){
//                        locate.gpsAlert()
//                    }
//                }
//            }
//
//        }
    }

    lateinit var dialog: ProgressDialog

    private fun doRefresh() {
        dialog = ProgressDialog.show(requireContext(), "", "Sedang Memuat...", true)

        reLoad()

        dialog.dismiss()
    }

    private fun reLoad(mode: String = "default") {

        if(mode == "default") {
            if (!handlerIsRunning) {
                setPageVisible(false)

                Handler(Looper.getMainLooper()).postDelayed({

                    waktuSholatViewModel.refresh()

                    if (mode == "default") {
                        //get lokasi ketika onResume,request permission selesai
                        if (sharedPref.getValueString(
                                "alamat_user",
                                ""
                            ) == null || sharedPref.getValueString("alamat_user", "") == "0"
                        ) {
                            textViewLocation.text = getString(R.string.unlocated)
                        } else {
                            textViewLocation.text =
                                sharedPref.getValueString("alamat_user", "")!!.replace("Kota", "")
                        }

                    }

                    setPageVisible(true)
                    handlerIsRunning = false
                }, 2000)
            }
        } else {
            setPageVisible(false)
            sharedPref.saveStringPref("prioritas_update", "using_gps")
            getMyLocation()
        }




    }

    override fun result(res: Dialog) {
        val resultSearch =
            res.findViewById<AutoCompleteTextView>(R.id.autoCompleteLokasi).text.toString()

        val location = dataLocation.find {
            it.locationName == resultSearch
        }


        if (location != null) {
            sharedPref.saveStringPref("alamat_user", location.locationName)
            sharedPref.saveDoublePref("latitude_user", location.latitude)
            sharedPref.saveDoublePref("longitude_user", location.longitude)
            sharedPref.saveDoublePref("altitude_user", location.altitude)
            sharedPref.saveStringPref("prioritas_update", Constant.MANUAL_LOCATION)
            sharedPref.saveDoublePref("timezone",location.timeZone)
            sharedPref.saveStringPref("timezone_title",location.timeZoneTitle)

        }

        LocalData(requireContext())

        reLoad()
    }

    private fun setPageVisible(show: Boolean) {

        val content = if (show) View.VISIBLE else View.INVISIBLE
        val loading = if (show) View.GONE else View.VISIBLE

        shimmerLoading.visibility = loading
        shimmerMenuLoading.visibility = loading
        shimmer_header_loading.visibility = loading


        list_waktu_sholat_dashboard.visibility = content
        constraintLayout.visibility = content
        constraintLayout2.visibility = content
    }

    private fun getMyLocation() {
        sharedPref = SharedPref(requireContext())
        val state = sharedPref.getValueString("prioritas_update", "using_gps")

        setPageVisible(false)

        if (state != "using_gps") {
            waktuSholatViewModel.refresh()

        } else {
            getMyLocation.getLastKnowLocation {
                if (it is Boolean) {
                    setPageVisible(!it)
                }

                if (it is String) {
                    EasyPermissions.requestPermissions(
                        this,
                        "Gunakan Gps sekarang ?",
                        LOCATION_PERMISSION,
                        *Constant.perms
                    )
                }
                if (it is Pair<*, *>) {
                    val location = it.first as Location
                    val textLocation = it.second as String

                    textViewLocation.text = textLocation

                    waktuSholatViewModel.refresh()

                    setPageVisible(true)
                }
            }
        }


    }


}
