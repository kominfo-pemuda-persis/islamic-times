package id.someah.islamictimes.ui.fragment.gerhana

import com.airbnb.epoxy.TypedEpoxyController
import id.someah.islamictimes.ItemGerhanaBindingModel_
import id.someah.islamictimes.data.DataGerhana
import id.someah.islamictimes.databinding.ItemGerhanaBinding
import id.someah.islamictimes.databinding.ItemGerhanaBindingImpl
import id.someah.islamictimes.util.EventListener
import java.util.*

class DaftarGerhanaController constructor(val eventListener: EventListener<InformasiGerhanaBulan>) : TypedEpoxyController<List<InformasiGerhanaBulan>>() {
    override fun buildModels(data: List<InformasiGerhanaBulan>?) {
        data?.forEachIndexed { index, dataGerhana ->
            ItemGerhanaBindingModel_()
                .id(index)
                .data(dataGerhana)
                .eventListener(eventListener)
                .addTo(this)
        }
    }
}