package id.someah.islamictimes.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import id.someah.islamictimes.constant.Common
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.R
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.viewmodel.WaktuSholatByDateViewModel
import id.someah.islamictimes.viewmodel.WaktuSholatByDateViewModelFactory
import com.itextpdf.text.*
import com.itextpdf.text.Element.*
import com.itextpdf.text.pdf.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_cetak_waktu_sholat.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*
import kotlin.collections.ArrayList

private const val REQUEST_GALLERY_PHOTO = 2
class CetakWaktuSholatActivity : AppCompatActivity() {
    private lateinit var waktuSholatByDateViewModel: WaktuSholatByDateViewModel
    private var bulan : String? = null
    private var tahun : String? = null
    private var bulanParsed : Date? = null
    @SuppressLint("SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cetak_waktu_sholat)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val arrBulan = arrayOf(getString(R.string.january), getString(
            R.string.february
        ),
            getString(R.string.march), getString(
                R.string.april
            ),
            getString(R.string.may), getString(
                R.string.june
            ),
            getString(R.string.july), getString(
                R.string.august
            ),
            getString(R.string.september), getString(
                R.string.october
            ),
            getString(R.string.november), getString(
                R.string.december
            ))

        val oldFormat = ArrayList<Date>()
        val bulanStr = ArrayList<String>()
        for (i in arrBulan.indices){
            oldFormat.add(SimpleDateFormat("MM", Locale.getDefault()).parse(arrBulan[i])!!)
            bulanStr.add(SimpleDateFormat("MMMM", Locale.getDefault()).format(oldFormat[i]))
        }
        val adapterMonths: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_spinner_item, bulanStr as List<String>
        )
        adapterMonths.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerBulan.adapter = adapterMonths
        val tambahTahun = 10
        val tahunStr = ArrayList<String>()
        val tahunSekarang = Calendar.getInstance().get(Calendar.YEAR)
        //tambah ke 10 tahun kedepan
        for (i in tahunSekarang until (tahunSekarang + tambahTahun + 1)){
            tahunStr.add(i.toString())
        }
        val adapterYears: ArrayAdapter<String> = ArrayAdapter(this, android.R.layout.simple_spinner_item, tahunStr
        )
        adapterYears.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerTahun.adapter = adapterYears
        
        spinnerBulan.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                bulan = parent?.getItemAtPosition(position).toString()
                bulanParsed = SimpleDateFormat("MMMM").parse(spinnerBulan.selectedItem.toString())
            }

        }
        spinnerTahun.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                tahun = parent?.getItemAtPosition(position).toString()
            }

        }

        //request permission pake dexter library
        Dexter.withContext(this)
            .withPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if(report!!.areAllPermissionsGranted()){
                        buttonCetak.setOnClickListener {
                            if(editTextLembaga.text.isEmpty()){
                                Toast.makeText(applicationContext, "Nama masjid/lembaga harus diisi", Toast.LENGTH_SHORT).show()
                            }
                            else{
                                val namaLembaga = editTextLembaga.text.toString()
                                val fileName = "Jadwal Sholat $namaLembaga $bulan $tahun.pdf"
                                createPDFFile(
                                    Common.getAppPath(
                                        this@CetakWaktuSholatActivity
                                    ) +fileName)

                            }
                        }
                        imageViewLogoPlaceholder.setOnClickListener {
                            pickImageFromGallery()
                        }
                        imageViewLogoLembaga.setOnClickListener {
                            pickImageFromGallery()
                        }
                    }
                    if(report.isAnyPermissionPermanentlyDenied){
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token!!.continuePermissionRequest()
                }

            })
            .withErrorListener { error -> Toast.makeText(applicationContext, "Terjadi Kesalahan ", Toast.LENGTH_SHORT)
                .show() }
            .onSameThread()
            .check()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == REQUEST_GALLERY_PHOTO && data != null){
                try {
                    val selectedImage: Uri? = data.data
                    val picturePath = getPath( this, selectedImage )
                    Glide.with(this)
                        .load(picturePath)
                        .into(imageViewLogoLembaga)
                    imageViewLogoPlaceholder.visibility = View.INVISIBLE
                    imageViewUploadPhoto.visibility = View.INVISIBLE
                }catch (e : IOException){
                    Log.d("error pick galerry" , e.message!!)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)

    }

    private fun pickImageFromGallery(){
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(pickPhoto,
            REQUEST_GALLERY_PHOTO
        )
    }

    private fun showSettingsDialog() {
        val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Butuh Izin")
        builder.setMessage(
            "Aplikasi ini butuh izin untuk menggunakan fitur ini"
        )
        builder.setPositiveButton("Buka Settings") { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    private fun getPath(context: Context, uri: Uri?): String? {
        var result: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor? = context.contentResolver.query(uri!!, proj, null, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                val columnIndex: Int = cursor.getColumnIndexOrThrow(proj[0])
                result = cursor.getString(columnIndex)
            }
            cursor.close()
        }
        if (result == null) {
            result = "Not found"
        }
        return result
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    @Throws(DocumentException::class)
    private fun createPDFFile(path: String) {
        if (File(path).exists()) {
            File(path).delete()
        }
        try {
            val document = Document()
            val pdfWriter = PdfWriter.getInstance(document, FileOutputStream(path))
            val event = Footer()
            pdfWriter.pageEvent = event
            document.open()
            document.pageSize = PageSize.A4
            document.addCreationDate()
            document.addAuthor("Islamic Times Application")
            val fontName =
                BaseFont.createFont("res/font/mukta_regular.ttf", "UTF-8", BaseFont.EMBEDDED)
            val titleStyle = Font(fontName, 26.0f, Font.NORMAL, BaseColor.BLACK)
            val namaLembaga = editTextLembaga.text.toString()
            Log.d("TAHUN", tahun!!)
            val bulan = spinnerBulan.selectedItem.toString()
            if(imageViewLogoLembaga.drawable != null) {
                val nullStyle = Font(fontName, 5.0f, Font.NORMAL, BaseColor.BLACK)
                addNewItem(document, " ", ALIGN_CENTER, nullStyle)//add null item for space above image
                addImage(document, imageViewLogoLembaga)
            }
            addNewItem(document, namaLembaga, ALIGN_CENTER, titleStyle)
            val subTitleStyle = Font(fontName, 18.0f, Font.NORMAL, BaseColor.BLACK)
            addNewItem(document, "$bulan $tahun", ALIGN_CENTER, subTitleStyle)
            val headerStyle = Font(fontName, 12.0f, Font.NORMAL, BaseColor.WHITE)
            addSpace(document)
            addTable(document, headerStyle)
            //printPDF()
        } catch (e: DocumentException) {
            Log.d("dokumen", e.localizedMessage!!)
        }
    }

    /*private fun printPDF() {
        val printManager = getSystemService(Context.PRINT_SERVICE) as PrintManager
        try {
            val namaLembaga = editTextLembaga.text.toString()
            val bulan = spinnerBulan.selectedItem.toString()
            val tahun = spinnerTahun.selectedItem.toString()
            val fileName = "Jadwal Sholat $namaLembaga $bulan $tahun.pdf"
            val printAdapter = PdfDocumentAdapter(
                this,
                Common.getAppPath(this@CetakWaktuSholatActivity) + fileName
            )
            printManager.print("Document", printAdapter, PrintAttributes.Builder().build())
        } catch (e: Exception) {
            Log.d("dokumen", e.message!!)
        }
    }*/

    @Throws(DocumentException::class)
    private fun addNewItem(document: Document, text: String, align: Int, style: Font) {
        try {
            val chunk = Chunk(text, style)
            val p = Paragraph(chunk)
            p.alignment = align
            document.add(p)
        }catch(e : DocumentException){
            Log.d("error add item", e.message!!)
        }

    }

    private fun addImage(document : Document, imageView : ImageView){
        try{
            val drawable : BitmapDrawable  = imageView.drawable as BitmapDrawable
            val bitmap : Bitmap = drawable.bitmap
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val imageInByteRight : ByteArray  = stream.toByteArray()
            val imageByte : Image = Image.getInstance(imageInByteRight)
            imageByte.scaleAbsoluteHeight(90.0f)
            imageByte.scaleAbsoluteWidth(90.0f)
            val chunkRight = Chunk(imageByte, 5f, 5f, true)
            val p = Paragraph(chunkRight)
            p.alignment = ALIGN_CENTER
            document.add(p)
        }
        catch(e : DocumentException){
            Log.d("error add image", e.message!!)
        }
    }
    @SuppressLint("SimpleDateFormat")
    @Throws(DocumentException::class)
    private fun addTable(document: Document, headerStyle: Font) {
        try{
            val table = PdfPTable(11)
            val headerTable = arrayOf("Tanggal",
                Constant.NAMA_SHOLAT_ZUHUR,
                Constant.NAMA_SHOLAT_ASAR,
                Constant.NAMA_SHOLAT_MAGRIB,
                Constant.NAMA_SHOLAT_ISYA,
                Constant.NAMA_AKHIR_ISYA,
                Constant.NAMA_SHOLAT_SUBUH,
                Constant.NAMA_AKHIR_SUBUH,
                Constant.NAMA_SHOLAT_DUHA,
                Constant.KIBLAT_HARIAN_1,
                Constant.KIBLAT_HARIAN_2
            )
            for (element in headerTable){
                val cell1 = PdfPCell(Paragraph(element,headerStyle))
                cell1.horizontalAlignment = ALIGN_CENTER
                cell1.backgroundColor = BaseColor(30, 113, 93, 255)
                cell1.fixedHeight = 40f
                cell1.verticalAlignment = ALIGN_MIDDLE
                table.addCell(cell1)
            }

            val bulanNumber =  SimpleDateFormat("MM").format(bulanParsed!!).toString()
            val calendar = Calendar.getInstance()
            calendar.clear()
            calendar.set(tahun!!.toInt(), bulanNumber.toInt(), 1)
            val date = LocalDate.of(tahun!!.toInt(), bulanNumber.toInt(), 1)

            table.widthPercentage = 100f
            table.horizontalAlignment = ALIGN_CENTER
            var cell : PdfPCell
            val dao = DaoManager()
            val viewModelFactoryWaktuSholat =
                WaktuSholatByDateViewModelFactory(
                    dao.waktuSholatDao(
                        this
                    )!!
                )
            waktuSholatByDateViewModel = ViewModelProvider(
                this,
                viewModelFactoryWaktuSholat
            ).get(WaktuSholatByDateViewModel::class.java)
            //get data sholat bulanan dari viewmodel
            waktuSholatByDateViewModel.getWaktuSholatBulanan(bulanNumber.toByte(), tahun!!.toInt().toLong(), this)
            waktuSholatByDateViewModel.dataWaktuSholatBulanan.observe(this, androidx.lifecycle.Observer {
                //populate data ke table pdf
                var start = 0
                var end = 10
                for (i in 1..date.lengthOfMonth()){
                    cell = PdfPCell(Paragraph(i.toString()))
                    cell.horizontalAlignment = ALIGN_CENTER
                    if(i % 5 ==  0){
                        cell.backgroundColor = BaseColor(194, 237, 227, 255)
                    }
                    table.addCell(cell)
                    if (end > it.size){
                        end -=1
                    }
                    for(j in start until end) {
                        cell = PdfPCell(
                            Paragraph(
                                it[j].jamSholat
                            )
                        )
                        Log.d("UWU", it[j].jamSholat)
                        cell.horizontalAlignment = ALIGN_CENTER
                        if(i % 5 ==  0){
                            cell.backgroundColor = BaseColor(194, 237, 227, 255)
                        }
                        table.addCell(cell)
                    }
                    start += 10
                    end += 10
                }
                table.widthPercentage = 100f
                table.horizontalAlignment = ALIGN_CENTER
                document.add(table)
                document.close()
                Toast.makeText(this, getString(R.string.succes_cetak), Toast.LENGTH_LONG).show()
                finish() //finish disimpen disini karena add table itu fungsi yang di tambahin sebelum document di close
                //ditambah karena ada viewmodel
            })
        }catch(e : DocumentException){
            Log.d("error add table", e.message!!)
        }
    }

    @Throws(DocumentException::class)
    private fun addSpace(document: Document) {
        try{
            val fontName =
                BaseFont.createFont("res/font/mukta_regular.ttf", "UTF-8", BaseFont.EMBEDDED)
            val nullStyle = Font(fontName, 10.0f, Font.NORMAL)
            document.add(Paragraph(" ", nullStyle))
        }catch(e : IOException){
            Log.d("error add space", e.message!!)
        }

    }

}

class Footer : PdfPageEventHelper() {
    override fun onEndPage(writer: PdfWriter, document: Document) {
        val fontNameMukta  = BaseFont.createFont("res/font/mukta_regular.ttf", "UTF-8", BaseFont.EMBEDDED)
        val fontNameElmessiri  = BaseFont.createFont("res/font/elmessiri_bold.ttf", "UTF-8", BaseFont.EMBEDDED)
        val fontStyle1 = Font(fontNameMukta, 18.0f, Font.BOLD, BaseColor.BLACK)
        val fontStyle2 = Font(fontNameElmessiri, 16.0f, Font.NORMAL, BaseColor.BLACK)
        val cb = writer.directContent
        val footer1 = Phrase("Powered By", fontStyle1)
        val footer2 = Phrase("IslamicTimes", fontStyle2)
        ColumnText.showTextAligned(
            cb, ALIGN_CENTER,
            footer1,
            (document.right() - document.left()) / 2 + document.leftMargin(),
            document.bottom() - 10, 0.0f)
        ColumnText.showTextAligned(
            cb, ALIGN_CENTER,
            footer2,
            (document.right() - document.left()) / 2 + document.leftMargin(),
            document.bottom() - 30, 0.0f)
    }
}
