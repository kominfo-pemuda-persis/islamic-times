package id.someah.islamictimes.ui.activity

import android.Manifest
import android.app.ActivityManager
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import id.someah.islamictimes.R
import id.someah.islamictimes.data.OfflineLocationModel
import id.someah.islamictimes.data.OfflineLocation
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.AlarmServiceChecker
import id.someah.islamictimes.util.Locate
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions


//class untuk menampilkan splashscreen, acitivity set di manifest jadi launcher.
class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        try {
            actionOnService()
        }catch (e : Exception) {

        }

        Handler().postDelayed({
            startActivity(Intent(this@SplashScreen, MainActivity::class.java))
            finish()
        }, 1500) //delay 1.5 detik
    }

    private fun actionOnService() {
        if(!isMyServiceRunning(this , AlarmServiceChecker::class.java)) {
            startService(Intent(this, AlarmServiceChecker::class.java))
        }
    }

    fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}
