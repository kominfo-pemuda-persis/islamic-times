package id.someah.islamictimes.ui.activity

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.WindowManager
import android.widget.RadioGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import id.someah.islamictimes.R
import kotlinx.android.synthetic.main.activity_privacy_and_policy.*

class PrivacyAndPolicyActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this,R.color.secondColorPrimary)

        setContentView(R.layout.activity_privacy_and_policy)

        inflateView()
    }

    private fun inflateView() {
        toolbar.apply {
            val actionView = menu.findItem(R.id.switchLanguage).actionView
            val switch = actionView.findViewById(R.id.toggle) as RadioGroup
            switch.check(R.id.id)
            switch.setOnCheckedChangeListener { group, checkedId ->
                var defaultText = ""
                when(checkedId) {
                    R.id.id -> {
                        defaultText = getString(R.string.privacy_policy_id)
                    }

                    R.id.en -> {
                        defaultText = getString(R.string.privacy_policy_en)
                    }
                }

                setContentText(defaultText)

            }


            setNavigationOnClickListener {
                finish()
            }
        }

        setContentText(getString(R.string.privacy_policy_id))
    }

    private fun setContentText(html : String) {
        tvContent.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }
    }
}