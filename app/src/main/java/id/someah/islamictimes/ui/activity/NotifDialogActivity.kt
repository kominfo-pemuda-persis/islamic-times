package id.someah.islamictimes.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.someah.islamictimes.R
import id.someah.islamictimes.services.BackgroundSound
import kotlinx.android.synthetic.main.custom_notif_dialog.*

//class untuk menampilkan notifikasi dialog untuk mematikan suara adzan.
class NotifDialogActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.custom_notif_dialog)
        val type = intent.getStringExtra("type_sholat")
        Intent(this, BackgroundSound::class.java).also { intent ->
            intent.putExtra("type_sholat", type)
            startService(intent) //start service audio background
        }
        //matikan suara adzan
        buttonYa.setOnClickListener {
            Intent(this, BackgroundSound::class.java).also { intent ->
                stopService(intent)
            }
            finish()
        }
        //suara adzan terus berjalan
        buttonTidak.setOnClickListener {
            finish()
        }
    }

    override fun onBackPressed() {
        Intent(this, BackgroundSound::class.java).also { intent ->
            stopService(intent)
        }
        finish()
        super.onBackPressed()
    }
}