package id.someah.islamictimes.ui.fragment.datamataharibulan

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.R
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.desimalFormat
import id.someah.islamictimes.services.utils.hideView
import id.someah.islamictimes.services.utils.showView
import id.someah.islamictimes.viewmodel.BulanMatahariViewModel
import id.someah.islamictimes.viewmodel.BulanMatahariViewModelFactory
import kotlinx.android.synthetic.main.fragment_data_bulan_matahari.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

//class untuk menampilkan data bulan dan matahari real time
class DataBulanMatahariFragment : Fragment() {
    private lateinit var matahariBulanViewModel: BulanMatahariViewModel
    private lateinit var someHandler: Handler
    private var acuanLokasi: String? = null
    private var pilihanDeltaT: String? = null
    private var valueDeltaT: Double? = 0.0
    private var jam: String? = null
    private var menit: String? = null
    private var detik: String? = null
    private var jamDesimal: Double? = null
    private val falak = FalakLib()
    private val decimalFormat = DecimalFormat("#,###.##")
    private val calendar: Calendar = Calendar.getInstance()
    private val tanggal: Byte = calendar.get(Calendar.DAY_OF_MONTH).toByte()
    private val bulan: Byte =
        (calendar.get(Calendar.MONTH) + 1).toByte() //di plus 1 karena output bulan dimulai dari 0
    private val tahun: Long = calendar.get(Calendar.YEAR).toLong()

    lateinit var  localData : LocalData

    val arrayListJam = ArrayList<String>()
    val arrayListMenit = ArrayList<String>()
    val arrayListDetik = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_data_bulan_matahari, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPref = SharedPref(context as Activity)
        localData  = LocalData(context as Activity)

        setSpinnerWaktuManual()

        //set latitude, longitude, altitude dari shared pref ke textview
        textViewLatitudeRealtime.text = decimalToSexagesimal(sharedPref.getValueDouble("latitude_user")!!)
        Log.d("value sexagesimal", decimalToSexagesimal(sharedPref.getValueDouble("latitude_user")!!))
        textViewLongitudeRealtime.text = decimalToSexagesimal(sharedPref.getValueDouble("longitude_user")!!)
        textViewAltitudeRealtime.text = desimalFormat(sharedPref.getValueDouble("altitude_user").toString().toDouble())

        //setup spinner
        setUpSpinnerPilihan()

        //inisialisasi viewmodel
        val dao = DaoManager()
        val viewModelFactoryBulanMatahari =
            BulanMatahariViewModelFactory(
                dao.dataBulanMatahariDao(
                    context as Activity
                )!!
            )
        matahariBulanViewModel = ViewModelProvider(this, viewModelFactoryBulanMatahari).get(
            BulanMatahariViewModel::class.java
        )
        editTextDeltaTRealtime1.visibility = View.GONE
        toggleVisibilityItem(View.GONE)
        Log.d("jam menit detik", "$jam $menit $detik")
        //check perubahan switch
        switchManual.setOnCheckedChangeListener { buttonView, isChecked ->
            //ketika switch on perhitungan menjadi manual
            if (isChecked) {
                switchManual.text = getString(R.string.on)
                toggleVisibilityItem(View.VISIBLE)
                someHandler.removeCallbacksAndMessages(null)

//                tv_jam.text = "00:00:00"


                hitungManualDataMatahariBulan()
                visibleWhenJamSetOn()

            } else {
                switchManual.text = getString(R.string.off)
                toggleVisibilityItem(View.GONE)
                invisbleWhenJamSetOff()
                startHandler()
            }
        }

        //get data dari viewmodel
        matahariBulanViewModel.dataBulanMatahari.observe(
            viewLifecycleOwner,
            Observer { listMatahariBulan ->
                tv_matahari_gha_value.text = listMatahariBulan[0].gha
                tv_matahari_lha_value.text = listMatahariBulan[0].lha
                tv_matahari_ra_value.text = listMatahariBulan[0].ra
                tv_matahari_dec_value.text = listMatahariBulan[0].dec
                tv_matahari_lintang_value.text = listMatahariBulan[0].lintang
                tv_matahari_bujur_value.text = listMatahariBulan[0].bujur
                tv_matahari_h_value.text = listMatahariBulan[0].alt
                tv_matahari_azimut_value.text = listMatahariBulan[0].azimut


                tv_bulan_gha_value.text = listMatahariBulan[1].gha
                tv_bulan_lha_value.text = listMatahariBulan[1].lha
                tv_bulan_ra_value.text = listMatahariBulan[1].ra
                tv_bulan_dec_value.text = listMatahariBulan[1].dec
                tv_bulan_lintang_value.text = listMatahariBulan[1].lintang
                tv_bulan_bujur_value.text = listMatahariBulan[1].bujur
                tv_bulan_h_value.text = listMatahariBulan[1].alt
                tv_bulan_azimut_value.text = listMatahariBulan[1].azimut

            })
    }

    override fun onResume() {
        if(!switchManual.isChecked) {
            startHandler()
        }
        super.onResume()
    }

    override fun onStop() {
        someHandler.removeCallbacksAndMessages(null)
        super.onStop()
    }

    override fun onPause() {
        someHandler.removeCallbacksAndMessages(null)
        super.onPause()
    }

    //fungsi yang membuat jam realtime
    private fun startHandler() {

        someHandler = Handler(Looper.getMainLooper())
        someHandler.postDelayed(object : Runnable {
            override fun run() {
                //jam di set sesuai waktu hari ini
                tv_jam.text = SimpleDateFormat("HH:mm:ss", Locale.US).format(Date())
//                textViewSetJamRealtime.text = SimpleDateFormat("HH:mm:ss", Locale.US).format(Date())
                someHandler.postDelayed(this, 1000) //di ubah setiap 1 detik
                //split untuk mendapatkan data tanggal,bulan,tahun
                val waktu = tv_jam.text.toString().split(":")
                val jam = waktu[0].toDouble()
                val menit = waktu[1].toDouble()
                val detik = waktu[2].toDouble()
                //perhitungan jam desimal
                val jamDesimal = (jam + (menit / 60)) + (detik / 3600)

                /*
                toggle untuk menampilkan dan menghilangkan view dari input delta T
                ketika pilihan dari spinner delta T berubah ketika switch nyala
                 */
                spinnerDeltaT.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {

                    }

                    @SuppressLint("SetTextI18n")
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        pilihanDeltaT = parent?.getItemAtPosition(position).toString()

                        when (pilihanDeltaT) {
                            getString(R.string.abaikan_delta_t) -> {
                                editTextDeltaTRealtime1.visibility = View.GONE
                                valueDeltaT = 0.0
                            }
                            getString(R.string.prediksi_delta_t) -> {
                                editTextDeltaTRealtime1.visibility = View.GONE

                                val jd: Double =
                                    falak.KMJD(tanggal, bulan, tahun, jamDesimal, localData.tzn)
                                valueDeltaT = falak.DeltaT(jd)
                            }
                            getString(R.string.manual_delta_t) -> {
                                //edit text dibuat visible ketika pilihan detla T nya manual
                                editTextDeltaTRealtime1.visibility = View.VISIBLE
                                valueDeltaT = if (editTextDeltaTRealtime.text.toString().isNotEmpty()) {
                                    editTextDeltaTRealtime.text.toString().toDouble()
                                } else {
                                    0.0
                                }

                            }
                        }
                        textViewValueDeltaTRealtime.text = "Nilai Delta T : ${decimalFormat.format(valueDeltaT!!)}"
                    }

                }
                if(spinnerDeltaT.selectedItem.toString() == getString(R.string.prediksi_delta_t)){
                    val jd: Double =
                        falak.KMJD(tanggal, bulan, tahun, jamDesimal, localData.tzn)
                    valueDeltaT = falak.DeltaT(jd)

                    textViewValueDeltaTRealtime.text = "Nilai Delta T : ${decimalFormat.format(valueDeltaT!!)}"
                }
                Log.d("value delta T Realtime", "$valueDeltaT $acuanLokasi")
                //setup viewmodel disini karena butuh value jamDesimal dan delta T
//                textViewTempatAcuan.text = acuanLokasi.toString()
                matahariBulanViewModel.dataBulanMatahari(jamDesimal, acuanLokasi!!, valueDeltaT!!)
            }
        }, 10)
    }

    private fun setUpSpinnerPilihan() {
        //isi data dari spinner
        val arrAcuanLokasi =
            arrayOf(getString(R.string.toposentris), getString(R.string.geosentris))
        val arrDeltaT = arrayOf(
            getString(R.string.prediksi_delta_t),
            getString(R.string.manual_delta_t),
            getString(R.string.abaikan_delta_t)
        )

        val listAcuanLokasi = ArrayList<String>()
        val listdeltaT = ArrayList<String>()

        //tambah data ke arraylist
        for (i in arrAcuanLokasi.indices) {
            listAcuanLokasi.add(arrAcuanLokasi[i])
        }

        //set up spinner dengan data yang ada di list
        val adapterAcuanLokasi: ArrayAdapter<String> = ArrayAdapter(
            context as Activity,
            android.R.layout.simple_spinner_dropdown_item,
            listAcuanLokasi
        )
        adapterAcuanLokasi.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerAcuanLokasi.adapter = adapterAcuanLokasi
        acuanLokasi = spinnerAcuanLokasi.selectedItem.toString()
        spinnerAcuanLokasi.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                acuanLokasi =
                    parent?.getItemAtPosition(position).toString() //get data ketika pilihan berubah
            }

        }
        //tambah data ke arraylist
        for (i in arrDeltaT.indices) {
            listdeltaT.add(arrDeltaT[i])
        }

        //set up spinner dengan arraylist
        val adapterDeltaT: ArrayAdapter<String> = ArrayAdapter(
            context as Activity,
            android.R.layout.simple_spinner_dropdown_item,
            listdeltaT
        )
        adapterDeltaT.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDeltaT.adapter = adapterDeltaT
        pilihanDeltaT = spinnerDeltaT.selectedItem.toString()
    }

    //fungsi untuk set up spinner jam menit detik
    private fun setSpinnerWaktuManual() {

        for (i in 0 until 60) {
            if (i.toString().length < 2) {
                arrayListMenit.add("0$i")
                arrayListDetik.add("0$i")
            } else {
                arrayListMenit.add(i.toString())
                arrayListDetik.add(i.toString())
            }
        }
        for (i in 0 until 25) {
            if (i.toString().length < 2) {
                arrayListJam.add("0$i")
            } else {
                arrayListJam.add(i.toString())
            }
        }

        //set adapter jam
        initAdapter()

    }

    private fun initAdapter () {
        val adapterJam: ArrayAdapter<String> =
            ArrayAdapter(context as Activity, android.R.layout.simple_spinner_item, arrayListJam)
        adapterJam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerJamRealtime.adapter = adapterJam

        //set adapter menit
        val adapterMenit: ArrayAdapter<String> =
            ArrayAdapter(context as Activity, android.R.layout.simple_spinner_item, arrayListMenit)
        adapterJam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerMenitRealtime.adapter = adapterMenit


        //set adapter detik
        val adapterDetik: ArrayAdapter<String> =
            ArrayAdapter(context as Activity, android.R.layout.simple_spinner_item, arrayListDetik)
        adapterJam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDetikRealtime.adapter = adapterDetik
    }
    //fungsi untuk mengatur visibility dari view ketika switch dinyalakan
    private fun toggleVisibilityItem(visibility: Int) {
        spinnerJamRealtime.visibility = visibility
        spinnerMenitRealtime.visibility = visibility
        spinnerDetikRealtime.visibility = visibility
        textViewJamRealtime.visibility = visibility
        textViewMenitRealtime.visibility = visibility
        textViewDetikRealtime.visibility = visibility
        buttonHitungRealtime.visibility = visibility
    }

    //fungsi untuk menghitung data secara manual ketika switch dinyalakan dan button hitung di click
    @SuppressLint("SetTextI18n")
    private fun hitungManualDataMatahariBulan() {
//        val localData = LocalData(context as Activity)
        buttonHitungRealtime.setOnClickListener {
            onListenerHitungManualDataBulan()
        }
    }

    private fun onListenerHitungManualDataBulan() {
        pilihanDeltaT = spinnerDeltaT.selectedItem.toString()
        //validasi form
        if(pilihanDeltaT == getString(R.string.manual_delta_t) && editTextDeltaTRealtime.text.toString().isEmpty()){
            Toast.makeText(context as Activity, R.string.error_message_input_delta_t, Toast.LENGTH_SHORT).show()
        }
        else{
            //inisialisasi jam, menit, detik, dan acuan lokasi
            jam = spinnerJamRealtime.selectedItem.toString()
            menit = spinnerMenitRealtime.selectedItem.toString()
            detik = spinnerDetikRealtime.selectedItem.toString()
            acuanLokasi = spinnerAcuanLokasi.selectedItem.toString()
            //perhitungan jam desimal
            jamDesimal =
                (jam!!.toDouble() + (menit!!.toDouble() / 60)) + (detik!!.toDouble() / 3600)

            //perhitungan value delta T sesuai pilihan dari spinner
            when (pilihanDeltaT) {
                getString(R.string.abaikan_delta_t) -> {
                    valueDeltaT = 0.0
                }
                getString(R.string.prediksi_delta_t) -> {
                    val jd: Double =
                        falak.KMJD(tanggal, bulan, tahun, jamDesimal!!, localData.tzn)
                    valueDeltaT = falak.DeltaT(jd)
                }
                getString(R.string.manual_delta_t) -> {
                    valueDeltaT = if (editTextDeltaTRealtime.text.toString().isNotEmpty()) {
                        editTextDeltaTRealtime.text.toString().toDouble()
                    } else {
                        0.0
                    }
                }
            }
            /*
            toggle untuk menampilkan dan menghilangkan view dari input delta T
            ketika pilihan dari spinner delta T berubah ketika switch mati
             */
            spinnerDeltaT.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    pilihanDeltaT = parent?.getItemAtPosition(position).toString()

                    when (pilihanDeltaT) {
                        getString(R.string.abaikan_delta_t) -> {
                            editTextDeltaTRealtime1.visibility = View.GONE
                        }
                        getString(R.string.prediksi_delta_t) -> {
                            editTextDeltaTRealtime1.visibility = View.GONE
                        }
                        getString(R.string.manual_delta_t) -> {
                            //edit text dibuat visible ketika pilihan detla T nya manual
                            editTextDeltaTRealtime1.visibility = View.VISIBLE
                        }
                    }
                }

            }
//                textViewTempatAcuan.text = acuanLokasi
//                textViewSetJamRealtime.text = "$jam:$menit:$detik"
            textViewValueDeltaTRealtime.text = "Nilai Delta T : ${decimalFormat.format(valueDeltaT!!)}"
            matahariBulanViewModel.dataBulanMatahari(jamDesimal!!, acuanLokasi!!, valueDeltaT!!)
        }

        jam = spinnerJamRealtime.selectedItem.toString()
        menit = spinnerMenitRealtime.selectedItem.toString()
        detik = spinnerDetikRealtime.selectedItem.toString()

        tv_jam.text = "$jam:$menit:$detik"
    }

    private fun decimalToSexagesimal(decimalValue : Double) : String{
        val degrees = decimalValue.toInt()
        val minutes : Double
        val seconds : Double
        if(decimalValue <=0 ){
            minutes = kotlin.math.floor(60.0 * (degrees - decimalValue))
            seconds = 3600.0 * (degrees - decimalValue) - (60.0 * minutes)
        }else{
            minutes = kotlin.math.floor(60.0 * (decimalValue - degrees))
            seconds = 3600.0 * (decimalValue - degrees) - (60.0 * minutes)
        }

        return "${degrees}° ${minutes.toInt()}′ ${seconds.toInt()}″"
    }

    private fun visibleWhenJamSetOn() {
        manual_jam.showView()

        val sliceWaktu = tv_jam.text.toString().split(':')
        Log.d("contentValues", "visibleWhenJamSetOn: ${sliceWaktu[1]}")

        spinnerJamRealtime.setSelection(arrayListJam.indexOf(sliceWaktu.first()))
        spinnerMenitRealtime.setSelection(arrayListMenit.indexOf(sliceWaktu[1]))
        spinnerDetikRealtime.setSelection(arrayListDetik.indexOf(sliceWaktu.last()))
//        spinnerDetik.setSelection(arrayListDetik.indexOf(sliceWaktu.last()))

    }

    private fun invisbleWhenJamSetOff() {
        manual_jam.hideView()
    }
}