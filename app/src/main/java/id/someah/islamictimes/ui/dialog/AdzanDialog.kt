package id.someah.islamictimes.ui.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class AdzanDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage("HENTIKAN SUARA")
            builder.create()
            builder.show()
        } ?: throw IllegalStateException("Activity cannot be null")

    }
}