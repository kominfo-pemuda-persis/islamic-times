package id.someah.islamictimes.ui.fragment.gerhana.listener

interface OnFilterSelectedListener {
    fun onSelected( month : Byte , year : Long)
}