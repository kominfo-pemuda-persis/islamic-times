package id.someah.islamictimes.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.INVISIBLE
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.viewmodel.ArahKiblatViewModel
import id.someah.islamictimes.viewmodel.ArahKiblatViewModelFactory
import id.someah.islamictimes.util.Compass
import id.someah.islamictimes.R
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.util.Locate
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.util.CustomSnackbar
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.*
import kotlinx.android.synthetic.main.activity_arah_kiblat.*
import kotlinx.android.synthetic.main.activity_arah_kiblat.view.*
import kotlinx.android.synthetic.main.bottom_sheet_arah_kiblat.*
import kotlinx.android.synthetic.main.bottom_sheet_arah_kiblat.view.*
import kotlinx.android.synthetic.main.bottom_sheet_arah_kiblat.view.txt_arah_kiblat
import kotlinx.android.synthetic.main.bottom_sheet_arah_kiblat.view.txt_jarak_kiblat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class ArahKiblatActivity : AppCompatActivity() {
    private var currentAzimuth = 0f
    private lateinit var compass: Compass
    private lateinit var arahKiblatViewModel: ArahKiblatViewModel

    private val calendar: Calendar = Calendar.getInstance()
    private val tanggal: Byte = calendar.get(Calendar.DAY_OF_MONTH).toByte()
    private val bulan: Byte =
        (calendar.get(Calendar.MONTH) + 1).toByte() //di plus 1 karena output bulan dimulai dari 0
    private val tahun: Long = calendar.get(Calendar.YEAR).toLong()

    lateinit var localData: LocalData
    val dec = DecimalFormat("#,###.##")
    private var sunAzimuth = 0.toFloat()
    private var monAzimuth = 0.toFloat()

    private var handler = Handler(Looper.getMainLooper())
    private val falak = FalakLib()

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    private lateinit var gps: Locate

    lateinit var alertDialog: AlertDialog
    val snackbarClicked: View.OnClickListener = View.OnClickListener {

    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_arah_kiblat)

        gps = Locate(this)
        alertDialog = gps.gpsAlertWithReturnValue().create()

        setDataAfterPermissionGranted()

        //inisialisasi viewmodel
        shimmer_view_container.showView()
        shimmer_view_container.startShimmer()
//        bottomSheet.hideView()

        contentArahKiblat.hideView()

        localData = LocalData(this)
        val daoManager = DaoManager()
        val viewModelFactory =
            ArahKiblatViewModelFactory(
                daoManager.dataArahKiblat(this)!!,
                daoManager.dataBulanMatahariDao(this)!!
            )

        arahKiblatViewModel =
            ViewModelProvider(this, viewModelFactory).get(ArahKiblatViewModel::class.java)

        initBottomSheet()

        //get data dari viewmodel
        if (isGPSon() && isPermissionGranted()) {
            arahKiblatViewModel.dataArahKiblat.observe(this, Observer {

                shimmer_view_container.stopShimmer()
                shimmer_view_container.hideView()
                shimmer_view_container.hideView()
                contentArahKiblat.showView()

                val jarakKiblat = it.jarakKeKiblat
                val formattedJarakKiblat = dec.format(jarakKiblat)
                contentArahKiblat.txt_kiblat_lokasi.text = it.lokasi.replace("Kota", "", true)
                bottomSheet.txt_arah_kiblat.text = "+${
                    RoundTo(it.arahKiblat.toDouble(),2)
                }° | ${DDDMS(it.arahKiblat.toDouble())}"
                bottomSheet.txt_jarak_kiblat.text = "$formattedJarakKiblat KM"
//            contentArahKiblat.txt_kiblat_harian.text = it.kiblatHarian
                bottomSheet.textViewKiblatHarian1.text = "${it.kiblatHarian}"
                bottomSheet.textViewKiblatHarian2.text = "${it.kiblatHarian2}"

                bottomSheet.txt_rashdul_kiblat1.text =
                    "${it.rashdulKiblat1.split(",")[0]} ${it.rashdulKiblat1.split(",")[1]}\n${
                        it.rashdulKiblat1.split(",")[1]
                    }"
                bottomSheet.txt_rashdul_kiblat2.text =
                    "${it.rashdulKiblat2.split(",")[0]} ${it.rashdulKiblat2.split(",")[1]}\n${
                        it.rashdulKiblat2.split(",")[1]
                    }"
                contentArahKiblat.txt_lat_long_satu.text =
                    "(${it.lattitude.decimalToSexagesimal()} | ${it.longitude.decimalToSexagesimal()})"
//            contentArahKiblat.txt_lat_long_satu.text = "(${it.lattitude} | ${it.lattitude.decimalToSexagesimal()})"
//            contentArahKiblat.txt_lat_long_dua.text = "(${it.longitude} | ${it.longitude.decimalToSexagesimal()})"
            })


            arahKiblatViewModel.dataBulanMatahari.observe(this, Observer {

                sunAzimuth = it[0].decimalAzimut?.toFloat()!!
                bottomSheet.textViewAzimuthMatahari.text =
                    "${it[0].decimalAzimut}°"
                monAzimuth = it[1].decimalAzimut?.toFloat()!!
                bottomSheet.textViewAzimuthBulan.text =
                    "${it[1].decimalAzimut}°"
                bottomSheet.textViewBayanganMatahari.text =
                    "${it[0].decimalBackAzimuth}°"
                bottomSheet.textViewAzimuthBayanganBulan.text =
                    "${it[1].decimalBackAzimuth}°"
            })
//            contentArahKiblat.imageViewJarumKiblat.visibility = View.GONE
        }


        //deteksi lokasi gps lalu set derajat  kiblat
        if (gps.canGetLocation)
            getDerajatKiblat(this)

        setupCompass()
        startHandler()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        isAnimationStop()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_lock) {
            arahKiblatViewModel.switchVisiblity()
        }
        if (item.itemId == R.id.menu_open) {
            arahKiblatViewModel.switchVisiblity()
        }
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return true
    }


    private var kiblatDerajat = 0.toFloat()


    private fun setupCompass() {
        compass = Compass(this)
        compass.setListener(object : Compass.CompassListener {
            override fun onNewAzimuth(azimuth: Float) {
                adjustGambarDial(azimuth)
                adjustArrowQiblat(azimuth)
                adjustArrowMoon(azimuth)
                adjustArrowSun(azimuth)
            }

            override fun onPauseCompass(azimuth: Float, isPause: Boolean?) {
                adjustGambarDial(azimuth, isPause)
                adjustArrowQiblat(azimuth, isPause)
                adjustArrowMoon(azimuth, isPause)
                adjustArrowSun(azimuth, isPause)
            }

        })
    }


    fun adjustGambarDial(azimuth: Float, isPause: Boolean? = false) {
        var an: RotateAnimation? = null
//        if(isPause == true) {
//             an = RotateAnimation(
//                -currentAzimuth, -azimuth,
//                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
//                0.5f
//            )
//            currentAzimuth = -270f
//        } else {
        an = RotateAnimation(
            -currentAzimuth, -azimuth,
            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
            0.5f
        )
        currentAzimuth = azimuth
//        }

        an.duration = 500
        an.repeatCount = 0
        an.fillAfter = true
        contentArahKiblat.ilustrasi_arah_kiblat.startAnimation(an)
//        contentArahKiblat.northPosition.text = "${(Math.toDegrees(azimuth.toDouble()) + 360 ) % 360}"
        contentArahKiblat.northPosition.text =
            "$azimuth".currentAzimuthConvertIntoOneNumbersBehindComma()
    }

    //set gambar jarum kiblat ke derajat / azimut yang dituju
    fun adjustArrowQiblat(azimuth: Float, isPause: Boolean? = false) {
        var an: RotateAnimation? = null
//        if(isPause == true) {
//            an = RotateAnimation(
//                -270f + kiblatDerajat, -270f + kiblatDerajat,
//                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
//                0.5f
//            )
//            currentAzimuth = -270f
//        } else {
        an = RotateAnimation(
            -(currentAzimuth) + kiblatDerajat, -azimuth,
            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
            0.5f
        )
        currentAzimuth = (azimuth)
//        }

        an.duration = 500
        an.repeatCount = 0
        an.fillAfter = true

        contentArahKiblat.imageViewJarumKiblat.startAnimation(an)
        if (kiblatDerajat > 0) {
            contentArahKiblat.imageViewJarumKiblat.visibility = View.VISIBLE
        } else {
            contentArahKiblat.imageViewJarumKiblat.visibility = INVISIBLE
            contentArahKiblat.imageViewJarumKiblat.visibility = View.GONE
        }
    }

    fun adjustArrowMoon(azimuth: Float, isPause: Boolean? = false) {
        var fromDegrees = -currentAzimuth + monAzimuth
        var toDegress = -azimuth

//        if(isPause == true) {
//            fromDegrees = -270f + monAzimuth
//            toDegress = fromDegrees
//        }

        currentAzimuth = azimuth
        contentArahKiblat.ilustrasi_arah_bulan.startAnimation(
            createAnimation(
                fromDegrees,
                toDegress
            )
        )

    }

    fun adjustArrowSun(azimuth: Float, isPause: Boolean? = false) {

        var fromDegrees = -currentAzimuth + sunAzimuth
        var toDegress = -azimuth

//        if(isPause == true) {
//            fromDegrees = -270f + sunAzimuth
//            toDegress = fromDegrees
//        }
        currentAzimuth = azimuth

        contentArahKiblat.ilustrasi_arah_matahari.startAnimation(
            createAnimation(
                fromDegrees,
                toDegress
            )
        )
    }


    override fun onStart() {
        super.onStart()
        if (::compass.isInitialized) {
            compass.startCompass()
        }

    }

    override fun onPause() {
        super.onPause()
        if (::compass.isInitialized) {
            compass.stopCompass()
        }

        handler.removeCallbacksAndMessages(null)
    }

    override fun onResume() {
        super.onResume()

        setDataAfterPermissionGranted()

        if(isGPSon() && isPermissionGranted()) {
            arahKiblatViewModel.fetch()
        }

        if (::compass.isInitialized) {
            compass.startCompass()
        }

        startHandler()
    }

    override fun onStop() {
        super.onStop()
        if (::compass.isInitialized) {
            compass.stopCompass()
        }

        handler.removeCallbacksAndMessages(null)
    }

    @SuppressLint("SetTextI18n")
    private fun getDerajatKiblat(context: Context) {
        val sharedPreferencesSetting = PreferenceManager.getDefaultSharedPreferences(context)
        val kiblatMode = sharedPreferencesSetting.getString("kiblat", "kiblat_spherical")
        if (gps.canGetLocation()) {
            val localData = LocalData(this)
            //get derajat kiblat by tipe kiblat
            kiblatDerajat = when {
                kiblatMode.equals("kiblat_spherical") -> {
                    localData.derajatKiblatSpherical.toFloat()
                }
                kiblatMode.equals("kiblat_ellipsoid") -> {
                    localData.derajatKiblatEllipsoid.toFloat()
                }
                else -> {
                    localData.derajatKiblatVincenty.toFloat()
                }
            }
            val title = kiblatMode?.split('_')?.last()

            bottomSheet.textView_jarak_ke_kiblat.text = "Jarak Ke Kiblat $title"
            bottomSheet.textView_arah_kiblat.text = "Arah Kibat $title"

        } else {
            gps.gpsAlert()
        }
    }

    private fun startHandler() {
        handler.postDelayed(object : Runnable {
            override fun run() {
                handler.postDelayed(this, 1000)

                val times = SimpleDateFormat("HH:mm:ss", Locale.US).format(Date()).toString()
                    .convertToParameters()

                val hours = times.first.toDouble()
                val minutes = times.second.toDouble()
                val seconds = times.third.toDouble()

                val dateCalulated = (hours + (minutes / 60)) + (seconds / 3600)
                val jd = falak.KMJD(tanggal, bulan, tahun, dateCalulated, localData.tzn)

                arahKiblatViewModel.dataBulanMatahari(
                    dateCalulated,
                    "toposentris",
                    falak.DeltaT(jd)
                )

            }

        }, 10)
    }

    private fun initBottomSheet() {
        val bottomSheetContent = bottomSheet
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetContent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_lock, menu)
        arahKiblatViewModel.isHold.observe(this, Observer {
            menu?.findItem(R.id.menu_open)?.isVisible = !it
            menu?.findItem(R.id.menu_lock)?.isVisible = it
        })
        return true
    }

    private fun isAnimationStop() {
        arahKiblatViewModel.isAnimationStop.observe(this, Observer {
            compass.setPause(it)

            val message = if (it) {
                "Rotasi Kompas telah di nonaktifkan."
            } else {
                "Rotasi Kompas telah di aktifkan."
            }

            CustomSnackbar.make(
                window.decorView.rootView,
                message,
                Snackbar.LENGTH_LONG,
                snackbarClicked
            )?.show()
        })
    }

    private fun setDataAfterPermissionGranted() {
        val sharedPref = SharedPref(this)
        val sharedPrefPrioritas = sharedPref.getValueString("prioritas_update", "using_gps")
        if (isPermissionGranted()) {
            if (gps.canGetLocation()) {
                try {
                    gps.getAddress()

                } catch (e: Exception) {
//                        Toast.makeText(requireContext(), "Lokasi Di Perbaharui", Toast.LENGTH_SHORT).show()
                }
            } else {
                if (sharedPrefPrioritas == "using_gps") {
                    if (!alertDialog.isShowing && !isGPSon()) {
                        alertDialog.show()
                    }
                }
            }
        }
    }

    private fun isGPSon(): Boolean {
        val manager: LocationManager =
            this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    private fun isPermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED
            ) {
                true
            } else {
                ActivityCompat.requestPermissions(
                    this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
                false
            }
        } else {
            true
        }
    }


}
