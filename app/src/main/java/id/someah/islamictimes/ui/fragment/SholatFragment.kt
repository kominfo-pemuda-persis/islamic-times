package id.someah.islamictimes.ui.fragment

import android.animation.ValueAnimator
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import androidx.core.content.ContextCompat
import androidx.core.view.forEachIndexed
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.model.InDateStyle
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.kizitonwose.calendarview.utils.*
import id.someah.islamictimes.R
import id.someah.islamictimes.adapters.WaktuSholatAdapter
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.databinding.CalendarDayLayoutBinding
import id.someah.islamictimes.databinding.FragmentSholatBinding
import id.someah.islamictimes.ui.activity.CetakWaktuSholatActivity
import id.someah.islamictimes.viewmodel.WaktuSholatByDateViewModel
import id.someah.islamictimes.viewmodel.WaktuSholatByDateViewModelFactory
import kotlinx.android.synthetic.main.fragment_dashboard.list_waktu_sholat
import kotlinx.android.synthetic.main.fragment_sholat.*
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.time.temporal.WeekFields
import java.util.*

/*
 class untuk menampilkan waktu sholat berdasarkan tanggal yang dipillih
 library calendar view yang digunakan dan referensi penggunaan : https://github.com/kizitonwose/CalendarView
*/
@Suppress("NAME_SHADOWING")
class SholatFragment : Fragment() {

    private lateinit var binding : FragmentSholatBinding
    private var selectedDate : LocalDate? = null
    private val today = LocalDate.now()
    private val dateFormat = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy")
    private lateinit var waktuSholatByDateViewModel: WaktuSholatByDateViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentSholatBinding.inflate(inflater,container,false)
//        val view = inflater.inflate(R.layout.fragment_sholat, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding = FragmentSholatBinding.bind(view)
        (activity as AppCompatActivity?)?.setSupportActionBar(binding.toolbarWaktuSholat)
        setHasOptionsMenu(true)
        val daysOfWeek = daysOfWeekFromLocale()
        //legend layout untuk menampilkan hari minggu-sabtu
        binding.legendLayout.legendLayout.forEachIndexed { index, view->
            (view as TextView).apply {
                text = daysOfWeek[index].getDisplayName(TextStyle.SHORT, Locale.getDefault()).toUpperCase(Locale.ENGLISH)
            }
        }

        //set data tanggal
        class DayViewContainer(view :View) : ViewContainer(view){
            lateinit var day: CalendarDay
            val textViewDay = CalendarDayLayoutBinding.bind(view).calendarDayText
            init {
                //notify calendar ketika tanggal di click
                textViewDay.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH) {
                        if (selectedDate == day.date) {
                            selectedDate = null
                            binding.calendarView.notifyDayChanged(day)
                        } else {
                            val oldDate = selectedDate
                            selectedDate = day.date
                            binding.calendarView.notifyDateChanged(day.date)
                            oldDate?.let { binding.calendarView.notifyDateChanged(oldDate) }
                        }
                    }
                }
            }
        }

        binding.calendarView.dayBinder = object : DayBinder<DayViewContainer>{
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                val textView = container.textViewDay
                textView.text = day.date.dayOfMonth.toString()

                if(day.owner == DayOwner.THIS_MONTH){
                    when(day.date){
                        //change background dari tanggal yang dipilih
                        selectedDate -> {
                            Log.e("selected date binder", selectedDate.toString())
                            getDataFromViewModel(selectedDate!!.dayOfMonth.toByte(), selectedDate!!.monthValue.toByte(), selectedDate!!.year.toLong())
                            textView.setTextColor(ContextCompat.getColor(context!!, android.R.color.white))
                            textView.setBackgroundResource(R.drawable.indikator_selected_date)
                            binding.toolbarWaktuSholat.title = dateFormat.format(selectedDate)
                        }
                        today-> {
                            //change background dari tanggal sekarang
                            Log.e("today binder", today.toString())
                            getDataFromViewModel(today.dayOfMonth.toByte(), today.monthValue.toByte(), today.year.toLong())
                            textView.setTextColor(ContextCompat.getColor(context!!, R.color.colorPrimary))
                            textView.setBackgroundResource(R.drawable.indikator_date_now)
                            binding.toolbarWaktuSholat.title = dateFormat.format(today)
                        }
                        else -> {
                            textView.setTextColor(ContextCompat.getColor(context!!, android.R.color.black))
                            textView.background = null
                        }
                    }
                }
                else{
                    textView.setTextColor(ContextCompat.getColor(context!!, android.R.color.darker_gray))
                    textView.background = null
                }
            }
            override fun create(view: View): DayViewContainer = DayViewContainer(view)
        }

        binding.calendarView.monthScrollListener = {
            val oneWeekHeight = binding.calendarView.daySize.height
            val oneMonthHeight = oneWeekHeight * 6
            /*toggle mode dari calendar*/
            //week mode
            Log.d("contentValues", "onViewCreated: ${binding.calendarView.maxRowCount}")
            if(binding.calendarView.maxRowCount == 6){
                Log.d("HEIGHT MONTH", "$oneMonthHeight $oneWeekHeight")
                binding.imageViewToggleWeekMonth.setOnClickListener {
                    binding.imageViewToggleWeekMonth.tag = R.drawable.ic_week_mode
                    if(binding.imageViewToggleWeekMonth.tag == R.drawable.ic_week_mode){
                        val animator = ValueAnimator.ofInt(oneMonthHeight, oneWeekHeight)
                        animator.addUpdateListener { animator ->
                            binding.calendarView.updateLayoutParams {
                                height = animator.animatedValue as Int
                            }
                        }
                        animator.doOnStart {
                            binding.calendarView.updateMonthConfiguration(
                                inDateStyle = InDateStyle.FIRST_MONTH,
                                maxRowCount = 1,
                                hasBoundaries = false
                            )
//                            binding.calendarView.apply {
//                                inDateStyle = InDateStyle.FIRST_MONTH
//                                maxRowCount = 1
//                                hasBoundaries = false
//                            }
                        }
                        animator.doOnEnd {
                            binding.calendarView.scrollToDate(LocalDate.now())
                        }
                        animator.duration = 250
                        animator.start()

                        imageViewToggleWeekMonth.setImageResource(R.drawable.ic_dropdown)
                }
                    binding.calendarView.notifyCalendarChanged()
                }
            }
            else{
                //month mode
                Log.d("HEIGHT WEEK", "$oneMonthHeight $oneWeekHeight")
                val firstDate = it.weekDays.first().first().date
                val lastDate = it.weekDays.last().last().date
                val endMonth = YearMonth.now().plusMonths(10)
                binding.imageViewToggleWeekMonth.setOnClickListener {

                    binding.imageViewToggleWeekMonth.tag = R.drawable.ic_dropdown
                    if(binding.imageViewToggleWeekMonth.tag == R.drawable.ic_dropdown){
                        Log.d("dropdown", " true")
                        val animator = ValueAnimator.ofInt(oneWeekHeight, oneMonthHeight)
                        animator.addUpdateListener { animator ->
                            binding.calendarView.updateLayoutParams {
                                height = animator.animatedValue as Int
                            }
                        }
                        animator.doOnStart {
//                            binding.calendarView.apply {
//                                inDateStyle = InDateStyle.ALL_MONTHS
//                                maxRowCount = 6
//                                hasBoundaries = true
//                            }
                            binding.calendarView.updateMonthConfiguration(
                                inDateStyle = InDateStyle.ALL_MONTHS,
                                maxRowCount = 6,
                                hasBoundaries = true
                            )
                        }
                        animator.doOnEnd {
                            if (firstDate.yearMonth == lastDate.yearMonth) {
                                binding.calendarView.scrollToMonth(firstDate.yearMonth)
                            } else {
                                binding.calendarView.scrollToMonth(minOf(firstDate.yearMonth.next, endMonth))
                            }
                        }
                        animator.duration = 250
                        animator.start()
                        imageViewToggleWeekMonth.setImageResource(R.drawable.ic_week_mode)
                    }

                    binding.calendarView.notifyCalendarChanged()


                }

            }

        }

        //inisialisasi week mode calendar
        val currentMonth = YearMonth.now()
        val endMonth = currentMonth.plusMonths(10)
        binding.calendarView.setup(currentMonth, endMonth, daysOfWeek.first())
        binding.calendarView.updateMonthConfiguration(
            inDateStyle = InDateStyle.FIRST_MONTH,
            maxRowCount = 1,
            hasBoundaries = false
        )
//        binding.calendarView.apply {
//            inDateStyle = InDateStyle.ALL_MONTHS
//            inDateStyle = InDateStyle.ALL_MONTHS
//            maxRowCount = 6
//            hasBoundaries = true
//        }
        binding.calendarView.scrollToDate(LocalDate.now())
    }

    private fun daysOfWeekFromLocale(): Array<DayOfWeek> {
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        var daysOfWeek = DayOfWeek.values()
        if (firstDayOfWeek != DayOfWeek.MONDAY) {
            val rhs = daysOfWeek.sliceArray(firstDayOfWeek.ordinal..daysOfWeek.indices.last)
            val lhs = daysOfWeek.sliceArray(0 until firstDayOfWeek.ordinal)
            daysOfWeek = rhs + lhs
        }
        return daysOfWeek
    }

    //fungsi untk get data dari viewmodel
    private fun getDataFromViewModel(tanggal : Byte, bulan : Byte, tahun : Long) {
        val adapter = WaktuSholatAdapter(context as Activity)
        val dao = DaoManager()
        val viewModelFactoryWaktuSholat =
            WaktuSholatByDateViewModelFactory(
                dao.waktuSholatDao(
                    context as Activity
                )!!
            )
        waktuSholatByDateViewModel = ViewModelProvider(
            this,
            viewModelFactoryWaktuSholat
        ).get(WaktuSholatByDateViewModel::class.java)
        binding.listWaktuSholat.hasFixedSize()
        binding.listWaktuSholat.layoutManager = LinearLayoutManager(context)
        binding.listWaktuSholat.adapter = adapter
        binding.listWaktuSholat.visibility = View.INVISIBLE
        waktuSholatByDateViewModel.getWaktuSholatByDate(tanggal, bulan, tahun, context as Activity)
        waktuSholatByDateViewModel.dataWaktuSholatByDate.observe(viewLifecycleOwner, Observer {
            adapter.setList(it)
            list_waktu_sholat.visibility = View.VISIBLE
            adapter.notifyDataSetChanged()
        })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_cetak, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_cetak){
            startActivity(Intent(context, CetakWaktuSholatActivity::class.java))
        }
        return true
    }
}



