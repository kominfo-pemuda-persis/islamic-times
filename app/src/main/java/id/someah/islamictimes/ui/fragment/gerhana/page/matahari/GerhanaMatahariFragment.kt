package id.someah.islamictimes.ui.fragment.gerhana.page.matahari

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.databinding.FragmentGerhanaMatahariBinding
import id.someah.islamictimes.ui.fragment.gerhana.DaftarGerhanaController
import id.someah.islamictimes.ui.fragment.gerhana.PageState
import id.someah.islamictimes.ui.fragment.gerhana.listener.OnFilterSelectedListener
import id.someah.islamictimes.ui.fragment.gerhana.listener.OnItemSelected
import id.someah.islamictimes.util.BaseFragment
import id.someah.islamictimes.util.EventObserver
import kotlinx.android.synthetic.main.fragment_gerhana_matahari.*


@AndroidEntryPoint
class GerhanaMatahariFragment constructor(val parentListener: OnItemSelected) : BaseFragment() , OnFilterSelectedListener {

    lateinit var binding : FragmentGerhanaMatahariBinding
    val viewModel : GerhanaMatahariViewModel by viewModels()

    lateinit var controller: DaftarGerhanaMatahariController

    lateinit var listener : OnFilterSelectedListener

    override fun initializedViewModel() {
        viewModel.getDataMatahari.observe(this, Observer {
            controller.setData(it)
            viewModel.stopLoading()
        })

        viewModel.loading.observe(this , Observer {
            if(it) {
                binding.shimmerGerhanaMatahari.visibility = View.VISIBLE
                binding.rvDaftarGerhanaMatahari.visibility = View.INVISIBLE
            } else {
                binding.shimmerGerhanaMatahari.visibility = View.GONE
                binding.rvDaftarGerhanaMatahari.visibility = View.VISIBLE
            }
        })

        viewModel.navigateToDetail.observe(this , EventObserver {
            parentListener.onItemSelected(it.month,it.year,PageState.MATAHARI)

            viewModel.cancelJob()
        })
    }

    override fun inflateView() {
        if (this::binding.isInitialized) {

            listener  = this
            binding.shimmerGerhanaMatahari.startShimmer()

            binding.rvDaftarGerhanaMatahari.apply {

                controller = DaftarGerhanaMatahariController(viewModel)
                adapter = controller.adapter
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentGerhanaMatahariBinding.inflate(inflater,container,false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onSelected(month: Byte, year: Long) {
        if(month == (0).toByte() ) {
            viewModel.mHijriYear.value = year
            viewModel.get()
        } else {
            viewModel.cancelJob()
        }
    }

    override fun onPause() {
        super.onPause()
        shimmer_gerhana_matahari.stopShimmer()
    }

    override fun onResume() {
        super.onResume()
        shimmer_gerhana_matahari.startShimmer()
        viewModel.get()
    }
}