package id.someah.islamictimes.ui.fragment.gerhana.page.matahari

import android.util.Log
import com.airbnb.epoxy.TypedEpoxyController
import id.someah.islamictimes.ItemGerhanaBindingModel_
import id.someah.islamictimes.ItemGerhanaEmptyBindingModel_
import id.someah.islamictimes.ItemGerhanaMatahariBindingModel_
import id.someah.islamictimes.data.DataGerhanaMatahari
import id.someah.islamictimes.ui.fragment.gerhana.InformasiGerhanaBulan
import id.someah.islamictimes.util.EventListener

class DaftarGerhanaMatahariController constructor(val eventListener: EventListener<DataGerhanaMatahari>) : TypedEpoxyController<List<DataGerhanaMatahari>>() {
    override fun buildModels(data: List<DataGerhanaMatahari>?) {


        if(data?.size ?: 0 == 0) {
            ItemGerhanaEmptyBindingModel_()
                .id(1)
                .message("Tidak Ada gerhana Matahari")
                .addTo(this)
        } else {
            data?.forEachIndexed { index, dataGerhana ->
                Log.d("Controller", "buildModels: $dataGerhana")
                ItemGerhanaMatahariBindingModel_()
                    .id(index)
                    .data(dataGerhana)
                    .eventListener(eventListener)
                    .addTo(this)
            }
        }
    }
}