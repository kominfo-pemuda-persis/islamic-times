package id.someah.islamictimes.ui.fragment.gerhana

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.button.MaterialButton
import com.google.android.material.tabs.TabLayoutMediator
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.databinding.FragmentDaftarGerhanaBinding
import id.someah.islamictimes.ui.fragment.gerhana.listener.OnItemSelected
import id.someah.islamictimes.ui.fragment.gerhana.page.bulan.GerhanaBulanFragment
import id.someah.islamictimes.ui.fragment.gerhana.page.matahari.GerhanaMatahariFragment
import id.someah.islamictimes.util.*
import kotlinx.android.synthetic.main.fragment_daftar_gerhana_.*
import java.util.*

@AndroidEntryPoint
class DaftarGerhanaFragment : BaseFragment(), IslamicCustomDialogBaseState, OnItemSelected {


    lateinit var binding: FragmentDaftarGerhanaBinding
    lateinit var mCustomDialog: IslamicCustomDialog
    lateinit var dataHijriah: List<String>

    private val viewModel: DaftarGerhanaViewModel by viewModels()
    lateinit var listener: OnItemSelected

    init {
        mCustomDialog = IslamicCustomDialog(R.layout.custom_dialog_filter)
        mCustomDialog.state = this
    }

    override fun initializedViewModel() {
        viewModel.listHijriah.observe(this, Observer {
            if (this::mSpinner.isInitialized) {
                dataHijriah = it
                mSpinner.apply {
                    adapter = ArrayAdapter(
                        requireContext(),
                        R.layout.support_simple_spinner_dropdown_item,
                        dataHijriah
                    )
                }
            }
        })
    }

    lateinit var mSpinner: Spinner
    var mSelectedFragment: Fragment? = null
    override fun inflateView() {
        if (this::binding.isInitialized) {
            listener = this

            binding.vpGerhana.apply {
                adapter = DaftarGerhanaViewPagerAdapter(this@DaftarGerhanaFragment, listener)
                registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageSelected(position: Int) {
                        binding.vpGerhana.currentItem.let {
                            val temp = childFragmentManager.findFragmentByTag("f${it}")

                            when (temp) {
                                is GerhanaBulanFragment -> {
                                    mSelectedFragment = (temp as GerhanaBulanFragment)
                                }

                                is GerhanaMatahariFragment -> {
                                    mSelectedFragment = temp
                                }
                            }

                        }

                        super.onPageSelected(position)
                    }

                    override fun onPageScrollStateChanged(state: Int) {
                        binding.vpGerhana.currentItem.let {
                            val temp = childFragmentManager.findFragmentByTag("f${it}")

                            when (temp) {
                                is GerhanaBulanFragment -> {
                                    mSelectedFragment = (temp as GerhanaBulanFragment)
                                }

                                is GerhanaMatahariFragment -> {
                                    mSelectedFragment = temp
                                }
                            }

                        }

                        Log.d("page", "onPageSelected: Changed")

                        super.onPageScrollStateChanged(state)
                    }
                }
                )
            }


            TabLayoutMediator(binding.tabLayout, binding.vpGerhana) { tab, pos ->
                tab.text = TITLE[pos]
            }.attach()

//            binding.shimmerGerhanaBulan.startShimmer()
            mCustomDialog.init(requireContext())
            mCustomDialog.mDialog.run {
                val editTextHijriah = this.findViewById<TextInputEditText>(R.id.editTextHijriah)
                val btnShowData = this.findViewById<MaterialButton>(R.id.btnSubmitFilterGerhana)
                val year = this.findViewById<TextInputEditText>(R.id.editTextTahun)

                mSpinner = this.findViewById(R.id.spinnerHijriah)
                mSpinner.onItemSelectedListener = object : AdapterView.OnItemClickListener,
                    AdapterView.OnItemSelectedListener {
                    override fun onItemClick(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        editTextHijriah.setText(dataHijriah[position])
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }

                editTextHijriah.setOnClickListener {
                    mSpinner.performClick()
                }

                btnShowData.setOnClickListener {
                    mCustomDialog.dismissCustomDialog()
                }

                year.doOnTextChanged { text, start, before, count ->
                    btnShowData.isEnabled = count != 0
                }

            }

            toolbar.apply {
                setNavigationOnClickListener {
                    requireActivity().finish()
                }
                setOnMenuItemClickListener {
                    if (it.itemId == R.id.btnFilterGerhana) {
                        mCustomDialog.showCustomDialog()
                    }
                    true
                }
            }

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDaftarGerhanaBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun result(res: Dialog) {
        val month = res.findViewById<TextInputEditText>(R.id.editTextHijriah).text.toString()
        val numberOfMonth = Constant.BULAN_HIJRI.find {
            it.first.equals(month, ignoreCase = true)
        }?.second
        viewModel.mHijriMonth.value = numberOfMonth?.toByte()

        val year = res.findViewById<TextInputEditText>(R.id.editTextTahun).text.toString()
        val numberOfYear = year.toLong()
        viewModel.mHijriYear.value = numberOfYear

        if (month.toLowerCase(Locale.ROOT) == "semua") {

            if (mSelectedFragment != null) {
                when (mSelectedFragment) {
                    is GerhanaBulanFragment -> {
                        (mSelectedFragment as GerhanaBulanFragment).listener.onSelected(
                            numberOfMonth?.toByte() ?: 0,
                            numberOfYear
                        )
                    }

                    is GerhanaMatahariFragment -> {
                        (mSelectedFragment as GerhanaMatahariFragment).listener.onSelected(
                            numberOfMonth?.toByte() ?: 0,
                            numberOfYear
                        )
                    }
                }
            }


        } else
            if(mSelectedFragment != null) {
                var defaultState = PageState.BULAN
                when(mSelectedFragment) {
                    is GerhanaMatahariFragment -> {

                        (mSelectedFragment as GerhanaMatahariFragment).listener.onSelected(
                            numberOfMonth?.toByte() ?: 0,
                            numberOfYear
                        )

                        defaultState = PageState.MATAHARI
                    }

                    is GerhanaBulanFragment -> {
                        (mSelectedFragment as GerhanaBulanFragment).listener.onSelected(
                            numberOfMonth?.toByte() ?: 0,
                            numberOfYear
                        )
                    }
                }

                findNavController().navigate(
                    DaftarGerhanaFragmentDirections.actionNavDaftarGerhanaToNavDaftarGerhanaDetail(
                        numberOfMonth ?: 0, numberOfYear,defaultState
                    )
                )
            }

    }

    override fun onPause() {
        super.onPause()
//        binding.shimmerGerhanaBulan.stopShimmer()
    }

    override fun onResume() {
        super.onResume()
        viewModel.reinit()
    }

    companion object {
        val TITLE = listOf("Gerhana Bulan", "Gerhana Matahari")
    }

    override fun onItemSelected(month: Byte, year: Long, state: PageState) {
        findNavController().navigate(
            DaftarGerhanaFragmentDirections.actionNavDaftarGerhanaToNavDaftarGerhanaDetail(
                month.toInt(),
                year,
                state
            )
        )
    }
}

class DaftarGerhanaViewPagerAdapter(fa: Fragment, val listener: OnItemSelected) :
    FragmentStateAdapter(fa) {
    override fun getItemCount(): Int {
        return PAGE.size
    }

    val PAGE = listOf(
        GerhanaBulanFragment(listener),
        GerhanaMatahariFragment(listener)
    )

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                GerhanaBulanFragment(listener)
            }
            else -> GerhanaMatahariFragment(listener)
        }
    }


}