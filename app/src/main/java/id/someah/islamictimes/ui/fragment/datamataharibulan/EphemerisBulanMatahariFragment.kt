package id.someah.islamictimes.ui.fragment.datamataharibulan

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.R
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.services.utils.hideKeyboard
import id.someah.islamictimes.services.utils.validateDate
import id.someah.islamictimes.viewmodel.EphemerisMatahariBulanViewModel
import id.someah.islamictimes.viewmodel.EphemerisMatahariBulanViewModelFactory
import kotlinx.android.synthetic.main.fragment_ephemeris_bulan_matahari.*
import java.util.*
import kotlin.collections.ArrayList

class EphemerisBulanMatahariFragment : Fragment() {

    private var jam : String? = null
    private var menit : String? = null
    private var detik : String? = null
    private var pilihanDeltaT : String? = null
    private var valueDeltaT : Double? = 0.0
    private val calendar : Calendar = Calendar.getInstance()
    private val tanggal: Byte       = calendar.get(Calendar.DAY_OF_MONTH).toByte()
    private val bulan: Byte         = (calendar.get(Calendar.MONTH) + 1).toByte() //di plus 1 karena output bulan dimulai dari 0
    private val tahun: Long         = calendar.get(Calendar.YEAR).toLong()

    val falak = FalakLib()
    private lateinit var ephemerisMatahariBulanViewModel: EphemerisMatahariBulanViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ephemeris_bulan_matahari, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //setupSpinner
        setUpAllSpinner()

        jam = spinnerJam.selectedItem.toString()
        menit = spinnerMenit.selectedItem.toString()
        detik = spinnerDetik.selectedItem.toString()

        //get value delta T sesuai pilihan spinner
        Log.d("pilihan delta T uwu", pilihanDeltaT!!)

//        textPilihTanggal.setOnClickListener {
//            showDatePicker()
//        }
        textPilihTanggal1.setEndIconOnClickListener {
            showDatePicker()
        }

        //inisialisasi card view invisible
        cardViewMatahari.visibility = View.INVISIBLE
        cardViewBulan.visibility = View.INVISIBLE

        buttonHitung.setOnClickListener {

            activity?.hideKeyboard()

            if(textPilihTanggal.text.toString().isEmpty()){
                Toast.makeText(context as Activity, getString(R.string.error_message_pilih_tanggal), Toast.LENGTH_SHORT).show()
            }
            else if(pilihanDeltaT == getString(R.string.manual_delta_t) && editTextDeltaTManual.text.isEmpty()){
                Toast.makeText(context as Activity, getString(R.string.error_message_input_delta_t), Toast.LENGTH_SHORT).show()
            }
            else {
                if (validateDate(textPilihTanggal.text.toString())) {
                    //input delta t tidak kosong ambil value delta t dari edit text
                    if (editTextDeltaTManual.text.isNotEmpty()) {
                        valueDeltaT = editTextDeltaTManual.text.toString().toDouble()
                    }
                    Log.d("value delta T", "$valueDeltaT")
                    //get valaue deltaT dari edit text
                    //cardview visible
                    cardViewMatahari.visibility = View.VISIBLE
                    cardViewBulan.visibility = View.VISIBLE

                    //inisialisasi data dari viewmodel
                    val dao = DaoManager()
                    val viewModelFactory =
                        EphemerisMatahariBulanViewModelFactory(
                            dao.dataEphemersisBulanMatahari(
                                context as Activity
                            )!!
                        )
                    ephemerisMatahariBulanViewModel = ViewModelProvider(
                        this,
                        viewModelFactory
                    ).get(EphemerisMatahariBulanViewModel::class.java)

                    //split text view buat dapetin tanggal, bulan, tahun


//                val pilihanTanggal = textPilihTanggal.text.toString().split("-")
                    val pilihanTanggal = textPilihTanggal.text.toString().split("/")
                    val tanggal = pilihanTanggal[0]
                    val bulan = pilihanTanggal[1]
                    val tahun = pilihanTanggal[2]

                    //set data viewmodel
                    ephemerisMatahariBulanViewModel.getEphemersisMatahariByDateHour(
                        tanggal.toByte(), bulan.toByte(), tahun.toLong(),
                        jam!!.toInt(), menit!!.toInt(), detik!!.toInt(),
                        valueDeltaT!!,
                        context as Activity
                    )

                    //get data dari viewmodel
                    ephemerisMatahariBulanViewModel.ephemerisMatahariByDateHour.observe(
                        viewLifecycleOwner,
                        androidx.lifecycle.Observer {
                            textApparentLongitudeMatahari.text = it.apparentLongitude
                            textEclipticLatitude.text = "${it.eclipticLatitude}\""
                            textApparentRightMatahari.text = it.apparentRightAscension
                            textApparentDecMatahari.text = it.apparentDeclination
                            textHorizontalParallaxMatahari.text = it.horizontalParallax
                            textSemiDiameterMatahari.text = it.semiDiameter
                            textTrueGeoDist.text = it.trueGeocentDist
                            textEquationOfTime.text = it.equationOfTime
                            textGhaMatahari.text = it.gha
                        })

                    ephemerisMatahariBulanViewModel.getEphemersisBulanByDateHour(
                        tanggal.toByte(), bulan.toByte(), tahun.toLong(),
                        jam!!.toInt(), menit!!.toInt(), detik!!.toInt(),
                        valueDeltaT!!, context as Activity
                    )
                    ephemerisMatahariBulanViewModel.ephemerisBulanByDateHour.observe(
                        viewLifecycleOwner,
                        androidx.lifecycle.Observer {
                            textApparentLongitudeBulan.text = it.apparentLongitude
                            textApparentLatitudeBulan.text = it.apparentLatitude
                            textApparentRightAscBulan.text = it.apparentRightAscension
                            textApparentDeclinationBulan.text = it.apparentDeclination
                            textHorizontalParallaxBulan.text = it.horizontalParallax
                            textSemiDiameterBulan.text = it.semiDiameter
                            textAngleBrightLimb.text = it.angleBrightLimb
                            textFractionIllumination.text = it.fractionIllumination
                            textGhaBulan.text = it.gha
                        })

                } else {
                    Toast.makeText(context as Activity, getString(R.string.error_format_tanggal_tidak_sesuai), Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    @SuppressLint("SetTextI18n")
    //date picker dialog
    private fun showDatePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            context as Activity,
            DatePickerDialog.OnDateSetListener { _, _year, monthOfYear, dayOfMonth ->
                textPilihTanggal.setText( "$dayOfMonth/${monthOfYear + 1}/$_year" ) //set tanggal bulan tahun ke textview
            },
            year,
            month,
            day
        )
        datePickerDialog.show()
    }

    private fun setUpAllSpinner(){
        val localData = LocalData(context as Activity)
        val arrayListJam = ArrayList<String>()
        val arrayListMenit = ArrayList<String>()
        val arrayListDetik = ArrayList<String>()
        val arrayListDeltaT = ArrayList<String>()
        //isi spinnerDeltaT
        val arrDeltaT = arrayOf(getString(R.string.prediksi_delta_t), getString(R.string.manual_delta_t), getString(R.string.abaikan_delta_t))

        //looping tambah data ke arraylist buat tampil data nya ke spinner
        for(i in arrDeltaT.indices){
            arrayListDeltaT.add(arrDeltaT[i])
        }
        for(i in 0 until 60){
            if(i.toString().length < 2){
                arrayListMenit.add("0$i")
                arrayListDetik.add("0$i")
            }else {
                arrayListMenit.add(i.toString())
                arrayListDetik.add(i.toString())
            }
        }
        for (i in 0 until 25){
            if(i.toString().length < 2){
                arrayListJam.add("0$i")
            }else {
                arrayListJam.add(i.toString())
            }
        }

        //set adapter deltaT
        val adapterDeltaT : ArrayAdapter<String> = ArrayAdapter(context as Activity, android.R.layout.simple_spinner_dropdown_item, arrayListDeltaT)
        adapterDeltaT.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDeltaTManual.adapter = adapterDeltaT
        spinnerDeltaTManual.setSelection(arrayListDeltaT.indexOf("abaikan"))
        pilihanDeltaT = spinnerDeltaTManual.selectedItem.toString()
        spinnerDeltaTManual.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                pilihanDeltaT = parent?.getItemAtPosition(position).toString()
                when (pilihanDeltaT) {
                    getString(R.string.abaikan_delta_t) -> {
                        textViewDeltaTManual.visibility = View.GONE
                        editTextDeltaTManual.visibility = View.GONE
                        valueDeltaT = 0.0
                    }
                    getString(R.string.prediksi_delta_t) -> {
                        textViewDeltaTManual.visibility = View.GONE
                        editTextDeltaTManual.visibility = View.GONE
                        val jamDesimal = (jam!!.toDouble() + (menit!!.toDouble() / 60)) + (detik!!.toDouble() / 3600)
                        val jd      : Double = falak.KMJD(tanggal,bulan,tahun,jamDesimal,localData.tzn)
                        valueDeltaT =  falak.DeltaT(jd)
                    }
                    getString(R.string.manual_delta_t) -> {
                        textViewDeltaTManual.visibility = View.VISIBLE
                        editTextDeltaTManual.visibility = View.VISIBLE
                    }
                }

            }

        }


        //set adapter jam
        val adapterJam : ArrayAdapter<String> = ArrayAdapter(context as Activity, android.R.layout.simple_spinner_item, arrayListJam)
        adapterJam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerJam.adapter = adapterJam
        spinnerJam.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                jam = parent?.getItemAtPosition(position).toString()
            }

        }

        //set adapter menit
        val adapterMenit : ArrayAdapter<String> = ArrayAdapter(context as Activity, android.R.layout.simple_spinner_item, arrayListMenit)
        adapterJam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerMenit.adapter = adapterMenit
        spinnerMenit.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                menit = parent?.getItemAtPosition(position).toString()
            }

        }

        //set adapter detik
        val adapterDetik : ArrayAdapter<String> = ArrayAdapter(context as Activity, android.R.layout.simple_spinner_item, arrayListDetik)
        adapterJam.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDetik.adapter = adapterDetik
        spinnerDetik.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                detik = parent?.getItemAtPosition(position).toString()
            }

        }
    }


}