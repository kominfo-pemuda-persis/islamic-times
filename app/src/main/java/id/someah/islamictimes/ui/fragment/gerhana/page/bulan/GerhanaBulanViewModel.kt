package id.someah.islamictimes.ui.fragment.gerhana.page.bulan

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.data.usesace.GetJenisDanWaktuGerhana
import id.someah.islamictimes.ui.fragment.gerhana.InformasiGerhanaBulan
import id.someah.islamictimes.util.Event
import id.someah.islamictimes.util.EventListener
import kotlinx.coroutines.*
import javax.inject.Inject

@HiltViewModel
class GerhanaBulanViewModel @Inject constructor(
    val getJenisDanWaktuGerhana: GetJenisDanWaktuGerhana
) : ViewModel() , EventListener<InformasiGerhanaBulan> {

    override fun openDetail(item: InformasiGerhanaBulan) {
        _navigateToDetail.value = Event(item)
    }

    private var job = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + job)

    private val _listHijriah = MutableLiveData<List<String>>()
    val listHijriah : LiveData<List<String>> get() = _listHijriah

    private val _getDataBulan = MutableLiveData<List<InformasiGerhanaBulan>>()
    val getDataBulan : LiveData<List<InformasiGerhanaBulan>> get() = _getDataBulan

    private val _loading = MutableLiveData<Boolean>()
    val loading : LiveData<Boolean> get() = _loading

    val mHijriMonth = MutableLiveData<Byte>()
    val mHijriYear = MutableLiveData<Long>()

    private val _navigateToDetail = MutableLiveData<Event<InformasiGerhanaBulan>>()
    val navigateToDetail : LiveData<Event<InformasiGerhanaBulan>> get() = _navigateToDetail

    init {
        mHijriYear.value = 1443


        reinit()
    }

    fun reinit() {
        var temp = ArrayList<String>()

        temp.add("Semua")
        Constant.BULAN_HIJRI.forEach {
            temp.add(it.first)
        }
        _listHijriah.value = temp.toList()
        get()
    }

    fun get() {
        _loading.value = true

        val year = mHijriYear.value ?: 1443

        uiScope.launch {
            var temp = ArrayList<InformasiGerhanaBulan>()

            Constant.BULAN_HIJRI.map {
                    data ->
                async {
                    var description = getDataJenisDanWaktu(data.second.toByte() , year.toLong() )

                    if(!description.toLowerCase().contains("tidak terjadi gerhana")) {
                        if(description.contains(",")) {
                            val formatedDescription = description.split(",")
                            if(formatedDescription.size > 2) {
                                description = "${formatedDescription.first()},${formatedDescription[1]}"
                            }
                        }

                        temp.add(
                            InformasiGerhanaBulan(
                                "${data.first} $year H",
                                description,
                                data.second.toByte(),
                                year.toLong()
                            )
                        )
                    }
                }
            }.awaitAll()

            _getDataBulan.value = temp.toList()

        }.start()


    }

    private suspend fun getDataJenisDanWaktu(bulanHijri : Byte , tahunHijri: Long) = withContext(
        Dispatchers.IO){
        getJenisDanWaktuGerhana(Pair(bulanHijri,tahunHijri))
    }

    override fun onCleared() {
        job.cancel()
    }

    fun stopLoading() {
        _loading.value = false
    }

}