package id.someah.islamictimes.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import dagger.hilt.android.AndroidEntryPoint
import id.someah.islamictimes.R
import id.someah.islamictimes.databinding.ActivityGerhanaBinding

lateinit var binding : ActivityGerhanaBinding

@AndroidEntryPoint
class GerhanaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this , R.layout.activity_gerhana)

    }
}