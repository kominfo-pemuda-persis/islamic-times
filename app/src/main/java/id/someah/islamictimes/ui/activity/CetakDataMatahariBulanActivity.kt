package id.someah.islamictimes.ui.activity

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.viewmodel.EphemerisMatahariBulanViewModel
import id.someah.islamictimes.viewmodel.EphemerisMatahariBulanViewModelFactory
import  androidx.lifecycle.Observer
import id.someah.islamictimes.*
import id.someah.islamictimes.constant.Common
import id.someah.islamictimes.constant.Constant
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.services.utils.desimalFormat
import id.someah.islamictimes.services.utils.validateDate
import id.someah.islamictimes.util.DateUtil
import com.itextpdf.text.*
import com.itextpdf.text.pdf.*
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_cetak_data_matahari_bulan.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.util.*
import kotlin.math.floor

class CetakDataMatahariBulanActivity : AppCompatActivity() {
    private lateinit var ephemerisMatahariBulanViewModel: EphemerisMatahariBulanViewModel
    private val falak = FalakLib()
    private var pilihanDeltaT : String? = null
    private var valueDeltaT : Double = 0.0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cetak_data_matahari_bulan)


        val arrayListDeltaT = ArrayList<String>()
        //isi spinnerDeltaT
        val arrDeltaT = arrayOf(getString(R.string.prediksi_delta_t), getString(R.string.manual_delta_t), getString(R.string.abaikan_delta_t))

        //looping tambah data ke arraylist buat tampil data nya ke spinner
        for(i in arrDeltaT.indices){
            arrayListDeltaT.add(arrDeltaT[i])
        }
        val adapterDeltaT : ArrayAdapter<String> = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_dropdown_item, arrayListDeltaT)
        adapterDeltaT.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDeltaTCetak.adapter = adapterDeltaT
        spinnerDeltaTCetak.setSelection(arrayListDeltaT.indexOf("abaikan"))
        pilihanDeltaT = spinnerDeltaTCetak.selectedItem.toString()
        spinnerDeltaTCetak.onItemSelectedListener = object  : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                pilihanDeltaT = parent?.getItemAtPosition(position).toString()//get value pilihan delta T ketika pilihan berubah
                when (pilihanDeltaT) {
                    getString(R.string.abaikan_delta_t) -> {
                        textViewDeltaTCetak.visibility = View.GONE
                        editTextDeltaTCetak.visibility = View.GONE
                        valueDeltaT = 0.0
                    }
                    getString(R.string.prediksi_delta_t) -> {
                        textViewDeltaTCetak.visibility = View.GONE
                        editTextDeltaTCetak.visibility = View.GONE
                    }
                    getString(R.string.manual_delta_t) -> {
                        textViewDeltaTCetak.visibility = View.VISIBLE
                        editTextDeltaTCetak.visibility = View.VISIBLE
                    }
                }

            }

        }
        //akhir set spinner

        //check permission pake library dexter
        Dexter.withContext(this)
            .withPermissions(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if(report!!.areAllPermissionsGranted()){
                        //permission granted
                        buttonCetakDataMatahariBulan.setOnClickListener {
                            //validasi form
                            if (textViewPilihTanggal.text.toString().isEmpty()) {
                                Toast.makeText(
                                    applicationContext,
                                    getString(R.string.error_message_pilih_tanggal),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                            else if(pilihanDeltaT == getString(R.string.manual_delta_t) && editTextDeltaTCetak.text.isEmpty()){
                                Toast.makeText(applicationContext, R.string.error_message_input_delta_t, Toast.LENGTH_SHORT).show()
                            }
                            else {
                                //CreatePDF ketika form sudah divalidasi
                                if(validateDate(textViewPilihTanggal.text.toString())) {
                                    val splitDate = textViewPilihTanggal.text.toString().split('/');
                                    val fileName =
                                        "Ephemersis Bulan dan Matahari ${splitDate.first()}-${splitDate[1]}-${splitDate.last()}.pdf"
                                    Log.e(
                                        "test",
                                        Common.getAppPath(this@CetakDataMatahariBulanActivity) + fileName
                                    )
                                    createPDFFile(Common.getAppPath(this@CetakDataMatahariBulanActivity) + fileName)
                                } else {
                                    Toast.makeText(
                                        applicationContext,
                                        getString(R.string.error_format_tanggal_tidak_sesuai),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }

                    }
                    if(report.isAnyPermissionPermanentlyDenied){
                        showSettingsDialog()
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    p0: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token!!.continuePermissionRequest()
                }

            })
            .withErrorListener { error -> Toast.makeText(applicationContext, "Terjadi Kesalahan ", Toast.LENGTH_SHORT)
                .show() }
            .onSameThread()
            .check()

        textViewPilihTanggal1.setEndIconOnClickListener {
            showDatePicker()    
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    //fungsi untuk open setting buat permission
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, 101)
    }

    //fungsi untuk menampilkan dialog agar user membuka setting jika permission denied
    private fun showSettingsDialog() {
        val builder: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(this)
        builder.setTitle("Butuh Izin")
        builder.setMessage(
            "Aplikasi ini butuh izin untuk menggunakan fitur ini"
        )
        builder.setPositiveButton("Buka Settings") { dialog, which ->
            dialog.cancel()
            openSettings()
        }
        builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }
        builder.show()
    }

    @SuppressLint("SetTextI18n")
    //fungsi untuk menampilkan datepicker dialog
    private fun showDatePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { _, _year, monthOfYear, dayOfMonth ->
                textViewPilihTanggal.setText( "$dayOfMonth/${monthOfYear + 1}/$_year" ) //set text view
            },
            year,
            month,
            day
        )

        datePickerDialog.show()
    }

    @Throws(DocumentException::class)
    //membuat file PDF, parameter input nya path direktori yang dibuat
    private fun createPDFFile(path: String) {
        if (File(path).exists()) {
            File(path).delete()
        }
        try {
            //inisialisasi document
            val document = Document()
            val pdfWriter = PdfWriter.getInstance(document, FileOutputStream(path))
            val event = Footer()
            pdfWriter.pageEvent = event
            document.pageSize = PageSize.A4.rotate()
            document.open()
            document.addCreationDate()
            document.addAuthor("Islamic Times Application")
            val tanggalText = textViewPilihTanggal.text.toString()
            val tanggalOldPattern = DateUtil.strToDate(tanggalText, "dd/MM/yyyy") //convert string ke date
            val tanggalFormatted = DateUtil.dateToStr(tanggalOldPattern, "EEEE, dd MMMM yyyy") //convert date ke string

            val fontName =
                BaseFont.createFont("res/font/mukta_regular.ttf", "UTF-8", BaseFont.EMBEDDED)
            val titleStyle = Font(fontName, 18.0f, Font.BOLD, BaseColor.BLACK)
            addNewItem(document, tanggalFormatted, Element.ALIGN_CENTER, titleStyle)//tambah text tanggal masehi ke PDF

            //hitungan tanggal
            //contoh output bisa dilihat di library
            val pilihanTanggal = textViewPilihTanggal.text.toString().split("/")
            val tanggal = pilihanTanggal[0]
            val bulan = pilihanTanggal[1]
            val tahun = pilihanTanggal[2]

            val localData = LocalData(this)
            val jdNow = falak.KMJD(tanggal.toByte(),bulan.toByte(),tahun.toLong(),0.0,localData.tzn)
            val cJDN = floor(jdNow + 0.5 + localData.tzn / 24).toLong()

            val tglH = falak.CJDNKH(cJDN,OptResult="TglH").toString().toByte()
            val blnH = falak.CJDNKH(cJDN,OptResult="BlnH").toString().toByte()
            val thnH = falak.CJDNKH(cJDN,OptResult="ThnH").toString().toLong()
            val titleStyle2 = Font(fontName, 16.0f, Font.NORMAL, BaseColor.BLACK)
            //end of hitungan tanggal urfi
            Log.d("contentValues", "createPDFFile bulan: $blnH")
            val bulanStrIslam = islamicMonth(blnH.toInt())
            val tanggalUrfi = "$tglH $bulanStrIslam $thnH H ('Urfi)"

            Log.d("contentValues", "createPDFFile bulan: $tanggalUrfi")
            addNewItem(document, tanggalUrfi, Element.ALIGN_CENTER, titleStyle2)//tambah text tanggalurfi ke PDF

            val headerStyle = Font(fontName, 12.0f, Font.NORMAL, BaseColor.WHITE)
            addTable(document, headerStyle)

        } catch (e: DocumentException) {
            Log.d("dokumen", e.localizedMessage!!)
        }
    }

    @Throws(DocumentException::class)
    //tambah jarak ke PDF
    private fun addSpace(document: Document) {
        try{
            val fontName =
                BaseFont.createFont("res/font/mukta_regular.ttf", "UTF-8", BaseFont.EMBEDDED)
            val nullStyle = Font(fontName, 4.0f, Font.NORMAL)
            document.add(Paragraph(" ", nullStyle))
        }catch(e : IOException){
            Log.d("error add space", e.message!!)
        }

    }

    @Throws(DocumentException::class)
    //tambah table ke PDF
    //header style untuk styling font table header
    private fun addTable(document: Document, headerStyle: Font) {
        try {
            var cellMatahari: PdfPCell
            var cellBulan: PdfPCell
            val dao = DaoManager()
            //split untuk mendapatkan tanggal,bulan,tahun dari textview
            val pilihanTanggal = textViewPilihTanggal.text.toString().split("/")
            val tanggal = pilihanTanggal[0]
            val bulan = pilihanTanggal[1]
            val tahun = pilihanTanggal[2]

            val viewModelFactoryEphemersisMatahariBulanView =
                EphemerisMatahariBulanViewModelFactory(
                    dao.dataEphemersisBulanMatahari(this)!!
                )
            ephemerisMatahariBulanViewModel = ViewModelProvider(
                this,
                viewModelFactoryEphemersisMatahariBulanView
            ).get(EphemerisMatahariBulanViewModel::class.java)


            if(editTextDeltaTCetak.text.isNotEmpty()){
                valueDeltaT = editTextDeltaTCetak.text.toString().toDouble()//get value delta T dari input manual
            }
            if(pilihanDeltaT == getString(R.string.prediksi_delta_t)){
                //set delta T prediksi disini karena butuh tanggal, bulan, dan tahun dari pilihan user
                val jd      : Double = falak.KMJD(tanggal.toByte(),bulan.toByte(),tahun.toLong())
                valueDeltaT =  falak.DeltaT(jd)
            }
            //set data viewmodel
            Log.d("VALUE DELTA T", "$valueDeltaT")
            ephemerisMatahariBulanViewModel.getEphemersisMatahariBulanByDate(
                tanggal.toByte(),
                bulan.toByte(),
                tahun.toLong(),
                valueDeltaT,
                this
            )
            //get data dari viewmodel
            //create tabel didalem viewmodel karena kalo ga kayagini bisa menyebabkan UI blocking
            ephemerisMatahariBulanViewModel.ephemerisMatahariBulanByDate.observe(this, Observer {
                val fontName = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1257, BaseFont.EMBEDDED)
                val jd      : Double = falak.KMJD(tanggal.toByte(),bulan.toByte(),tahun.toLong())
                val deltaT =  falak.DeltaT(jd)
                Log.d("delta t cetak", valueDeltaT.toString())
                addSpace(document)
                addSpace(document)
                addSpace(document)
                val tableTitle = PdfPTable(3)
                //set text delta T dan text Data Matahari dibuat tabel karena biar sejajar
                val cellTitle = PdfPCell(Paragraph("Delta T = ${desimalFormat(deltaT)}s"))
                cellTitle.horizontalAlignment = Element.ALIGN_LEFT
                cellTitle.border = PdfPCell.NO_BORDER
                val cellTitle2 = PdfPCell(Paragraph(getString(R.string.data_matahari)))
                cellTitle2.horizontalAlignment = Element.ALIGN_CENTER
                cellTitle2.border = PdfPCell.NO_BORDER
                val cellTitle3 = PdfPCell(Paragraph(""))
                cellTitle3.horizontalAlignment = Element.ALIGN_CENTER
                cellTitle3.border = PdfPCell.NO_BORDER
                tableTitle.addCell(cellTitle)
                tableTitle.addCell(cellTitle2)
                tableTitle.addCell(cellTitle3)
                tableTitle.widthPercentage = 100f
                document.add(tableTitle)
                //akhir dari set text delta T dan text Data Matahari
                addSpace(document)
                val table = PdfPTable(10)//inisialisasi tabel 10 kolom
                val table2 = PdfPTable(10)//inisialisasi tabel 10 kolom
                val headerTableMatahari = arrayOf(
                    "Jam\n(TD)",
                    Constant.APPARENT_LONGITUDE,
                    Constant.ECLIPTIC_LATITUDE,
                    Constant.APPARENT_RIGHT_ASC,
                    Constant.APPARENT_DECLINATION,
                    Constant.HORIZONTAL_PARALLAX,
                    Constant.SEMI_DIAMETER,
                    Constant.TRUE_GEOCENT_DIST,
                    Constant.EQUATION_OF_TIME,
                    Constant.GHA
                )
                val headerTablebulan = arrayOf(
                    "Jam\n(TD)",
                    Constant.APPARENT_LONGITUDE, Constant.APPARENT_LATITUDE, Constant.APPARENT_RIGHT_ASC,
                    Constant.APPARENT_DECLINATION, Constant.HORIZONTAL_PARALLAX, Constant.SEMI_DIAMETER,
                    Constant.ANGLE_BRIGHT_LIMB, Constant.FRACTION_ILLUMINATION, Constant.GHA
                )
                //tambah data buat tabel header
                for (i in headerTableMatahari.indices) {
                    //header tabel matahari
                    val cellHeaderMatahari = PdfPCell(Paragraph(headerTableMatahari[i], headerStyle))
                    cellHeaderMatahari.horizontalAlignment = Element.ALIGN_CENTER
                    cellHeaderMatahari.backgroundColor = BaseColor(30, 113, 93, 255)
                    cellHeaderMatahari.fixedHeight = 40f
                    cellHeaderMatahari.verticalAlignment = Element.ALIGN_MIDDLE
                    table.addCell(cellHeaderMatahari)
                    //header table bulan
                    val cellHeaderBulan= PdfPCell(Paragraph(headerTablebulan[i], headerStyle))
                    cellHeaderBulan.horizontalAlignment = Element.ALIGN_CENTER
                    cellHeaderBulan.backgroundColor = BaseColor(30, 113, 93, 255)
                    cellHeaderBulan.fixedHeight = 40f
                    cellHeaderBulan.verticalAlignment = Element.ALIGN_MIDDLE
                    table2.addCell(cellHeaderBulan)
                }

                table.widthPercentage = 100f
                table.horizontalAlignment = Element.ALIGN_CENTER

                table2.widthPercentage = 100f
                table2.horizontalAlignment = Element.ALIGN_CENTER

                //ada simbol derajat encoding BaseFont.CP1252
                val cellStyle = Font(fontName, 10.0f, Font.NORMAL, BaseColor.BLACK)

                //UTF-8 gamuncul simbol derajat
               /* val fontName =
                    BaseFont.createFont("res/font/mukta_regular.ttf", "UTF-8", BaseFont.EMBEDDED)*/

                //add cell isi tabel, end nya 9 karena kolom tabel nya 10
                var start = 0
                var end = 9
                for (i in 0..24) {
                    cellMatahari = PdfPCell(Paragraph(i.toString(), cellStyle))
                    cellBulan = PdfPCell(Paragraph(i.toString(), cellStyle))
                    cellMatahari.horizontalAlignment = Element.ALIGN_CENTER
                    cellBulan.horizontalAlignment = Element.ALIGN_CENTER
                    if (i % 5 == 0) {
                        cellMatahari.backgroundColor = BaseColor(194, 237, 227, 255)
                        cellBulan.backgroundColor = BaseColor(194, 237, 227, 255)
                    }
                    table.addCell(cellMatahari)
                    table2.addCell(cellBulan)

                    //kondisi ini ditambahin buat ngehindarin out of bound exception
                    if (end > it.size) {
                        end -= 1
                    }

                    for (j in start until end) {
                        //add cell matahari
                        cellMatahari = PdfPCell(
                            Paragraph(
                                it[j].ephemerisMatahari.valueDataMatahari,
                                cellStyle
                            )
                        )

                        //add cell bulan
                        cellBulan = PdfPCell(
                            Paragraph(
                                it[j].ephemerisBulan.valueDataBulan,
                                cellStyle
                            )
                        )
                        cellMatahari.horizontalAlignment = Element.ALIGN_CENTER
                        cellBulan.horizontalAlignment = Element.ALIGN_CENTER

                        //ganti background setiap baris kelipatan 5
                        if (i % 5 == 0) {
                            cellMatahari.backgroundColor = BaseColor(194, 237, 227, 255)
                            cellBulan.backgroundColor = BaseColor(194, 237, 227, 255)
                        }
                        table.addCell(cellMatahari)
                        table2.addCell(cellBulan)
                    }
                    start += 9
                    end += 9
                }
                table.widthPercentage = 100f
                table.horizontalAlignment = Element.ALIGN_CENTER
                document.add(table)
                addSpace(document)
                val fontName2 =
                    BaseFont.createFont("res/font/mukta_regular.ttf", "UTF-8", BaseFont.EMBEDDED)
                val subTitleStyle = Font(fontName2, 14.0f, Font.NORMAL, BaseColor.BLACK)
                addSpace(document)
                addSpace(document)
                addSpace(document)
                addSpace(document)
                addSpace(document)
                addNewItem(document, getString(R.string.data_bulan), Element.ALIGN_CENTER, subTitleStyle)
                addSpace(document)
                document.add(table2)
                document.close()
                Toast.makeText(this, getString(R.string.succes_cetak), Toast.LENGTH_LONG).show()
                finish()
            })


        } catch (e: Exception) {
            Log.d("error add Table", e.localizedMessage!!)
        }
    }

    /*private fun addItemLeftRight(document: Document, textLeft : String, textRight : String, leftStyle : Font, rightStyle : Font){
        val chunkTextLeft = Chunk(textLeft, leftStyle)
        val chunkTextRight = Chunk(textRight, rightStyle)
        val p = Paragraph(chunkTextLeft)
        p.alignment = Element.ALIGN_LEFT
        p.add(VerticalPositionMark())
        p.alignment = Element.ALIGN_CENTER
        p.add(chunkTextRight)
        document.add(p)
    }*/

    @Throws(DocumentException::class)
    //fungsi untuk tambah item berupa text ke dokumen / PDF
    private fun addNewItem(document: Document, text: String, align: Int, style: Font) {
        try {
            val chunk = Chunk(text, style)
            val p = Paragraph(chunk)
            p.alignment = align
            document.add(p)
        } catch (e: DocumentException) {
            Log.d("error add item", e.message!!)
        }

    }

    //fungsi untuk mengkonversi bulan islam yang asalnya bilangan ke teks
    private fun islamicMonth(month : Int) : String{
        when(month){
            1 -> return "al-Muharram"
            2 -> return "Shafar"
            3 -> return "Rabi al-Awwal"
            4 -> return  "Rabi al-Akhir"
            5 -> return "Jumada al-Ula"
            6 -> return "Jumada al-Akhirah"
            7 -> return "Rajab"
            8 -> return "Sya'ban"
            9 -> return "Ramadhan"
            10-> return "Syawal"
            11 -> return "Dzulqa'dah"
            12 -> return "Dzulhijjah"
        }
       return ""
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
        }
        return true
    }


}

