package id.someah.islamictimes.ui.fragment

import android.content.Intent
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import id.someah.islamictimes.R
import id.someah.islamictimes.ui.activity.TentangAplikasiActivity
import id.someah.islamictimes.preferences.CustomDialogAltMatahariPreference
import id.someah.islamictimes.ui.activity.IhtiyatActivity
import id.someah.islamictimes.ui.activity.PerbaruiLokasiActivity
import id.someah.islamictimes.ui.activity.PrivacyAndPolicyActivity
import id.someah.islamictimes.util.CustomPreference
import id.someah.islamictimes.util.PreferenceStyle

class PengaturanFragment : PreferenceFragmentCompat() {

//    lateinit var prefIhtiyat : CustomPreference

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)

        val prefIhtiyat =
            findPreference(getString(R.string.ihtiyat)) as Preference?
        //intent ketika preference di click
        prefIhtiyat!!.onPreferenceClickListener =
            Preference.OnPreferenceClickListener {
                startActivity(Intent(context, IhtiyatActivity::class.java))
                true
            }
        val prefPengembang =
            findPreference(getString(R.string.key_pengembang)) as Preference?
        prefPengembang!!.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            startActivity(Intent(context, TentangAplikasiActivity::class.java))
            true
        }
        val prefLokasi =
            findPreference(getString(R.string.perbarui_lokasi)) as Preference?
        prefLokasi!!.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            startActivity(Intent(context, PerbaruiLokasiActivity::class.java))
            true
        }
        val prefPrivacyAndPolicy = findPreference(getString(R.string.key_privacy_and_policy)) as Preference?
        prefPrivacyAndPolicy?.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            startActivity(Intent(context,PrivacyAndPolicyActivity::class.java))
            true
        }
    }

    //overriding method agar preferences bisa menampilkan preference dialog
    override fun onDisplayPreferenceDialog(preference: Preference?) {
        when (preference) {
            is CustomDialogAltMatahariPreference -> {
                val dialogFragment =
                    CustomDialogAltMatahariPreference.DialogPreferenceAltMatahariCompat.newInstance(
                        preference.key,
                        context
                    )
                dialogFragment.setTargetFragment(this, 0)
                dialogFragment.show(parentFragmentManager, null) //menampilkan preference dialog dari altitude matahari di settings
            }
            else -> super.onDisplayPreferenceDialog(preference)
        }

    }



}