package id.someah.islamictimes.constant

import android.Manifest
import android.content.Context
import id.someah.islamictimes.data.HariPentingModel
import id.someah.islamictimes.data.OfflineLocationModel

object Constant {

    const val APPARENT_LONGITUDE = "Apparent Longitude"
    const val ECLIPTIC_LATITUDE = "Ecliptic Latitude"
    const val APPARENT_LATITUDE = "Apparent Latitude"
    const val APPARENT_RIGHT_ASC = "Apparent Right Ascension"
    const val APPARENT_DECLINATION = "Apparent Declination"
    const val HORIZONTAL_PARALLAX = "Horizontal Parallax"
    const val SEMI_DIAMETER = "Semi Diameter"
    const val ANGLE_BRIGHT_LIMB = "Angle Bright Limb"
    const val TRUE_GEOCENT_DIST = "True Geocentric Distance"
    const val EQUATION_OF_TIME = "Equation of Time"
    const val FRACTION_ILLUMINATION = "Fraction Illumination"
    const val GHA = "GHA"

    const val NAMA_SHOLAT_ZUHUR = "Zuhur"
    const val NAMA_SHOLAT_ASAR = "Asar"
    const val NAMA_SHOLAT_MAGRIB = "Magrib"
    const val NAMA_SHOLAT_ISYA = "Isya"
    const val NAMA_AKHIR_ISYA = "Akhir Isya"
    const val NAMA_SHOLAT_SUBUH = "Subuh"
    const val NAMA_AKHIR_SUBUH = "Akhir Subuh"
    const val NAMA_SHOLAT_DUHA = "Duha"
    const val KIBLAT_HARIAN_1 = "Waktu\nKiblat1"
    const val KIBLAT_HARIAN_2 = "Waktu\nKiblat2"

    const val DATABASE_FILE_NAME = "location.sqlite"
    const val MANUAL_LOCATION = "manual_location"
    val BULAN_HIJRI = listOf(
        "Al-Muharram" to 1,
        "Shafar" to 2,
        "Rabi al-Awwal" to 3,
        "Rabi al-Akhir" to 4,
        "Jumada al-Ula" to 5,
        "Jumada al-Akhirah" to 6,
        "Rajab" to 7,
        "Syaban" to 8,
        "Ramadhan" to 9,
        "Syawal" to 10,
        "Dzulqa'dah" to 11,
        "Dzulhijjah" to 12
    )

    val JUMLAH_HARI_HIJRI = listOf(
        "Al-Muharram" to 30,
        "Shafar" to 29,
        "Rabi al-Awwal" to 30,
        "Rabi al-Akhir" to 29,
        "Jumada al-Ula" to 30,
        "Jumada al-Akhirah" to 29,
        "Rajab" to 30,
        "Syaban" to 29,
        "Ramadhan" to 35,
        "Syawal" to 29,
        "Dzulqa'dah" to 30,
        "Dzulhijjah" to 12
    )

    val BULAN = listOf(
        "Januari" to 1,
        "Februari" to 2,
        "Maret" to 3,
        "April" to 4,
        "Mei" to 5,
        "Juni" to 6,
        "Juli" to 7 ,
        "Agustus" to 8,
        "September" to 9,
        "Oktober" to 10 ,
        "November" to 11,
        "December" to 12
    )

    val perms = arrayOf<String>(
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    val HARI_PENTING = listOf(
        HariPentingModel(
            title = "Idul fitri",
            day = 1,
            month = 10,
            isImportant = true
        ),
        HariPentingModel(
            title = "Shaum arafah",
            day = 9,
            month = 12,
            isEvent = true
        ),
        HariPentingModel(
            title = "Idul adha",
            day = 10,
            month = 12,
            isImportant = true
        ),
        HariPentingModel(
            title = "Hari tasyrik",
            day = 11,
            month = 12,
            howLong = 2,
            listDay = listOf(11,12,13),
        ),
        HariPentingModel(
            title = "Shaum tasua asyuro",
            day = 9,
            month = 1,
            howLong = 2,
            isEvent = true,
            listDay = listOf(9,10),
        ),
        HariPentingModel(
            title = "Shaum ayyamul bidh",
            month = 0,
            listDay = listOf(13,14,15),
            day = 13,
            howLong = 2,
            isEvent = true,
            isRepeatEveryMonth = true
        )

    )

}


fun List<OfflineLocationModel>.getLocationAutocomplete() : List<String> {
    if(this.isNotEmpty()) {
        var temp = ArrayList<String>()

        this.forEach {
            temp.add(it.locationName)
        }

        return temp
    }

    return emptyList()
}


fun Double.round(decimals: Int = 2): String {
    return "%.${decimals}f".format(this)
}