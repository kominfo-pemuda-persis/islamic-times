package id.someah.islamictimes.util

abstract class UseCase<in P , R>{
    abstract operator fun invoke(parameter : P , result : R) : R
}
