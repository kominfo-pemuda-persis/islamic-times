package id.someah.islamictimes.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import androidx.preference.Preference;

import id.someah.islamictimes.R;

public class CustomPreference extends Preference
        implements PreferenceStyle {

    private int style;

    public CustomPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public CustomPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomPreference(Context context) {
        super(context);
    }


    @SuppressLint("WrongConstant")
    @Override
    public void onBindView(View view) {
        int alarmREDColor = view.getContext().getResources().getColor(R.color.snackbar_close_color);
        TextView titleView = (TextView) view.findViewById(android.R.id.title);
        titleView.setTextColor(alarmREDColor);
        Typeface face = Typeface.createFromAsset(view.getContext().getAssets(),
                "font/mukta_regular.ttf");
        titleView.setTypeface(face);
    }

    @Override
    public void setStyle(int style) {
        this.style = STYLE_NORMAL;
    }
}
