package id.someah.islamictimes.util

sealed class Result<out R> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val throwable: Throwable) : Result<Nothing>()
    data class Loading(val show: Boolean) : Result<Nothing>()
}

val Result<*>.succeeded
    get() = this is Result.Success && data != null

val Result<Throwable>.error
    get() = this

val Result<*>.loading
    get() = this is Result.Loading && show
