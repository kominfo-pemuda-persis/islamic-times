package id.someah.islamictimes.util;

import com.airbnb.epoxy.EpoxyDataBindingLayouts;
import com.airbnb.epoxy.PackageModelViewConfig;

import id.someah.islamictimes.R;

@EpoxyDataBindingLayouts({
    R.layout.item_gerhana ,
    R.layout.item_gerhana_matahari,
    R.layout.item_gerhana_empty,
    R.layout.item_calendar,
    R.layout.item_calendar_libur,
    R.layout.item_header_day,
    R.layout.item_empty_calendar
})

@PackageModelViewConfig(rClass = R.class)
interface epoxyConfig { }
