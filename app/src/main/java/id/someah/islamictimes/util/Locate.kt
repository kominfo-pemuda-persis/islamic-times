package id.someah.islamictimes.util

import android.Manifest
import android.app.AlertDialog
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import androidx.core.app.ActivityCompat
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.log
import java.util.*

/*
    utility untuk mendapatkan lokasi user
    references : https://github.id/rhmkds/kiblat-android/blob/master/app/src/main/java/id/islam/idpass_noads/GPSTracker.java
 */
class Locate(private val mContext: Context) : Service(),
    LocationListener {
    private var isGPSEnabled = false
    private var isNetworkEnabled = false
    var canGetLocation = false
    private var location: Location? = null
    private var latitude = 0.0
    private var longitude = 0.0
    private var altitude = 0.0
    private var locationManager: LocationManager? = null
    private val DISTANCE_CHANGE_FOR_UPDATES: Long = 100
    private var MIN_TIME_BW_UPDATES: Long? = null
    private val sharedPref = SharedPref(mContext)
    private val sharedPrefDurasi = sharedPref.getValueString("durasi_update", "15_menit")

    //get location user
    private fun getLocation() {
        try {
            locationManager = mContext
                .getSystemService(Context.LOCATION_SERVICE) as LocationManager
            isGPSEnabled = locationManager!!
                .isProviderEnabled(LocationManager.GPS_PROVIDER)
            isNetworkEnabled = locationManager!!
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            val locationProvider = locationManager!!.getProvider(LocationManager.GPS_PROVIDER)
            locationProvider!!.supportsAltitude()
            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                //durasi update sesuai settings
                when (sharedPrefDurasi) {
                    "15_menit" -> {
                        MIN_TIME_BW_UPDATES = 900000
                    }
                    "20_menit" -> {
                        MIN_TIME_BW_UPDATES = 1200000
                    }
                    "25_menit" -> {
                        MIN_TIME_BW_UPDATES = 1500000
                    }
                    "30_menit" -> {
                        MIN_TIME_BW_UPDATES = 1800000
                    }
                }
                canGetLocation = true
                //get lokasi menggunakan network
                if (isNetworkEnabled) {
                    if (ActivityCompat.checkSelfPermission(
                            mContext,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        return
                    }
                    locationManager!!.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES!!,
                        DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                    )

                    if (locationManager != null) {
                        if (ActivityCompat.checkSelfPermission(
                                mContext,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                                mContext,
                                Manifest.permission.ACCESS_COARSE_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {

                            return
                        }
                        location =
                            locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                        if (location != null) {
                            latitude = location!!.latitude
                            longitude = location!!.longitude
                        }
                    }
                }
                //get lokasi menggunakan GPS provider
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager!!.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES!!,
                            DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                        )

                        if (locationManager != null) {
                            location =
                                locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                            if (location != null) {
                                latitude = location!!.latitude
                                longitude = location!!.longitude
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //fungsi untuk get data latitude
    private fun getLatitude(): Double {
        if (location != null) {
            latitude = location!!.latitude
        }
        return latitude
    }

    //fungsi untuk get data longitude
    private fun getLongitude(): Double {
        if (location != null) {
            longitude = location!!.longitude
        }
        return longitude
    }

    //fungsi untuk get data altitude
    private fun getAltitude(): Double {
        if (locationManager != null) {
            if (location != null) {
                if (location!!.hasAltitude()) {
                    altitude = location!!.altitude
                    return altitude
                }
            } else {
                return 0.0
            }
        }
        return 0.0
    }

    fun canGetLocation(): Boolean {
        return canGetLocation
    }

    //fungsi untuk menampilkan dialog agar user mengaktifkan GPS di setting
    fun gpsAlert() {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(mContext)
        alertDialog.setTitle("Nyalakan GPS")
        alertDialog.setMessage("GPS tidak aktif, apakah kamu ingin megaktifkannya?")
        alertDialog.setPositiveButton(
            "Iya"
        ) { dialog, which ->
            dialog.cancel()
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            mContext.startActivity(intent)
        }

//        alertDialog.setNegativeButton(
//           "Tidak"
//        ) { dialog, which -> dialog.cancel() }
        alertDialog.show()
    }

    //fungsi untuk mendapatkan alamat dari user
    //latitude, longitude, altitude, dan alamat user disimpan ke shared preferences ketika fungsi ini dipanggil
    fun getAddress() {
        val sharedPref = SharedPref(mContext)
        val latitude = getLatitude()
        val longitude = getLongitude()
        val altitude = getAltitude()
        sharedPref.saveDoublePref("latitude_user", latitude)
        sharedPref.saveDoublePref("longitude_user", longitude)
        sharedPref.saveDoublePref("altitude_user", altitude)
        Log.d("altitude ", "$altitude")
        val geocoder = Geocoder(mContext, Locale.getDefault())
        try {
            val addresses: List<Address> = geocoder.getFromLocation(latitude, longitude, 1)
            Log.d("error", "getAddress: ${addresses}")
            var namaKota = addresses[0].subAdminArea
            var kelurahan = addresses[0].subLocality

            if(namaKota == null) {
                namaKota = addresses[0].featureName
            }

            if(kelurahan == null) {
                kelurahan = addresses[0].countryName
            }
            Log.d("error", "getAddress: $kelurahan, $namaKota")
            Log.d("alamat ", "$kelurahan, $namaKota")

            sharedPref.saveStringPref("alamat_user", "$kelurahan,\n$namaKota")
        } catch (e: Exception) {
            e.localizedMessage
            Log.d("error", "getAddress: ${e.printStackTrace()}")
        }
    }

    override fun onLocationChanged(location: Location) {

    }

    override fun onProviderDisabled(provider: String) {

    }

    override fun onProviderEnabled(provider: String) {

    }

    override fun onStatusChanged(
        provider: String,
        status: Int,
        extras: Bundle
    ) {

    }

    override fun onBind(intent: Intent): IBinder? {

        return null
    }


    init {
        getLocation()
    }

    fun gpsAlertWithReturnValue(): AlertDialog.Builder {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(mContext)
        alertDialog.setCancelable(false)
        alertDialog.setTitle("Nyalakan GPS")
        alertDialog.setMessage("GPS tidak aktif, apakah kamu ingin megaktifkannya?")
        alertDialog.setPositiveButton(
            "Iya"
        ) { dialog, which ->
            dialog.cancel()
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            mContext.startActivity(intent)
        }
        return alertDialog
    }
}