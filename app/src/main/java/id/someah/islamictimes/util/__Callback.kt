package id.someah.islamictimes.util

class __Callback<T> : Callback<T> {
    lateinit var success : ((T) -> Unit)
    lateinit var failure : ((T) -> Unit)
    lateinit var loading : ((T) -> Unit)
    lateinit var error : ((T) -> Unit)


    override fun onFailure(fail: T) {
        this.failure.invoke(fail)
    }

    override fun onError(error: T) {
        this.error.invoke(error)
    }

    override fun onLoading(loading: T) {
        this.loading.invoke(loading)
    }

    override fun onSuccess(data: T) {
        success.invoke(data)
    }
}

interface Callback<T> {
    fun onSuccess(data : T )
    fun onFailure(fail : T)
    fun onError(error : T)
    fun onLoading(loading : T)
}