package id.someah.islamictimes.util

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.OvershootInterpolator
import android.widget.ImageButton
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.snackbar.ContentViewCallback
import id.someah.islamictimes.R

class BaseSnackbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context , attrs , defStyleAttr) , ContentViewCallback {

    lateinit var tvMsg : TextView
    lateinit var imgAction : ImageButton
    lateinit var layoutRoot : ConstraintLayout

    init {
        View.inflate(context, R.layout.view_custom_snackbar,this)
        clipToPadding = false

        this.tvMsg = findViewById(R.id.tvMessage)
        this.imgAction = findViewById(R.id.btnClose)
        this.layoutRoot = findViewById(R.id.layoutRoot)

    }

    override fun animateContentIn(delay: Int, duration: Int) {
        val scaleX = ObjectAnimator.ofFloat(imgAction, View.SCALE_X, 0f, 1f)
        val scaleY = ObjectAnimator.ofFloat(imgAction, View.SCALE_Y, 0f, 1f)
        val animatorSet = AnimatorSet().apply {
            interpolator = OvershootInterpolator()
            setDuration(500)
            playTogether(scaleX, scaleY)
        }
        animatorSet.start()
    }

    override fun animateContentOut(delay: Int, duration: Int) {
    }
}