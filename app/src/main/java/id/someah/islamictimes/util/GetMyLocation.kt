package id.someah.islamictimes.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.utils.convertGMTtoDouble
import java.text.SimpleDateFormat
import java.util.*

class GetMyLocationImpl constructor(val context: Context) : GetMyLocation {

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    init {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    }

    override fun getLastKnowLocation(callback: (data: Any) -> Unit) {
        val sharedPref = SharedPref(context)
        val gps = sharedPref.getValueString("prioritas_update", "using_gps")
        if(gps != "using_gps") {
            callback.invoke("NOT GRANTED")
            return
        }
        callback.invoke(true)
        if(this::fusedLocationProviderClient.isInitialized) {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                callback.invoke("NOT GRANTED")
                return
            }

            fusedLocationProviderClient.lastLocation
                .addOnSuccessListener { location ->
                    if (location != null) {
                        val latitude = location.latitude
                        val longitude = location.longitude
                        val altitude = location.altitude

                        sharedPref.saveDoublePref("latitude_user", latitude)
                        sharedPref.saveDoublePref("longitude_user", longitude)
                        sharedPref.saveDoublePref("altitude_user", altitude)

                        val geocoder = Geocoder(context, Locale.getDefault())

                        val addresses: List<Address> =
                            geocoder.getFromLocation(latitude, longitude, 1)
                        var namaKota = addresses[0].subAdminArea
                        var kelurahan = addresses[0].subLocality

                        if (namaKota == null) {
                            namaKota = addresses[0].featureName
                        }

                        if (kelurahan == null) {
                            kelurahan = addresses[0].countryName
                        }

                        val tz    =  java.util.TimeZone.getDefault()
                        val TZn   : Double  = tz.getDisplayName(false,java.util.TimeZone.SHORT).convertGMTtoDouble()

                        val calendar : Calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault())
                        val timezoneOffset = SimpleDateFormat("Z", Locale.getDefault()).format(calendar.time)

                        sharedPref.saveStringPref("alamat_user", "$kelurahan,\n$namaKota")
                        sharedPref.saveDoublePref("timezone",TZn)
                        sharedPref.saveStringPref("timezone_title",timezoneOffset)

                        callback.invoke(Pair(location,"$kelurahan,\n" + "$namaKota"))

                    }
                }.addOnCompleteListener {
                    callback.invoke(false)
                }
        }

    }
}

interface GetMyLocation {
    fun getLastKnowLocation(callback : (data : Any) -> Unit )
}