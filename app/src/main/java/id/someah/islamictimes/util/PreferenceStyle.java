package id.someah.islamictimes.util;

import android.content.Context;
import android.view.View;

public interface PreferenceStyle {

    public static final int STYLE_DISABLED = 0;
    public static final int STYLE_NORMAL = 10;
    public static final int STYLE_ALARMED = 20;
    public static final int STYLE_WARNING = 30;

    public static final int STYLE_SUMMARY_ALARM = 40;
    public static final int STYLE_SUMMARY_WARNING = 50;

    public void onBindView(View view);

    public void setStyle(int style);
    public Context getContext();
    public void setSummary(CharSequence summary);

}
