package id.someah.islamictimes.util

import android.annotation.SuppressLint
import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Exception

//object buat utility date
object DateUtil {
    //Ex pattern long date EEE, dd MM yyyy : Tue, 11 08 2020
    //Ex pattern short date dd-MMM-yyyy : 11-Aug-2020

    @SuppressLint("SimpleDateFormat")
    //ubah tipe date string menjadi date
    fun strToDate(str : String, usedPattern : String) : Date {
        var format : Date? = null
        try {
            format = SimpleDateFormat(usedPattern, Locale.getDefault()).parse(str)!!
        }
        catch (e : Exception){
            Log.e("invalid format", e.localizedMessage!!)
        }
        return format!!
    }

    //ubah tipe data date jadi string
    fun dateToStr(date : Date, newPattern: String) : String{
        var formatter : String? = null
        try {
            formatter = SimpleDateFormat(newPattern, Locale.getDefault()).format(date)
        }catch (e : Exception){
            Log.e("invalid format", e.localizedMessage!!)
        }
        return formatter!!
    }
}