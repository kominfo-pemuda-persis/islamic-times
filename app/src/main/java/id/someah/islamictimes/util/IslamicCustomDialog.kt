package id.someah.islamictimes.util

import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.Window

class IslamicCustomDialog(resources : Int?)   {

    lateinit var mDialog: Dialog

    var customLayout : Int = 0
    lateinit var state : IslamicCustomDialogBaseState

    init {
        if(resources != null) {
            customLayout  = resources
        }
    }

    fun init(context : Context?){

        if(context != null) {
            mDialog = Dialog(context)
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

            if(customLayout != 0) {
                mDialog.setContentView(customLayout)
            }

            mDialog.setCancelable(true)
        }

    }



    fun showCustomDialog() {
        if(this::mDialog.isInitialized && !mDialog.isShowing) {
                mDialog.show()
        }
    }

    fun dismissCustomDialog() {
        if(this::mDialog.isInitialized && mDialog.isShowing) {
            mDialog.dismiss()
            if(this::state.isInitialized) {
                state.result(mDialog)
            }

        }
    }


}


interface IslamicCustomDialogBaseState  {
    fun result(res : Dialog)
}