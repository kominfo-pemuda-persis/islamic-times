package id.someah.islamictimes.util

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import id.someah.islamictimes.R
import com.google.android.material.snackbar.BaseTransientBottomBar
import java.lang.Exception
import java.lang.IllegalArgumentException

class CustomSnackbar constructor(parent : ViewGroup , content : BaseSnackbar) : BaseTransientBottomBar<CustomSnackbar>(parent,content,content) {

    init {
        view.setBackgroundColor(ContextCompat.getColor(view.context,android.R.color.transparent))
        view.setPadding(0,0,0,0)
    }

    companion object {
        fun make(view : View,message : String,duration : Int,listener : View.OnClickListener) : CustomSnackbar? {
            val parent = view.findSuitableParent() ?: throw IllegalArgumentException(
                "No suitable parent found from the given view. Please provide a valid view."
            )
            try {
                val customView = LayoutInflater.from(view.context).inflate(R.layout.layout_simple_custom_snackbar,parent,false) as BaseSnackbar

                customView.tvMsg.text = message

                return CustomSnackbar(parent,customView).setDuration(duration)

            }catch ( e: Exception) {
                Log.d("CustomSnackbar", "make: ${e.message}")
            }
            return null
        }
    }


}

internal fun View?.findSuitableParent(): ViewGroup? {
    var view = this
    var fallback: ViewGroup? = null
    do {
        if (view is CoordinatorLayout) {
            return view
        } else if (view is FrameLayout) {
            if (view.id == android.R.id.content) {
                return view
            } else {
                fallback = view
            }
        } else if(view is ConstraintLayout) {
            return view
        }

        if (view != null) {
            val parent = view.parent
            view = if (parent is View) parent else null
        }
    } while (view != null)

    return fallback
}
