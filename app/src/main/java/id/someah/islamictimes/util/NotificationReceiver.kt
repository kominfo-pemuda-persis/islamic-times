    package id.someah.islamictimes.util

import android.app.*
import android.appwidget.AppWidgetManager
import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.preference.PreferenceManager
import id.someah.islamictimes.ui.activity.NotifDialogActivity
import id.someah.islamictimes.R
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.model.ActionNotification
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.services.AdzanSoundService
import id.someah.islamictimes.services.AlarmReceiver
import id.someah.islamictimes.services.AlarmServiceChecker
import id.someah.islamictimes.services.BackgroundSound
import id.someah.islamictimes.services.utils.updateWidget
import id.someah.islamictimes.ui.activity.MainActivity
import id.someah.islamictimes.widget.DaftarJadwalSholatWidget
import id.someah.islamictimes.widget.JadwalSholatWidget
import java.util.*
import kotlin.reflect.KClass


    const val ID_NOTIFICATION_SHOLAT_DZUHUR = 10
const val ID_NOTIFICATION_SHOLAT_ASHAR = 11
const val ID_NOTIFICATION_SHOLAT_MAGHRIB = 12
const val ID_NOTIFICATION_SHOLAT_ISYA = 13
const val ID_NOTIFICATION_SHOLAT_NISFLU_LAIL = 14
const val ID_NOTIFICATION_SHOLAT_SHUBUH = 15
const val ID_NOTIFICATION_SHOLAT_SYURUQ = 16
const val ID_NOTIFICATION_SHOLAT_DHUHA = 17
const val NOTIFICATION_TYPE = "type"
const val TYPE_SHOLAT_DZUHUR = "Sudah Masuk Waktu Sholat Zuhur"
const val TYPE_SHOLAT_ASHAR = "Sudah Masuk Waktu Sholat Asar"
const val TYPE_SHOLAT_MAGHRIB = "Sudah Masuk Waktu Sholat Magrib"
const val TYPE_SHOLAT_ISYA = "Sudah Masuk Waktu Sholat Isya"
const val TYPE_SHOLAT_NISFU_LAIL = "Sudah Masuk Waktu Sholat Nisfu Lail"
const val TYPE_SHOLAT_SHUBUH = "Sudah Masuk Waktu Sholat Subuh"
const val TYPE_SHOLAT_SYURUQ = "Sudah Masuk Waktu Sholat Syuruq"
const val TYPE_SHOLAT_DHUHA = "Sudah Masuk Waktu Sholat Dhuha"

/*
utility untuk pengaturan notifikasi
reference : https://developer.android.id/training/notify-user/build-notification , https://developer.android.id/guide/topics/ui/notifiers/notifications.html
 */
class NotificationReceiver() : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val localData = LocalData(context!!)
        val falak = FalakLib()
        val sharedPref = SharedPref(context)
        val sharedPreferencesSetting = PreferenceManager.getDefaultSharedPreferences(context)
        //get mode notifikasi dari setting
        val modeDariSetting = sharedPreferencesSetting.getString("mode_pemberitahuan", "mode_suara")
        //get waktu sholat
        val waktuDzuhur = falak.DHHMS(localData.iZuhur, "HH:MM")
        val waktuAShar = falak.DHHMS(localData.iAshar, "HH:MM")
        val waktuMaghrib = falak.DHHMS(localData.iMagrib, "HH:MM")
        val waktuIsya = falak.DHHMS(localData.iIsya, "HH:MM")
        //val waktuNisfuLail = falak.DHHMS(localData.nisfuLail, "HH:MM")
        val waktuShubuh = falak.DHHMS(localData.iSubuh, "HH:MM")
        //val waktuSyuruk = falak.DHHMS(localData.iSyuruk, "HH:MM")
        val waktuDhuha = falak.DHHMS(localData.iDuha, "HH:MM")
        val type = intent!!.getStringExtra(NOTIFICATION_TYPE)

        updateWidget(context,0)
        updateWidget(context,1)

        //menampilkan notifikasi sesuai waktu sholat
        when (type) {
            TYPE_SHOLAT_DZUHUR -> {
                val toggleNotif = sharedPref.getValueNotifBool("notifikasi_dzuhur", true)!!
                val mode = notificationMode(toggleNotif, modeDariSetting!!)
                showNotificationWaktuSholat(context, type, waktuDzuhur, mode)
                if (mode == "mode_suara") {
//                    showNotifDialog(context, type)
                }
            }
            TYPE_SHOLAT_ASHAR -> {
                val toggleNotif = sharedPref.getValueNotifBool("notifikasi_ashar", true)!!
                val mode = notificationMode(toggleNotif, modeDariSetting!!)
                showNotificationWaktuSholat(context, type, waktuAShar, mode)
                if (mode == "mode_suara") {
//                    showNotifDialog(context, type)
                }
            }
            TYPE_SHOLAT_MAGHRIB -> {
                val toggleNotif = sharedPref.getValueNotifBool("notifikasi_maghrib", true)!!

                val mode = notificationMode(toggleNotif, modeDariSetting!!)
                showNotificationWaktuSholat(context, type, waktuMaghrib, mode)

//                if (mode == "mode_suara") {
////                    context.sendBroadcast(Intent("AZAN"))
////                    val services = Intent(context, BackgroundSound::class.java).also { intent ->
////                        intent.putExtra("type_sholat", type)
////                    }
////                    context.startService(services)
////                    showNotifDialog(context, type)
//                }
            }
            TYPE_SHOLAT_ISYA -> {
                val toggleNotif = sharedPref.getValueNotifBool("notifikasi_isya", true)!!
                val mode = notificationMode(toggleNotif, modeDariSetting!!)
                showNotificationWaktuSholat(context, type, waktuIsya, mode)
                if (mode == "mode_suara") {
//                    showNotifDialog(context, type)
                }
            }
            TYPE_SHOLAT_SHUBUH -> {
                val toggleNotif = sharedPref.getValueNotifBool("notifikasi_shubuh", true)!!
                val mode = notificationMode(toggleNotif, modeDariSetting!!)
                showNotificationWaktuSholat(context, type, waktuShubuh, mode)
                if (mode == "mode_suara") {
//                    showNotifDialog(context, type)
                }
            }
            TYPE_SHOLAT_DHUHA -> {
                val toggleNotif = sharedPref.getValueNotifBool("notifikasi_dhuha", true)!!
                val mode = notificationMode(toggleNotif, modeDariSetting!!)
                showNotificationWaktuSholat(context, type, waktuDhuha, mode)
                if (mode == "mode_suara") {
//                    showNotifDialog(context, type)
                }
            }
        }

        if(!isMyServiceRunning(context,AlarmServiceChecker::class.java)) {
            context.startService(Intent(context,AlarmServiceChecker::class.java))
        }

        /*if(type == TYPE_SHOLAT_DZUHUR || type == TYPE_SHOLAT_ASHAR || type == TYPE_SHOLAT_MAGHRIB
            || type == TYPE_SHOLAT_ISYA || type == TYPE_SHOLAT_NISFU_LAIL || type == TYPE_SHOLAT_SHUBUH
            || type == TYPE_SHOLAT_SYURUQ || type == TYPE_SHOLAT_DHUHA
        ){

        }*/
    }

    //get waktu pengingat sholat
    private fun getReminderTime(type: String, context: Context): Calendar {
        val localData = LocalData(context)
        val falak = FalakLib()
        var hour = 0
        var minute = 0
        lateinit var splitData: List<String>
        val calendar = Calendar.getInstance()
        val waktuDzuhur = falak.DHHMS(localData.iZuhur, "HH:MM")
        val waktuAShar = falak.DHHMS(localData.iAshar, "HH:MM")
        val waktuMaghrib = falak.DHHMS(localData.iMagrib, "HH:MM")
        val waktuIsya = falak.DHHMS(localData.iIsya, "HH:MM")
        val waktuNisfuLail = falak.DHHMS(localData.nisfuLail, "HH:MM")
        val waktuShubuh = falak.DHHMS(localData.iSubuh, "HH:MM")
        val waktuSyuruk = falak.DHHMS(localData.iSyuruk, "HH:MM")
        val waktuDhuha = falak.DHHMS(localData.iDuha, "HH:MM")

        //split data untuk mendapatkan jam dan menit
        if (type == TYPE_SHOLAT_DZUHUR) {
            splitData = waktuDzuhur.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }
        if (type == TYPE_SHOLAT_ASHAR) {
            splitData = waktuAShar.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }
        if (type == TYPE_SHOLAT_MAGHRIB) {
            splitData = waktuMaghrib.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }
        if (type == TYPE_SHOLAT_ISYA) {
            splitData = waktuIsya.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }
        if (type == TYPE_SHOLAT_NISFU_LAIL) {
            splitData = waktuNisfuLail.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }
        if (type == TYPE_SHOLAT_SHUBUH) {
            splitData = waktuShubuh.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }
        if (type == TYPE_SHOLAT_SYURUQ) {
            splitData = waktuSyuruk.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }
        if (type == TYPE_SHOLAT_DHUHA) {
            splitData = waktuDhuha.split(":")
            hour = splitData[0].toInt()
            minute = splitData[1].toInt()
        }

        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute)
        calendar.set(Calendar.SECOND, 0)

        if (calendar.before(Calendar.getInstance())) {
            calendar.add(Calendar.DATE, 1)
        }
        return calendar
    }

    //get intent yang dikirim
     fun getNotificationIntent(type: String, context: Context?): Intent {
        val intent = Intent(context, NotificationReceiver::class.java)
        intent.putExtra(NOTIFICATION_TYPE, type)
        return intent
    }

    /*set notifikasi berdasarkan waktu sholat*/
    fun setNotificationWaktuSholatDzuhur(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentDzuhur = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_DZUHUR, getNotificationIntent(
                TYPE_SHOLAT_DZUHUR, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_DZUHUR, context).timeInMillis,
                pendingIntentDzuhur
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_DZUHUR, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentDzuhur
            )
        }
    }

    fun setNotificationWaktuSholatAshar(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentAshar = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_ASHAR, getNotificationIntent(
                TYPE_SHOLAT_ASHAR, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_ASHAR, context).timeInMillis,
                pendingIntentAshar
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_ASHAR, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentAshar
            )
        }

    }

    fun setNotificationWaktuSholatMaghrib(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentMaghrib = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_MAGHRIB, getNotificationIntent(
                TYPE_SHOLAT_MAGHRIB, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_MAGHRIB, context).timeInMillis,
                pendingIntentMaghrib
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_MAGHRIB, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentMaghrib
            )
        }

    }

    fun setNotificationWaktuSholatIsya(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentIsya = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_ISYA, getNotificationIntent(
                TYPE_SHOLAT_ISYA, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_ISYA, context).timeInMillis,
                pendingIntentIsya
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_ISYA, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentIsya
            )
        }

    }

    fun setNotificationWaktuSholatNisfuLail(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentNisfuLail = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_NISFLU_LAIL, getNotificationIntent(
                TYPE_SHOLAT_NISFU_LAIL, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_NISFU_LAIL, context).timeInMillis,
                pendingIntentNisfuLail
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_NISFU_LAIL, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentNisfuLail
            )
        }
    }

    fun setNotificationWaktuSholatShubuh(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentShubuh = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_SHUBUH, getNotificationIntent(
                TYPE_SHOLAT_SHUBUH, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_SHUBUH, context).timeInMillis,
                pendingIntentShubuh
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_SHUBUH, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentShubuh
            )
        }
    }

    fun setNotificationWaktuSholatSyuruq(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentSyuruq = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_SYURUQ, getNotificationIntent(
                TYPE_SHOLAT_SYURUQ, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_SYURUQ, context).timeInMillis,
                pendingIntentSyuruq
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_SYURUQ, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentSyuruq
            )
        }
    }

    fun setNotificationWaktuSholatDhuha(context: Context) {
        val alarmManager =
            context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntentDhuha = PendingIntent.getBroadcast(
            context,
            ID_NOTIFICATION_SHOLAT_DHUHA, getNotificationIntent(
                TYPE_SHOLAT_DHUHA, context
            ), 0
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_DHUHA, context).timeInMillis,
                pendingIntentDhuha
            )
        } else {
            alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                getReminderTime(TYPE_SHOLAT_DHUHA, context).timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntentDhuha
            )
        }
    }

    //fungsi untuk menampilkan notifikasi
    private fun showNotificationWaktuSholat(
        context: Context?,
        title: String,
        waktuSholat: String,
        modePemberitahuan: String
    ) {

        var NOTIFICATION_ID = 1
        var CHANNEL_ID = "channel_waktu_sholat"
        val CHANNEL_NAME = "Waktu Sholat"

        val intent = Intent(context, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            context,
            NOTIFICATION_ID,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        //check mode dari setting
        var uriRingtone: Uri? = null
        when (modePemberitahuan) {
            "mode_getar" -> {
                NOTIFICATION_ID = 1
                CHANNEL_ID = "channel_1"
                uriRingtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
            }
            "mode_suara" -> {
                NOTIFICATION_ID = 2
                CHANNEL_ID = "channel_2"
                uriRingtone = null
            }
            "mode_diam" -> {
                NOTIFICATION_ID = 3
                CHANNEL_ID = "channel_3"
                uriRingtone = null
            }
        }

        val notificationManager =
            context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val noficationBuilder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_logo_alternative)
            .setLargeIcon(
                (ResourcesCompat.getDrawable(
                    context.resources,
                    R.drawable.ic_logo_islamic_times,
                    null
                ))!!.toBitmap()
            )
            .setContentTitle("$title $waktuSholat")
            .setContentText("Yuk kita sholat")
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setSound(uriRingtone)
            .setPriority(NotificationCompat.PRIORITY_HIGH) //priority high agar notifikasi menjadi popup/heads-up
            .setDefaults(NotificationCompat.DEFAULT_VIBRATE)

        if(modePemberitahuan == "mode_suara") {
            val services = Intent(context, AdzanSoundService::class.java)

            if(isMyServiceRunning(context,AdzanSoundService::class.java)) {
                context.stopService(services)
            }

            services.putExtra("type_sholat", title)
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                context.startForegroundService(services)
            } else {
                context.startService(services)
            }

            val noReceiver = Intent(context, AlarmReceiver::class.java)
            val noBundle = Bundle()

            noBundle.putString("action", ActionNotification.STOP.text())
            noBundle.putString("title", title)
            noReceiver.putExtras(noBundle)

            val noPendingIntent = PendingIntent.getBroadcast(
                context,
                1234,
                noReceiver,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

            noficationBuilder.addAction(
                R.drawable.ic_snooze_24px, "STOP SUARA",
                noPendingIntent
            )
                .setOngoing(true)
        }


//        if(modePemberitahuan == "mode_suara") {
//
//            val services =  Intent(context, BackgroundSound::class.java)
//            services.putExtra("type_sholat", title)
//            context.startService(services)

//            val noReceiver = Intent(context,AlarmReceiver::class.java)
//            val noBundle = Bundle()
//
//            noBundle.putString("action", ActionNotification.STOP.text())
//            noBundle.putString("title",title)
//            noReceiver.putExtras(noBundle)
//
//            val noPendingIntent = PendingIntent.getBroadcast(context,1234,noReceiver,PendingIntent.FLAG_UPDATE_CURRENT)
//
//            noficationBuilder.addAction(R.drawable.ic_snooze_24px, "STOP SUARA",
//                noPendingIntent)
//                .setOngoing(true)
//        }

        //pengecualian untuk setup notifikasi dari versi O keatas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val attributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()

            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                importance
            )
            channel.description = CHANNEL_NAME
            if (modePemberitahuan == "mode_suara") {
                channel.setSound(uriRingtone, attributes)
            } else {
                channel.setSound(null, null)
            }
            channel.setShowBadge(true)
            channel.enableVibration(true)
            noficationBuilder.setChannelId(CHANNEL_ID)
            notificationManager.createNotificationChannel(channel)
        }
        val notification: Notification = noficationBuilder.build()
        notificationManager.notify(NOTIFICATION_ID, notification)

//        if(modePemberitahuan == "mode_suara") {
//
//            val services =  Intent(context, AdzanSoundService::class.java)
//            services.putExtra("type_sholat", title)
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                context.startForegroundService(services)
//            } else {
//                context.startService(services)
//            }
//
//            val noReceiver = Intent(context,AlarmReceiver::class.java)
//            val noBundle = Bundle()
//
//            noBundle.putString("action", ActionNotification.STOP.text())
//            noBundle.putString("title",title)
//            noReceiver.putExtras(noBundle)
//
//            val noPendingIntent = PendingIntent.getBroadcast(context,1234,noReceiver,PendingIntent.FLAG_UPDATE_CURRENT)
//
//            noficationBuilder.addAction(R.drawable.ic_snooze_24px, "STOP SUARA",
//                noPendingIntent)
//                .setOngoing(true)

//            val services =  Intent(context, BackgroundSound::class.java)
//            services.putExtra("type_sholat", title)
//            context.startService(services)

//            showNotifDialog(context, title)
//        }
    }

    //fungsi untuk menampilkan activity dialog untuk mematikan suara adzan
    private fun showNotifDialog(context: Context, type: String) {
        val popup = Intent(context, NotifDialogActivity::class.java)
        popup.putExtra("type_sholat", type)
        popup.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(popup)
    }

    //fungsi untuk cancel notifikasi
    fun cancelNotification(context: Context?, type: String) {
        val alarmManager: AlarmManager =
            context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, NotificationReceiver::class.java)
        var requestCode = 0
        if (type.equals(TYPE_SHOLAT_DZUHUR, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_DZUHUR
        }
        if (type.equals(TYPE_SHOLAT_ASHAR, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_ASHAR
        }
        if (type.equals(TYPE_SHOLAT_MAGHRIB, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_MAGHRIB
        }
        if (type.equals(TYPE_SHOLAT_ISYA, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_ISYA
        }
        if (type.equals(TYPE_SHOLAT_NISFU_LAIL, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_NISFLU_LAIL
        }
        if (type.equals(TYPE_SHOLAT_SHUBUH, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_SHUBUH
        }
        if (type.equals(TYPE_SHOLAT_SYURUQ, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_SYURUQ
        }
        if (type.equals(TYPE_SHOLAT_DHUHA, false)) {
            requestCode =
                ID_NOTIFICATION_SHOLAT_DHUHA
        }
        val pendingIntent = PendingIntent.getBroadcast(context, requestCode, intent, 0)
        pendingIntent.cancel()
        alarmManager.cancel(pendingIntent)
    }

    fun cancelNotificationWaktuSholatDzuhur(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_DZUHUR
        )
    }

    fun cancelNotificationWaktuSholatAshar(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_ASHAR
        )
    }

    fun cancelNotificationWaktuSholatMaghrib(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_MAGHRIB
        )
    }

    fun cancelNotificationWaktuSholatIsya(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_ISYA
        )
    }

    fun cancelNotificationWaktuSholatNisfuLail(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_NISFU_LAIL
        )
    }

    fun cancelNotificationWaktuSholatShubuh(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_SHUBUH
        )
    }

    fun cancelNotificationWaktuSholatSyuruq(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_SYURUQ
        )
    }

    fun cancelNotificationWaktuSholatDhuha(context: Context?) {
        cancelNotification(
            context,
            TYPE_SHOLAT_DHUHA
        )
    }

    private fun notificationMode(toggleNotif: Boolean, modeDariSetting: String): String {
        Log.d("NOTIFICATION ", "$toggleNotif $modeDariSetting")
        var mode = ""
        //notif per waktu sholat nyala
        if (toggleNotif) {
            if (modeDariSetting == "mode_getar") {
                mode = "mode_getar"
            }
            if (modeDariSetting == "mode_diam" || modeDariSetting == "mode_suara") {
                mode = "mode_suara"
            }
        }
        //notif per waktu sholat mati
        else {
            mode = "mode_diam"
        }
        return mode
    }

    fun isMyServiceRunning(context: Context,serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager?
        for (service in manager!!.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }
}