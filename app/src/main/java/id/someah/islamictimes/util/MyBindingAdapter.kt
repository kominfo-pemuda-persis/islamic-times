package id.someah.islamictimes.util

import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import id.someah.islamictimes.R

object MyBindingAdapter {
    @JvmStatic
    @BindingAdapter(value = ["customColor"])
    fun setTextViewColor(textView : TextView, customColor : Int) {
//        textView.setTextColor(ContextCompat.getColor(textView.context,customColor))
        textView.setTextColor(ContextCompat.getColor(textView.context, customColor))
    }
}