package id.someah.islamictimes.util

import com.google.android.material.appbar.AppBarLayout
import kotlin.math.abs

abstract class AppBarStateChangeListener : AppBarLayout.OnOffsetChangedListener {

    var currentState = AppBarState.IDLE

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if( verticalOffset == 0) {
            if(currentState != AppBarState.EXPANDED) {
                onStateChanged(appBarLayout,AppBarState.EXPANDED)
            }
            currentState = AppBarState.EXPANDED
        } else if(abs(verticalOffset) >= appBarLayout?.totalScrollRange ?: 0) {
            if(currentState != AppBarState.COLLAPSED) {
                onStateChanged(appBarLayout,AppBarState.COLLAPSED)
            }
            currentState = AppBarState.COLLAPSED
        } else {
            if(currentState != AppBarState.IDLE) {
                onStateChanged(appBarLayout, AppBarState.IDLE)
            }
            currentState = AppBarState.IDLE
        }

    }

    abstract fun onStateChanged(appBarLayout: AppBarLayout? , state : AppBarState)
}

enum class AppBarState {
    EXPANDED ,
    COLLAPSED ,
    IDLE
}