package id.someah.islamictimes.util

interface EventListener<T> {
    fun openDetail(item : T)
}