package id.someah.islamictimes.util

abstract class BaseUseCase<P,R> {
    abstract operator fun invoke(parameterName: P) : R
}