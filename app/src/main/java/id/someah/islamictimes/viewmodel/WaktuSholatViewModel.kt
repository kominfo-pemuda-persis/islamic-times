package id.someah.islamictimes.viewmodel

import android.os.CountDownTimer
import android.util.Log
import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import id.someah.islamictimes.data.WaktuSholatDao
import id.someah.islamictimes.data.usesace.DaoUseCase
import id.someah.islamictimes.data.usesace.GetDataWaktuSholatUseCase
import id.someah.islamictimes.data.usesace.GetNextWaktuSholatUseCase
import id.someah.islamictimes.model.WaktuSholat
import kotlinx.coroutines.*
import java.util.*
import javax.inject.Inject

@HiltViewModel
class WaktuSholatViewModel @Inject constructor(
   private val getDataWaktuSholatUseCase: GetDataWaktuSholatUseCase,
    private val getNextWaktuSholatUseCase: GetNextWaktuSholatUseCase
) : ViewModel(){

    private val TICK_TIME = 1000L
    private lateinit var timer : CountDownTimer
    private var viewModel = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModel)

    private var _dataWaktuSholat = MutableLiveData<List<WaktuSholat>>()
    val dataWaktuSholat : LiveData<List<WaktuSholat>>
        get() = _dataWaktuSholat

    private var _ellapsedTime = MutableLiveData<Long>()
    val ellapsedTime : LiveData<Long>
        get()  = _ellapsedTime

    private var _namaSholat = MutableLiveData<String>()
    val namaSholat : LiveData<String>
        get()  = _namaSholat

    private var _waktuSholat = MutableLiveData<String>()
    val waktuSholat : LiveData<String>
        get()  = _waktuSholat

    private val _mili = MutableLiveData<Long>()
    val mili : LiveData<Long> get() =  _mili

    val countDown = MediatorLiveData<Long>()

    override fun onCleared() {
        viewModel.cancel()
        timer.cancel()
    }

    init {
//        uiScope.launch {
//            _dataWaktuSholat.value = getDataWaktuSholat()
//            _namaSholat.value = getNextWaktuSholat().namaSholat
//            _waktuSholat.value = getNextWaktuSholat().waktuSholat
//            val timeMili = getNextWaktuSholat().timeMili
//            //set countdown timer buat countdown ke waktu sholat selanjutnya.
//            timer = object : CountDownTimer(timeMili, TICK_TIME){
//                override fun onFinish() {
//
//                }
//                override fun onTick(millisUntilFinished: Long) {
//                    _ellapsedTime.value = millisUntilFinished
//                }
//            }.start()
//        }

        countDown.addSource(mili) {
            if(this::timer.isInitialized) {
                timer.cancel()
            }
            timer = object : CountDownTimer(it, TICK_TIME) {
                override fun onTick(millisUntilFinished: Long) {
                    countDown.value = millisUntilFinished
                }

                override fun onFinish() {
                }
            }
            timer.start()
        }
    }

    fun refresh() {

        uiScope.launch {
            _dataWaktuSholat.value = getDataWaktuSholat()
            _namaSholat.value = getNextWaktuSholat().namaSholat
            _waktuSholat.value = getNextWaktuSholat().waktuSholat
            _mili.value = getNextWaktuSholat().timeMili
//            Log.d("contentValues", "refresh: ${timeMili}")
//            //set countdown timer buat countdown ke waktu sholat selanjutnya.
//
//            timer = object : CountDownTimer(timeMili, TICK_TIME){
//                override fun onFinish() {
//
//                }
//                override fun onTick(millisUntilFinished: Long) {
//                    Log.d("refresh", "onTick: $millisUntilFinished")
//                    _ellapsedTime.value = millisUntilFinished
//                }
//            }.start()
            //
        }
    }

    private suspend fun getDataWaktuSholat() = withContext(Dispatchers.IO){
        getDataWaktuSholatUseCase(0)
    }

    private suspend fun getNextWaktuSholat() = withContext(Dispatchers.IO){
        getNextWaktuSholatUseCase(Unit)
    }

}

