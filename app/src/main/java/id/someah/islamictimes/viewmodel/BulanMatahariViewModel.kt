package id.someah.islamictimes.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.data.DataBulanMatahariDao
import id.someah.islamictimes.model.MatahariBulan
import kotlinx.coroutines.*

//viewmodel buat data bulan dan matahari
class BulanMatahariViewModel(private val dao : DataBulanMatahariDao) : ViewModel() {
    private var viewModel = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModel)

    private var _dataBulanMatahari = MutableLiveData<ArrayList<MatahariBulan>>()
    val dataBulanMatahari : LiveData<ArrayList<MatahariBulan>>
        get() = _dataBulanMatahari

    //coroutine suspend data, menghindari blocking UI
    private suspend fun getDataBulanMatahari(jamDesimal : Double, lokasiAcuan : String, deltaT : Double) = withContext(Dispatchers.IO){
        dao.getListBulanMatahari(jamDesimal, lokasiAcuan, deltaT)
    }

    //fungsi yang nantinya di observe buat nampilin data ke view
    fun  dataBulanMatahari(jamDesimal : Double, lokasiAcuan: String, deltaT: Double){
        uiScope.launch {
            _dataBulanMatahari.value = getDataBulanMatahari(jamDesimal, lokasiAcuan, deltaT)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModel.cancel()
    }
}

class BulanMatahariViewModelFactory(private val dao: DataBulanMatahariDao)
    : ViewModelProvider.Factory{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BulanMatahariViewModel::class.java)) {
            return BulanMatahariViewModel(dao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}