package id.someah.islamictimes.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.data.WaktuSholatDao
import id.someah.islamictimes.model.WaktuSholat
import kotlinx.coroutines.*

class WaktuSholatByDateViewModel(
    private val dao: WaktuSholatDao
) : ViewModel() {

    private var viewModel = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModel)

    private var _dataWaktuSholatByDate = MutableLiveData<ArrayList<WaktuSholat>>()
    val dataWaktuSholatByDate: LiveData<ArrayList<WaktuSholat>>
        get() = _dataWaktuSholatByDate
    private var _dataWaktuSholatBulanan = MutableLiveData<ArrayList<WaktuSholat>>()
    val dataWaktuSholatBulanan: LiveData<ArrayList<WaktuSholat>>
        get() = _dataWaktuSholatBulanan

    //coroutine suspend data, menghindari blocking UI
    private suspend fun getDataWaktuSholatByDate(tanggal: Byte, bulan: Byte, tahun: Long, context : Context) =
        withContext(Dispatchers.IO) {
            dao.getWaktuSholatByDate(tanggal, bulan, tahun, context)
        }
    //fungsi yang nantinya di observe buat nampilin data ke view
    fun getWaktuSholatByDate(tanggal: Byte, bulan: Byte, tahun: Long, context: Context){
        uiScope.launch {
            _dataWaktuSholatByDate.value = getDataWaktuSholatByDate(tanggal, bulan, tahun, context)
        }
    }

    private suspend fun getDataWaktuSholatBulanan(bulan: Byte, tahun: Long, context: Context) =
        withContext(Dispatchers.IO) {
            dao.getWaktuSholatBulanan(bulan, tahun, context)
        }

    fun getWaktuSholatBulanan(bulan: Byte, tahun: Long, context: Context){
        uiScope.launch {
            _dataWaktuSholatBulanan.value = getDataWaktuSholatBulanan(bulan, tahun, context)
        }
    }
}

class WaktuSholatByDateViewModelFactory(
    private val dao: WaktuSholatDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WaktuSholatByDateViewModel::class.java)) {
            return WaktuSholatByDateViewModel(dao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}