package id.someah.islamictimes.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.data.DataEphemerisMatahariBulanDao
import id.someah.islamictimes.model.EphemerisBulanDateHour
import id.someah.islamictimes.model.EphemerisMatahariBulan
import id.someah.islamictimes.model.EphemerisMatahariDateHour
import kotlinx.coroutines.*

class EphemerisMatahariBulanViewModel(private val dao : DataEphemerisMatahariBulanDao) : ViewModel(){
    private var viewModel = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModel)

    private var _ephemerisMatahariBulanByDate = MutableLiveData<ArrayList<EphemerisMatahariBulan>>()
    val ephemerisMatahariBulanByDate: LiveData<ArrayList<EphemerisMatahariBulan>>
        get() = _ephemerisMatahariBulanByDate

    private var _ephemerisMatahariByDateHour = MutableLiveData<EphemerisMatahariDateHour>()
    val ephemerisMatahariByDateHour: LiveData<EphemerisMatahariDateHour>
        get() = _ephemerisMatahariByDateHour

    private var _ephemerisBulanByDateHour = MutableLiveData<EphemerisBulanDateHour>()
    val ephemerisBulanByDateHour: LiveData<EphemerisBulanDateHour>
        get() = _ephemerisBulanByDateHour

    //coroutine suspend data, menghindari blocking UI
    private suspend fun getDataEphemerisMatahariBulanByDate(tanggal: Byte, bulan: Byte, tahun: Long, deltaT: Double, context : Context) =
        withContext(Dispatchers.IO) {
            dao.getEphemersisMatahariBulanByDate(tanggal, bulan, tahun, deltaT, context)
        }
    //fungsi yang nantinya di observe buat nampilin data ke view
    fun getEphemersisMatahariBulanByDate(tanggal: Byte, bulan: Byte, tahun: Long, deltaT: Double, context: Context){
        uiScope.launch {
            _ephemerisMatahariBulanByDate.value = getDataEphemerisMatahariBulanByDate(tanggal, bulan, tahun, deltaT, context)
        }
    }

    private suspend fun getDataEphemerisMataharinByDateHour(
        tanggal: Byte,
        bulan: Byte,
        tahun: Long,
        jam: Int,
        menit: Int,
        detik : Int,
        deltaT : Double,
        context: Context
    ) = withContext(Dispatchers.IO) {
        dao.getEphemerisMatahariByDateHour(tanggal, bulan, tahun, jam, menit, detik, deltaT, context)
    }
    fun getEphemersisMatahariByDateHour(
        tanggal: Byte,
        bulan: Byte,
        tahun: Long,
        jam: Int,
        menit: Int,
        detik : Int,
        deltaT: Double,
        context: Context
    ) {
        uiScope.launch {
            _ephemerisMatahariByDateHour.value =
                getDataEphemerisMataharinByDateHour(tanggal, bulan, tahun, jam, menit, detik, deltaT, context)
        }
    }

    private suspend fun getDataEphemerisBulanByDateHour(
        tanggal: Byte,
        bulan: Byte,
        tahun: Long,
        jam: Int,
        menit: Int,
        detik: Int,
        deltaT: Double,
        context: Context
    ) = withContext(Dispatchers.IO) {
        dao.getEphemerisBulanByDateHour(tanggal, bulan, tahun, jam, menit,detik, deltaT, context)
    }

    fun getEphemersisBulanByDateHour(
        tanggal: Byte,
        bulan: Byte,
        tahun: Long,
        jam: Int,
        menit: Int,
        detik: Int,
        deltaT: Double,
        context: Context
    ) {
        uiScope.launch {
            _ephemerisBulanByDateHour.value =
                getDataEphemerisBulanByDateHour(tanggal, bulan, tahun, jam, menit, detik, deltaT, context)
        }
    }

}

class EphemerisMatahariBulanViewModelFactory(private val dao: DataEphemerisMatahariBulanDao)
    : ViewModelProvider.Factory{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EphemerisMatahariBulanViewModel::class.java)) {
            return EphemerisMatahariBulanViewModel(dao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}