package id.someah.islamictimes.viewmodel

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.data.WaktuSholatDao
import id.someah.islamictimes.data.usesace.GetDataWaktuSholatUseCase
import id.someah.islamictimes.data.usesace.GetNextWaktuSholatUseCase
import id.someah.islamictimes.model.WaktuSholat
import kotlinx.coroutines.*
import javax.inject.Inject

class WaktuSholatViewModelDao constructor(
   private val dao : WaktuSholatDao
) : ViewModel(){

    private val TICK_TIME = 1000L
    private lateinit var timer : CountDownTimer
    private var viewModel = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModel)

    private var _dataWaktuSholat = MutableLiveData<ArrayList<WaktuSholat>>()
    val dataWaktuSholat : LiveData<ArrayList<WaktuSholat>>
        get() = _dataWaktuSholat

    private var _ellapsedTime = MutableLiveData<Long>()
    val ellapsedTime : LiveData<Long>
        get()  = _ellapsedTime

    private var _namaSholat = MutableLiveData<String>()
    val namaSholat : LiveData<String>
        get()  = _namaSholat

    private var _waktuSholat = MutableLiveData<String>()
    val waktuSholat : LiveData<String>
        get()  = _waktuSholat

    override fun onCleared() {
        viewModel.cancel()
        timer.cancel()
    }

    init {
        uiScope.launch {
            _dataWaktuSholat.value = getDataWaktuSholat()
            _namaSholat.value = getNextWaktuSholat().namaSholat
            _waktuSholat.value = getNextWaktuSholat().waktuSholat
            val timeMili = getNextWaktuSholat().timeMili
            //set countdown timer buat countdown ke waktu sholat selanjutnya.
            timer = object : CountDownTimer(timeMili, TICK_TIME){
                override fun onFinish() {

                }
                override fun onTick(millisUntilFinished: Long) {
                    _ellapsedTime.value = millisUntilFinished
                }
            }.start()
        }

    }

    fun refresh() {
        viewModel.cancel()

        uiScope.launch {
            _dataWaktuSholat.value = getDataWaktuSholat()
            _namaSholat.value = getNextWaktuSholat().namaSholat
            _waktuSholat.value = getNextWaktuSholat().waktuSholat
            val timeMili = getNextWaktuSholat().timeMili
            //set countdown timer buat countdown ke waktu sholat selanjutnya.
            timer = object : CountDownTimer(timeMili, TICK_TIME){
                override fun onFinish() {

                }
                override fun onTick(millisUntilFinished: Long) {
                    _ellapsedTime.value = millisUntilFinished
                }
            }.start()
        }
    }

    private suspend fun getDataWaktuSholat() = withContext(Dispatchers.IO){
        dao.getWaktuSholat()
    }

    private suspend fun getNextWaktuSholat() = withContext(Dispatchers.IO){
        dao.getNextWaktuSholat()
    }

}

class WaktuSholatViewModelFactory(private val dao: WaktuSholatDao)
    : ViewModelProvider.Factory{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WaktuSholatViewModelDao::class.java)) {
            return WaktuSholatViewModelDao(dao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
