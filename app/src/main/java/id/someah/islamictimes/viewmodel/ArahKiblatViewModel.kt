package id.someah.islamictimes.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.someah.islamictimes.data.ArahKiblatDao
import id.someah.islamictimes.data.DataBulanMatahariDao
import id.someah.islamictimes.model.ArahKiblat
import id.someah.islamictimes.model.MatahariBulan
import kotlinx.coroutines.*

//viewmodel buat data arah kiblat
class ArahKiblatViewModel(private val dao : ArahKiblatDao , private val daoDataMatahari : DataBulanMatahariDao) : ViewModel(){
    private var viewModel = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModel)

    private val _dataArahKiblat = MutableLiveData<ArahKiblat>()
    val dataArahKiblat : LiveData<ArahKiblat>
        get() = _dataArahKiblat

    private var _dataBulanMatahari = MutableLiveData<ArrayList<MatahariBulan>>()
    val dataBulanMatahari : LiveData<ArrayList<MatahariBulan>>
        get() = _dataBulanMatahari

    private val _isHold = MutableLiveData<Boolean>(false)
    val isHold : LiveData<Boolean>
        get() = _isHold

    private val _isAnimationStop = MutableLiveData<Boolean>(false)
    val isAnimationStop : LiveData<Boolean>
        get() = _isAnimationStop

    //coroutine suspend data, menghindari blocking UI
    private suspend fun getDataBulanMatahari(jamDesimal : Double, lokasiAcuan : String, deltaT : Double) = withContext(Dispatchers.IO){
        daoDataMatahari.getListBulanMatahari(jamDesimal, lokasiAcuan, deltaT)
    }

    //fungsi yang nantinya di observe buat nampilin data ke view
    fun  dataBulanMatahari(jamDesimal : Double, lokasiAcuan: String, deltaT: Double){
        uiScope.launch {
            _dataBulanMatahari.value = getDataBulanMatahari(jamDesimal, lokasiAcuan, deltaT)
        }
    }

    //langsung set data ketika inisialisasi/observe viewmodel. jadi ga perlu di set dulu datanya
    init {
       fetch()
    }

    fun fetch() {
        uiScope.launch {
            _dataArahKiblat.value = getDataArahKiblat()
        }
    }

    //coroutine buat suspend data (async)
    private suspend fun getDataArahKiblat() = withContext(Dispatchers.IO){
        dao.dataArahKiblat()
    }
    override fun onCleared() {
        super.onCleared()
        viewModel.cancel()
    }

    fun switchVisiblity() {
        val currentValue = isHold.value

        if( currentValue != null) {
            _isHold.value = currentValue != true
            _isAnimationStop.value = currentValue != true
        }
    }

    fun switchHold(boolean: Boolean) {
        _isHold.value = boolean
        _isAnimationStop.value = boolean
    }

}
class ArahKiblatViewModelFactory(private val dao: ArahKiblatDao ,  private val daoDataMatahari : DataBulanMatahariDao )
    : ViewModelProvider.Factory{
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ArahKiblatViewModel::class.java)) {
            return ArahKiblatViewModel(dao , daoDataMatahari) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}