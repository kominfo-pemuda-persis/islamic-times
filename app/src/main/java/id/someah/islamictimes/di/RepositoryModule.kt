package id.someah.islamictimes.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.data.repository.*
import id.someah.islamictimes.data.repository.library.LibraryRumusCalendar
import id.someah.islamictimes.data.repository.library.LibraryRumusGerhanaBulan
import id.someah.islamictimes.data.repository.library.LibraryRumusGerhanaMatahari
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref


@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    fun provideGerhanaRepository(
        rumusGerhanaBulan: LibraryRumusGerhanaBulan,
        rumusGerhanaMatahari: LibraryRumusGerhanaMatahari
    ) : GerhanaRepository = GerhanaRepositoryImpl(rumusGerhanaBulan , rumusGerhanaMatahari)

    @Provides
    fun provideWaktuSholatRepository(
        sharedPref: SharedPref,
        falakLib: FalakLib,
        @ApplicationContext context: Context,
    ) : WaktuSholatRepository {
        return WaktuSholatRepositoryImpl(sharedPref,falakLib,context)
    }

    @Provides
    fun provideCalendarRepository (
        libraryRumusCalendar: LibraryRumusCalendar
    ) : CalendarRepository = CalendarRepositoryImpl(libraryRumusCalendar)

}