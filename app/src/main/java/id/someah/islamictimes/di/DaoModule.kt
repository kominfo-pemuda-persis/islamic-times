package id.someah.islamictimes.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.someah.islamictimes.data.DataWaktuSholat
import id.someah.islamictimes.data.WaktuSholatDao

@Module
@InstallIn(SingletonComponent::class)
object DaoModule {

}