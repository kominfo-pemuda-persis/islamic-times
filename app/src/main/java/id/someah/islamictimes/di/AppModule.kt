package id.someah.islamictimes.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import id.someah.islamictimes.data.DaoManager
import id.someah.islamictimes.data.LocalData
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref
import id.someah.islamictimes.util.GetMyLocation
import id.someah.islamictimes.util.GetMyLocationImpl
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideStorageSharedPreference(@ApplicationContext context: Context) : SharedPref = SharedPref(context)

    @Singleton
    @Provides
    fun provideLibrary() : FalakLib = FalakLib()

    @Singleton
    @Provides
    fun provideLocalData(@ApplicationContext context: Context) : LocalData = LocalData(context)

    @Singleton
    @Provides
    fun provideGetLastLocation(@ApplicationContext context: Context) : GetMyLocation = GetMyLocationImpl(context)

}