package id.someah.islamictimes.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import id.someah.islamictimes.data.repository.library.*
import id.someah.islamictimes.modules.FalakLib
import id.someah.islamictimes.preferences.SharedPref

@Module
@InstallIn(SingletonComponent::class)
class LibraryModule {

    @Provides
    fun provideMatahariLib(
        falakLib: FalakLib
    ) : MatahariLib = MatahariLib(falakLib)

    @Provides
    fun provideLibraryGerhanaBulan(
        sharedPref: SharedPref,
        falakLib: FalakLib
    ) : LibraryRumusGerhanaBulan = LibraryRumusGerhanaBulanImpl(sharedPref , falakLib)

    @Provides
    fun provideLibraryGerhanaMatahari(
        sharedPref: SharedPref,
        falakLib: FalakLib,
        matahariLib: MatahariLib,
    ) : LibraryRumusGerhanaMatahari = LibraryRumusGerhanaMatahariImpl(sharedPref , falakLib , matahariLib)

    @Provides
    fun provideLibraryRumusCalendar(
        sharedPref: SharedPref,
        falakLib: FalakLib
    ) : LibraryRumusCalendar = LibraryRumusCalendarImpl(sharedPref,falakLib)



}